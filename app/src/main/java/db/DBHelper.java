package db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class DBHelper extends SQLiteOpenHelper {

    public DBHelper(Context context){
        super(context, DBInfo.DB_NAME, null, DBInfo.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        String createTable = "CREATE TABLE " +
                DBInfo.ItemEntry.TABLE_NAME + " ( " +
                DBInfo.ItemEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DBInfo.ItemEntry.COL_NAME + " TEXT NOT NULL, " +
                DBInfo.ItemEntry.COL_STATUS + " TEXT NOT NULL);";

        String createTableLicensePlating = "CREATE TABLE " +
                DBInfo.LicensePlating.TABLE_NAME + " ( " +
                DBInfo.LicensePlating._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DBInfo.LicensePlating.COL_SERIAL_NUMBER + " TEXT NOT NULL, " +
                DBInfo.LicensePlating.COL_LICENSE_PLATING + " TEXT NOT NULL);";

        db.execSQL(createTable);
        db.execSQL(createTableLicensePlating);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        db.execSQL("DROP TABLE NAME IF EXISTS " +
                DBInfo.ItemEntry.TABLE_NAME);
        db.execSQL("DROP TABLE NAME IF EXISTS " +
                DBInfo.LicensePlating.TABLE_NAME);
        onCreate(db);
    }

    public void tambahData(String item, String status){
        SQLiteDatabase mWriteableDB = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DBInfo.ItemEntry.COL_NAME, item);
        values.put(DBInfo.ItemEntry.COL_STATUS, status);

        mWriteableDB.insertWithOnConflict(DBInfo.ItemEntry.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
        mWriteableDB.close();
    }

    public void tambahDataLicensePlating(String serial_number, String license_plating){
        SQLiteDatabase mWriteableDB = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DBInfo.LicensePlating.COL_SERIAL_NUMBER, serial_number);
        values.put(DBInfo.LicensePlating.COL_LICENSE_PLATING, license_plating);

        mWriteableDB.insertWithOnConflict(DBInfo.LicensePlating.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
        mWriteableDB.close();
    }

    public ArrayList<String> ambilSemuaData(){
        ArrayList<String> itemList = new ArrayList<>();
        SQLiteDatabase mReadableDB = getReadableDatabase();
        Cursor cursor = mReadableDB.query(DBInfo.ItemEntry.TABLE_NAME, new String[]{DBInfo.ItemEntry._ID, DBInfo.ItemEntry.COL_NAME, DBInfo.ItemEntry.COL_STATUS}, null,null,null,null,null);

        while(cursor.moveToNext()){
            int idx1 = cursor.getColumnIndex(DBInfo.ItemEntry.COL_NAME);
            int idx2 = cursor.getColumnIndex(DBInfo.ItemEntry.COL_NAME);
            itemList.add(cursor.getString(idx1));
            itemList.add(cursor.getString(idx2));
        }

        cursor.close();
        mReadableDB.close();
        return itemList;
    }

    public ArrayList<String> ambilSemuaDataLicensePlating(){
        ArrayList<String> itemList = new ArrayList<>();
        SQLiteDatabase mReadableDB = getReadableDatabase();
        Cursor cursor = mReadableDB.query(DBInfo.LicensePlating.TABLE_NAME, new String[]{DBInfo.LicensePlating._ID, DBInfo.LicensePlating.COL_LICENSE_PLATING}, null,null,null,null,null);

        while(cursor.moveToNext()){
            int idx = cursor.getColumnIndex(DBInfo.LicensePlating.COL_LICENSE_PLATING);
            itemList.add(cursor.getString(idx));
        }

        cursor.close();
        mReadableDB.close();
        return itemList;
    }

}
