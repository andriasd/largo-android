package db;

import android.provider.BaseColumns;

public class DBInfo {
    public static final String DB_NAME = "db";
    public static final int DB_VERSION = 1;

    public class ItemEntry implements BaseColumns{
        public static final String TABLE_NAME = "ms_item";
        public static final String COL_NAME = "serial_number";
        public static final String COL_STATUS = "status";
    }

    public class LicensePlating implements BaseColumns{
        public static final String TABLE_NAME = "tr_license_plating";
        public static final String COL_SERIAL_NUMBER = "serial_number";
        public static final String COL_LICENSE_PLATING = "license_plating";
    }
}
