package largo.largo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class BinTransferDocListAdapter extends ArrayAdapter<BinTransferGetSet> {

    private List<BinTransferGetSet> dokumenItemList;

    private Context context;

    private int column;

    public BinTransferDocListAdapter(List<BinTransferGetSet> dokumenItemList, Context context, int column) {
        super(context, R.layout.adapter_list_vertical, dokumenItemList);
        this.dokumenItemList = dokumenItemList;
        this.context = context;
        this.column = column;
    }



    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);

        View listViewItem = inflater.inflate(R.layout.adapter_list_vertical, null, true);

        TextView textView1 = listViewItem.findViewById(R.id.textView1);
        TextView textView2 = listViewItem.findViewById(R.id.textView2);
        TextView textView3 = listViewItem.findViewById(R.id.textView3);
        TextView textView4 = listViewItem.findViewById(R.id.textView4);

        BinTransferGetSet binTransferGetSet = dokumenItemList.get(position);

        if(column == 1) {
            textView2.setVisibility(View.GONE);
            textView3.setVisibility(View.GONE);
            textView4.setVisibility(View.GONE);

            textView1.setText(binTransferGetSet.getDocNum());
        } else if(column == 2) {
            textView3.setVisibility(View.GONE);
            textView4.setVisibility(View.GONE);

            textView1.setText(binTransferGetSet.getItem_name());
            textView2.setText(binTransferGetSet.getScanned_qty() + "/" + binTransferGetSet.getOrder_qty() + " " + binTransferGetSet.getUnit_code());
        }

        return listViewItem;
    }
}

