package largo.largo;

import java.io.Serializable;

public class PickingItemsGetterSetter implements Serializable {
    String sku, qty, pick_qty, serial_number;

    public PickingItemsGetterSetter(String sku, String qty, String pick_qty, String serial_number) {
        this.sku = sku;
        this.qty = qty;
        this.pick_qty = pick_qty;
        this.serial_number = serial_number;
    }


    public String getSku() {
        return sku;
    }

    public String getQty() {
        return qty;
    }

    public String getPickQty() {
        return pick_qty;
    }

    public String getSerialNumber() {
        return serial_number;
    }
}
