package largo.largo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class LoadingDokumenListAdapter extends ArrayAdapter<LoadingDocumentGetterSetter> {

    private List<LoadingDocumentGetterSetter> dokumenItemList;

    private Context context;

    public LoadingDokumenListAdapter(List<LoadingDocumentGetterSetter> dokumenItemList, Context context) {
        super(context, R.layout.list_view_loading_dokumen, dokumenItemList);
        this.dokumenItemList = dokumenItemList;
        this.context = context;
    }



    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);

        View listViewItem = inflater.inflate(R.layout.list_view_loading_dokumen, null, true);

        TextView textViewKdReceiving = listViewItem.findViewById(R.id.textViewDokumen);
        TextView textViewPicking = listViewItem.findViewById(R.id.textViewPicking);
        TextView textViewDestination = listViewItem.findViewById(R.id.textViewDestination);


        LoadingDocumentGetterSetter dokumenItem = dokumenItemList.get(position);

        textViewKdReceiving.setText(dokumenItem.getPlName());
        textViewPicking.setText(dokumenItem.getPicking());
        textViewDestination.setText(dokumenItem.getDestination_name());

        return listViewItem;
    }
}

