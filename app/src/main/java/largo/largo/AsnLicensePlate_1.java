package largo.largo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AsnLicensePlate_1 extends AppCompatActivity {

    private Button cancelButton, doneButton;
    private ImageButton scanButton;
    private EditText license_plat;
    private TextView totalItems;
    private ListView qrList;
    private List<AsnGetSet> asnArrayList = new ArrayList<>();
    private List<String> tagList, skuList;
    private AsnGetSet asnGetSet;
    private AsnListAdapter adapter;
    private String param, TAG, receiving_document;
    private String fClass = this.getClass().getSimpleName();
    private Intent intent;
    private AsnScanRFID asnScanRFID;
    private String docType = "";
    private String fromClass = "";

    private JSONObject paramsJson;
    public static final Map<String, String> params = new HashMap<String, String>();
    public static final String MyPREFERENCES = "MyPrefs" ;

    AsnItem asnItem;
    DatabaseHelper myDb;
    String url;
    int checkPost;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inbound_license_plating_by_asn);

        asnScanRFID = new AsnScanRFID();
        paramsJson = new JSONObject();

        checkPost = 0;
        TAG = this.getClass().getSimpleName();
        myDb = new DatabaseHelper(this);
        getServerURL();

        intent = getIntent();
        param = this.getClass().getSimpleName();
        fromClass = intent.getStringExtra("class");
        if(intent.getStringExtra("fromClass") != null) {
            fromClass = intent.getStringExtra("fromClass");
        }

        cancelButton = findViewById(R.id.back);
        doneButton = findViewById(R.id.done);
        scanButton = findViewById(R.id.scanButton);
        license_plat = findViewById(R.id.license_plat);
//        license_plat.setText(String.format("OL%d", System.currentTimeMillis() / 1000));

        qrList = findViewById(R.id.QRList);
        totalItems = findViewById(R.id.qty);
        totalItems.setText("0");

        tagList = new ArrayList<>();
        System.out.println("tagList : " + tagList);
        skuList = new ArrayList<>();
        if(tagList != null) {
            tagList.clear();
        }
        if(skuList != null) {
            skuList.clear();
        }
        asnArrayList = (List<AsnGetSet>) intent.getSerializableExtra("asnArrayList");
        receiving_document = intent.getStringExtra("receiving_document");
        asnGetSet = (AsnGetSet) intent.getSerializableExtra("asnGetSet");
        skuList = (List<String>) intent.getSerializableExtra("skuList");
        tagList = (List<String>) intent.getSerializableExtra("tagList");

        if(asnArrayList != null) {
            adapter = new AsnListAdapter(asnArrayList, getApplicationContext(), 3);
            qrList.setAdapter(adapter);
            totalItems.setText(Integer.toString(asnArrayList.size()));
        }

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2 = new Intent();
                if(fromClass.equals("AsnScanRFID")) {
                    intent2 = new Intent(getApplicationContext(), AsnScanRFID.class);
                } else {
                    intent2 = new Intent(getApplicationContext(), AsnScanQR.class);
                }
                intent2.putExtra("asnArrayList", (Serializable) (List<AsnGetSet>) intent.getSerializableExtra("asnArrayList"));
                intent2.putExtra("skuList", (Serializable) intent.getSerializableExtra("skuList"));
                intent2.putExtra("tagList", (Serializable) intent.getSerializableExtra("tagList"));
                intent2.putExtra("receiving_document", receiving_document);
                intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent2);
                finish();
            }
        });

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(license_plat.getText() != null || license_plat.getText().toString().length() > 0) {
                    if(license_plat.getText().toString().contains("OL")) {
                        try {
                            if (asnArrayList.size() > 0) {
                                int arraySize;
                                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                                String usernameKey = prefs.getString("usernameKey", "No name defined");
                                Long tsLong = System.currentTimeMillis() / 1000;
                                String jsonData = "";

                                String outer_label = license_plat.getText().toString();
                                if(outer_label.length() > 14) {
                                    outer_label = outer_label.substring(0, 20);
                                }

                                try {
                                    arraySize = asnArrayList.size();
                                } catch (Exception e) {
                                    arraySize = 0;
                                }

                                if (arraySize > 0) {
                                    jsonData = "[";
                                    for (int i = 0; i < arraySize; i++) {
                                        if (!asnArrayList.get(i).getPcs().equals("0")) {
                                            paramsJson.put("receiving_code", receiving_document);
                                            paramsJson.put("item_code", asnArrayList.get(i).getSku());
                                            paramsJson.put("serial_number", asnArrayList.get(i).getUpc().toUpperCase());
                                            paramsJson.put("tgl_exp", new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("dd MMM yyyy").parse(asnArrayList.get(i).getExp())));
                                            paramsJson.put("qty", asnArrayList.get(i).getQty());
                                            paramsJson.put("batch_code", asnArrayList.get(i).getBatch());
                                            paramsJson.put("qc_code", "GOOD");
                                            paramsJson.put("tgl_in", new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
                                            paramsJson.put("uname", usernameKey);
                                            paramsJson.put("parent_code", outer_label);

                                            jsonData = jsonData + paramsJson.toString() + ",";
                                        }
                                    }

                                    jsonData = jsonData.substring(0, jsonData.length() - 1);
                                    jsonData = jsonData + "]";

                                    Log.d("JSON Param", jsonData);
                                    Log.d("STRLEN", Integer.toString(jsonData.length()));
                                    params.clear();
                                    params.put("data", jsonData);
                                    System.out.println("postReceivingParam : " + jsonData);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            if(checkPost == 0) {
                                                postReceiving(params);
                                                checkPost = 1;
                                            }
                                        }
                                    });
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        try {
                            finish();
                            Intent intent2 = new Intent(getApplicationContext(), AsnItem.class);
                            intent2.putExtra("receiving_document", receiving_document);
                            intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent2);
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Outer label format should start with OL.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        license_plat.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 0) {
                    if (license_plat.length() != 0) {
                        if(license_plat.getText().toString().contains("OL")) {
                            getDocType(receiving_document);
                        } else {
                            Toast.makeText(getApplicationContext(), "Outer label format should start with OL.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                return false;
            }
        });

        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getApplicationContext(), QRScanner.class);
                intent.putExtra("param", "asnLicensePlateQR");
                intent.putExtra("fromClass", fromClass);
                intent.putExtra("fClass", fClass);
                intent.putExtra("asnArrayList", (Serializable) asnArrayList);
                intent.putExtra("asnGetSet", asnGetSet);
                intent.putExtra("skuList", (Serializable) skuList);
                intent.putExtra("tagList", (Serializable) tagList);
                intent.putExtra("receiving_document", receiving_document);
                startActivity(intent);
            }
        });
        // LocalBroadcastManager.getInstance(this).registerReceiver(qrScan, new IntentFilter("broadcastQrResult"));
        if (intent.getStringExtra("asnLicensePlateQR") != null) {
            if (intent.getStringExtra("asnLicensePlateQR").contains("OL")) {
                license_plat.setText(intent.getStringExtra("asnLicensePlateQR"));
                getDocType(receiving_document);
            } else {
                Toast.makeText(getApplicationContext(), "Outer label format should start with OL.", Toast.LENGTH_SHORT).show();
            }
            asnArrayList = (List<AsnGetSet>) intent.getSerializableExtra("asnArrayList");
            asnGetSet = (AsnGetSet) intent.getSerializableExtra("asnGetSet");
            skuList = (List<String>) intent.getSerializableExtra("skuList");
            tagList = (List<String>) intent.getSerializableExtra("tagList");
            receiving_document = intent.getStringExtra("receiving_document");
        }
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }

    private void postReceiving(final Map<String, String> parameters) {
        String postUrl = this.url + "/Api_receiving/post_json";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        checkPost = 0;

                        try {
                            JSONObject data = new JSONObject(response);
                            String results = data.getString("results");
                            JSONArray resultsArray = new JSONArray(results);
                            JSONObject resultsObject = new JSONObject(resultsArray.get(0).toString());
                            int status = resultsObject.getInt("status");
                            String message = resultsObject.getString("message");

                            System.out.println("message : " + message);
                            if(status == 200) {
                                if(message.contains("already exist")) {
                                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                } else {
                                    Intent intent3;
                                    System.out.println("fromClass intent : " + intent.getStringExtra("fromClass"));
                                    //if (intent.getStringExtra("fromClass").equals("AsnScanRFID")) {
                                    //intent3 = new Intent(getApplicationContext(), AsnScanRFID.class);
                                    //} else {
                                    intent3 = new Intent(getApplicationContext(), AsnItem.class);
                                    //}
                                    intent3.putExtra("asnArrayList", (Serializable) (List<AsnGetSet>) intent.getSerializableExtra("asnArrayList"));
                                    intent3.putExtra("skuList", (Serializable) (List<String>) intent.getSerializableExtra("skuList"));
                                    intent3.putExtra("tagList", (Serializable) (List<String>) intent.getSerializableExtra("tagList"));
                                    intent3.putExtra("receiving_document", receiving_document);
                                    intent3.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent3);
                                    finish();
                                }
                            } else {
                                // showMessage("Alert", message);
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;

            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    public void getOuterLabelData(final Map<String, String> parameters) {
        String postUrl = this.url + "/Api_receiving/getValidOuterLabel/";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);

                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            String status = jsonResponse.getString("status");
                            if(status.equals("400")) {
                                license_plat.requestFocus();
                                license_plat.hasFocus();
                                license_plat.setText("");
                                Toast.makeText(getApplicationContext(), "Outer label ini sudah ada.", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException jsonEx) {
                            jsonEx.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    public void getDocType(final String parameters) {
        String postUrl = this.url + "/Api_receiving/get_doc_type/" + parameters;

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.GET, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);

                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            String message = jsonResponse.getString("message");
                            if(message.equals("3") || message.equals("14")) {
                                docType = "ok";
                            } else {
                                params.put("outer_label", license_plat.getText().toString());
                                params.put("receiving_code", receiving_document);
                                getOuterLabelData(params);
                            }
                        } catch (JSONException jsonEx) {
                            jsonEx.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }
}