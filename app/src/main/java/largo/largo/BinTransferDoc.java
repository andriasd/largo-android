package largo.largo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BinTransferDoc extends AppCompatActivity {
    String TAG = this.getClass().getSimpleName();

    private Button backButton, nextButton;
    private EditText docNum;
    private Intent intent;
    private ListView listView;
    private List<BinTransferGetSet> dokumenItemList;
    private List<String> docList;

    DatabaseHelper myDb;
    String url;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bin_tranfer_doc);

        intent = getIntent();

        myDb = new DatabaseHelper(this);
        getServerURL();

        backButton = findViewById(R.id.back);
        nextButton = findViewById(R.id.next);
        listView = findViewById(R.id.listTransferDoc);
        docNum = findViewById(R.id.doc_num);

        docList = new ArrayList<>();
        dokumenItemList = new ArrayList<>();
        if(intent.getSerializableExtra("docList") != null) {
            docList = (List<String>) intent.getSerializableExtra("docList");
        }
        DataListDokument(url + "/Api_bin_transfer/get");

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(docNum.getText().toString().length() > 0) {
                    // Intent intent = new Intent(getApplicationContext(), AsnItem.class);
                    Intent intent = new Intent(getApplicationContext(), BinTransferDocItem.class);
                    intent.putExtra("bin_transfer_code", docNum.getText().toString());
                    startActivity(intent);
                } else {
                    Toast.makeText(BinTransferDoc.this, "Please input bin transfer document first.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                docNum.setText(dokumenItemList.get(position).getDocNum());
                Intent intent = new Intent(getApplicationContext(), BinTransferDocItem.class);
                intent.putExtra("bin_transfer_code", dokumenItemList.get(position).getDocNum());
                startActivity(intent);
            }
        });

        docNum.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 0) {
                    if(docNum.getText().toString().length() > 0) {
                        // Intent intent = new Intent(getApplicationContext(), AsnItem.class);
                        Intent intent = new Intent(getApplicationContext(), BinTransferDocItem.class);
                        intent.putExtra("bin_transfer_code", docNum.getText().toString());
                        startActivity(intent);
                    } else {
                        Toast.makeText(BinTransferDoc.this, "Please input bin transfer document first.", Toast.LENGTH_SHORT).show();
                    }
                }
                return false;
            }
        });
    }

    private void DataListDokument(String getDoc){
        RequestQueue requestQueue=Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, getDoc, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);
                try{
                    JSONObject jsonObject=new JSONObject(response);
                    if(jsonObject.getInt("status")==200){
                        JSONArray jsonArray1=jsonObject.getJSONArray("results");
                        if(docList.size() > 0) {
                            docList.clear();
                        }
                        for(int i=0;i<jsonArray1.length();i++){
                            JSONObject jsonObject1=jsonArray1.getJSONObject(i);
                            BinTransferGetSet item_dokumen = new BinTransferGetSet(
                                    jsonObject1.getString("bin_transfer_code")
                            );
                            dokumenItemList.add(item_dokumen);
                            docList.add(jsonObject1.getString("bin_transfer_code"));
                        }
                        BinTransferDocListAdapter adapter = new BinTransferDocListAdapter(dokumenItemList, getApplicationContext(), 1);

                        listView.setAdapter(adapter);

                    }
                }catch (JSONException e){e.printStackTrace();}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;


            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }
}
