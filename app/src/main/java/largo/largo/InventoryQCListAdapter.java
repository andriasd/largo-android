package largo.largo;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class InventoryQCListAdapter extends ArrayAdapter<InventoryQCGetSet> {
    private List<InventoryQCGetSet> inventoryQCGetSetList;

    private Context context;

    private int column;

    public InventoryQCListAdapter(List<InventoryQCGetSet> inventoryQCGetSetList, Context context, int column) {
        super(context, R.layout.adapter_list_horizontal, inventoryQCGetSetList);
        this.inventoryQCGetSetList = inventoryQCGetSetList;
        this.context = context;
        this.column = column;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);

        View listViewItem = inflater.inflate(R.layout.adapter_list_horizontal, null, true);

        TextView snText = listViewItem.findViewById(R.id.textView1);
        TextView locText = listViewItem.findViewById(R.id.textView2);
        TextView qcText = listViewItem.findViewById(R.id.textView3);
        TextView hexText = listViewItem.findViewById(R.id.textView4);

        InventoryQCGetSet inventoryQCGetSet = inventoryQCGetSetList.get(position);

        snText.setText(inventoryQCGetSet.getSn());
        locText.setText(inventoryQCGetSet.getLoc());
        qcText.setText(inventoryQCGetSet.getQc());

        if(inventoryQCGetSet.getFlag().length() > 0) {
            snText.setTextColor(Color.parseColor("#77B24C"));
            locText.setTextColor(Color.parseColor("#77B24C"));
        }

        if(column == 2) {
            qcText.setVisibility(View.GONE);
            hexText.setVisibility(View.GONE);
        }

        return listViewItem;
    }
}
