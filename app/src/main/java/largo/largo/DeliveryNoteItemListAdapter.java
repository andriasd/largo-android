package largo.largo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class DeliveryNoteItemListAdapter extends ArrayAdapter<DeliveryNoteItemGetterSetter> {

    private List<DeliveryNoteItemGetterSetter> deliveryNoteItemList;

    private Context context;

    public DeliveryNoteItemListAdapter(List<DeliveryNoteItemGetterSetter> deliveryNoteItemList, Context context) {
        super(context, R.layout.list_view_picking_document, deliveryNoteItemList);
        this.deliveryNoteItemList = deliveryNoteItemList;
        this.context = context;
    }



    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);

        View listViewItem = inflater.inflate(R.layout.list_view_picking_document, null, true);

        TextView textViewPickingDoc = listViewItem.findViewById(R.id.textViewPickingDoc);
        TextView textViewQty = listViewItem.findViewById(R.id.textViewQty);
        TextView textViewCustomer = listViewItem.findViewById(R.id.textViewCustomer);
        TextView textViewPicker = listViewItem.findViewById(R.id.textViewPicker);


        DeliveryNoteItemGetterSetter deliveryItem = deliveryNoteItemList.get(position);

        textViewPickingDoc.setText(String.valueOf(deliveryItem.getItem_name()));
        textViewQty.setText(String.valueOf(deliveryItem.getCode()));
        textViewCustomer.setText(String.valueOf(deliveryItem.getQty()));
        textViewPicker.setText(String.valueOf(deliveryItem.getUser_name()));

        return listViewItem;
    }
}
