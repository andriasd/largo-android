package largo.largo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.zxing.Result;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class QRScanner extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    private ZXingScannerView mScannerView;
    private Intent intent;
    private String param = "";
    private String fClass = "";
    private String fromClass = "";

    // LoginActivity
    private String usernameText = "";

    // Asn
    private List<String> docList = new ArrayList<>();

    // AsnScanQR
    private List<AsnGetSet> asnArrayList = new ArrayList<>();
    private AsnGetSet asnGetSet;
    private List<String> skuList = new ArrayList<>();
    private List<String> tagList = new ArrayList<>();
    private String asnQty = "";
    private String asnBatch = "";
    private String asnExpDate = "";
    private String item_code = "";
    private String receiving_document = "";
    private List<ReceivingDocumentItemGetterSetter> dokumenItemList;

    // PutawayLocation
    private String location = "";

    // AsnPicking
    private List<PickingDocumentGetterSetter> docItemList = new ArrayList<>();
    private PickingDocumentGetterSetter pickDocGetSet;
    private String picking_document = "";
    private String pickQty = "";

    // AsnPickingScanQR
    private List<PickingItemGetterSetter> itemList = new ArrayList<>();
    private PickingItemGetterSetter pickingItemGetSet;

    // Loading
    private List<LoadingDocumentGetterSetter> loadingDocItemList = new ArrayList<>();
    private LoadingDocumentGetterSetter loadingDocGetSet;
    private String loading_document = "";

    // LoadingScanQR
    private String loadingPlateNumberText = "";

    // InventoryQC
    private InventoryQCGetSet inventoryQCGetSet;
    private List<InventoryQCGetSet> inventoryQCGetSetList = new ArrayList<>();
    private String statusQC = "";

    // LicensePlating
    private LicensePlatingGetSet licensePlatingGetSet;
    private List<LicensePlatingGetSet> licensePlatingGetSetList = new ArrayList<>();

    // InventoryTransfer
    private InventoryTransferGetSet inventoryTransferGetSet;
    private List<InventoryTransferGetSet> inventoryTransferGetSetList = new ArrayList<>();

    // CycleCount
    private CycleCountDocumentGetterSetter cycleCountDocumentGetterSetter;
    private List<CycleCountDocumentGetterSetter> cycleCountDocumentGetterSetterList = new ArrayList<>();
    private String warehouse_name = "";
    private String cc_document = "";
    private String cc_type = "";
    private String type = "";
    private String asnOL = "";
    private String tallyQty = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mScannerView = new ZXingScannerView(this);
        setContentView(mScannerView);

        intent = getIntent();
        System.out.println("param : " + intent.getStringExtra("param"));
        System.out.println("fClass : " + intent.getStringExtra("fClass"));
        try {
            if (intent.getStringExtra("param") != null) {
                param = intent.getStringExtra("param");
            }
            if (intent.getStringExtra("fClass") != null) {
                fClass = intent.getStringExtra("fClass");
            }

            // LoginActivity
            if (intent.getStringExtra("usernameText") != null) {
                usernameText = intent.getStringExtra("usernameText");
            }

            // Asn
            if (intent.getSerializableExtra("docList") != null) {
                docList = (List<String>) intent.getSerializableExtra("docList");
            }

            // AsnScanQR, AsnLicensePlate
            if (intent.getSerializableExtra("asnArrayList") != null) {
                asnArrayList = (List<AsnGetSet>) intent.getSerializableExtra("asnArrayList");
            }
            if (intent.getSerializableExtra("asnGetSet") != null) {
                asnGetSet = (AsnGetSet) intent.getSerializableExtra("asnGetSet");
            }
            if (intent.getSerializableExtra("skuList") != null) {
                skuList = (List<String>) intent.getSerializableExtra("skuList");
            }
            if (intent.getSerializableExtra("tagList") != null) {
                tagList = (List<String>) intent.getSerializableExtra("tagList");
            }
            if (intent.getStringExtra("receiving_document") != null) {
                receiving_document = intent.getStringExtra("receiving_document");
            }
            if (intent.getStringExtra("asnQty") != null) {
                asnQty = intent.getStringExtra("asnQty");
            }
            if (intent.getStringExtra("asnBatch") != null) {
                asnBatch = intent.getStringExtra("asnBatch");
            }
            if (intent.getStringExtra("asnExpDate") != null) {
                asnExpDate = intent.getStringExtra("asnExpDate");
            }
            if (intent.getSerializableExtra("dokumenItemList") != null) {
                dokumenItemList = (List<ReceivingDocumentItemGetterSetter>) intent.getSerializableExtra("dokumenItemList");
            }
            if (intent.getStringExtra("item_code") != null) {
                item_code = intent.getStringExtra("item_code");
            }

            // PutawayLocation
            if (intent.getStringExtra("location") != null) {
                location = intent.getStringExtra("location");
            }

            // AsnPicking
            if (intent.getSerializableExtra("docItemList") != null) {
                docItemList = (List<PickingDocumentGetterSetter>) intent.getSerializableExtra("docItemList");
            }
            if (intent.getSerializableExtra("pickDocGetSet") != null) {
                pickDocGetSet = (PickingDocumentGetterSetter) intent.getSerializableExtra("pickDocGetSet");
            }
            if (intent.getSerializableExtra("pickQty") != null) {
                pickQty = intent.getStringExtra("pickQty");
            }

            // AsnPickingScanQR
            if (intent.getSerializableExtra("itemList") != null) {
                itemList = (List<PickingItemGetterSetter>) intent.getSerializableExtra("itemList");
            }
            if (intent.getSerializableExtra("pickingItemGetSet") != null) {
                pickingItemGetSet = (PickingItemGetterSetter) intent.getSerializableExtra("pickingItemGetSet");
            }
            if (intent.getStringExtra("picking_document") != null) {
                picking_document = intent.getStringExtra("picking_document");
            }

            // Loading
            if (intent.getSerializableExtra("loadingDocItemList") != null) {
                loadingDocItemList = (List<LoadingDocumentGetterSetter>) intent.getSerializableExtra("loadingDocItemList");
            }
            if (intent.getSerializableExtra("pickingItemGetSet") != null) {
                loadingDocGetSet = (LoadingDocumentGetterSetter) intent.getSerializableExtra("loadingDocGetSet");
            }
            if (intent.getStringExtra("loading_document") != null) {
                loading_document = intent.getStringExtra("loading_document");
            }

            // LoadingScanQR
            if (intent.getStringExtra("loadingPlateNumberText") != null) {
                loadingPlateNumberText = intent.getStringExtra("loadingPlateNumberText");
            }

            // InventoryQC
            if (intent.getSerializableExtra("inventoryQCGetSetList") != null) {
                inventoryQCGetSetList = (List<InventoryQCGetSet>) intent.getSerializableExtra("inventoryQCGetSetList");
            }
            if (intent.getSerializableExtra("inventoryQCGetSet") != null) {
                inventoryQCGetSet = (InventoryQCGetSet) intent.getSerializableExtra("inventoryQCGetSet");
            }
            if (intent.getStringExtra("statusQC") != null) {
                statusQC = intent.getStringExtra("statusQC");
            }

            // LicensePlating
            if (intent.getSerializableExtra("licensePlatingGetSetList") != null) {
                licensePlatingGetSetList = (List<LicensePlatingGetSet>) intent.getSerializableExtra("licensePlatingGetSetList");
            }
            if (intent.getSerializableExtra("licensePlatingGetSet") != null) {
                licensePlatingGetSet = (LicensePlatingGetSet) intent.getSerializableExtra("licensePlatingGetSet");
            }

            // InventoryTransfer
            if (intent.getSerializableExtra("inventoryTransferGetSetList") != null) {
                inventoryTransferGetSetList = (List<InventoryTransferGetSet>) intent.getSerializableExtra("inventoryTransferGetSetList");
            }
            if (intent.getSerializableExtra("inventoryTransferGetSet") != null) {
                inventoryTransferGetSet = (InventoryTransferGetSet) intent.getSerializableExtra("inventoryTransferGetSet");
            }

            // CycleCount
            if (intent.getSerializableExtra("cycleCountDocumentGetterSetterList") != null) {
                cycleCountDocumentGetterSetterList = (List<CycleCountDocumentGetterSetter>) intent.getSerializableExtra("cycleCountDocumentGetterSetterList");
            }
            if (intent.getSerializableExtra("ccDocGetSet") != null) {
                cycleCountDocumentGetterSetter = (CycleCountDocumentGetterSetter) intent.getSerializableExtra("ccDocGetSet");
            }
            if (intent.getStringExtra("warehouse_name") != null) {
                warehouse_name = intent.getStringExtra("warehouse_name");
            }
            if (intent.getStringExtra("cc_document") != null) {
                cc_document = intent.getStringExtra("cc_document");
            }
            if (intent.getStringExtra("cc_type") != null) {
                cc_type = intent.getStringExtra("cc_type");
            }
            if (intent.getStringExtra("type") != null) {
                type = intent.getStringExtra("type");
            }
            if (intent.getStringExtra("asnOL") != null) {
                asnOL = intent.getStringExtra("asnOL");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result rawResult) {
        mScannerView.resumeCameraPreview(this);
        mScannerView.stopCamera();

        if(fClass.equals("SettingActivity")) {
            intent = new Intent(this, SettingActivity.class);
            intent.putExtra(param, rawResult.getText());
        } else if(fClass.equals("LoginActivity")) {
            intent = new Intent(this, LoginActivity.class);
            intent.putExtra(param, rawResult.getText());
            if(usernameText.length() > 0) {
                intent.putExtra("usernameText", usernameText);
            }
        } else if(fClass.equals("Asn")) {
            intent = new Intent(this, Asn.class);
            intent.putExtra(param, rawResult.getText());
            intent.putExtra("docList", (Serializable) docList);
        } else if(fClass.equals("AsnScanQR")) {
            intent = new Intent(this, AsnScanQR.class);
            intent.putExtra(param, rawResult.getText());
            intent.putExtra("asnArrayList", (Serializable) asnArrayList);
            intent.putExtra("asnGetSet", asnGetSet);
            intent.putExtra("dokumenItemList", (Serializable) dokumenItemList);
            intent.putExtra("tagList", (Serializable) tagList);
            intent.putExtra("receiving_document", receiving_document);
            intent.putExtra("item_code", item_code);
            intent.putExtra("asnQty", asnQty);
            intent.putExtra("asnBatch", asnBatch);
            intent.putExtra("asnExpDate", asnExpDate);
            /*intent.putExtra("docList", (Serializable) docList);
            intent.putExtra("skuList", (Serializable) skuList);
            intent.putExtra("tagList", (Serializable) tagList);
            intent.putExtra("asnArrayList", (Serializable) asnArrayList);
            intent.putExtra("dokumenItemList", (Serializable) dokumenItemList);
            intent.putExtra("asnGetSet", asnGetSet);
            intent.putExtra("receiving_document", receiving_document);
            intent.putExtra("asnQty", asnQty);
            intent.putExtra("asnBatch", asnBatch);
            intent.putExtra("asnExpDate", asnExpDate);*/
        } else if(fClass.equals("AsnLicensePlate")) {
            intent = new Intent(this, AsnLicensePlate.class);
            intent.putExtra(param, rawResult.getText());
            intent.putExtra("asnArrayList", (Serializable) asnArrayList);
            intent.putExtra("dokumenItemList", (Serializable) dokumenItemList);
            intent.putExtra("receiving_document", receiving_document);
            intent.putExtra("item_code", item_code);
            intent.putExtra("asnQty", asnQty);
            intent.putExtra("asnBatch", asnBatch);
            intent.putExtra("asnExpDate", asnExpDate);
            intent.putExtra("fromClass", fromClass);
        } else if(fClass.equals("PutawaySerialNumber")) {
            intent = new Intent(this, PutawaySerialNumber.class);
            intent.putExtra(param, rawResult.getText());
            intent.putExtra("asnArrayList", (Serializable) asnArrayList);
            intent.putExtra("tagList", (Serializable) tagList);
            intent.putExtra("asnGetSet", asnGetSet);
        } else if(fClass.equals("PutawayLocation")) {
            intent = new Intent(this, PutawayLocation.class);
            intent.putExtra(param, rawResult.getText());
            intent.putExtra("asnArrayList", (Serializable) asnArrayList);
            intent.putExtra("tagList", (Serializable) tagList);
            intent.putExtra("asnGetSet", asnGetSet);
            if(param.equals("putLocQR")) {
                intent.putExtra("location", location);
            }
        } else if(fClass.equals("AsnPicking")) {
            intent = new Intent(this, AsnPicking.class);
            intent.putExtra(param, rawResult.getText());
            intent.putExtra("docList", (Serializable) docList);
            intent.putExtra("docItemList", (Serializable) docItemList);
            intent.putExtra("pickDocGetSet", pickDocGetSet);
        } else if(fClass.equals("AsnPickingScanQR")) {
            intent = new Intent(this, AsnPickingScanQR.class);
            intent.putExtra(param, rawResult.getText());
            intent.putExtra("docList", (Serializable) docList);
            intent.putExtra("itemList", (Serializable) itemList);
            intent.putExtra("pickingItemGetSet", pickingItemGetSet);
            intent.putExtra("picking_document", picking_document);
            intent.putExtra("pickQty", pickQty);
        } else if(fClass.equals("AsnPickingLocation")) {
            intent = new Intent(this, AsnPickingLocation.class);
            intent.putExtra(param, rawResult.getText());
            intent.putExtra("docList", (Serializable) docList);
            intent.putExtra("itemList", (Serializable) itemList);
            intent.putExtra("pickingItemGetSet", pickingItemGetSet);
            intent.putExtra("picking_document", picking_document);
        } else if(fClass.equals("Loading")) {
            intent = new Intent(this, Loading.class);
            intent.putExtra(param, rawResult.getText());
            intent.putExtra("docList", (Serializable) docList);
            intent.putExtra("loadingDocItemList", (Serializable) loadingDocItemList);
            intent.putExtra("loadingDocGetSet", loadingDocGetSet);
            intent.putExtra("loading_document", loading_document);
        } else if(fClass.equals("LoadingScanQR")) {
            intent = new Intent(this, LoadingScanQR.class);
            intent.putExtra(param, rawResult.getText());
            intent.putExtra("docList", (Serializable) docList);
            intent.putExtra("loadingDocItemList", (Serializable) loadingDocItemList);
            intent.putExtra("loadingDocGetSet", loadingDocGetSet);
            intent.putExtra("loading_document", loading_document);
            if(param.equals("loadingQR")) {
                intent.putExtra("loadingPlateNumberText", loadingPlateNumberText);
            }
        } else if(fClass.equals("InventoryQCInSerialNumber")) {
            intent = new Intent(this, InventoryQCInSerialNumber.class);
            intent.putExtra(param, rawResult.getText());
            intent.putExtra("tagList", (Serializable) tagList);
            intent.putExtra("inventoryQCGetSetList", (Serializable) inventoryQCGetSetList);
            intent.putExtra("inventoryQCGetSet", inventoryQCGetSet);
        } else if(fClass.equals("InventoryQCOutSerialNumber")) {
            intent = new Intent(this, InventoryQCOutSerialNumber.class);
            intent.putExtra(param, rawResult.getText());
            intent.putExtra("tagList", (Serializable) tagList);
            intent.putExtra("inventoryQCGetSetList", (Serializable) inventoryQCGetSetList);
            intent.putExtra("inventoryQCGetSet", inventoryQCGetSet);
        } else if(fClass.equals("InventoryQCInLocation")) {
            intent = new Intent(this, InventoryQCInLocation.class);
            intent.putExtra(param, rawResult.getText());
            intent.putExtra("tagList", (Serializable) tagList);
            intent.putExtra("inventoryQCGetSetList", (Serializable) inventoryQCGetSetList);
            intent.putExtra("inventoryQCGetSet", inventoryQCGetSet);
            if(param.equals("inventoryQCInLocQR")) {
                intent.putExtra("location", location);
            }
        } else if(fClass.equals("InventoryQCOutLocation")) {
            intent = new Intent(this, InventoryQCOutLocation.class);
            intent.putExtra(param, rawResult.getText());
            intent.putExtra("tagList", (Serializable) tagList);
            intent.putExtra("inventoryQCGetSetList", (Serializable) inventoryQCGetSetList);
            intent.putExtra("inventoryQCGetSet", inventoryQCGetSet);
            if(param.equals("inventoryQCOutLocQR")) {
                intent.putExtra("location", location);
                intent.putExtra("statusQC", statusQC);
            }
        } else if(fClass.equals("LicensePlating")) {
            intent = new Intent(this, LicensePlating.class);
            intent.putExtra(param, rawResult.getText());
            intent.putExtra("licensePlatingGetSetList", (Serializable) licensePlatingGetSetList);
            intent.putExtra("tagList", (Serializable) tagList);
            intent.putExtra("licensePlatingGetSet", licensePlatingGetSet);
        } else if(fClass.equals("LicensePlatingPC")) {
            intent = new Intent(this, LicensePlatingPC.class);
            intent.putExtra(param, rawResult.getText());
            intent.putExtra("licensePlatingGetSetList", (Serializable) licensePlatingGetSetList);
            intent.putExtra("tagList", (Serializable) tagList);
            intent.putExtra("licensePlatingGetSet", licensePlatingGetSet);
        } else if(fClass.equals("InventoryQCInLocation")) {
            intent = new Intent(this, InventoryQCInLocation.class);
            intent.putExtra(param, rawResult.getText());
            intent.putExtra("tagList", (Serializable) tagList);
            intent.putExtra("inventoryQCGetSetList", (Serializable) inventoryQCGetSetList);
            intent.putExtra("inventoryQCGetSet", inventoryQCGetSet);
            if(param.equals("inventoryQCInLocQR")) {
                intent.putExtra("location", location);
            }
        } else if(fClass.equals("InventoryTransferSN")) {
            intent = new Intent(this, InventoryTransferSN.class);
            intent.putExtra(param, rawResult.getText());
            intent.putExtra("tagList", (Serializable) tagList);
            intent.putExtra("inventoryTransferGetSetList", (Serializable) inventoryTransferGetSetList);
            intent.putExtra("inventoryTransferGetSet", inventoryTransferGetSet);
        } else if(fClass.equals("InventoryTransferLocation")) {
            intent = new Intent(this, InventoryTransferLocation.class);
            intent.putExtra(param, rawResult.getText());
            intent.putExtra("tagList", (Serializable) tagList);
            intent.putExtra("inventoryTransferGetSetList", (Serializable) inventoryTransferGetSetList);
            intent.putExtra("inventoryTransferGetSet", inventoryTransferGetSet);
            if(param.equals("inventoryTransferLocQR")) {
                intent.putExtra("location", location);
            }
        } else if(fClass.equals("InventoryCycleCount")) {
            intent = new Intent(this, InventoryCycleCount.class);
            intent.putExtra(param, rawResult.getText());
            intent.putExtra("docList", (Serializable) docList);
            intent.putExtra("docItemList", (Serializable) docItemList);
            intent.putExtra("ccDocGetSet", cycleCountDocumentGetterSetter);
            intent.putExtra("warehouse_name", warehouse_name);
            intent.putExtra("cc_type", cc_type);
        } else if(fClass.equals("InventoryCycleCountScanQR")) {
            intent = new Intent(this, InventoryCycleCountScanQR.class);
            intent.putExtra(param, rawResult.getText());
            intent.putExtra("tagList", (Serializable) tagList);
            intent.putExtra("asnArrayList", (Serializable) asnArrayList);
            intent.putExtra("asnGetSet", asnGetSet);
            intent.putExtra("cc_document", cc_document);
            intent.putExtra("tallyQty", tallyQty);
            intent.putExtra("warehouse_name", warehouse_name);
            intent.putExtra("cc_type", cc_type);
            intent.putExtra("type", type);
            intent.putExtra("asnOL", asnOL);
        } else if(fClass.equals("InventoryCycleCountPositive")) {
            intent = new Intent(this, InventoryCycleCountPositive.class);
            intent.putExtra(param, rawResult.getText());
            intent.putExtra("tagList", (Serializable) tagList);
            intent.putExtra("asnArrayList", (Serializable) asnArrayList);
            intent.putExtra("asnGetSet", asnGetSet);
            intent.putExtra("cc_document", cc_document);
            intent.putExtra("tallyQty", tallyQty);
            intent.putExtra("warehouse_name", warehouse_name);
            intent.putExtra("cc_type", cc_type);
            intent.putExtra("type", type);
            intent.putExtra("asnOL", asnOL);
        } else if(fClass.equals("InventoryActiveStock")) {
            intent = new Intent(this, InventoryActiveStock.class);
            intent.putExtra(param, rawResult.getText());
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
