package largo.largo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class CycleCountDocumentListAdapter extends ArrayAdapter<CycleCountDocumentGetterSetter> {

    private List<CycleCountDocumentGetterSetter> cycleCountDocumentItemList;

    private Context context;

    public CycleCountDocumentListAdapter(List<CycleCountDocumentGetterSetter> cycleCountDocumentItemList, Context context) {
        super(context, R.layout.list_view_cc_document, cycleCountDocumentItemList);
        this.cycleCountDocumentItemList = cycleCountDocumentItemList;
        this.context = context;
    }



    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);

        View listViewItem = inflater.inflate(R.layout.list_view_cc_document, null, true);

        TextView textViewCycleCountDoc = listViewItem.findViewById(R.id.textViewCycleCountDoc);
        TextView textViewQty = listViewItem.findViewById(R.id.textViewQty);
        TextView textViewCustomer = listViewItem.findViewById(R.id.textViewCustomer);

        CycleCountDocumentGetterSetter cycleCountDocument = cycleCountDocumentItemList.get(position);

        textViewCycleCountDoc.setText(String.valueOf(cycleCountDocument.getCc_code()));
        textViewQty.setText(cycleCountDocument.getCc_type());
        if(cycleCountDocument.getSource() != null) {
            if(cycleCountDocument.getSource2() != null) {
                textViewCustomer.setText(cycleCountDocument.getSource() + "/" + cycleCountDocument.getSource2());
            } else {
                textViewCustomer.setText(cycleCountDocument.getSource());
            }
        } else {
            textViewCustomer.setVisibility(View.GONE);
        }

        return listViewItem;
    }
}
