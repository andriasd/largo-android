package largo.largo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class ReceivingItemListAdapter extends ArrayAdapter<ReceivingItemGetterSetter> {

    private List<ReceivingItemGetterSetter> skuItemList;

    private Context context;

    public ReceivingItemListAdapter(List<ReceivingItemGetterSetter> skuItemList, Context context) {
        super(context, R.layout.list_view_receiving_item, skuItemList);
        this.skuItemList = skuItemList;
        this.context = context;
    }



    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);

        View listViewItem = inflater.inflate(R.layout.list_view_receiving_item, null, true);

        TextView textViewSku = listViewItem.findViewById(R.id.textViewSku);
        TextView textViewSkuName = listViewItem.findViewById(R.id.textViewSkuName);
        TextView textViewQty = listViewItem.findViewById(R.id.textViewQty);
        TextView textViewSource = listViewItem.findViewById(R.id.textViewSource);

        ReceivingItemGetterSetter skuItem = skuItemList.get(position);

        textViewSku.setText(skuItem.getSku());
        textViewSkuName.setText(skuItem.getSkuName());
        textViewQty.setText(skuItem.getQty());
        textViewSource.setText(skuItem.getSource());

        return listViewItem;
    }
}
