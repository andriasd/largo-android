package largo.largo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class PutawaySerialNumberListAdapter extends ArrayAdapter<PutawaySerialNumberGetterSetter> {

    private List<PutawaySerialNumberGetterSetter> serialNumberItemList;

    private Context context;

    public PutawaySerialNumberListAdapter(List<PutawaySerialNumberGetterSetter> serialNumberItemList, Context context) {
        super(context, R.layout.list_view_putaway_serial_number, serialNumberItemList);
        this.serialNumberItemList = serialNumberItemList;
        this.context = context;
    }



    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);

        View listViewItem = inflater.inflate(R.layout.list_view_putaway_serial_number, null, true);

        TextView textViewSerialNumber = listViewItem.findViewById(R.id.textViewSerialNumber);
        TextView textViewSku = listViewItem.findViewById(R.id.textViewSku);
        TextView textViewSkuName = listViewItem.findViewById(R.id.textViewSkuName);
        TextView textViewLocation = listViewItem.findViewById(R.id.textViewLocation);


        PutawaySerialNumberGetterSetter putawaySN = serialNumberItemList.get(position);

        textViewSerialNumber.setText(String.valueOf(putawaySN.getSN()));
        textViewSku.setText(putawaySN.getSku());
        textViewSkuName.setText(putawaySN.getSkuName());
        textViewLocation.setText(putawaySN.getLocation());

        return listViewItem;
    }
}
