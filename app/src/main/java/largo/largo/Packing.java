package largo.largo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static largo.largo.LoginActivity.MyPREFERENCES;

public class Packing extends AppCompatActivity {
    String TAG = this.getClass().getSimpleName();

    private Button backButton, newButton, saveButton;
    private TextView destination;
    private EditText packing_number, picking_document, item_code, quantity, weight, volume, destination_code;
    private Map<String, String> params = new HashMap<String, String>();
    private String param;
    private String fClass = this.getClass().getSimpleName();
    private Intent intent;
    private ListView listView;
    private List<PackingDocumentGetterSetter2> dokumenItemList;
    private PackingDocumentGetterSetter2 packDocGetSet;
    private PackingDocumentListAdapter2 adapter;

    SharedPreferences prefs;
    String usernameKey;

    DatabaseHelper myDb;
    String url;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.packing);

        prefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
        usernameKey = prefs.getString("usernameKey", "No name defined");

        intent = getIntent();
        param = this.getClass().getSimpleName();

        backButton = findViewById(R.id.back);
        newButton = findViewById(R.id.new_pack);
        saveButton = findViewById(R.id.save);
        destination = findViewById(R.id.destination);
        destination_code = findViewById(R.id.destination_code);

        packing_number = findViewById(R.id.packing_number);
        picking_document = findViewById(R.id.picking_document);
        item_code = findViewById(R.id.item_code);
        quantity = findViewById(R.id.quantity);
        weight = findViewById(R.id.weight);
        volume = findViewById(R.id.volume);
        listView = findViewById(R.id.ListPackingDoc);

        dokumenItemList = new ArrayList<>();
        adapter = new PackingDocumentListAdapter2(dokumenItemList, getApplicationContext());
        listView.setAdapter(adapter);

        myDb = new DatabaseHelper(this);
        getServerURL();

        // getPackingCode(url + "/Api_packing/getPackingCode");
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        newButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPackingCode(url + "/Api_packing/getPackingCode/1");
                picking_document.setText("");
                item_code.setText("");
                quantity.setText("");
                packing_number.requestFocus();
                packing_number.hasFocus();
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backButton.setEnabled(false);
                params.clear();
                params.put("packing_number", packing_number.getText().toString());
                params.put("carton", "");
                params.put("delivery_note", picking_document.getText().toString());
                params.put("item_code", item_code.getText().toString());
                params.put("destination_code", destination_code.getText().toString());
                params.put("quantity", quantity.getText().toString());
                params.put("weight", weight.getText().toString());
                params.put("volume", volume.getText().toString());
                params.put("user", usernameKey);

                savePacking(params);

                newButton.setEnabled(false);
                saveButton.setEnabled(false);
            }
        });

        packing_number.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    if(packing_number.getText().toString().length() > 0) {
                        params.clear();
                        params.put("pc_code", packing_number.getText().toString());
                        getPackingDetail(params);
                        picking_document.setText("");
                        picking_document.requestFocus();
                        picking_document.hasFocus();
                    }
                }
                return false;
            }
        });

        picking_document.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    if(packing_number.getText().toString().length() > 0) {
                        if(picking_document.getText().toString().length() > 0) {
                            params.clear();
                            params.put("packing_number", packing_number.getText().toString());
                            params.put("delivery_note", picking_document.getText().toString());
                            getRecList(params);
                        }
                    } else {
                        Toast.makeText(Packing.this, "Packing Number can't be empty.", Toast.LENGTH_SHORT).show();
                        adapter.clear();
                        adapter.notifyDataSetChanged();
                        destination.setText("-");
                        picking_document.setText("");
                        packing_number.requestFocus();
                    }
                }
                return false;
            }
        });

        item_code.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    if(picking_document.getText().toString().length() > 0) {
                        params.clear();
                        params.put("item_code", item_code.getText().toString());
                        params.put("delivery_note", picking_document.getText().toString());
                        getItemQtyByDN(params);
                    }
                }
                return false;
            }
        });
    }

    private void getPackingCode(String getDoc){
        RequestQueue requestQueue=Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, getDoc, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);
                try{
                    JSONObject jsonObject=new JSONObject(response);
                    packing_number.setText("");
                    weight.setText("0");
                    volume.setText("0");
                }catch (JSONException e){e.printStackTrace();}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;

            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void getRecList(final Map<String, String> parameters) {
        StringRequest strReq = new StringRequest(Request.Method.POST, url + "/Api_packing/getRecList/", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("getRecList", response.toString());

                try {
                    JSONObject data = new JSONObject(response);
                    Integer status = data.getInt("status");
                    String message = data.getString("message");
                    String result = data.getString("results");
                    JSONObject results = new JSONObject(result);

                    if(results.getBoolean("status")) {
                        JSONArray dataRes = new JSONArray(results.getString("message"));
                        dokumenItemList = new ArrayList<>();
                        for(int x=0;x<dataRes.length();x++){
                            JSONObject jsonObject1=dataRes.getJSONObject(x);
                            packDocGetSet = new PackingDocumentGetterSetter2(
                                    jsonObject1.getString("item_code"),
                                    jsonObject1.getString("item_name"),
                                    jsonObject1.getString("outbound_id"),
                                    jsonObject1.getString("outbound_code"),
                                    jsonObject1.getString("destination_code"),
                                    jsonObject1.getString("destination_name"),
                                    jsonObject1.getString("destination_address"),
                                    jsonObject1.getString("picking_id"),
                                    jsonObject1.getString("picking_code"),
                                    jsonObject1.getString("uom"),
                                    jsonObject1.getString("qty_picked"),
                                    jsonObject1.getString("qty_packed")
                            );
                            dokumenItemList.add(packDocGetSet);
                            destination.setText(jsonObject1.getString("destination_name"));
                            destination_code.setText(jsonObject1.getString("destination_code"));
                        }
                        adapter = new PackingDocumentListAdapter2(dokumenItemList, getApplicationContext());
                        listView.setAdapter(adapter);
                    } else {
                        Toast.makeText(getApplicationContext(), results.getString("message"), Toast.LENGTH_SHORT).show();
                        adapter.clear();
                        adapter.notifyDataSetChanged();
                        destination.setText("-");
                        picking_document.setText("");
                        picking_document.requestFocus();
                    }
                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;

            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);

    }

    private void getPackingDetail(final Map<String, String> parameters) {
        StringRequest strReq = new StringRequest(Request.Method.POST, url + "/Api_packing/getPackingDetail/", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("getPackingDetail", response.toString());

                try {
                    JSONObject data = new JSONObject(response);
                    Integer status = data.getInt("status");
                    String message = data.getString("message");
                    String result = data.getString("results");
                    JSONObject results = new JSONObject(result);

                    if(results.length() > 0) {
                        weight.setText(results.getString("weight"));
                        volume.setText(results.getString("volume"));
                        destination.setText(results.getString("destination_name"));
                        destination_code.setText(results.getString("destination_code"));
                    } else {
                        weight.setText("0");
                        volume.setText("0");
                        destination.setText("-");
                    }
                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;

            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);

    }

    private void getItemQtyByDN(final Map<String, String> parameters) {
        StringRequest strReq = new StringRequest(Request.Method.POST, url + "/Api_packing/getItemQtyByDN/", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("getPackingDetail", response.toString());

                try {
                    JSONObject data = new JSONObject(response);
                    String result = data.getString("results");
                    JSONObject results = new JSONObject(result);

                    if(results.length() > 0) {
                        quantity.setText(results.getString("qty_picked"));
                    }
                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;

            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);

    }

    private void savePacking(final Map<String, String> parameters) {
        StringRequest strReq = new StringRequest(Request.Method.POST, url + "/Api_packing/savePacking/", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("savePacking", response.toString());

                try {
                    JSONObject data = new JSONObject(response);

                    Toast.makeText(Packing.this, data.getString("message"), Toast.LENGTH_SHORT).show();

                    if(data.getBoolean("status")) {
                        params.clear();
                        params.put("packing_number", packing_number.getText().toString());
                        params.put("delivery_note", picking_document.getText().toString());
                        getRecList(params);

                        quantity.setText("");
                        item_code.requestFocus();
                        item_code.setText("");
                    }

                    backButton.setEnabled(true);
                    newButton.setEnabled(true);
                    saveButton.setEnabled(true);
                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                backButton.setEnabled(true);
                newButton.setEnabled(true);
                saveButton.setEnabled(true);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;

            }
        };

        int socketTimeout = 3000000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        strReq.setRetryPolicy(policy);

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);

    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }
}
