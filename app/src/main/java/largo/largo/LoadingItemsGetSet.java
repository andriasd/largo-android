package largo.largo;

import java.io.Serializable;

public class LoadingItemsGetSet implements Serializable {
    String kd_barang, nama_barang, order_qty, unit_code, scanned_qty, colly;

    public LoadingItemsGetSet(String kd_barang, String nama_barang, String order_qty, String unit_code, String scanned_qty) {
        this.kd_barang = kd_barang;
        this.nama_barang = nama_barang;
        this.order_qty = order_qty;
        this.unit_code = unit_code;
        this.scanned_qty = scanned_qty;
    }

    public String getColly() {
        return colly;
    }

    public void setColly(String colly) {
        this.colly = colly;
    }

    public LoadingItemsGetSet(String kd_barang, String nama_barang, String order_qty, String unit_code, String scanned_qty, String colly) {
        this.kd_barang = kd_barang;
        this.nama_barang = nama_barang;
        this.order_qty = order_qty;
        this.unit_code = unit_code;
        this.scanned_qty = scanned_qty;
        this.colly = colly;
    }

    public String getKd_barang() {
        return kd_barang;
    }

    public void setKd_barang(String kd_barang) {
        this.kd_barang = kd_barang;
    }

    public String getNama_barang() {
        return nama_barang;
    }

    public void setNama_barang(String nama_barang) {
        this.nama_barang = nama_barang;
    }

    public String getOrder_qty() {
        return order_qty;
    }

    public void setOrder_qty(String order_qty) {
        this.order_qty = order_qty;
    }

    public String getUnit_code() {
        return unit_code;
    }

    public void setUnit_code(String unit_code) {
        this.unit_code = unit_code;
    }

    public String getScanned_qty() {
        return scanned_qty;
    }

    public void setScanned_qty(String scanned_qty) {
        this.scanned_qty = scanned_qty;
    }
}
