package largo.largo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static largo.largo.LoginActivity.MyPREFERENCES;

public class PickingDocument extends AppCompatActivity {
    DatabaseHelper myDb;

    String TAG = PickingDocument.class.getSimpleName();
    Button next,backButton;
    EditText picking_doc;

    String url;

    List<String> listDataHeader;
    List<String> qty = new ArrayList<String>();
    HashMap<String, List<String>> listDataChild;

    ListView listView;
    private List<PickingDocumentGetterSetter> dokumenItemList;

    private int lastExpandedPosition = -1;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.outbound_picking_document);

        backButton = findViewById(R.id.back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        myDb = new DatabaseHelper(this);

        getServerURL();

        next = findViewById(R.id.next);
        picking_doc = findViewById(R.id.picking_document);
        picking_doc.setShowSoftInputOnFocus(false);

        listView =  findViewById(R.id.ListPickingDoc);

        dokumenItemList = new ArrayList<>();
        DataListPickingDocument(url+"/api/picking/get_document");

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                picking_doc.setText(dokumenItemList.get(position).getPlName());
            }
        });

        next.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        postPickingDoc();
                    }
                }
        );
    }

    private void DataListPickingDocument(String getDoc){
//        final ArrayList<String> items = new ArrayList<String>();

        RequestQueue requestQueue=Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, getDoc, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                try{
                    JSONObject jsonObject=new JSONObject(response);
                    if(jsonObject.getInt("status")==200){
                        JSONArray jsonArray1=jsonObject.getJSONArray("results");
                        for(int i=0;i<jsonArray1.length();i++){
                            JSONObject jsonObject1=jsonArray1.getJSONObject(i);
                            PickingDocumentGetterSetter item_dokumen = new PickingDocumentGetterSetter(
                                    jsonObject1.getString("pl_name"),
                                    jsonObject1.getString("qty"),
                                    ""
                            );
                            dokumenItemList.add(item_dokumen);
                        }
                        PickingDocumentListAdapter adapter = new PickingDocumentListAdapter(dokumenItemList, getApplicationContext());

                        listView.setAdapter(adapter);

                    }
                } catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;


            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void postPickingDoc() {
        StringRequest strReq = new StringRequest(Request.Method.GET, url+"/Api_picking/get/" + String.valueOf(picking_doc.getText()), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());

                try {
                    JSONObject data = new JSONObject(response);
                    Integer status = data.getInt("status");
                    String message = data.getString("message");

                    if (status == 200) {
                        JSONArray jsonArray1=data.getJSONArray("locations");
                        for(int i=0;i<jsonArray1.length();i++){
                            JSONObject jsonObject1=jsonArray1.getJSONObject(i);
                            Intent intent = new Intent(getApplicationContext(), PickingSerialNumber.class);
                            intent.putExtra("picking_document",String.valueOf(picking_doc.getText()));
                            intent.putExtra("kd_barang",jsonObject1.getString("kd_barang"));
                            intent.putExtra("loc_name",jsonObject1.getString("loc_name"));
                            intent.putExtra("has_qty",jsonObject1.getString("has_qty"));
                            intent.putExtra("qty",jsonObject1.getString("qty"));
                            intent.putExtra("multi_qty",jsonObject1.getString("multi_qty"));
                            startActivity(intent);
                        }

                        // finish();
                    } else if (status == 401) {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;


            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }
}
