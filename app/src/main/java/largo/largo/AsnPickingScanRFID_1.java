package largo.largo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.zebra.rfid.api3.Antennas;
import com.zebra.rfid.api3.ENUM_TRANSPORT;
import com.zebra.rfid.api3.ENUM_TRIGGER_MODE;
import com.zebra.rfid.api3.HANDHELD_TRIGGER_EVENT_TYPE;
import com.zebra.rfid.api3.InvalidUsageException;
import com.zebra.rfid.api3.MEMORY_BANK;
import com.zebra.rfid.api3.OperationFailureException;
import com.zebra.rfid.api3.RFIDReader;
import com.zebra.rfid.api3.ReaderDevice;
import com.zebra.rfid.api3.Readers;
import com.zebra.rfid.api3.RfidEventsListener;
import com.zebra.rfid.api3.RfidReadEvents;
import com.zebra.rfid.api3.RfidStatusEvents;
import com.zebra.rfid.api3.START_TRIGGER_TYPE;
import com.zebra.rfid.api3.STATUS_EVENT_TYPE;
import com.zebra.rfid.api3.STOP_TRIGGER_TYPE;
import com.zebra.rfid.api3.TAG_FIELD;
import com.zebra.rfid.api3.TagAccess;
import com.zebra.rfid.api3.TagData;
import com.zebra.rfid.api3.TagStorageSettings;
import com.zebra.rfid.api3.TriggerInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class AsnPickingScanRFID_1 extends AppCompatActivity {
    String TAG = this.getClass().getSimpleName();

    private static AsyncTask<Void, Void, Boolean> connectReader;
    String scan32;
    private static Readers readers;
    private static ArrayList availableRFIDReaderList;
    private static ReaderDevice readerDevice;
    public static RFIDReader reader;
    private EventHandler eventHandler;
    private TagAccess tagAccess;
    private TagAccess.ReadAccessParams readAccessParams;

    private List<String> tagList, tagListGet;

    Button cancelButton, doneButton, scanButton, scan32Button;
    private String param;
    private String fClass = this.getClass().getSimpleName();
    private Intent intent;
    private ListView listView;
    private List<PickingItemGetterSetter> itemList;
    private List<String> docList;
    private String pickingDocument;
    private PickingItemListAdapter adapter;
    private PickingItemGetterSetter pickingItemGetSet;

    private JSONObject paramsJson;
    public static final Map<String, String> params = new HashMap<String, String>();
    public static final String MyPREFERENCES = "MyPrefs" ;

    DatabaseHelper myDb;
    String url;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.outbound_picking_by_asn_rfid);

        intent = getIntent();
        param = this.getClass().getSimpleName();

        paramsJson = new JSONObject();

        myDb = new DatabaseHelper(this);
        getServerURL();

        cancelButton = findViewById(R.id.cancel);
        doneButton = findViewById(R.id.done);
        listView = findViewById(R.id.rfid_list);

        docList = new ArrayList<>();
        itemList = new ArrayList<>();
        tagListGet = new ArrayList<>();
        pickingDocument = "";

        adapter = new PickingItemListAdapter(itemList, getApplicationContext());
        listView.setAdapter(adapter);

        tagList = new ArrayList<>();
        if(intent.getSerializableExtra("tagList") != null) {
            tagList = (List<String>) intent.getSerializableExtra("tagList");
        }
        if(intent.getStringExtra("picking_document") != null) {
            pickingDocument = intent.getStringExtra("picking_document");
            new getPickingASync(url + "/Api_picking/get/" + pickingDocument).execute();
        }

        tagAccess = new TagAccess();
        readAccessParams = tagAccess.new ReadAccessParams();
        readAccessParams.setAccessPassword(0);
        readAccessParams.setCount(8);
        readAccessParams.setMemoryBank(MEMORY_BANK.MEMORY_BANK_EPC);
        readAccessParams.setOffset(2);

        scanButton = findViewById(R.id.scan);
        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(scanButton.getText().equals("SCAN")) {
                    scan32 = "1";
                    triggerStart();
                } else if(scanButton.getText().equals("STOP")) {
                    triggerStop();
                } else {
                    triggerStop();
                    triggerStart();
                }
            }
        });

        scan32Button = findViewById(R.id.scan32);
        scan32Button.setVisibility(View.GONE);
        scan32Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(scan32Button.getText().equals("SCAN32")) {
                    scan32 = "1";
                    triggerStart();
                } else if(scan32Button.getText().equals("STOP32")) {
                    triggerStop();
                } else {

                }
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    finish();
                    Intent intent2 = new Intent(getApplicationContext(), AsnPickingItem.class);
                    intent2.putExtra("picking_document", pickingDocument);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (reader != null) {
                        triggerStop();
                        reader.Config.setTriggerMode(ENUM_TRIGGER_MODE.BARCODE_MODE, true);
                        reader.Events.removeEventsListener(eventHandler);
                        reader.disconnect();
                        Toast.makeText(getApplicationContext(), "Disconnecting reader", Toast.LENGTH_SHORT).show();
                        reader = null;
                        readers.Dispose();
                        readers = null;
                        scanButton.setText("Connect to Reader");
                        scanButton.setBackgroundColor(Color.parseColor("#FFBEBE"));
                        scan32Button.setText("Connect to Reader");
                        scan32Button.setBackgroundColor(Color.parseColor("#FFBEBE"));
                    }
                } catch (InvalidUsageException e) {
                    e.printStackTrace();
                } catch (OperationFailureException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                intent = new Intent(getApplicationContext(), AsnPickingLocation.class);
                intent.putExtra("picking_document", pickingDocument);
                startActivity(intent);
            }
        });

        // SDK
        if (readers == null) {
            readers = new Readers(this, ENUM_TRANSPORT.SERVICE_SERIAL);
        }

        connectReader = new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... voids) {
                try {
                    if (readers != null) {
                        if (readers.GetAvailableRFIDReaderList() != null) {
                            availableRFIDReaderList = readers.GetAvailableRFIDReaderList();
                            if (availableRFIDReaderList.size() != 0) {
                                // get first reader from list
                                readerDevice = (ReaderDevice) availableRFIDReaderList.get(0);
                                reader = readerDevice.getRFIDReader();
                                if (!reader.isConnected()) {
                                    // Establish connection to the RFID Reader
                                    reader.connect();
                                    ConfigureReader();
                                    return true;
                                }
                            }
                        }
                    }
                } catch (InvalidUsageException e) {
                    e.printStackTrace();
                } catch (OperationFailureException e) {
                    e.printStackTrace();
                    Log.d(TAG, "OperationFailureException " + e.getVendorMessage());
                }
                return false;
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                if (aBoolean) {
                    Toast.makeText(getApplicationContext(), "Reader Connected", Toast.LENGTH_SHORT).show();
                    scanButton.setText("SCAN");
                    scanButton.setBackgroundColor(Color.parseColor("#32C5D2"));
                    scan32Button.setText("SCAN32");
                    scan32Button.setBackgroundColor(Color.parseColor("#32C5D2"));
                }
            }
        };

        Toast.makeText(getApplicationContext(), "Connecting to Reader...", Toast.LENGTH_SHORT).show();
        connectReader.execute();
    }

    private void triggerStop() {
        try {
            reader.Actions.Inventory.stop();
        } catch (InvalidUsageException e) {
            e.printStackTrace();
        } catch (OperationFailureException e) {
            e.printStackTrace();
        }
        scanButton.setText("SCAN");
        scanButton.setBackgroundColor(Color.parseColor("#32C5D2"));
        scan32Button.setText("SCAN32");
        scan32Button.setBackgroundColor(Color.parseColor("#32C5D2"));
    }

    private void triggerStart() {
        try {
            reader.Actions.Inventory.perform();
        } catch (InvalidUsageException e) {
            e.printStackTrace();
        } catch (OperationFailureException e) {
            e.printStackTrace();
        }
        scanButton.setText("STOP");
        scanButton.setBackgroundColor(Color.parseColor("#FFBEBE"));
        scan32Button.setText("STOP32");
        scan32Button.setBackgroundColor(Color.parseColor("#FFBEBE"));
    }

    private void ConfigureReader() {
        if (reader.isConnected()) {
            TriggerInfo triggerInfo = new TriggerInfo();
            triggerInfo.StartTrigger.setTriggerType(START_TRIGGER_TYPE.START_TRIGGER_TYPE_IMMEDIATE);
            triggerInfo.StopTrigger.setTriggerType(STOP_TRIGGER_TYPE.STOP_TRIGGER_TYPE_IMMEDIATE);

            TagStorageSettings tagStorageSettings = new TagStorageSettings();
            tagStorageSettings.setTagFields(TAG_FIELD.ALL_TAG_FIELDS);

            try {
                Antennas.AntennaRfConfig antennaRfConfig = reader.Config.Antennas.getAntennaRfConfig(1);
                antennaRfConfig.setrfModeTableIndex(0);
                antennaRfConfig.setTari(0);
                antennaRfConfig.setTransmitPowerIndex(150);
                // set the configuration
                reader.Config.Antennas.setAntennaRfConfig(1, antennaRfConfig);

                // receive events from reader
                if (eventHandler == null)
                    eventHandler = new EventHandler();
                reader.Events.addEventsListener(eventHandler);
                // HH event
                reader.Events.setHandheldEvent(true);
                // tag event with tag data
                reader.Events.setTagReadEvent(true);
                // application will collect tag using getReadTags API
                reader.Events.setAttachTagDataWithReadEvent(false);
                // set trigger mode as rfid so scanner beam will not come
                reader.Config.setTriggerMode(ENUM_TRIGGER_MODE.RFID_MODE, true);
                // set start and stop triggers
                reader.Config.setStartTrigger(triggerInfo.StartTrigger);
                reader.Config.setStopTrigger(triggerInfo.StopTrigger);
                reader.Config.setUniqueTagReport(true);
                reader.Config.setTagStorageSettings(tagStorageSettings);

            } catch (InvalidUsageException e) {
                e.printStackTrace();
            } catch (OperationFailureException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (reader != null) {
                triggerStop();
                reader.Config.setTriggerMode(ENUM_TRIGGER_MODE.BARCODE_MODE, true);
                reader.Events.removeEventsListener(eventHandler);
                reader.disconnect();
                Toast.makeText(getApplicationContext(), "Disconnecting reader", Toast.LENGTH_SHORT).show();
                reader = null;
                readers.Dispose();
                readers = null;
                scanButton.setText("Connect to Reader");
                scanButton.setBackgroundColor(Color.parseColor("#FFBEBE"));
                scan32Button.setText("Connect to Reader");
                scan32Button.setBackgroundColor(Color.parseColor("#FFBEBE"));
            }
        } catch (InvalidUsageException e) {
            e.printStackTrace();
        } catch (OperationFailureException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Read/Status Notify handler
    // Implement the RfidEventsLister class to receive event notifications
    public class EventHandler implements RfidEventsListener {
        // Read Event Notification
        public void eventReadNotify(RfidReadEvents e) {
            try {
                // Recommended to use new method getReadTagsEx for better performance in case of large tag population
                final TagData[] myTags = reader.Actions.getReadTags(500);
                String rfidTagHex = "";
//            if(adapter != null) {
//                adapter.notifyDataSetChanged();
//            }

                if (myTags != null) {
                    for (int index = 0; index < myTags.length; index++) {
                        rfidTagHex = myTags[index].getTagID().toUpperCase();
                        if(rfidTagHex.equals("111122223333444455559999")) {
                            rfidTagHex = "36314a4130304d31333038303030313335".toUpperCase();
                        } else if(rfidTagHex.equals("36314A4130304D3000000009")) {
                            rfidTagHex = "36314a4130304d31333038303030323335".toUpperCase();
                        } else if(rfidTagHex.equals("0000000000000029")) {
                            rfidTagHex = "36314a4130304d31333038303030333335".toUpperCase();
                        } else if(rfidTagHex.equals("00000000010101122825000000000028")) {
                            rfidTagHex = "36314a4130304d31333038303030343335".toUpperCase();
                        }

                        try {
                            if(tagList != null) {
                                if(!tagList.contains(rfidTagHex)) {
                                    final String finalRfidTagHex = rfidTagHex;
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            System.out.println(finalRfidTagHex);
                                            new postPickingASync(pickingDocument ,finalRfidTagHex).execute();
                                        }
                                    });
                                }
                            } else {
                                final String finalRfidTagHex1 = rfidTagHex;
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        System.out.println(finalRfidTagHex1);
                                        tagList.add(finalRfidTagHex1);
                                        new postPickingASync(pickingDocument ,finalRfidTagHex1).execute();
                                    }
                                });
                            }
                        } catch (StringIndexOutOfBoundsException ex) {
                            Toast.makeText(getApplicationContext(), "Format barcode tidak sesuai.", Toast.LENGTH_SHORT).show();
                        } catch (IndexOutOfBoundsException ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            } catch(IllegalStateException ex) {
                ex.printStackTrace();
            }
        }

        // Status Event Notification
        public void eventStatusNotify(RfidStatusEvents rfidStatusEvents) {
            Log.d(TAG, "Status Notification: " + rfidStatusEvents.StatusEventData.getStatusEventType());
            if (rfidStatusEvents.StatusEventData.getStatusEventType() == STATUS_EVENT_TYPE.HANDHELD_TRIGGER_EVENT) {
                if (rfidStatusEvents.StatusEventData.HandheldTriggerEventData.getHandheldEvent() == HANDHELD_TRIGGER_EVENT_TYPE.HANDHELD_TRIGGER_PRESSED) {
                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... voids) {
                            try {
                                reader.Actions.Inventory.perform();
                            } catch (InvalidUsageException e) {
                                e.printStackTrace();
                            } catch (OperationFailureException e) {
                                e.printStackTrace();
                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            scanButton.setText("STOP");
                            scanButton.setBackgroundColor(Color.parseColor("#FFBEBE"));
                            scan32Button.setText("STOP32");
                            scan32Button.setBackgroundColor(Color.parseColor("#FFBEBE"));
                        }
                    }.execute();
                }
                if (rfidStatusEvents.StatusEventData.HandheldTriggerEventData.getHandheldEvent() == HANDHELD_TRIGGER_EVENT_TYPE.HANDHELD_TRIGGER_RELEASED) {
                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... voids) {
                            try {
                                reader.Actions.Inventory.stop();
                            } catch (InvalidUsageException e) {
                                e.printStackTrace();
                            } catch (OperationFailureException e) {
                                e.printStackTrace();
                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            scanButton.setText("SCAN");
                            scanButton.setBackgroundColor(Color.parseColor("#32C5D2"));
                            scan32Button.setText("SCAN32");
                            scan32Button.setBackgroundColor(Color.parseColor("#32C5D2"));
                        }
                    }.execute();
                }
            }
        }
    }

    private class postPickingASync extends AsyncTask<Void, Void, Void> {
        String pickingCodeParam = "";
        String serialNumberParam = "";

        postPickingASync(String pickingCodeParam, String serialNumberParam) {
            super();
            this.pickingCodeParam = pickingCodeParam;
            this.serialNumberParam = serialNumberParam;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    postPicking(pickingCodeParam, serialNumberParam);
                }
            });
            return null;
        }
    }

    private void postPicking(final String pickingCodeParam, final String serialNumberParam) {
//        System.out.println("PostPicking, " + params);
//        checkPost = 0;
        String postUrl = this.url + "/Api_picking/post_serial/";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);

                        try {
                            JSONObject data = new JSONObject(response);
                            Integer status = data.getInt("status");
                            String message = data.getString("message");
                            if(status == 200) {
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                new getPickingASync(url + "/Api_picking/get/" + pickingCodeParam).execute();
                            } else {
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String usernameKey = prefs.getString("usernameKey", "No name defined");

                params.put("picking_code", pickingCodeParam);
                params.put("serial_number", serialNumberParam);
                params.put("qty", "1");
                params.put("kit_time", new SimpleDateFormat("YYYY-MM-dd HH:mm:ss").format(new Date()));
                params.put("pick_time", new SimpleDateFormat("YYYY-MM-dd HH:mm:ss").format(new Date()));
                params.put("user", usernameKey);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;

            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    private class getPickingASync extends AsyncTask<Void, Void, Void> {
        String getDocParam = "";

        getPickingASync(String getDocParam) {
            super();
            this.getDocParam = getDocParam;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    getPicking(getDocParam);
                }
            });
            return null;
        }
    }

    private void getPicking(String getDoc){
        /*if(itemList != null) {
            itemList.clear();
        }*/

        RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, getDoc, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG + "getPicking", response);
                try{
                    JSONObject jsonObject = new JSONObject(response);
                    if(jsonObject.getInt("status")==200){
                        JSONArray jsonArray1 = jsonObject.getJSONArray("locations");
                        for(int i = 0; i < jsonArray1.length(); i++){
                            JSONObject jsonObject1=jsonArray1.getJSONObject(i);
                            System.out.println(jsonObject1);
                            pickingItemGetSet = new PickingItemGetterSetter(
                                    jsonObject1.getString("kd_barang"),
                                    jsonObject1.getString("picked_qty"),
                                    jsonObject1.getString("has_qty"),
                                    jsonObject1.getString("qty"),
                                    jsonObject1.getString("available_qty"),
                                    jsonObject1.getString("loc_name")
                            );

                            if(itemList.size() > 0) {
                                int position = 0;
                                for(int j = 0; j < itemList.size(); j++) {
                                    if(jsonObject1.getString("kd_barang").equals(itemList.get(j).getSku())) {
                                        position = j;
                                    }
                                }
                                itemList.set(position, pickingItemGetSet);
                            } else {
                                itemList.add(pickingItemGetSet);
                                docList.add(jsonObject1.getString("pl_name"));
                                tagListGet.add(jsonObject1.getString("kd_barang"));
                            }
                        }

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                adapter.notifyDataSetChanged();
                            }
                        });

                    }
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;


            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }
}