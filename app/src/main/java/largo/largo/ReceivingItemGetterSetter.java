package largo.largo;

import java.io.Serializable;

public class ReceivingItemGetterSetter implements Serializable {
    String sku, sku_name, qty, source;

    public ReceivingItemGetterSetter(String sku, String sku_name, String qty, String source) {
        this.sku = sku;
        this.sku_name = sku_name;
        this.qty = qty;
        this.source = source;
    }


    public String getSku() {
        return sku;
    }

    public String getSkuName() {
        return sku_name;
    }

    public String getQty() {
        return qty;
    }

    public String getSource() {
        return source;
    }
}
