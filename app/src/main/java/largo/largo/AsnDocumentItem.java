package largo.largo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AsnDocumentItem extends AppCompatActivity {
    String TAG = this.getClass().getSimpleName();

    private Button backButton, nextButton;
    private ImageButton scanButton;
    private TextView header_text;
    private EditText receivingDocument;
    private String param;
    private String fClass = this.getClass().getSimpleName();
    private String receiving_document, inbound_document;
    private Intent intent;
    private ListView listView;
    private ReceivingDocumentItemGetterSetter receivingDocumentItemGetterSetter;
    private List<ReceivingDocumentItemGetterSetter> dokumenItemList;
    private List<String> docList;

    DatabaseHelper myDb;
    String url;

    public static final Map<String, String> params = new HashMap<String, String>();
    public static final String MyPREFERENCES = "MyPrefs" ;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inbound_tally_receiving_document_item);

        intent = getIntent();
        param = this.getClass().getSimpleName();

        myDb = new DatabaseHelper(this);
        getServerURL();

        backButton = findViewById(R.id.back);
        nextButton = findViewById(R.id.next);
        listView = findViewById(R.id.ListTallyDoc);
        receivingDocument = findViewById(R.id.receiving_document);

        docList = new ArrayList<>();
        dokumenItemList = new ArrayList<>();
        receiving_document = intent.getStringExtra("receiving_document");
        inbound_document = intent.getStringExtra("inbound_document");
        try {
            System.out.println(url + "/api/receiving/get_document_item/" + URLEncoder.encode(receiving_document.replace("/","+"),"utf-8") + "/" + URLEncoder.encode(inbound_document.replace("/","+"), "utf-8"));
            DataListDokument(url + "/api/receiving/get_document_item/" + URLEncoder.encode(receiving_document.replace("/","+"),"utf-8") + "/" + URLEncoder.encode(inbound_document.replace("/","+"), "utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        header_text = findViewById(R.id.header_tally);
        String header_string = header_text.getText().toString();
        header_text.setText(header_string + " - " + receiving_document + ", " + inbound_document);
        header_text.setSelected(true);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(receivingDocument.getText().toString().length() > 0) {
                    Intent intent = new Intent(getApplicationContext(), AsnDocumentItemDetail.class);
                    intent.putExtra("receiving_document", receiving_document);
                    intent.putExtra("inbound_document", inbound_document);
                    intent.putExtra("item_code", receivingDocument.getText().toString());
                    intent.putExtra("dokumenItemList", (Serializable) dokumenItemList);
                    startActivity(intent);
                } else {
                    Toast.makeText(AsnDocumentItem.this, "Please input item code first.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        receivingDocument.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 0) {
                    if(receivingDocument.getText().toString().length() > 0) {
                        Intent intent = new Intent(getApplicationContext(), AsnDocumentItemDetail.class);
                        intent.putExtra("receiving_document", receiving_document);
                        intent.putExtra("inbound_document", inbound_document);
                        intent.putExtra("item_code", receivingDocument.getText().toString());
                        intent.putExtra("dokumenItemList", (Serializable) dokumenItemList);
                        startActivity(intent);
                    } else {
                        Toast.makeText(AsnDocumentItem.this, "Please input item code first.", Toast.LENGTH_SHORT).show();
                    }
                }
                return false;
            }
        });

//        listView.setVisibility(View.GONE);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                receivingDocument.setText(dokumenItemList.get(position).getItem_code());
                Intent intent = new Intent(getApplicationContext(), AsnDocumentItemDetail.class);
                intent.putExtra("receiving_document", receiving_document);
                intent.putExtra("inbound_document", inbound_document);
                intent.putExtra("item_code", dokumenItemList.get(position).getItem_code());
                intent.putExtra("dokumenItemList", (Serializable) dokumenItemList);
                startActivity(intent);
            }
        });

        scanButton = findViewById(R.id.scanButton);
        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getApplicationContext(), QRScanner.class);
                intent.putExtra("param", "item_code");
                intent.putExtra("receiving_document", receiving_document);
                intent.putExtra("inbound_document", inbound_document);
                intent.putExtra("fClass", fClass);
                intent.putExtra("docList", (Serializable) docList);
                startActivity(intent);
            }
        });

//        if(intent.getStringExtra("item_code") != null) {
//            if (docList.contains(intent.getStringExtra("item_code"))) {
//                Intent intent2 = new Intent(getApplicationContext(), AsnDocumentItemDetail.class);
//                intent2.putExtra("item_code", intent.getStringExtra("item_code"));
//                intent2.putExtra("receiving_document", intent.getStringExtra("receiving_document"));
//                intent2.putExtra("inbound_document", intent.getStringExtra("inbound_document"));
//                intent2.putExtra("dokumenItemList", (Serializable) dokumenItemList);
//                startActivity(intent2);
//                intent2.removeExtra("receiving_document");
//                intent2.removeExtra("inbound_document");
//                intent2.removeExtra("item_code");
//                intent2.removeExtra("dokumentItemList");
//            } else {
//                Toast.makeText(getApplicationContext(), "Document not available.", Toast.LENGTH_SHORT).show();
//            }
//        }
    }

    private void DataListDokument(String getDoc){
        RequestQueue requestQueue=Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, getDoc, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);
                try{
                    JSONObject jsonObject=new JSONObject(response);
                    if(jsonObject.getInt("status")==200){
                        JSONArray jsonArray1=jsonObject.getJSONArray("results");
                        if(docList.size() > 0) {
                            docList.clear();
                        }
                        for(int i=0;i<jsonArray1.length();i++){
                            JSONObject jsonObject1=jsonArray1.getJSONObject(i);
                            ReceivingDocumentItemGetterSetter item_dokumen = new ReceivingDocumentItemGetterSetter(
                                    jsonObject1.getString("receiving_code"),
                                    jsonObject1.getString("item_code"),
                                    jsonObject1.getString("item_name"),
                                    jsonObject1.getString("qty"),
                                    jsonObject1.getString("nama_satuan"),
                                    jsonObject1.getString("source"),
                                    jsonObject1.getString("image"),
                                    jsonObject1.getString("default_qty"),
                                    jsonObject1.getString("received_qty"),
                                    jsonObject1.getString("has_expdate"),
                                    jsonObject1.getString("has_batch"),
                                    jsonObject1.getString("has_qty")
                            );
                            dokumenItemList.add(item_dokumen);
                            docList.add(jsonObject1.getString("item_code"));
                        }
                        ReceivingDokumenItemListAdapter adapter = new ReceivingDokumenItemListAdapter(dokumenItemList, getApplicationContext());

                        listView.setAdapter(adapter);

                    }
                }catch (JSONException e){e.printStackTrace();}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;


            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }

    private BroadcastReceiver qrScan = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getStringExtra(param) != null) {
                String getParam = intent.getStringExtra(param);
                intent.removeExtra(param);

                if(docList.contains(getParam)) {
                    Intent intent2 = new Intent(getApplicationContext(), AsnItem.class);
                    intent2.putExtra("receiving_document", getParam);
                    startActivity(intent2);
                } else {
                    Toast.makeText(getApplicationContext(), "Document not available.", Toast.LENGTH_SHORT).show();
                }
            }
        }
    };
}
