package largo.largo;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PutawayLocation extends AppCompatActivity {
    DatabaseHelper myDb;
    String TAG = PutawayLocation.class.getSimpleName();

    private JSONObject paramsJson;
    public static final Map<String, String> parameters = new HashMap<String, String>();
    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String user_name = "usernameKey";
    public static final String handheld_session_code = "handheldsessioncodeKey";
    SharedPreferences sharedpreferences;

    private List<AsnGetSet> asnArrayList = new ArrayList<>();
    private List<String> tagList;
    private AsnGetSet asnGetSet;
    private AsnPutawayListAdapter asnAdapter;
    private AsnScanRFID asnScanRFID;
    private AsnScanQR asnScanQR;
    private PutawaySerialNumber putawaySerialNumber;
    private Intent intent;
    int checkPost;

    private TextView totalItems;
    private ImageButton scanButton, scanLocButton, scanPartButton;
    private String param;
    private String fClass = this.getClass().getSimpleName();

    String url;

    ListView listView;

    Button next,backButton;
    EditText location, serial_number, sku;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inbound_putaway_location);

        intent = getIntent();
        checkPost = 0;

        asnScanQR = new AsnScanQR();
        asnScanRFID = new AsnScanRFID();
        putawaySerialNumber = new PutawaySerialNumber();

        param = this.getClass().getSimpleName();
        totalItems = findViewById(R.id.qty);
        totalItems.setText("0");

        backButton = findViewById(R.id.back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getRecommendLoc();
            }
        });

        next = findViewById(R.id.next);

        location = findViewById(R.id.location);
        serial_number = findViewById(R.id.serial_number);
//        part_number = findViewById(R.id.part_number);
        sku = findViewById(R.id.sku);
        location.setShowSoftInputOnFocus(false);
        serial_number.setShowSoftInputOnFocus(false);
        sku.setShowSoftInputOnFocus(false);
//        part_number.setShowSoftInputOnFocus(false);

        myDb = new DatabaseHelper(this);

        getServerURL();

        listView = findViewById(R.id.ListItem);

        asnArrayList = new ArrayList<>();
        tagList = new ArrayList<>();
        tagList = (List<String>) intent.getSerializableExtra("tagList");
        asnArrayList = (List<AsnGetSet>) intent.getSerializableExtra("asnArrayList");
        try {
            if (asnArrayList.size() > 0) {
                asnAdapter = new AsnPutawayListAdapter(asnArrayList, getApplicationContext(), 3);
                listView.setAdapter(asnAdapter);
                totalItems.setText("0/" + asnArrayList.size());

                tagList = new ArrayList<>();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        for(int i = 0; i < asnArrayList.size(); i++) {
                            tagList.add(asnArrayList.get(i).getUpc());
                            //checkSerialNumber(asnArrayList.get(i).getUpc());
                            //new checkSerialNumberASync(asnArrayList.get(i).getUpc()).execute();
                            /*try {
                                Thread.sleep(100);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }*/
                        }
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        location.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    getLocation(location.getText().toString());
                }
                return false;
            }
        });

        sku.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)){
                    if(sku.length()>0){
                        getSku(sku.getText().toString());
                    }
                }
                return false;
            }
        });

        serial_number.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
//                    checkQty();
//                    if(check_qty >= Float.parseFloat(intent.getStringExtra("qty"))){
//                        showMessage("Alert", "Seluruh item sudah di proses putaway !");
//                    }else {
                    if(location.length() > 0) {
                        if(tagList.contains(serial_number.getText().toString().toUpperCase()) || serial_number.getText().toString().toUpperCase().contains("OL")) {
                            getSkuSerialNumber(serial_number.getText().toString().toUpperCase(), sku.getText().toString());
                            // getLocationSerialNumber(serial_number.getText().toString().toUpperCase(), location.getText().toString(), part_number.getText().toString());
                        } else {
                            Toast.makeText(getApplicationContext(), "Serial Number ini tidak ada di dalam list.", Toast.LENGTH_SHORT).show();
                            serial_number.requestFocus();
                            serial_number.setText("");
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Please insert location first.", Toast.LENGTH_SHORT).show();
                    }
//                        serialNumberItemList.clear();
//                        DataListSerialNumber();
//                    }
                }
                return false;
            }
        });

        next.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(next.getText().toString().equals("NEXT")) {
                            location.setText("");
                            serial_number.setText("");
                            sku.setText("");
                            location.setFocusableInTouchMode(true);
                            location.setFocusable(true);
                            location.requestFocus();
                        } else {
                            intent = new Intent(PutawayLocation.this,PutawaySerialNumber.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    }
                }
        );

        scanButton = findViewById(R.id.scanButton);
        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getApplicationContext(), QRScanner.class);
                intent.putExtra("param", "putLocQR");
                intent.putExtra("fClass", fClass);
                intent.putExtra("asnArrayList", (Serializable) asnArrayList);
                intent.putExtra("tagList", (Serializable) tagList);
                intent.putExtra("asnGetSet", asnGetSet);
                intent.putExtra("location", location.getText().toString());
                startActivity(intent);
            }
        });

        scanLocButton = findViewById(R.id.scanLocButton);
        scanLocButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getApplicationContext(), QRScanner.class);
                intent.putExtra("param", "putLoc");
                intent.putExtra("fClass", fClass);
                intent.putExtra("asnArrayList", (Serializable) asnArrayList);
                intent.putExtra("tagList", (Serializable) tagList);
                intent.putExtra("asnGetSet", asnGetSet);
                startActivity(intent);
            }
        });
        // LocalBroadcastManager.getInstance(this).registerReceiver(qrScan, new IntentFilter("broadcastQrResult"));
        if(intent.getStringExtra("putLoc") != null) {
            asnArrayList = (List<AsnGetSet>) intent.getSerializableExtra("asnArrayList");
            asnGetSet = (AsnGetSet) intent.getSerializableExtra("asnGetSet");
            tagList = (List<String>) intent.getSerializableExtra("tagList");
            getLocation(intent.getStringExtra("putLoc"));
        }
        if(intent.getStringExtra("putLocQR") != null) {
            if(intent.getStringExtra("location").length() > 0) {
                location.setText(intent.getStringExtra("location"));
                asnArrayList = (List<AsnGetSet>) intent.getSerializableExtra("asnArrayList");
                asnGetSet = (AsnGetSet) intent.getSerializableExtra("asnGetSet");
                tagList = (List<String>) intent.getSerializableExtra("tagList");
                getLocationSerialNumber(intent.getStringExtra("putLocQR").toUpperCase(), location.getText().toString());
            } else {
                Toast.makeText(getApplicationContext(), "Please insert location first.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /*
    private void postPutawayCancel(final String locParam, final String qrParam) {
        StringRequest strReq = new StringRequest(Request.Method.POST, url+"/Api_putaway/post_cancel/", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);

                try {
                    JSONObject data = new JSONObject(response);
                    String param = data.getString("param");
                    Integer status = data.getInt("status");
                    String message = data.getString("message");

                    if (status == 200) {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
//                        sku.setText(intent.getStringExtra("sku"));
//                        bin.setText(param);

                    } else if (status == 401) {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams(){
                Map<String, String> params = new HashMap<String, String>();

                params.put("location", locParam);
                params.put("serial_number", qrParam);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;


            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }
    */

    private void getRecommendLoc() {
        String postUrl = this.url + "/Api_putaway/get_recommend_location_by_item_code";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Response", response);
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            String results = jsonResponse.getString("results");
                            JSONArray jsonResults = new JSONArray(results);
                            String reccLoc = "";
                            for(int i = 0; i < jsonResults.length(); i++) {
                                JSONObject rs = new JSONObject(jsonResults.get(i).toString());
                                reccLoc = reccLoc + "<b>" + rs.getString("item_code") + " :</b><br/>" + rs.getString("location") + "<br/><br/>";
                            }

                            try {
                                AlertDialog.Builder builder = new AlertDialog.Builder(PutawayLocation.this);
                                builder.setTitle("Rekomendasi Lokasi");
                                builder.setMessage(Html.fromHtml(reccLoc.substring(0, reccLoc.length()-10)));
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                });
                                AlertDialog dialog = builder.create();
                                dialog.show();
                            } catch (WindowManager.BadTokenException bde) {
                                bde.printStackTrace();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                List<String> itemCheck = new ArrayList<>();
                String itemCodeParam = "";

                try {
                    for(int i = 0; i < asnArrayList.size(); i++) {
                        if(itemCheck != null) {
                            if(!itemCheck.contains(asnArrayList.get(i).getQty())) {
                                itemCodeParam = itemCodeParam + "\"" + asnArrayList.get(i).getQty() + "\",";
                                itemCheck.add(asnArrayList.get(i).getQty());
                            }
                        } else {
                            itemCodeParam = itemCodeParam + asnArrayList.get(i).getQty() + "\",";
                            itemCheck.add(asnArrayList.get(i).getQty());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                itemCodeParam = "[" + itemCodeParam.substring(0, itemCodeParam.length()-1) + "]";
                params.put("item_code", itemCodeParam);
                System.out.println("Params getRecommendLocation : " + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    private void getLocation(final String locParam) {
        StringRequest strReq = new StringRequest(Request.Method.GET, url+"/Api_putaway/get_location/" + locParam, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG + "getLocation", response.toString());

                try {
                    JSONObject data = new JSONObject(response);
                    String param = data.getString("param");
                    Integer status = data.getInt("status");
                    String message = data.getString("message");

                    if (status == 200) {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        location.setText(locParam);
                        sku.setFocusableInTouchMode(true);
                        sku.setFocusable(true);
                        sku.requestFocus();
                        sku.setSelection(0);
//                        sku.setText(intent.getStringExtra("sku"));
//                        bin.setText(param);

                    } else if (status == 401) {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        location.setText("");
                        location.setFocusableInTouchMode(true);
                        location.setFocusable(true);
                        location.requestFocus();
                        location.setSelection(0);
                    }

                } catch (JSONException e) {

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;


            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }

    private void getSku(final String locParam) {
        StringRequest strReq = new StringRequest(Request.Method.POST, url+"/Api_putaway/get_sku/", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG + "get_sku", response.toString());

                try {
                    JSONObject data = new JSONObject(response);
                    String param = data.getString("param");
                    Integer status = data.getInt("status");
                    String message = data.getString("message");

                    if (status == 200) {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        sku.setText(locParam);
                        serial_number.setFocusableInTouchMode(true);
                        serial_number.setFocusable(true);
                        serial_number.requestFocus();
                        serial_number.setSelection(0);
//                        sku.setText(intent.getStringExtra("sku"));
//                        bin.setText(param);

                    } else if (status == 401) {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        sku.setText("");
                        sku.setFocusableInTouchMode(true);
                        sku.setFocusable(true);
                        sku.requestFocus();
                        sku.setSelection(0);
                    }

                } catch (JSONException e) {

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams(){
                Map<String, String> params = new HashMap<String, String>();
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String usernameKey = prefs.getString("usernameKey", "No name defined");

                params.put("sku",locParam);
                System.out.println("Params sku : " + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;


            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }

    private void getSkuSerialNumber(final String qrParam, final String skuParam) {
        StringRequest strReq = new StringRequest(Request.Method.POST, url+"/Api_putaway/get_sku_serial/", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG + "get_sku_serial", response.toString());

                try {
                    JSONObject data = new JSONObject(response);
                    String param = data.getString("param");
                    Integer status = data.getInt("status");
                    String message = data.getString("message");

                    if (status == 200) {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        getLocationSerialNumber(serial_number.getText().toString().toUpperCase(), location.getText().toString());
//                        sku.setText(intent.getStringExtra("sku"));
//                        bin.setText(param);

                    } else if (status == 401) {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams(){
                Map<String, String> params = new HashMap<String, String>();
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String usernameKey = prefs.getString("usernameKey", "No name defined");

                params.put("sku",skuParam);
                params.put("qr",qrParam);
                System.out.println("Params SKU SN : " + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;


            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }

    private void getLocationSerialNumber(final String qrParam, final String locParam) {
        StringRequest strReq = new StringRequest(Request.Method.GET, url+"/Api_putaway/get_location_serial/" + locParam +"/"+ qrParam.toUpperCase(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG + "getLocationSerialNumber", response);

                try {
                    JSONObject data = new JSONObject(response);
                    Integer status = data.getInt("status");
                    String message = data.getString("message");

                    if (status == 200) {
                        if(message.contains("should be located in")) {
                            // showMessage("Alert", message);
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        } else {
                            if(qrParam.contains("OL")) {
                                Map<String, String> parameters = new HashMap<String, String>();
                                parameters.put("outer_label", qrParam);
                                new postOuterLabelASync(parameters).execute();
                            } else {
                                try {
                                    if(checkPost == 0) {
                                        for(int i = 0; i < asnArrayList.size(); i++) {
                                            if(asnArrayList.get(i).getUpc().equals(qrParam)) {
                                                if(!asnArrayList.get(i).getSku().contains("OL")) {
                                                    new postPutawayASync(qrParam, qrParam).execute();
                                                    checkPost = 1;
                                                } else {
                                                    Toast.makeText(getApplicationContext(), "Serial Number ini tidak dapat diproses karena memiliki Outer Label, harap scan Outer Label.", Toast.LENGTH_LONG).show();
                                                    serial_number.setText("");
                                                    serial_number.setFocusableInTouchMode(true);
                                                    serial_number.setFocusable(true);
                                                    serial_number.requestFocus();
                                                }
                                            }
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
//                        }

                    } else {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams(){
                Map<String, String> params = new HashMap<String, String>();

                params.put("location", locParam);
                params.put("serial_number", qrParam.toUpperCase());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;


            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }

    private class postPutawayASync extends AsyncTask<Void, Void, Void> {
        String sku = "";
        String qr = "";

        postPutawayASync(String sku, String qr) {
            super();
            this.sku = sku;
            this.qr = qr;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            postPutaway(sku, qr);
            return null;
        }
    }

    private void postPutaway(final String sku, final String qr) {
        if(location.getText().toString().length() > 0) {
            StringRequest strReq = new StringRequest(Request.Method.POST, url+"/Api_putaway/post/", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d(TAG + "postPutaway", response);
                    checkPost = 0;

                    try {
                        JSONObject data = new JSONObject(response);
                        Integer status = data.getInt("status");
                        String message = data.getString("message");

                        if (status == 200) {
                            intent = getIntent();
                            final String sku_data = sku.toUpperCase();

                            SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);

                            String usernameKey = prefs.getString("usernameKey", "No name defined");

                            Log.d(TAG, usernameKey);
                            asnAdapter = new AsnPutawayListAdapter(asnArrayList, getApplicationContext(), 3);
                            listView.setAdapter(asnAdapter);

                            Integer position = 0;
                            // asnGetSet = new AsnGetSet(qr.toUpperCase(), asnScanRFID.hexToString(qr).substring(0, 12).toUpperCase(), "2");
                            for(int i = 0; i < asnArrayList.size(); i++) {
                                if(asnArrayList.get(i).getUpc().equals(qr)) {
                                    position = i;
                                    asnGetSet = new AsnGetSet(qr.toUpperCase(), qr.toUpperCase(), "2", asnArrayList.get(i).getQty(), asnArrayList.get(i).getLast_loc(), asnArrayList.get(i).getSkip());
                                }
                            }

                            asnArrayList.set(position, asnGetSet);
                            asnAdapter.notifyDataSetChanged();
                            serial_number.setText("");
                            serial_number.setFocusableInTouchMode(true);
                            serial_number.setFocusable(true);
                            serial_number.requestFocus();

                            int scannedQ = 0;
                            for(int i = 0; i < asnArrayList.size(); i++) {
                                if(asnArrayList.get(i).getPcs().equals("2")) {
                                    scannedQ = scannedQ + 1;
                                }
                            }
                            totalItems.setText(scannedQ + "/" + asnArrayList.size());

                            int bCheck = 1;
                            for(int i = 0; i < asnArrayList.size(); i++) {
                                if(asnArrayList.get(i).getPcs().equals("1")) {
                                    bCheck = 0;
                                }
                            }
                            if(bCheck == 1) {
                                next.setText("DONE");
                            }
                            //} else if (status == 401) {
                        } else {
                            location.requestFocus();
                            location.setText("");
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                }
            }) {

                @Override
                protected Map<String, String> getParams(){
                    Map<String, String> params = new HashMap<String, String>();
                    SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                    String usernameKey = prefs.getString("usernameKey", "No name defined");

                    Calendar calendar = Calendar.getInstance();
                    SimpleDateFormat mdformat = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");

                    params.put("item_code",sku.toUpperCase());
                    params.put("serial_number",qr.toUpperCase());
                    params.put("location",String.valueOf(location.getText()));
                    params.put("time",mdformat.format(calendar.getTime()));
                    params.put("user",usernameKey);
                    params.put("pick_time", mdformat.format(calendar.getTime()));
                    params.put("user_pick",usernameKey);
                    System.out.println("Params post putaway : " + params);

                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                    String restoredText = prefs.getString("handheldsessioncodeKey", null);

                    String usernameKey = prefs.getString("usernameKey", "No name defined");
                    String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                    Log.d(TAG, usernameKey);
                    Log.d(TAG, handheldsessioncodeKey);

                    Map<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "application/x-www-form-urlencoded");
                    headers.put("User",usernameKey);
                    headers.put("Authorization", handheldsessioncodeKey);
                    return headers;
                }
            };

            int socketTimeout = 3000000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            strReq.setRetryPolicy(policy);

            controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
        } else {
            Toast.makeText(getApplicationContext(), "Please insert location first.", Toast.LENGTH_SHORT);
        }
    }

    private class postOuterLabelASync extends AsyncTask<Void, Void, Void> {
        Map<String, String> param = new HashMap<String, String>();

        postOuterLabelASync(final Map<String, String> param) {
            super();
            this.param = param;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            postOuterLabel(param);
            return null;
        }
    }

    public void postOuterLabel(final Map<String, String> parameters) {
        String postUrl = this.url + "/Api_putaway/getOuterLabelItem";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d(TAG + "postOuterLabel", response);

                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            String message = jsonResponse.getString("message");
                            JSONArray jsonMessage = new JSONArray(message);

//                            asnArrayList.clear();
                            for(int i = 0; i < jsonMessage.length(); i++) {
                                String unique_code = jsonMessage.getString(i);
                                JSONObject jsonHex = new JSONObject(unique_code);
                                String hex = jsonHex.getString("unique_code");
                                if(tagList.contains(hex)) {
                                    try {
                                        new postPutawayASync(hex, hex).execute();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    Toast.makeText(getApplicationContext(), "This item is not exist in the list.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException jsonEx) {
                            jsonEx.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    private class checkSerialNumberASync extends AsyncTask<Void, Void, Void> {
        String qr = "";

        checkSerialNumberASync(String qr) {
            super();
            this.qr = qr;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            checkSerialNumber(qr);
            return null;
        }
    }

    private void checkSerialNumber(final String qr) {
        StringRequest strReq = new StringRequest(Request.Method.POST,url+"/Api_putaway/get/", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG + "checkSerialNumber", response);

                try {
                    JSONObject data = new JSONObject(response);

                    Integer status = data.getInt("status");
                    String message = data.getString("message");
                    JSONArray resultsArr = new JSONArray(data.getString("results"));
                    JSONObject results = new JSONObject(resultsArr.getString(0));
                    String item_code = results.getString("kd_barang");

                    if (status.equals(200)) {
                        if(message.equals("Serial Number Ready for Putaway.")) {
                            Integer position = 0;
                            for(int i = 0; i < asnArrayList.size(); i++) {
                                if(asnArrayList.get(i).getUpc() == qr) {
                                    position = i;
                                    asnGetSet = new AsnGetSet(qr.toUpperCase(), qr.toUpperCase(), "2", asnArrayList.get(i).getQty(), asnArrayList.get(i).getLast_loc(), asnArrayList.get(i).getSkip());
                                }
                            }
                            asnArrayList.set(position, asnGetSet);
                            asnAdapter.notifyDataSetChanged();
                        } else if(message.contains("has been stored") || message.contains("has been putaway ")) {
                            Integer position = 0;
                            for(int i = 0; i < asnArrayList.size(); i++) {
                                if(asnArrayList.get(i).getUpc() == qr) {
                                    position = i;
                                    asnGetSet = new AsnGetSet(qr.toUpperCase(), qr.toUpperCase(), "2", asnArrayList.get(i).getQty(), asnArrayList.get(i).getLast_loc(), asnArrayList.get(i).getSkip());
                                }
                            }
                            asnArrayList.set(position, asnGetSet);
                            asnAdapter.notifyDataSetChanged();
                        }
                    }else{
//                        showMessage("Alert",message);
                        Integer position = 0;
                        for(int i = 0; i < asnArrayList.size(); i++) {
                            if(asnArrayList.get(i).getUpc() == qr) {
                                position = i;
                                asnGetSet = new AsnGetSet(qr.toUpperCase(), qr.toUpperCase(), "2", asnArrayList.get(i).getQty(), asnArrayList.get(i).getLast_loc(), asnArrayList.get(i).getSkip());
                            }
                        }
                        asnArrayList.set(position, asnGetSet);
                        asnAdapter.notifyDataSetChanged();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    String jsonError = new String(networkResponse.data);

                    try {
                        JSONObject data = new JSONObject(jsonError);

                        String message = data.getString("message");

                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        })  {

            @Override
            protected Map<String, String> getParams(){
                Map<String, String> params = new HashMap<String, String>();
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String usernameKey = prefs.getString("usernameKey", "No name defined");

                params.put("serial_number",String.valueOf(qr).toUpperCase());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }
}
