package largo.largo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class ReceivingItemDetailListAdapter extends ArrayAdapter<ReceivingItemDetailGetterSetter> {

    private List<ReceivingItemDetailGetterSetter> skuDetailItemList;

    private Context context;

    public ReceivingItemDetailListAdapter(List<ReceivingItemDetailGetterSetter> skuDetailItemList, Context context) {
        super(context, R.layout.list_view_receiving_item_detail, skuDetailItemList);
        this.skuDetailItemList = skuDetailItemList;
        this.context = context;
    }



    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);

        View listViewItem = inflater.inflate(R.layout.list_view_receiving_item_detail, null, true);

        TextView textViewSerialNumber = listViewItem.findViewById(R.id.textViewSerialNumber);
        TextView textViewQcStatus = listViewItem.findViewById(R.id.textViewQcStatus);

        ReceivingItemDetailGetterSetter skuItem = skuDetailItemList.get(position);

        textViewSerialNumber.setText(skuItem.getSerialNumber());
        textViewQcStatus.setText(skuItem.getStatus());

        return listViewItem;
    }
}
