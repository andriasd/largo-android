package largo.largo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AsnPackingTray extends AppCompatActivity {
    String TAG = this.getClass().getSimpleName();

    private Button backButton, nextButton;
    private String pickingDocument, outboundCode;
    private EditText packingDocument;
    private Intent intent;
    private ListView listView;
    private List<PackingDocumentGetterSetter> dokumenItemList;
    private PackingDocumentGetterSetter packDocGetSet;
    private List<String> docList;

    DatabaseHelper myDb;
    String url;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.outbound_packing_tray);

        intent = getIntent();

        myDb = new DatabaseHelper(this);
        getServerURL();

        backButton = findViewById(R.id.back);
        nextButton = findViewById(R.id.next);
        listView = findViewById(R.id.ListPackingDoc);
        packingDocument = findViewById(R.id.picking_document);

        docList = new ArrayList<>();
        if(intent.getSerializableExtra("picking_code") != null) {
            pickingDocument = intent.getStringExtra("picking_code");
            outboundCode = intent.getStringExtra("outbound_code");
        }
        dokumenItemList = new ArrayList<>();
        DataListDokument(url + "/Api_picking/get_tray_by_outbound/" + outboundCode);
        System.out.println(url + "/Api_picking/get_tray_by_outbound/" + outboundCode);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if(packingDocument.getText().toString().length() > 0) {
                    Intent intent = new Intent(getApplicationContext(), AsnPackingScanQR.class);
                    intent.putExtra("picking_code", pickingDocument);
                    intent.putExtra("outbound_code", outboundCode);
                    intent.putExtra("tray_location", packingDocument.getText().toString());
                    startActivity(intent);
                /*} else {
                    Toast.makeText(getApplicationContext(), "Please choose document first.", Toast.LENGTH_SHORT).show();
                }*/
            }
        });

        packingDocument.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 0) {
                    //if(packingDocument.getText().toString().length() > 0) {
                        Intent intent = new Intent(getApplicationContext(), AsnPackingScanQR.class);
                        intent.putExtra("picking_code", pickingDocument);
                        intent.putExtra("outbound_code", outboundCode);
                        intent.putExtra("tray_location", packingDocument.getText().toString());
                        startActivity(intent);
                    /*} else {
                        Toast.makeText(getApplicationContext(), "Please choose document first.", Toast.LENGTH_SHORT).show();
                    }*/
                }
                return false;
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                if(!dokumenItemList.get(position).getPl_name().equals("--no tray--")) {
                    packingDocument.setText(dokumenItemList.get(position).getPl_name());
                }
            }
        });
    }

    private void DataListDokument(String getDoc){
        RequestQueue requestQueue=Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, getDoc, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);
                try{
                    JSONObject jsonObject=new JSONObject(response);
                    if(jsonObject.getInt("status")==200){
                        JSONArray jsonArray1=jsonObject.getJSONArray("results");
                        if(jsonArray1.length() > 1) {
                            for(int i=0;i<jsonArray1.length();i++){
                                JSONObject jsonObject1=jsonArray1.getJSONObject(i);
                                packDocGetSet = new PackingDocumentGetterSetter(
                                        jsonObject1.getString("tray_name"),
                                        "","","","",
                                        2
                                );
                                dokumenItemList.add(packDocGetSet);
                                docList.add(jsonObject1.getString("tray_name"));
                            }
                        } else {
                            packDocGetSet = new PackingDocumentGetterSetter(
                                    "--no tray--",
                                    "","","","",
                                    2
                            );
                            dokumenItemList.add(packDocGetSet);
                            docList.add("--no tray--");
                        }

                        PackingDocumentListAdapter adapter = new PackingDocumentListAdapter(dokumenItemList, getApplicationContext());

                        listView.setAdapter(adapter);

                    }
//                    spinner.setAdapter(new ArrayAdapter<String>(TallyReceivingItemSerial.this, android.R.layout.simple_spinner_dropdown_item, QC));
                }catch (JSONException e){e.printStackTrace();}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;


            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }
}
