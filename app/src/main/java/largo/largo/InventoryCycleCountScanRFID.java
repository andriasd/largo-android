package largo.largo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.zebra.rfid.api3.Antennas;
import com.zebra.rfid.api3.ENUM_TRANSPORT;
import com.zebra.rfid.api3.ENUM_TRIGGER_MODE;
import com.zebra.rfid.api3.HANDHELD_TRIGGER_EVENT_TYPE;
import com.zebra.rfid.api3.InvalidUsageException;
import com.zebra.rfid.api3.MEMORY_BANK;
import com.zebra.rfid.api3.OperationFailureException;
import com.zebra.rfid.api3.RFIDReader;
import com.zebra.rfid.api3.ReaderDevice;
import com.zebra.rfid.api3.Readers;
import com.zebra.rfid.api3.RfidEventsListener;
import com.zebra.rfid.api3.RfidReadEvents;
import com.zebra.rfid.api3.RfidStatusEvents;
import com.zebra.rfid.api3.START_TRIGGER_TYPE;
import com.zebra.rfid.api3.STATUS_EVENT_TYPE;
import com.zebra.rfid.api3.STOP_TRIGGER_TYPE;
import com.zebra.rfid.api3.TAG_FIELD;
import com.zebra.rfid.api3.TagAccess;
import com.zebra.rfid.api3.TagData;
import com.zebra.rfid.api3.TagStorageSettings;
import com.zebra.rfid.api3.TriggerInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class InventoryCycleCountScanRFID extends AppCompatActivity {

    Button cancelButton, doneButton, scanButton, scan32Button;
    String scan32;
    private static AsyncTask<Void, Void, Boolean> connectReader;
    private static Readers readers;
    private static ArrayList availableRFIDReaderList;
    private static ReaderDevice readerDevice;
    public static RFIDReader reader;
    private String param, TAG, cc_document, warehouse_name, cc_type, type;
    private String fClass = this.getClass().getSimpleName();
    private List<String> tagList;
    private ListView rfidListView;
    private List<AsnGetSet> asnArrayList;
    private AsnGetSet asnGetSet;
    private AsnListAdapter adapter;
    private EventHandler eventHandler;
    private Intent intent;
    private TextView totalItems, textView40;

    private InventoryCycleCountScanQR inventoryCycleCountScanQR = new InventoryCycleCountScanQR();

    String type1 = "";
    String kd_qc = "";
    String tgl_exp = "";
    String tgl_in = "";
    String putaway_time = "";

    private TagAccess tagAccess;
    private TagAccess.ReadAccessParams readAccessParams;

    private AsnScanRFID asnScanRFID;

    private JSONObject paramsJson;
    public static final Map<String, String> params = new HashMap<String, String>();
    public static final String MyPREFERENCES = "MyPrefs" ;

    DatabaseHelper myDb;
    String url;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cycle_count_scan_rfid);

        asnScanRFID = new AsnScanRFID();
        paramsJson = new JSONObject();

        TAG = this.getClass().getSimpleName();
        myDb = new DatabaseHelper(this);
        getServerURL();

        intent = getIntent();
        param = this.getClass().getSimpleName();

        rfidListView = findViewById(R.id.rfid_list);
        totalItems = findViewById(R.id.qty);
        totalItems.setText("0");
        textView40 = findViewById(R.id.textView40);

        tagList = new ArrayList<>();
        if(intent.getSerializableExtra("tagList") != null) {
            tagList = (List<String>) intent.getSerializableExtra("tagList");
            System.out.println("tagListAfter : " + tagList);
        }
        asnArrayList = new ArrayList<>();
        if(intent.getSerializableExtra("asnArrayList") != null) {
            asnArrayList = (List<AsnGetSet>) intent.getSerializableExtra("asnArrayList");
            totalItems.setText(Integer.toString(asnArrayList.size()));
            try {
                if(reader != null) {
                    reader.Config.setTriggerMode(ENUM_TRIGGER_MODE.RFID_MODE, true);
                }
            } catch (InvalidUsageException e) {
                e.printStackTrace();
            } catch (OperationFailureException e) {
                e.printStackTrace();
            }
        }

        tagAccess = new TagAccess();
        readAccessParams = tagAccess.new ReadAccessParams();
        readAccessParams.setAccessPassword(0);
        readAccessParams.setCount(8);
        readAccessParams.setMemoryBank(MEMORY_BANK.MEMORY_BANK_EPC);
        readAccessParams.setOffset(2);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter = new AsnListAdapter(asnArrayList, getApplicationContext(), 2);
                rfidListView.setAdapter(adapter);

                /* TEST */
                /*int runningNumber = 10000000;
                for(int i = 0;i < 100;i++) {
                    String testTag = "61JA00M" + Integer.toString(runningNumber);
                    tagList.add(testTag);
                    asnGetSet = new AsnGetSet(inventoryCycleCountScanQR.toHex(testTag), testTag.substring(0, 20), testTag.substring(0, 12), "1", "1");
                    asnArrayList.add(asnGetSet);
                    adapter.notifyDataSetChanged();
                    totalItems.setText(Integer.toString(asnArrayList.size()));
                    runningNumber = runningNumber + 1;
                }*/
            }
        });

        cc_document = intent.getStringExtra("cc_document");
        warehouse_name = intent.getStringExtra("warehouse_name");
        cc_type = intent.getStringExtra("cc_type");
        type = intent.getStringExtra("type");

        cancelButton = findViewById(R.id.cancel);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        scanButton = findViewById(R.id.scan);
        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(scanButton.getText().equals("SCAN")) {
                    scan32 = "1";
                    triggerStart();
                } else if(scanButton.getText().equals("STOP")) {
                    triggerStop();
                } else {
                    triggerStop();
                    triggerStart();
                }
            }
        });

        scan32Button = findViewById(R.id.scan32);
        scan32Button.setVisibility(View.GONE);
        scan32Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(scan32Button.getText().equals("SCAN32")) {
                    scan32 = "1";
                    triggerStart();
                } else if(scan32Button.getText().equals("STOP32")) {
                    triggerStop();
                } else {

                }
            }
        });

        doneButton = findViewById(R.id.done);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doneButton.setText("Posting...");
                doneButton.setEnabled(false);

                try {
                    reader.Config.setTriggerMode(ENUM_TRIGGER_MODE.BARCODE_MODE, true);
                } catch (InvalidUsageException e) {
                    e.printStackTrace();
                } catch (OperationFailureException e) {
                    e.printStackTrace();
                }
                List<String> skuExistList = new ArrayList<>();

                try {
                    if (asnArrayList.size() > 0) {
                        int arraySize;
                        SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                        String usernameKey = prefs.getString("usernameKey", "No name defined");
                        Long tsLong = System.currentTimeMillis() / 1000;
                        String jsonData = "";
                        String jsonParam = "";
                        String snArray = "";

                        try {
                            arraySize = asnArrayList.size();
                        } catch (Exception e) {
                            arraySize = 0;
                        }

                        if (arraySize > 0) {
                            jsonData = "[";
                            jsonParam = "";
                            snArray = "";
                            jsonParam = jsonParam + "{\"cc_code\":\"" + cc_document + "\",\"location\":\"" + type + "\",\"outer_label\":\"\",\"serial_number\":";
                            paramsJson.put("serial_number", "Serial Number");
                            paramsJson.put("sku", "SKU");
                            paramsJson.put("warehouse_name", "Warehouse");

                            jsonData = jsonData + paramsJson.toString() + ",";
                            for (int i = 0; i < arraySize; i++) {
                                if (!asnArrayList.get(i).getPcs().equals("0")) {
                                    paramsJson.put("serial_number", asnArrayList.get(i).getUpc().toUpperCase());
                                    paramsJson.put("sku", asnArrayList.get(i).getSku());
                                    paramsJson.put("warehouse_name", warehouse_name);

                                    jsonData = jsonData + paramsJson.toString() + ",";

                                    snArray = snArray + "\"" + asnArrayList.get(i).getUpc().toUpperCase() + "\",";

                                    // new getSerialASync(cc_document, cc_type, type, asnArrayList.get(i).getUpc().toUpperCase(), "").execute();

                                }
                            }

                            jsonData = jsonData.substring(0, jsonData.length() - 1);
                            jsonData = jsonData + "]";

                            snArray = "[" + snArray.substring(0, snArray.length() - 1) + "]";
                            jsonParam = jsonParam + snArray + "}";
                            System.out.println("jsonParam : " + jsonParam);
                            // postBulk(jsonParam);
                            new postBulkASync(jsonParam).execute();

                            Log.d("JSON Param", jsonData);
                            Log.d("STRLEN", Integer.toString(jsonData.length()));
                            params.clear();
                            params.put("json", jsonData);
                            params.put("cc_document", cc_document);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    postCC(params);
                                }
                            });
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        // SDK
        if (readers == null) {
            readers = new Readers(this, ENUM_TRANSPORT.SERVICE_SERIAL);
        }

        connectReader = new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... voids) {
                try {
                    if (readers != null) {
                        if (readers.GetAvailableRFIDReaderList() != null) {
                            availableRFIDReaderList = readers.GetAvailableRFIDReaderList();
                            if (availableRFIDReaderList.size() != 0) {
                                // get first reader from list
                                readerDevice = (ReaderDevice) availableRFIDReaderList.get(0);
                                reader = readerDevice.getRFIDReader();
                                if (!reader.isConnected()) {
                                    // Establish connection to the RFID Reader
                                    reader.connect();
                                    ConfigureReader();
                                    return true;
                                }
                            }
                        }
                    }
                } catch (InvalidUsageException e) {
                    e.printStackTrace();
                } catch (OperationFailureException e) {
                    e.printStackTrace();
                    Log.d(TAG, "OperationFailureException " + e.getVendorMessage());
                }
                return false;
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                if (aBoolean) {
                    Toast.makeText(getApplicationContext(), "Reader Connected", Toast.LENGTH_SHORT).show();
                    scanButton.setText("SCAN");
                    scanButton.setBackgroundColor(Color.parseColor("#32C5D2"));
                    scan32Button.setText("SCAN32");
                    scan32Button.setBackgroundColor(Color.parseColor("#32C5D2"));
                }
            }
        };

        Toast.makeText(InventoryCycleCountScanRFID.this, "Connecting to Reader...", Toast.LENGTH_SHORT).show();
        connectReader.execute();
    }

    private void triggerStop() {
        try {
            reader.Actions.Inventory.stop();
        } catch (InvalidUsageException e) {
            e.printStackTrace();
        } catch (OperationFailureException e) {
            e.printStackTrace();
        }
        scanButton.setText("SCAN");
        scanButton.setBackgroundColor(Color.parseColor("#32C5D2"));
        scan32Button.setText("SCAN32");
        scan32Button.setBackgroundColor(Color.parseColor("#32C5D2"));
    }

    private void triggerStart() {
        try {
            reader.Actions.Inventory.perform();
        } catch (InvalidUsageException e) {
            e.printStackTrace();
        } catch (OperationFailureException e) {
            e.printStackTrace();
        }
        scanButton.setText("STOP");
        scanButton.setBackgroundColor(Color.parseColor("#FFBEBE"));
        scan32Button.setText("STOP32");
        scan32Button.setBackgroundColor(Color.parseColor("#FFBEBE"));
    }

    private void ConfigureReader() {
        if (reader.isConnected()) {
            TriggerInfo triggerInfo = new TriggerInfo();
            triggerInfo.StartTrigger.setTriggerType(START_TRIGGER_TYPE.START_TRIGGER_TYPE_IMMEDIATE);
            triggerInfo.StopTrigger.setTriggerType(STOP_TRIGGER_TYPE.STOP_TRIGGER_TYPE_IMMEDIATE);

            TagStorageSettings tagStorageSettings = new TagStorageSettings();
            tagStorageSettings.setTagFields(TAG_FIELD.ALL_TAG_FIELDS);

            try {
                Antennas.AntennaRfConfig antennaRfConfig = reader.Config.Antennas.getAntennaRfConfig(1);
                antennaRfConfig.setrfModeTableIndex(0);
                antennaRfConfig.setTari(0);
                antennaRfConfig.setTransmitPowerIndex(300);
                // set the configuration
                reader.Config.Antennas.setAntennaRfConfig(1, antennaRfConfig);

                // receive events from reader
                if (eventHandler == null)
                    eventHandler = new EventHandler();
                reader.Events.addEventsListener(eventHandler);
                // HH event
                reader.Events.setHandheldEvent(true);
                // tag event with tag data
                reader.Events.setTagReadEvent(true);
                // application will collect tag using getReadTags API
                reader.Events.setAttachTagDataWithReadEvent(false);
                // set trigger mode as rfid so scanner beam will not come
                reader.Config.setTriggerMode(ENUM_TRIGGER_MODE.RFID_MODE, true);
                // set start and stop triggers
                reader.Config.setStartTrigger(triggerInfo.StartTrigger);
                reader.Config.setStopTrigger(triggerInfo.StopTrigger);
                reader.Config.setUniqueTagReport(true);
                reader.Config.setTagStorageSettings(tagStorageSettings);

            } catch (InvalidUsageException e) {
                e.printStackTrace();
            } catch (OperationFailureException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (reader != null) {
                triggerStop();
                reader.Config.setTriggerMode(ENUM_TRIGGER_MODE.BARCODE_MODE, true);
                reader.Events.removeEventsListener(eventHandler);
                reader.disconnect();
                Toast.makeText(getApplicationContext(), "Disconnecting reader", Toast.LENGTH_SHORT).show();
                reader = null;
                readers.Dispose();
                readers = null;
                scanButton.setText("Connect to Reader");
                scanButton.setBackgroundColor(Color.parseColor("#FFBEBE"));
                scan32Button.setText("Connect to Reader");
                scan32Button.setBackgroundColor(Color.parseColor("#FFBEBE"));
            }
        } catch (InvalidUsageException e) {
            e.printStackTrace();
        } catch (OperationFailureException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Read/Status Notify handler
    // Implement the RfidEventsLister class to receive event notifications
    public class EventHandler implements RfidEventsListener {
        // Read Event Notification
        public void eventReadNotify(RfidReadEvents e) {
            try {
                // Recommended to use new method getReadTagsEx for better performance in case of large tag population
                final TagData[] myTags = reader.Actions.getReadTags(500);
                String rfidTagHex = "";
                String rfidTagText = "";
//            if(adapter != null) {
//                adapter.notifyDataSetChanged();
//            }
                if (myTags != null) {
                    for (int index = 0; index < myTags.length; index++) {
                        rfidTagHex = myTags[index].getTagID().toUpperCase();
                        if(rfidTagHex.equals("111122223333444455559999")) {
                            rfidTagHex = "36314a4130304d323230383139 3031".toUpperCase();
                        } else if(rfidTagHex.equals("36314A4130304D3000000009")) {
                            rfidTagHex = "36314a4130304d3232303831393032".toUpperCase();
                        } else if(rfidTagHex.equals("0000000000000029")) {
                            rfidTagHex = "36314a4130304d3232303831393033".toUpperCase();
                        } else if(rfidTagHex.equals("00000000010101122825000000000028")) {
                            rfidTagHex = "36314a4130304d3232303831393034".toUpperCase();
                        } else if(rfidTagHex.equals("0000000000000038")) {
                            rfidTagHex = "36314a4130304d3232303831393035".toUpperCase();
                        } else if(rfidTagHex.equals("0000000000000037")) {
                            rfidTagHex = "36314a4130304d3232303831393036".toUpperCase();
                        } else if(rfidTagHex.equals("0000000000000048")) {
                            rfidTagHex = "36314a4130304d3232303831393037".toUpperCase();
                        }
                        rfidTagText = hexToString(rfidTagHex).toUpperCase();
                        System.out.println(rfidTagHex);

                        try {
                            if(tagList != null) {
                                if(!tagList.contains(rfidTagHex)) {
                                    final String finalRfidTagHex = rfidTagHex;
                                    final String finalRfidTagText = rfidTagText;
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                tagList.add(finalRfidTagHex);
                                                asnGetSet = new AsnGetSet(finalRfidTagHex, finalRfidTagText.substring(0, 20), finalRfidTagText.substring(0, 12), "1", "1");
                                                asnArrayList.add(asnGetSet);
                                                adapter.notifyDataSetChanged();
                                                totalItems.setText(Integer.toString(asnArrayList.size()));
                                            } catch(IndexOutOfBoundsException ex) {
                                                ex.printStackTrace();
                                            }
                                        }
                                    });
                                }
                            } else {
                                final String finalRfidTagHex1 = rfidTagHex;
                                final String finalRfidTagText1 = rfidTagText;
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            tagList.add(finalRfidTagHex1);
                                            asnGetSet = new AsnGetSet(finalRfidTagHex1, finalRfidTagText1.substring(0, 20), finalRfidTagText1.substring(0, 12), "1", "1");
                                            asnArrayList.add(asnGetSet);
                                            adapter.notifyDataSetChanged();
                                            totalItems.setText(Integer.toString(asnArrayList.size()));
                                        } catch(IndexOutOfBoundsException ex) {
                                            ex.printStackTrace();
                                        }
                                    }
                                });
                            }
                        } catch (StringIndexOutOfBoundsException ex) {
                            Toast.makeText(getApplicationContext(), "Format barcode tidak sesuai.", Toast.LENGTH_SHORT).show();
                        } catch (IndexOutOfBoundsException ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            } catch(IllegalStateException ex) {
                ex.printStackTrace();
            }
        }

        // Status Event Notification
        public void eventStatusNotify(RfidStatusEvents rfidStatusEvents) {
            Log.d(TAG, "Status Notification: " + rfidStatusEvents.StatusEventData.getStatusEventType());
            if (rfidStatusEvents.StatusEventData.getStatusEventType() == STATUS_EVENT_TYPE.HANDHELD_TRIGGER_EVENT) {
                if (rfidStatusEvents.StatusEventData.HandheldTriggerEventData.getHandheldEvent() == HANDHELD_TRIGGER_EVENT_TYPE.HANDHELD_TRIGGER_PRESSED) {
                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... voids) {
                            try {
                                reader.Actions.Inventory.perform();
                            } catch (InvalidUsageException e) {
                                e.printStackTrace();
                            } catch (OperationFailureException e) {
                                e.printStackTrace();
                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            scanButton.setText("STOP");
                            scanButton.setBackgroundColor(Color.parseColor("#FFBEBE"));
                            scan32Button.setText("STOP32");
                            scan32Button.setBackgroundColor(Color.parseColor("#FFBEBE"));
                        }
                    }.execute();
                }
                if (rfidStatusEvents.StatusEventData.HandheldTriggerEventData.getHandheldEvent() == HANDHELD_TRIGGER_EVENT_TYPE.HANDHELD_TRIGGER_RELEASED) {
                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... voids) {
                            try {
                                reader.Actions.Inventory.stop();
                            } catch (InvalidUsageException e) {
                                e.printStackTrace();
                            } catch (OperationFailureException e) {
                                e.printStackTrace();
                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            scanButton.setText("SCAN");
                            scanButton.setBackgroundColor(Color.parseColor("#32C5D2"));
                            scan32Button.setText("SCAN32");
                            scan32Button.setBackgroundColor(Color.parseColor("#32C5D2"));
                        }
                    }.execute();
                }
            }
        }
    }

    public void getSerial(final String cycle_code, final String type, final String item_code, final String serial_number, final String outer_label) {
        String postUrl = this.url + "/Api_cycle_count/get_serial/";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                        final String usernameKey = prefs.getString("usernameKey", "No name defined");

                        Calendar calendar = Calendar.getInstance();
                        SimpleDateFormat mdformat = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
                        final String time = mdformat.format(calendar.getTime());
                        // response
                        Log.d("Cycle Count getSerial : ", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if (jsonObject.getInt("status") == 200) {
                                JSONArray jsonArray1 = jsonObject.getJSONArray("results");

                                JSONObject jsonObject1 = jsonArray1.getJSONObject(0);
                                if(!jsonObject1.getString("type1").equals("null")){
                                    type1 = jsonObject1.getString("type1");
                                }
                                if(!jsonObject1.getString("kd_qc").equals("null")){
                                    kd_qc = jsonObject1.getString("kd_qc");
                                }
                                if(!jsonObject1.getString("tgl_exp").equals("null")){
                                    tgl_exp = jsonObject1.getString("tgl_exp");
                                }
                                if(!jsonObject1.getString("tgl_in").equals("null")){
                                    tgl_in = jsonObject1.getString("tgl_in");
                                }
                                if(!jsonObject1.getString("putaway_time").equals("null")){
                                    putaway_time = jsonObject1.getString("putaway_time");
                                }

                                postCycleCount(cycle_code, item_code, type1, serial_number, "1", time, usernameKey, kd_qc, tgl_in, putaway_time, tgl_exp, outer_label);
                            } else {
//                                try {
//                                    AlertDialog.Builder builder = new AlertDialog.Builder(InventoryCycleCountScanQR.this);
//                                    builder.setTitle("Alert");
//                                    builder.setMessage("Serial number ini ("+asnScanRFID.hexToString(serial_number).substring(0, 20)+") memiliki perbedaan data dengan sistem, lanjutkan ?");
//                                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int which) {
                                getSerialPositive(cycle_code, type, item_code, serial_number, outer_label);
//                                        }
//                                    });
//                                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int which) {
//                                            asnQr.requestFocus();
//                                            asnQr.setFocusableInTouchMode(true);
//                                            asnQr.setFocusable(true);
//                                            asnQr.setText("");
//                                            asnQty.setText("1");
//                                            totalItems.setText(Integer.toString(asnArrayList.size()));
//                                        }
//                                    });
//                                    AlertDialog dialog = builder.create();
//                                    dialog.show();
//                                } catch (WindowManager.BadTokenException bde) {
//                                    bde.printStackTrace();
//                                } catch (IllegalStateException ise) {
//                                    ise.printStackTrace();
//                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("cycle_code",cycle_code);
                params.put("type",type);
                params.put("item_code",item_code);
                params.put("serial_number",serial_number.toUpperCase());
                System.out.println("getSerial Params : " + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    public void getSerialPositive(final String cycle_code, final String type, final String item_code, final String serial_number, final String outer_label) {
        String postUrl = this.url + "/Api_cycle_count/get_positive_serial/";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                        final String usernameKey = prefs.getString("usernameKey", "No name defined");

                        Calendar calendar = Calendar.getInstance();
                        SimpleDateFormat mdformat = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
                        final String time = mdformat.format(calendar.getTime());
                        // response
                        Log.d("Cycle Count getPositiveSerial : ", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if (jsonObject.getInt("status") == 200) {
                                JSONArray jsonArray1 = jsonObject.getJSONArray("results");

                                JSONObject jsonObject1 = jsonArray1.getJSONObject(0);
                                if(!jsonObject1.getString("type1").equals("null")){
                                    type1 = jsonObject1.getString("type1");
                                }
                                if(!jsonObject1.getString("kd_qc").equals("null")){
                                    kd_qc = jsonObject1.getString("kd_qc");
                                }
                                if(!jsonObject1.getString("tgl_exp").equals("null")){
                                    tgl_exp = jsonObject1.getString("tgl_exp");
                                }
                                if(!jsonObject1.getString("tgl_in").equals("null")){
                                    tgl_in = jsonObject1.getString("tgl_in");
                                }
                                if(!jsonObject1.getString("putaway_time").equals("null")){
                                    putaway_time = jsonObject1.getString("putaway_time");
                                }

                                postCycleCount(cycle_code, item_code, type1, serial_number, "1", time, usernameKey, kd_qc, tgl_in, putaway_time, tgl_exp, outer_label);
                            } else {
//                                try {
//                                    AlertDialog.Builder builder = new AlertDialog.Builder(InventoryCycleCountScanQR.this);
//                                    builder.setTitle("Alert");
//                                    builder.setMessage("Serial number ini belum pernah masuk ke dalam sistem, apakah ingin dimasukkan ?");
//                                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int which) {
                                postCycleCount(cycle_code, item_code, asnScanRFID.hexToString(serial_number).substring(0, 12), serial_number, "1", time, usernameKey, "CC HOLD", time, time, tgl_exp, outer_label);
//                                        }
//                                    });
//                                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int which) {
//                                            asnQr.requestFocus();
//                                            asnQr.setFocusableInTouchMode(true);
//                                            asnQr.setFocusable(true);
//                                            asnQr.setText("");
//                                            asnQty.setText("1");
//                                            totalItems.setText(Integer.toString(asnArrayList.size()));
//                                        }
//                                    });
//                                    AlertDialog dialog = builder.create();
//                                    dialog.show();
//                                } catch (WindowManager.BadTokenException bde) {
//                                    bde.printStackTrace();
//                                } catch (IllegalStateException ise) {
//                                    ise.printStackTrace();
//                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("cycle_code",cycle_code);
                params.put("type",type);
                params.put("item_code",item_code);
                params.put("serial_number",serial_number.toUpperCase());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    private class getSerialASync extends AsyncTask<Void, Void, Void> {
        String cycle_code = "";
        String type = "";
        String item_code = "";
        String serial_number = "";
        String outer_label = "";

        getSerialASync(final String cycle_code, final String type, final String item_code, final String serial_number, final String outer_label) {
            super();
            this.cycle_code = cycle_code;
            this.type = type;
            this.item_code = item_code;
            this.serial_number = serial_number;
            this.outer_label = outer_label;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            doneButton.setText("Posting...");
            doneButton.setEnabled(false);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            finish();
            Intent intent2 = new Intent(getApplicationContext(), Inventory.class);
            intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent2);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            getSerial(cycle_code, type, item_code, serial_number, outer_label);
            return null;
        }
    }

    public void postCycleCount(final String cc_code, final String type, final String type1, final String serial_number, final String qty, final String time, final String uname, final String kd_qc, final String tgl_in, final String putaway_time, final String tgl_exp, final String outer_label) {
        String postUrl = this.url + "/Api_cycle_count/post/";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Cycle Count getSerial : ", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getInt("status") == 200) {
                                Toast.makeText(getApplicationContext(),  jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("cc_code", cc_code);
                params.put("type", type);
                params.put("type1", type1);
                params.put("serial_number", serial_number.toUpperCase());
                params.put("qty", qty);
                params.put("time", time);
                params.put("kd_qc", kd_qc);
                params.put("uname", uname);
                params.put("tgl_in", tgl_in);
                params.put("putaway_time", putaway_time);
                params.put("tgl_exp", tgl_exp);
                params.put("outer_label", outer_label);
                System.out.println("Postd Cycle Count params : " + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    private class postBulkASync extends AsyncTask<Void, Void, Void> {
        String param = "";

        postBulkASync(final String param) {
            super();
            this.param = param;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            doneButton.setText("Posting...");
            doneButton.setEnabled(false);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            postBulk(param);
            return null;
        }
    }

    public void postBulk(final String param) {
        String postUrl = this.url + "/Api_cycle_count/post_bulk/";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Cycle Count Post Bulk ", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getInt("status") == 200) {
                                Toast.makeText(getApplicationContext(),  jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), InventoryCycleCountType.class);
                                intent.putExtra("cc_document", cc_document);
                                intent.putExtra("warehouse_name", warehouse_name);
                                intent.putExtra("cc_type", cc_type);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            } else {
                                Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("data", param);
                System.out.println("Post Bulk Cycle Count params : " + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    public static String hexToString(final String tagID) {
        if(tagID != null) {
            List<String> forbiddenChar = new ArrayList<>();
            forbiddenChar.add("");

            StringBuilder sb = new StringBuilder();
            char[] hexData = tagID.toCharArray();
            for (int count = 0; count < hexData.length - 1; count += 2) {
                int firstDigit = Character.digit(hexData[count], 16);
                int lastDigit = Character.digit(hexData[count + 1], 16);
                int decimal = firstDigit * 16 + lastDigit;
                if(!forbiddenChar.contains((char)decimal)) {
                    sb.append((char)decimal);
                }
            }
            return sb.toString();
        } else {
            return "";
        }
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }

    private void postCC(final Map<String, String> parameters) {
        String postUrl = this.url + "/Api_cycle_count/tempPost";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        /*Toast.makeText(getApplicationContext(), "Done", Toast.LENGTH_SHORT).show();
                        finish();
                        Intent intent2 = new Intent(getApplicationContext(), Inventory.class);
                        intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent2);*/
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;

            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }
}