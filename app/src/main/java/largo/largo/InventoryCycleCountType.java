package largo.largo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InventoryCycleCountType extends AppCompatActivity {
    String TAG = this.getClass().getSimpleName();

    private Button backButton, nextButton;
    private ImageButton scanButton;
    private EditText ccDocument;
    private String param;
    private String fClass = this.getClass().getSimpleName();
    private Intent intent;
    private ListView listView;
    private List<CycleCountDocumentGetterSetter> dokumenItemList;
    private CycleCountDocumentGetterSetter ccDocGetSet;
    private List<String> docList;
    private String warehouse_name = "";
    private String cc_type = "";
    private String cc_document = "";
    private String type, pick_qty, avail_qty = "";
    private TextView scanTypeText;
    private String location = "";

    DatabaseHelper myDb;
    String url;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cycle_count_type);

        intent = getIntent();
        param = this.getClass().getSimpleName();

        myDb = new DatabaseHelper(this);
        getServerURL();

        backButton = findViewById(R.id.back);
        nextButton = findViewById(R.id.next);
        listView = findViewById(R.id.ListType);
        ccDocument = findViewById(R.id.cc_document);
        scanTypeText = findViewById(R.id.textView13);

        docList = new ArrayList<>();
        if(intent.getSerializableExtra("docList") != null) {
            docList = (List<String>) intent.getSerializableExtra("docList");
        }
        if(intent.getStringExtra("cc_document") != null) {
            cc_document = intent.getStringExtra("cc_document");
            cc_type = intent.getStringExtra("cc_type");
            warehouse_name = intent.getStringExtra("warehouse_name");
        }
        dokumenItemList = new ArrayList<>();
        DataListDokument(url + "/Api_cycle_count/get/"+cc_document+"/"+cc_type);
        if(cc_type.equals("LOC")) {
            scanTypeText.setText("Scan Location");
        } else {
            finish();
        }

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ccDocument.getText().toString().length() > 0) {
                    Intent intent = new Intent(getApplicationContext(), InventoryCycleCountItems.class);
                    intent.putExtra("cc_document", cc_document);
                    intent.putExtra("warehouse_name", warehouse_name);
                    intent.putExtra("cc_type", cc_type);
                    intent.putExtra("type", ccDocument.getText().toString());
                    intent.putExtra("pick_qty", pick_qty);
                    intent.putExtra("avail_qty", avail_qty);
                    intent.putExtra("location",ccDocument.getText().toString());
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "Please input cycle count document first.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                ccDocument.setText(dokumenItemList.get(position).getCc_type());
                type = dokumenItemList.get(position).getCc_type();
                pick_qty = dokumenItemList.get(position).getSource();
                avail_qty = dokumenItemList.get(position).getSource2();
            }
        });

        scanButton = findViewById(R.id.scanButton);
        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getApplicationContext(), QRScanner.class);
                intent.putExtra("param", "ccDoc");
                intent.putExtra("fClass", fClass);
                intent.putExtra("docList", (Serializable) docList);
                intent.putExtra("docItemList", (Serializable) dokumenItemList);
                intent.putExtra("ccDocGetSet", ccDocGetSet);
                intent.putExtra("warehouse_name", warehouse_name);
                startActivity(intent);
            }
        });
        // LocalBroadcastManager.getInstance(this).registerReceiver(qrScan, new IntentFilter("broadcastQrResult"));
        if(intent.getStringExtra("ccDoc") != null) {
            if(docList.contains(intent.getStringExtra("asnCCDoc"))) {
                Intent intent2 = new Intent(getApplicationContext(), AsnItem.class);
                docList = (List<String>) intent.getSerializableExtra("docList");
                dokumenItemList = (List<CycleCountDocumentGetterSetter>) intent.getSerializableExtra("docItemList");
                ccDocGetSet = (CycleCountDocumentGetterSetter) intent.getSerializableExtra("ccDocGetSet");
                intent2.putExtra("cc_document", intent.getStringExtra("asnCCDoc"));
                intent2.putExtra("warehouse_name", intent.getStringExtra("warehouse_name"));
                startActivity(intent2);
            } else {
                Toast.makeText(getApplicationContext(), "Document not available.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void DataListDokument(String getDoc){
        RequestQueue requestQueue=Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, getDoc, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);
                try{
                    JSONObject jsonObject=new JSONObject(response);
                    if(jsonObject.getInt("status")==200){
                        JSONArray jsonArray1=jsonObject.getJSONArray("results");
                        for(int i=0;i<jsonArray1.length();i++){
                            JSONObject jsonObject1=jsonArray1.getJSONObject(i);
                            ccDocGetSet = new CycleCountDocumentGetterSetter(
                                    jsonObject1.getString("cc_code"),
                                    jsonObject1.getString("type"),
                                    jsonObject1.getString("pick_qty"),
                                    jsonObject1.getString("avail_qty")
                            );
                            dokumenItemList.add(ccDocGetSet);
                            docList.add(jsonObject1.getString("type"));
                        }
                        CycleCountDocumentListAdapter adapter = new CycleCountDocumentListAdapter(dokumenItemList, getApplicationContext());

                        listView.setAdapter(adapter);

                    }
//                    spinner.setAdapter(new ArrayAdapter<String>(TallyReceivingItemSerial.this, android.R.layout.simple_spinner_dropdown_item, QC));
                }catch (JSONException e){e.printStackTrace();}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;


            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }
}
