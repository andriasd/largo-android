package largo.largo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class LoadingItemsListAdapter extends ArrayAdapter<LoadingItemsGetSet> {

    private List<LoadingItemsGetSet> dokumenItemList;

    private Context context;

    private int column;

    public LoadingItemsListAdapter(List<LoadingItemsGetSet> dokumenItemList, Context context, int column) {
        super(context, R.layout.adapter_list_vertical, dokumenItemList);
        this.dokumenItemList = dokumenItemList;
        this.context = context;
        this.column = column;
    }



    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);

        View listViewItem = inflater.inflate(R.layout.adapter_list_vertical, null, true);

        TextView textView1 = listViewItem.findViewById(R.id.textView1);
        TextView textView2 = listViewItem.findViewById(R.id.textView2);
        TextView textView3 = listViewItem.findViewById(R.id.textView3);
        TextView textView4 = listViewItem.findViewById(R.id.textView4);

        LoadingItemsGetSet loadingItemsGetSet = dokumenItemList.get(position);

        if(column == 1) {
            textView2.setVisibility(View.GONE);
            textView3.setVisibility(View.GONE);
            textView4.setVisibility(View.GONE);

            textView1.setText(loadingItemsGetSet.getKd_barang());
        } else if(column == 2) {
            textView3.setVisibility(View.GONE);
            textView4.setVisibility(View.GONE);

            textView1.setText(loadingItemsGetSet.getKd_barang() + " - " + loadingItemsGetSet.getNama_barang());
            textView2.setText(loadingItemsGetSet.getScanned_qty() + "/" + loadingItemsGetSet.getOrder_qty() + " " + loadingItemsGetSet.getUnit_code());
        } else if(column == 0) {
            textView2.setVisibility(View.GONE);
            textView3.setVisibility(View.GONE);
            textView4.setVisibility(View.GONE);

            textView1.setText(loadingItemsGetSet.getColly());
        }

        return listViewItem;
    }
}

