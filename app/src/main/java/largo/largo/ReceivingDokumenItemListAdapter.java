package largo.largo;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class ReceivingDokumenItemListAdapter extends ArrayAdapter<ReceivingDocumentItemGetterSetter> {

    private List<ReceivingDocumentItemGetterSetter> dokumenItemList;

    private Context context;

    public ReceivingDokumenItemListAdapter(List<ReceivingDocumentItemGetterSetter> dokumenItemList, Context context) {
        super(context, R.layout.list_view_receiving_dokumen, dokumenItemList);
        this.dokumenItemList = dokumenItemList;
        this.context = context;
    }



    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);

        View listViewItem = inflater.inflate(R.layout.list_view_receiving_dokumen, null, true);

        TextView textViewKdReceiving = listViewItem.findViewById(R.id.textViewDokumen);
        TextView textViewQty = listViewItem.findViewById(R.id.textViewQty);
        TextView textViewSource = listViewItem.findViewById(R.id.textViewSource);

        ReceivingDocumentItemGetterSetter dokumenItem = dokumenItemList.get(position);

        textViewKdReceiving.setText(dokumenItem.getItem_code() + "\n" + dokumenItem.getItem_name());
        textViewKdReceiving.setTypeface(Typeface.DEFAULT_BOLD);
        textViewQty.setText(dokumenItem.getReceived_qty() + "/" + dokumenItem.getQty() + " " + dokumenItem.getNama_satuan());
        textViewSource.setText(dokumenItem.getSource());

        if(Float.parseFloat(dokumenItem.getReceived_qty()) != 0){
            textViewKdReceiving.setBackgroundColor(Color.parseColor("#e6e600"));
            textViewQty.setBackgroundColor(Color.parseColor("#e6e600"));
            textViewSource.setBackgroundColor(Color.parseColor("#e6e600"));
        }else if(dokumenItem.getReceived_qty() == dokumenItem.getQty()){
//            textViewQty.setBackgroundColor(Color.parseColor("#00FF00"));
            textViewKdReceiving.setBackgroundColor(Color.parseColor("#99ff33"));
            textViewQty.setBackgroundColor(Color.parseColor("#99ff33"));
            textViewSource.setBackgroundColor(Color.parseColor("#99ff33"));
//            textViewQty.setText(dokumenItem.getReceived_qty() + "/" + dokumenItem.getQty() + " " + dokumenItem.getNama_satuan());
        }

        return listViewItem;
    }
}

