package largo.largo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LicensePlatingPC extends AppCompatActivity {
    String TAG = LicensePlating.class.getSimpleName();

    private LicensePlatingGetSet licensePlatingGetSet;
    private List<LicensePlatingGetSet> licensePlatingGetSetList = new ArrayList<>();
    private List<String> tagList = new ArrayList<>();
    private LicensePlatingAdapter licensePlatingAdapter;
    private ListView lpList;
    private AsnScanRFID asnScanRFID = new AsnScanRFID();
    private Map<String, String> params = new HashMap<String, String>();
    private Intent intent;
    private ImageButton scanButton;
    private String fClass = this.getClass().getSimpleName();

    private Calendar calendar;
    private SimpleDateFormat mdformat;
    private SharedPreferences prefs;
    private String usernameKey;

    DatabaseHelper myDb;

    String url;

    EditText serial_number;
    Button nextButton, backButton;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.license_plating_pc);

        calendar = Calendar.getInstance();
        mdformat = new SimpleDateFormat("YYYY-MM-dd");
        prefs = getSharedPreferences(LoginActivity.MyPREFERENCES, MODE_PRIVATE);
        usernameKey = prefs.getString("usernameKey", "No name defined");
        intent = getIntent();

        if(intent.getSerializableExtra("tagList") != null) {
            tagList = (List<String>) intent.getSerializableExtra("tagList");
        }
        if(intent.getSerializableExtra("licensePlatingGetSet") != null) {
            licensePlatingGetSet = (LicensePlatingGetSet) intent.getSerializableExtra("licensePlatingGetSet");
        }
        if(intent.getSerializableExtra("licensePlatingGetSetList") != null) {
            licensePlatingGetSetList = (List<LicensePlatingGetSet>) intent.getSerializableExtra("licensePlatingGetSetList");
        }

        lpList = findViewById(R.id.listLicensePlatingSN);
        licensePlatingAdapter = new LicensePlatingAdapter(licensePlatingGetSetList, getApplicationContext(), 2);
        lpList.setAdapter(licensePlatingAdapter);

        myDb = new DatabaseHelper(this);

        getServerURL();

        serial_number = findViewById(R.id.serial_number);
        serial_number.setShowSoftInputOnFocus(false);

        serial_number.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    params.put("kd_parent", serial_number.getText().toString().toUpperCase());
                    validatePC(params);
                }
                return false;
            }
        });

        backButton = findViewById(R.id.back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        nextButton = findViewById(R.id.next);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(serial_number.length() > 0) {
                    String kd_unik = "[\"";
                    for(int i = 0; i < licensePlatingGetSetList.size(); i++) {
                        kd_unik += licensePlatingGetSetList.get(i).getHex();
                        kd_unik += "\",\"";
                    }
                    kd_unik = kd_unik.substring(0, kd_unik.length()-3);
                    kd_unik += "\"]";
                    params.put("kd_unik", kd_unik);
                    params.put("kd_parent", serial_number.getText().toString().toUpperCase());
                    System.out.println("params : " + params);
                    submitPC(params);
                } else {
                    Toast.makeText(getApplicationContext(), "Harap isi License Plate Number terlebih dahulu.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        scanButton = findViewById(R.id.scanButton);
        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getApplicationContext(), QRScanner.class);
                intent.putExtra("param", "licensePlating");
                intent.putExtra("fClass", fClass);
                intent.putExtra("licensePlatingGetSetList", (Serializable) licensePlatingGetSetList);
                intent.putExtra("tagList", (Serializable) tagList);
                intent.putExtra("licensePlatingGetSet", licensePlatingGetSet);
                startActivity(intent);
            }
        });

        if(intent.getStringExtra("licensePlating") != null) {
            serial_number.setText(intent.getStringExtra("licensePlating"));
            params.put("kd_parent", intent.getStringExtra("licensePlating").toUpperCase());
            validatePC(params);
            licensePlatingGetSetList = (List<LicensePlatingGetSet>) intent.getSerializableExtra("licensePlatingGetSetList");
            licensePlatingGetSet = (LicensePlatingGetSet) intent.getSerializableExtra("licensePlatingGetSet");
            tagList = (List<String>) intent.getSerializableExtra("tagList");
        }
    }

    private void validatePC(final Map<String, String> parameters) {
        StringRequest strReq = new StringRequest(Request.Method.POST,url+"/Api_license_plating/validatePC/", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("validatePC", response);
                    JSONObject data = new JSONObject(response);

                    Integer status = data.getInt("status");
                    String message = data.getString("message");

                    JSONObject paramData = new JSONObject(parameters);
                    String serialNumberParam = paramData.getString("kd_parent");

                    if(!message.contains("is valid")) {
                        serial_number.requestFocus();
                        serial_number.hasFocus();
                        serial_number.setText("");
                    }
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    String jsonError = new String(networkResponse.data);

                    try {
                        JSONObject data = new JSONObject(jsonError);

                        String message = data.getString("message");

                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        })  {

            @Override
            protected Map<String, String> getParams(){
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(LoginActivity.MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }

    private void submitPC(final Map<String, String> parameters) {
        StringRequest strReq = new StringRequest(Request.Method.POST,url+"/Api_license_plating/submit/", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("submitPC", response);
                    JSONObject data = new JSONObject(response);

                    Integer status = data.getInt("status");
                    String message = data.getString("message");

                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    String jsonError = new String(networkResponse.data);

                    try {
                        JSONObject data = new JSONObject(jsonError);

                        String message = data.getString("message");

                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        })  {

            @Override
            protected Map<String, String> getParams(){
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(LoginActivity.MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }
}
