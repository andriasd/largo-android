package largo.largo;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

public class MenuActivity extends AppCompatActivity {
    String TAG = MenuActivity.class.getSimpleName();
    Button inbound, outbound, inventory, setting, license_plating, split_items;
    ImageButton backButton;

    SharedPreferences sharedpreferences;

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.logout_menu, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//        switch (id) {
//            case R.id.logout:
//                Intent intent = new Intent(MenuActivity.this,LoginActivity.class);
//                startActivity(intent);
//                return false;
//        }
//
//        return super.onOptionsItemSelected(item); // important line
//    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu);

        backButton = findViewById(R.id.back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMessage("Notification", "Are you sure want to logout ?");
            }
        });

        inbound = findViewById(R.id.inbound);
        outbound = findViewById(R.id.outbound);
        inventory = findViewById(R.id.inventory);
        license_plating = findViewById(R.id.license_plating);
        split_items = findViewById(R.id.split_items);

        setting = findViewById(R.id.setting);
//        logout = findViewById(R.id.logout);

        inbound.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        Intent intent = new Intent(MenuActivity.this,Inbound.class);
                        startActivity(intent);
                    }
                }
        );

        outbound.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        Intent intent = new Intent(MenuActivity.this,Outbound.class);
                        startActivity(intent);
                    }
                }
        );

        inventory.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        Intent intent = new Intent(MenuActivity.this,Inventory.class);
                        startActivity(intent);
                    }
                }
        );

        setting.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        Intent intent = new Intent(MenuActivity.this,SettingActivity.class);
                        startActivity(intent);
                    }
                }
        );

        license_plating.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        // Intent intent = new Intent(MenuActivity.this,LicensePlatingOption.class);
                        Intent intent = new Intent(MenuActivity.this,LicensePlating.class);
                        startActivity(intent);
                    }
                }
        );

        split_items.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        // Intent intent = new Intent(MenuActivity.this,LicensePlatingOption.class);
                        Intent intent = new Intent(MenuActivity.this,SplitItems.class);
                        startActivity(intent);
                    }
                }
        );

//        logout.setOnClickListener(
//                new View.OnClickListener(){
//                    @Override
//                    public void onClick(View v){
//                        Intent intent = new Intent(MenuActivity.this,LoginActivity.class);
//                        startActivity(intent);
//
//                    }
//                }
//        );
    }

    public void showMessage(String title, String Message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.setMessage(Message);
        builder.show();

    }
}
