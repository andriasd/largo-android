package largo.largo;

import java.io.Serializable;

public class PutawaySerialNumberGetterSetter implements Serializable {
    String serial_number, sku, sku_name, location;

    public PutawaySerialNumberGetterSetter(String serial_number, String sku, String sku_name, String location) {
        this.serial_number = serial_number;
        this.sku = sku;
        this.sku_name = sku_name;
        this.location = location;
    }


    public String getSN() {
        return serial_number;
    }

    public String getSku() {
        return sku;
    }

    public String getSkuName() {
        return sku_name;
    }

    public String getLocation() {
        return location;
    }
}
