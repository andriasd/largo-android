package largo.largo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static largo.largo.LoginActivity.MyPREFERENCES;

public class AsnPackingItemCheck extends AppCompatActivity {
    String TAG = this.getClass().getSimpleName();
    private Button backButton, doneButton, nextPack;
    private EditText asnQr, asnQty;
    private Intent intent;
    private ListView listView;
    private List<PackingDocumentGetterSetter> dokumenItemList;
    private PackingDocumentGetterSetter packDocGetSet;
    private List<String> docList;
    private String outbound_code, picking_code, packing_code, item_code, inf_text, pac_text, item_info_text;
    private PackingDocumentListAdapter adapter;
    private TextView information_text, pack_text, item_info;
    private CheckBox checkBox;
    private int qty = 0;

    public static final Map<String, String> params = new HashMap<String, String>();
    public static final String MyPREFERENCES = "MyPrefs" ;

    DatabaseHelper myDb;
    String url;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.outbound_packing_by_item_check);

        intent = getIntent();

        outbound_code = intent.getStringExtra("outbound_code");
        picking_code = intent.getStringExtra("picking_code");
        packing_code = intent.getStringExtra("packing_code");
        item_info_text = intent.getStringExtra("item_info");
        inf_text = intent.getStringExtra("inf_text");
        pac_text = intent.getStringExtra("pac_text");
        item_code = intent.getStringExtra("item_code");
        information_text = findViewById(R.id.information_text);
        pack_text = findViewById(R.id.pack_text);
        information_text.setText(inf_text);
        pack_text.setText(pac_text);
        item_info = findViewById(R.id.textView40);
        item_info.setText(item_info_text);

        myDb = new DatabaseHelper(this);
        getServerURL();

        listView = findViewById(R.id.QRList);

        backButton = findViewById(R.id.back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AsnPackingItemCheck.this,AsnPackingScanQR.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("picking_code", picking_code);
                intent.putExtra("outbound_code", outbound_code);
                intent.putExtra("inf_text", inf_text);
                intent.putExtra("pac_text", pac_text);
                intent.putExtra("packing_code", packing_code);
                startActivity(intent);
            }
        });

        nextPack = findViewById(R.id.next_pack);
        nextPack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject jsonObject = null;
                String tmp_packing_code = "";
                int tmp_inc = 0;
                try {
                    jsonObject = new JSONObject(packing_code);
                    tmp_packing_code = jsonObject.getString("pc_code");
                    tmp_inc = jsonObject.getInt("inc");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                params.clear();
                params.put("id_outbound", outbound_code);
                params.put("picking_code", picking_code);
                params.put("pc_code", tmp_packing_code);
                params.put("inc", String.valueOf(tmp_inc));
                get_next_pc_code(params, packing_code);
            }
        });

        doneButton = findViewById(R.id.next);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doneButton.setEnabled(false);
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                int count = 0;
                boolean check = false;

                for(int i = 0; i < dokumenItemList.size(); i++) {
                    if(dokumenItemList.get(i).isChecked()) {
                        count++;
                    }
                }

                if(count == 0) {
                    Intent intent = new Intent(AsnPackingItemCheck.this, AsnPackingScanQR.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("picking_code", picking_code);
                    intent.putExtra("outbound_code", outbound_code);
                    intent.putExtra("inf_text", inf_text);
                    intent.putExtra("pac_text", pac_text);
                    intent.putExtra("packing_code", packing_code);
                    startActivity(intent);
                } else {
                    for(int i = 0; i < dokumenItemList.size(); i++) {
                        if(dokumenItemList.get(i).isChecked()) {
                            if(count > 0) {
                                if(i == (count-1)) {
                                    check = true;
                                }
                            }

                            new postPackingASync(dokumenItemList.get(i).getPl_name(), check).execute();
                        }
                    }
                }
            }
        });

        asnQr = findViewById(R.id.asnQr);
        asnQr.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 0) {
                    //postPacking(asnQr.getText().toString());
                }
                return false;
            }
        });

        asnQty = findViewById(R.id.asnQty);
        asnQty.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 0) {
                    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                    int random_int = (int)(Math.random() * (100 - 1) + 1);

                    String new_sn = "SPLITBYSYS"+timestamp.getTime()+String.valueOf(random_int);
                    if(Integer.parseInt(asnQty.getText().toString()) > qty) {
                        Toast.makeText(AsnPackingItemCheck.this, "Qty tidak boleh lebih besar dari " + String.valueOf(qty), Toast.LENGTH_SHORT).show();
                    } else if (Integer.parseInt(asnQty.getText().toString()) == qty) {
                        postPacking(asnQr.getText().toString());
                    } else {
                        postSplitItems(new_sn, asnQr.getText().toString(), asnQty.getText().toString());
                    }
                }
                return false;
            }
        });

        docList = new ArrayList<>();
        if(intent.getSerializableExtra("docList") != null) {
            docList = (List<String>) intent.getSerializableExtra("docList");
        }
        dokumenItemList = new ArrayList<>();
        adapter = new PackingDocumentListAdapter(dokumenItemList, getApplicationContext());
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                asnQr.setText(dokumenItemList.get(position).getPl_name());
                String[] split = dokumenItemList.get(position).getKd_outbound().split(" ");
                qty = Integer.parseInt(split[0]);
                asnQty.setText(split[0]);
                asnQty.requestFocus();
            }
        });

        try {
            getItems(url + "/Api_packing/get_serial_number_by_picking/" + picking_code + "/" + URLEncoder.encode(item_code.replace("/","|"), StandardCharsets.UTF_8.toString()).replace("+","%20") + "/" + outbound_code);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void get_next_pc_code(final Map<String, String> parameters, final String jsonPacking){
        String postUrl = this.url + "/Api_packing/check_pc_code/";
        System.out.println("get_next_pc_codeParams() : " + parameters);

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, response);
                        try{
                            JSONObject jsonResult = new JSONObject(response);
                            if(jsonResult.getString("status").equals("OK")) {
                                JSONObject jsonObject = new JSONObject(jsonPacking);
                                String tmp_packing_code = jsonObject.getString("pc_code");
                                int tmp_inc = jsonObject.getInt("inc");
                                String tmp_pc_string = jsonObject.getString("pc_string");
                                tmp_inc = tmp_inc + 1;

                                inf_text = picking_code + ", " + outbound_code + "\n";
                                inf_text = inf_text + "Packing Label: " + tmp_packing_code;
                                pac_text = "Colly\n" + tmp_inc;
                                information_text.setText(inf_text);
                                pack_text.setText(pac_text);

                                packing_code = "{\"pc_code\":\"" +tmp_packing_code+ "\",\"inc\":" +tmp_inc+ ",\"pc_string\":\"<b>"+tmp_pc_string+"<\\/b>\"}";
                            } else {
                                String message = jsonResult.getString("message");
                                Toast.makeText(AsnPackingItemCheck.this, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e){ e.printStackTrace(); }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;

            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    private void postSplitItems(final String newSerialNumber, final String oldSerialNumber, final String qtyPars) {
        if(Float.parseFloat(asnQty.getText().toString()) > 0) {
            StringRequest strReq = new StringRequest(Request.Method.POST, url+"/Api_split_serial_number/post/", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d(TAG + "postSplitItems", response);

                    try {
                        JSONObject data = new JSONObject(response);
                        Integer status = data.getInt("status");
                        String message = data.getString("message");

                        if (status == 200) {
                            //Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                            postPacking(newSerialNumber);
                        } else {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                }
            }) {

                @Override
                protected Map<String, String> getParams(){
                    Map<String, String> params = new HashMap<String, String>();
                    SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                    String usernameKey = prefs.getString("usernameKey", "No name defined");

                    Calendar calendar = Calendar.getInstance();
                    SimpleDateFormat mdformat = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");

                    params.put("new_serial_number", newSerialNumber);
                    params.put("old_serial_number", oldSerialNumber);
                    params.put("qty", qtyPars);
                    params.put("flag", "1");
                    params.put("picking", picking_code);
                    params.put("outbound", outbound_code);
                    System.out.println("Params post split items : " + params);

                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                    String restoredText = prefs.getString("handheldsessioncodeKey", null);

                    String usernameKey = prefs.getString("usernameKey", "No name defined");
                    String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                    Log.d(TAG, usernameKey);
                    Log.d(TAG, handheldsessioncodeKey);

                    Map<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "application/x-www-form-urlencoded");
                    headers.put("User",usernameKey);
                    headers.put("Authorization", handheldsessioncodeKey);
                    return headers;


                }
            };

            controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
        } else {
            Toast.makeText(getApplicationContext(), "Qty tidak boleh 0.", Toast.LENGTH_SHORT);
        }
    }

    private void getItems(String getDoc){
        RequestQueue requestQueue=Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, getDoc, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);
                try{
                    JSONObject jsonObject=new JSONObject(response);
                    if(jsonObject.getInt("status")==200){
                        JSONArray jsonArray1=jsonObject.getJSONArray("response");
                        dokumenItemList.clear();
                        for(int i=0;i<jsonArray1.length();i++){
                            JSONObject jsonObject1=jsonArray1.getJSONObject(i);
                            packDocGetSet = new PackingDocumentGetterSetter(
                                    jsonObject1.getString("serial_number"),
                                    jsonObject1.getString("qty"),
                                    "",
                                    "",
                                    "",
                                    1
                            );
                            dokumenItemList.add(packDocGetSet);
                            docList.add(jsonObject1.getString("serial_number"));
                        }
                        adapter.notifyDataSetChanged();
                    }
                } catch (JSONException e){ e.printStackTrace(); }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;


            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private class postPackingASync extends AsyncTask<Void, Void, Void> {
        String serial_number;
        boolean check;

        postPackingASync(String serial_number, boolean check) {
            super();
            this.serial_number = serial_number;
            this.check = check;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(check) {
                Intent intent = new Intent(AsnPackingItemCheck.this, AsnPackingScanQR.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("picking_code", picking_code);
                intent.putExtra("outbound_code", outbound_code);
                startActivity(intent);
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            postPacking(serial_number);

            System.out.println("postPackingASync param : " + params + ", " + check);
            return null;
        }
    }

    private void postPacking(final String serial_number) {
        String postUrl = this.url + "/Api_packing/post/";
        System.out.println("postPackingParams() : sn:" + serial_number + ",packing:" + packing_code + ",outbound:" + outbound_code);

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if(jsonObject.getInt("status") == 200){
                                getItems(url + "/Api_packing/get_serial_number_by_picking/" + picking_code + "/" + URLEncoder.encode(item_code, StandardCharsets.UTF_8.toString()).replace("+","%20") + "/" + outbound_code);
                                asnQr.requestFocus();
                            } else {
                                String message = jsonObject.getString("message");
                                Toast.makeText(AsnPackingItemCheck.this, message, Toast.LENGTH_SHORT).show();
                            }

                            doneButton.setEnabled(true);
                        } catch (JSONException | UnsupportedEncodingException e) { e.printStackTrace(); }
                        asnQr.setText("");
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                params.clear();
                params.put("serial_number", serial_number);
                params.put("outbound_code", outbound_code);
                params.put("packing_code", packing_code);
                params.put("picking_code", picking_code);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;

            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }
}
