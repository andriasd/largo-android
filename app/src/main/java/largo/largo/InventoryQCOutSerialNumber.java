package largo.largo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static largo.largo.LoginActivity.MyPREFERENCES;

public class InventoryQCOutSerialNumber extends AppCompatActivity {
    String TAG = InventoryQCOutSerialNumber.class.getSimpleName();
    EditText serial_number;
    Button next,backButton;

    private InventoryQCGetSet inventoryQCGetSet;
    private List<InventoryQCGetSet> inventoryQCGetSetList = new ArrayList<>();
    private List<String> tagList = new ArrayList<>();
    private InventoryQCListAdapter inventoryQCListAdapter;
    private ListView qcList;
    private AsnScanRFID asnScanRFID = new AsnScanRFID();
    private Map<String, String> params = new HashMap<String, String>();
    private Intent intent;
    private ImageButton scanButton;
    private String fClass = this.getClass().getSimpleName();

    private Calendar calendar;
    private SimpleDateFormat mdformat;
    private SharedPreferences prefs;
    private String usernameKey;

    DatabaseHelper myDb;

    String url;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qc_out_receiving_item_serial);

        calendar = Calendar.getInstance();
        mdformat = new SimpleDateFormat("YYYY-MM-dd");
        prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        usernameKey = prefs.getString("usernameKey", "No name defined");
        intent = getIntent();

        qcList = findViewById(R.id.listQCOutItem);
        inventoryQCListAdapter = new InventoryQCListAdapter(inventoryQCGetSetList, getApplicationContext(), 2);
        qcList.setAdapter(inventoryQCListAdapter);

        myDb = new DatabaseHelper(this);

        getServerURL();

        serial_number = findViewById(R.id.serial_number);
        serial_number.setShowSoftInputOnFocus(false);

        serial_number.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    postQCOutSerialNumber(serial_number.getText().toString());
                }
                return false;
            }
        });

        backButton = findViewById(R.id.back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(inventoryQCGetSetList != null) {
                    for(int i = 0; i < inventoryQCGetSetList.size(); i++) {
                        params.put("serial_number", inventoryQCGetSetList.get(i).getHex().toUpperCase());
                        params.put("loc_name_old", inventoryQCGetSetList.get(i).getLoc());
                        params.put("user_pick", usernameKey);
                        params.put("old_qc", inventoryQCGetSetList.get(i).getQc());
                        params.put("pick_time", mdformat.format(calendar.getTime()));
                        //new postQCCancelASync(params).execute();
                        try {
                            postQCCancel(params);
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
                finish();
            }
        });

        next = findViewById(R.id.next);
        next.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        finish();
                        Intent intent = new Intent(getApplicationContext(), InventoryQCOutLocation.class);
                        intent.putExtra("inventoryQCGetSet", inventoryQCGetSet);
                        intent.putExtra("inventoryQCGetSetList", (Serializable) inventoryQCGetSetList);
                        intent.putExtra("tagList", (Serializable) tagList);
                        startActivity(intent);
                    }
                }
        );

        scanButton = findViewById(R.id.scanButton);
        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getApplicationContext(), QRScanner.class);
                intent.putExtra("param", "inventoryQCOut");
                intent.putExtra("fClass", fClass);
                intent.putExtra("inventoryQCGetSetList", (Serializable) inventoryQCGetSetList);
                intent.putExtra("tagList", (Serializable) tagList);
                intent.putExtra("inventoryQCGetSet", inventoryQCGetSet);
                startActivity(intent);
            }
        });

        if(intent.getStringExtra("inventoryQCOut") != null) {
            postQCOutSerialNumber(intent.getStringExtra("inventoryQCOut"));
            inventoryQCGetSetList = (List<InventoryQCGetSet>) intent.getSerializableExtra("inventoryQCGetSetList");
            inventoryQCGetSet = (InventoryQCGetSet) intent.getSerializableExtra("inventoryQCGetSet");
            tagList = (List<String>) intent.getSerializableExtra("tagList");
        }
    }

    private void postQCOutSerialNumber(final String serialNumber) {
        StringRequest strReq = new StringRequest(Request.Method.GET, url+"/Api_QC/get_out/" + serialNumber.toUpperCase(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);

                try {
                    JSONObject data = new JSONObject(response);
                    Integer status = data.getInt("status");
                    String message = data.getString("message");

                    if (status == 200) {
                        if(message.contains(" is available.")) {
                            JSONArray jsonArray = data.getJSONArray("results");

                            for(int i=0;i<jsonArray.length();i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                String loc_name = jsonObject1.getString("loc_name");
                                String kd_qc = jsonObject1.getString("kd_qc");
                                if(tagList != null) {
                                    if(tagList.contains(serialNumber)) {
                                        Toast.makeText(getApplicationContext(), serialNumber + " already scanned.", Toast.LENGTH_SHORT).show();
                                    } else {
                                        tagList.add(serialNumber);
                                        inventoryQCGetSet = new InventoryQCGetSet(serialNumber, loc_name, kd_qc, serialNumber, mdformat.format(calendar.getTime()), "");
                                        inventoryQCGetSetList.add(inventoryQCGetSet);
                                    }
                                } else {
                                    tagList.add(serialNumber);
                                    inventoryQCGetSet = new InventoryQCGetSet(serialNumber, loc_name, kd_qc, serialNumber, mdformat.format(calendar.getTime()), "");
                                    inventoryQCGetSetList.add(inventoryQCGetSet);
                                }
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        }
                    } else if (status == 401) {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {

                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        inventoryQCListAdapter = new InventoryQCListAdapter(inventoryQCGetSetList, getApplicationContext(), 2);
                        qcList.setAdapter(inventoryQCListAdapter);
                        inventoryQCListAdapter.notifyDataSetChanged();
                        serial_number.setText("");
                        serial_number.hasFocus();
                    }
                });
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;


            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }

    private class postQCCancelASync extends AsyncTask<Void, Void, Void> {
        final Map<String, String> parameters;

        postQCCancelASync(Map<String, String> parameters) {
            super();
            this.parameters = parameters;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    postQCCancel(parameters);
                }
            });
            return null;
        }
    }

    private void postQCCancel(final Map<String, String> parameters) {
        StringRequest strReq = new StringRequest(Request.Method.POST,url+"/Api_QC/cancel/", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("postQCCancel", response);
                    JSONObject data = new JSONObject(response);

                    Integer status = data.getInt("status");
                    String message = data.getString("message");

                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    String jsonError = new String(networkResponse.data);

                    try {
                        JSONObject data = new JSONObject(jsonError);

                        String message = data.getString("message");

                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        })  {

            @Override
            protected Map<String, String> getParams(){
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }
}
