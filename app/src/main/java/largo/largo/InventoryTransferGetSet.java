package largo.largo;

import java.io.Serializable;

public class InventoryTransferGetSet implements Serializable {
    String sn, loc, hex, pickTime, flag;

    public InventoryTransferGetSet(String sn, String loc, String hex, String pickTime, String flag) {
        this.sn = sn;
        this.loc = loc;
        this.hex = hex;
        this.pickTime = pickTime;
        this.flag = flag;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public String getHex() {
        return hex;
    }

    public void setHex(String hex) {
        this.hex = hex;
    }

    public String getPickTime() {
        return pickTime;
    }

    public void setPickTime(String pickTime) {
        this.pickTime = pickTime;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
}
