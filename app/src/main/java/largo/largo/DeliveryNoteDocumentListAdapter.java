package largo.largo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class DeliveryNoteDocumentListAdapter extends ArrayAdapter<DeliveryNoteDocumentGetterSetter> {

    private List<DeliveryNoteDocumentGetterSetter> deliveryNoteDocumentItemList;

    private Context context;

    public DeliveryNoteDocumentListAdapter(List<DeliveryNoteDocumentGetterSetter> deliveryNoteDocumentItemList, Context context) {
        super(context, R.layout.list_view_picking_document, deliveryNoteDocumentItemList);
        this.deliveryNoteDocumentItemList = deliveryNoteDocumentItemList;
        this.context = context;
    }



    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);

        View listViewItem = inflater.inflate(R.layout.list_view_picking_document, null, true);

        TextView textViewPickingDoc = listViewItem.findViewById(R.id.textViewPickingDoc);
        TextView textViewQty = listViewItem.findViewById(R.id.textViewQty);
        TextView textViewCustomer = listViewItem.findViewById(R.id.textViewCustomer);


        DeliveryNoteDocumentGetterSetter deliveryDocument = deliveryNoteDocumentItemList.get(position);

        textViewPickingDoc.setText(String.valueOf(deliveryDocument.getDn_name()));
        textViewQty.setText("");
        textViewCustomer.setText("");

        return listViewItem;
    }
}
