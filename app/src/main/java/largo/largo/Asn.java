package largo.largo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Asn extends AppCompatActivity {
    String TAG = this.getClass().getSimpleName();

    private Button backButton, nextButton;
    private ImageButton scanButton;
    private EditText receivingDocument;
    private String param;
    private String fClass = this.getClass().getSimpleName();
    private Intent intent;
    private ListView listView;
    private List<ReceivingDocumentGetterSetter> dokumenItemList;
    private List<String> docList;

    DatabaseHelper myDb;
    String url;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inbound_tally_receiving_document_by_asn);

        intent = getIntent();
        param = this.getClass().getSimpleName();

        myDb = new DatabaseHelper(this);
        getServerURL();

        backButton = findViewById(R.id.back);
        nextButton = findViewById(R.id.next);
        listView = findViewById(R.id.ListTallyDoc);
        receivingDocument = findViewById(R.id.receiving_document);

        docList = new ArrayList<>();
        dokumenItemList = new ArrayList<>();
        if(intent.getSerializableExtra("docList") != null) {
            docList = (List<String>) intent.getSerializableExtra("docList");
        }
        DataListDokument(url + "/api/receiving/get_document");

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(receivingDocument.getText().toString().length() > 0) {
                    // Intent intent = new Intent(getApplicationContext(), AsnItem.class);
                    // Intent intent = new Intent(getApplicationContext(), AsnDocumentInbound.class);

                    int pos = 0;
                    for(int i=0;i<dokumenItemList.size();i++) {
                        if(dokumenItemList.get(i).getKdReceiving().equals(receivingDocument.getText().toString())) {
                            pos = i;
                        }
                    }

                    Intent intent = new Intent(getApplicationContext(), AsnDocumentItem.class);
                    intent.putExtra("receiving_document", receivingDocument.getText().toString());
                    intent.putExtra("inbound_document", dokumenItemList.get(pos).getInbound_code());
                    startActivity(intent);
                } else {
                    Toast.makeText(Asn.this, "Please input receiving document first.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                receivingDocument.setText(dokumenItemList.get(position).getKdReceiving());

                // Intent intent = new Intent(getApplicationContext(), AsnDocumentInbound.class);
                Intent intent = new Intent(getApplicationContext(), AsnDocumentItem.class);
                intent.putExtra("receiving_document", dokumenItemList.get(position).getKdReceiving());
                intent.putExtra("inbound_document", dokumenItemList.get(position).getInbound_code());
                startActivity(intent);
            }
        });

        receivingDocument.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 0) {
                    if(receivingDocument.getText().toString().length() > 0) {
                        // Intent intent = new Intent(getApplicationContext(), AsnItem.class);
                        // Intent intent = new Intent(getApplicationContext(), AsnDocumentInbound.class);

                        int pos = 0;
                        for(int i=0;i<dokumenItemList.size();i++) {
                            if(dokumenItemList.get(i).getKdReceiving().equals(receivingDocument.getText().toString())) {
                                pos = i;
                            }
                        }

                        Intent intent = new Intent(getApplicationContext(), AsnDocumentItem.class);
                        intent.putExtra("receiving_document", receivingDocument.getText().toString());
                        intent.putExtra("inbound_document", dokumenItemList.get(pos).getInbound_code());
                        startActivity(intent);
                    } else {
                        Toast.makeText(Asn.this, "Please input receiving document first.", Toast.LENGTH_SHORT).show();
                    }
                }
                return false;
            }
        });

        scanButton = findViewById(R.id.scanButton);
        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getApplicationContext(), QRScanner.class);
                intent.putExtra("param", "receiving_document");
                intent.putExtra("fClass", fClass);
                intent.putExtra("docList", (Serializable) docList);
                startActivity(intent);
            }
        });
//        LocalBroadcastManager.getInstance(this).registerReceiver(qrScan, new IntentFilter("broadcastQrResult"));

//        if(intent.getStringExtra("receiving_document") != null) {
//            if (docList.contains(intent.getStringExtra("receiving_document"))) {
//                Intent intent2 = new Intent(getApplicationContext(), AsnDocumentItem.class);
//                intent2.putExtra("receiving_document", intent.getStringExtra("receiving_document"));
//                startActivity(intent2);
//                intent2.removeExtra("receiving_document");
//            } else {
//                Toast.makeText(getApplicationContext(), "Document not available.", Toast.LENGTH_SHORT).show();
//            }
//        }
    }

    private void DataListDokument(String getDoc){
//        final ArrayList<String> items = new ArrayList<String>();

        RequestQueue requestQueue=Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, getDoc, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);
                try{
                    JSONObject jsonObject=new JSONObject(response);
                    if(jsonObject.getInt("status")==200){
                        JSONArray jsonArray1=jsonObject.getJSONArray("results");
                        if(docList.size() > 0) {
                            docList.clear();
                        }
                        for(int i=0;i<jsonArray1.length();i++){
                            JSONObject jsonObject1=jsonArray1.getJSONObject(i);
                            ReceivingDocumentGetterSetter item_dokumen = new ReceivingDocumentGetterSetter(
                                    jsonObject1.getString("kd_receiving"),
                                    jsonObject1.getString("inbound_code"),
                                    jsonObject1.getString("qty"),
                                    jsonObject1.getString("source"),
                                    jsonObject1.getString("qty_item")
                            );
                            dokumenItemList.add(item_dokumen);
                            docList.add(jsonObject1.getString("kd_receiving"));
                        }
                        ReceivingDokumenListAdapter adapter = new ReceivingDokumenListAdapter(dokumenItemList, getApplicationContext());

                        listView.setAdapter(adapter);

                    }
//                    spinner.setAdapter(new ArrayAdapter<String>(TallyReceivingItemSerial.this, android.R.layout.simple_spinner_dropdown_item, QC));
                }catch (JSONException e){e.printStackTrace();}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;


            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }

    private BroadcastReceiver qrScan = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getStringExtra(param) != null) {
                String getParam = intent.getStringExtra(param);
                intent.removeExtra(param);

                if(docList.contains(getParam)) {
                    Intent intent2 = new Intent(getApplicationContext(), AsnItem.class);
                    intent2.putExtra("receiving_document", getParam);
                    startActivity(intent2);
                } else {
                    Toast.makeText(getApplicationContext(), "Document not available.", Toast.LENGTH_SHORT).show();
                }
            }
        }
    };
}
