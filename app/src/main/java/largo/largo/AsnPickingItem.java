package largo.largo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AsnPickingItem extends AppCompatActivity {

    private Button cancelButton, qrButton, rfidButton;
    private Intent intent;
    private ListView listView;
    private List<PickingItemGetterSetter> itemList;
    private PickingItemGetterSetter item_dokumen;
    private PickingItemListAdapter adapter;
    private List<String> docList, skuList;
    private String param, pickingDocument, TAG;
    private float countTotalQty, countPickedQty;
    private TextView qtyText;

    DatabaseHelper myDb;
    String url;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.outbound_picking_by_asn_item);

        TAG = this.getClass().getSimpleName();
        myDb = new DatabaseHelper(this);
        getServerURL();

        intent = getIntent();
        param = this.getClass().getSimpleName();
        docList = new ArrayList<>();
        skuList = new ArrayList<>();
        itemList = new ArrayList<>();
        pickingDocument = "";

        if(intent.getStringExtra("picking_document") != null) {
            pickingDocument = intent.getStringExtra("picking_document");
            getPicking(url + "/Api_picking/get/" + pickingDocument);
        }

        cancelButton = findViewById(R.id.cancel);
        qrButton = findViewById(R.id.qr_button);
        rfidButton = findViewById(R.id.rfid_button);
        qtyText = findViewById(R.id.qty_text);
        listView = findViewById(R.id.QRList);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        qrButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getApplicationContext(), AsnPickingScanQR.class);
                intent.putExtra("picking_document", pickingDocument);
                startActivity(intent);
            }
        });

        rfidButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getApplicationContext(), AsnPickingScanRFID.class);
                intent.putExtra("picking_document", pickingDocument);
                intent.putExtra("skuList", (Serializable) skuList);
                startActivity(intent);
            }
        });
    }

    private void getPicking(String getDoc){
//        final ArrayList<String> items = new ArrayList<String>();

        RequestQueue requestQueue=Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, getDoc, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG + "getPicking", response);
                try{
                    JSONObject jsonObject = new JSONObject(response);
                    if(jsonObject.getInt("status")==200){
                        JSONArray jsonArray1 = jsonObject.getJSONArray("locations");
                        countPickedQty = 0;
                        countTotalQty = 0;

                        for(int i = 0; i < jsonArray1.length(); i++){
                            JSONObject jsonObject1=jsonArray1.getJSONObject(i);
                            System.out.println(jsonObject1);
                            item_dokumen = new PickingItemGetterSetter(
                                    jsonObject1.getString("kd_barang"),
                                    jsonObject1.getString("picked_qty"),
                                    jsonObject1.getString("has_qty"),
                                    jsonObject1.getString("qty"),
                                    jsonObject1.getString("available_qty"),
                                    jsonObject1.getString("loc_name")
                            );
                            itemList.add(item_dokumen);
                            docList.add(jsonObject1.getString("pl_name"));
                            skuList.add(jsonObject1.getString("kd_barang"));

                            countPickedQty = countPickedQty + Float.parseFloat(jsonObject1.getString("picked_qty"));
                            countTotalQty = countTotalQty + Float.parseFloat(jsonObject1.getString("qty"));
                        }

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                adapter = new PickingItemListAdapter(itemList, getApplicationContext());
                                listView.setAdapter(adapter);
                                adapter.notifyDataSetChanged();

                                qtyText.setText(Float.toString(countPickedQty) + "/" + Float.toString(countTotalQty) + " pcs");
                            }
                        });

                    }
//                    spinner.setAdapter(new ArrayAdapter<String>(TallyReceivingItemSerial.this, android.R.layout.simple_spinner_dropdown_item, QC));
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;


            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }
}
