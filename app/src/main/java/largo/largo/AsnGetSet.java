package largo.largo;

import java.io.Serializable;

public class AsnGetSet implements Serializable {
    String upc, sn, sku, pcs, qty, batch, exp, last_loc, skip;

    public AsnGetSet(String upc, String sku, String pcs) {
        this.upc = upc;
        this.sku = sku;
        this.pcs = pcs;
    }

    public AsnGetSet(String sn, String pcs){
        this.sn = sn;
        this.pcs = pcs;
    }

    public AsnGetSet(String upc, String sku, String pcs, String qty) {
        this.upc = upc;
        this.sku = sku;
        this.pcs = pcs;
        this.qty = qty;
    }

    public AsnGetSet(String upc, String sku, String pcs, String qty, String last_loc, String skip) {
        this.upc = upc;
        this.sku = sku;
        this.pcs = pcs;
        this.qty = qty;
        this.last_loc = last_loc;
        this.skip = skip;
    }

    public AsnGetSet(String upc, String sn, String sku, String pcs, String qty) {
        this.upc = upc;
        this.sn = sn;
        this.sku = sku;
        this.pcs = pcs;
        this.qty = qty;
    }

    public AsnGetSet(String upc, String sn, String sku, String pcs, String qty, String batch, String exp) {
        this.upc = upc;
        this.sn = sn;
        this.sku = sku;
        this.pcs = pcs;
        this.qty = qty;
        this.batch = batch;
        this.exp = exp;
    }

    public String getLast_loc() {
        return last_loc;
    }

    public void setLast_loc(String last_loc) {
        this.last_loc = last_loc;
    }

    public String getSkip() {
        return skip;
    }

    public void setSkip(String skip) {
        this.skip = skip;
    }

    public String getUpc() {
        return upc;
    }

    public void setUpc(String upc) {
        this.upc = upc;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getPcs() {
        return pcs;
    }

    public void setPcs(String pcs) {
        this.pcs = pcs;
    }

    public String getQty() { return qty; }

    public void setQty(String qty) { this.qty = qty; }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public String getExp() {
        return exp;
    }

    public void setExp(String exp) {
        this.exp = exp;
    }
}
