package largo.largo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class Inventory extends AppCompatActivity {
    Button active_stock,cycle_count,qc_in,qc_out,transfer,transferDoc;
    ImageButton backButton;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inventory);

        backButton = findViewById(R.id.back);

        active_stock = findViewById(R.id.active_stock);
        cycle_count = findViewById(R.id.cycle_count);
        qc_in = findViewById(R.id.qc_in);
        qc_out = findViewById(R.id.qc_out);
        transfer = findViewById(R.id.transfer);
        transferDoc = findViewById(R.id.transfer_doc);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        active_stock.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        Intent intent = new Intent(Inventory.this,InventoryActiveStock.class);
                        startActivity(intent);
                    }
                }
        );

        cycle_count.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        Intent intent = new Intent(Inventory.this,InventoryCycleCount.class);
                        startActivity(intent);
                    }
                }
        );

        qc_in.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        Intent intent = new Intent(Inventory.this,InventoryQCInSerialNumber.class);
                        startActivity(intent);
                    }
                }
        );

        qc_out.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        Intent intent = new Intent(Inventory.this,InventoryQCOutSerialNumber.class);
                        startActivity(intent);
                    }
                }
        );

        transfer.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        Intent intent = new Intent(Inventory.this,InventoryTransferSN.class);
                        startActivity(intent);
                    }
                }
        );

        transferDoc.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        Intent intent = new Intent(Inventory.this,BinTransferDoc.class);
                        startActivity(intent);
                    }
                }
        );
    }

}
