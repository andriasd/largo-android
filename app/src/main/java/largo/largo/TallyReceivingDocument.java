package largo.largo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TallyReceivingDocument extends AppCompatActivity {
    String TAG = TallyReceivingDocument.class.getSimpleName();

    DatabaseHelper myDb;

    public static final String MyPREFERENCES = "MyPrefs" ;

    Button next,backButton;
    ImageButton scanButton;
    EditText receiving_doc;
    TextView header_tally;
    String param = this.getClass().getSimpleName();
    Intent intent;

    String url;

    ListView listView;
    private List<ReceivingDocumentGetterSetter> dokumenItemList;

//    ExpandableListAdapter listAdapter;
//    ExpandableListView expListView;
//    List<String> listDataHeader;
//    List<String> expand_detail = new ArrayList<String>();
//    HashMap<String, List<String>> listDataChild;


    private int lastExpandedPosition = -1;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inbound_tally_receiving_document);

        intent = getIntent();

        LocalBroadcastManager.getInstance(this).registerReceiver(qrScan, new IntentFilter("broadcastQrResult"));

        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);

        backButton = findViewById(R.id.back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        scanButton = findViewById(R.id.scanButton);

        next = findViewById(R.id.next);
        receiving_doc = findViewById(R.id.receiving_document);
        receiving_doc.setShowSoftInputOnFocus(false);

        myDb = new DatabaseHelper(this);

        getServerURL();

        listView =  findViewById(R.id.ListTallyDoc);

        dokumenItemList = new ArrayList<>();
        DataListDokument(url + "/api/receiving/get_document");

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                receiving_doc.setText(dokumenItemList.get(position).getKdReceiving());
            }
        });

        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), QRScanner.class);
                intent.putExtra("param", param);
                startActivity(intent);
            }
        });

//        expListView = (ExpandableListView)findViewById(R.id.ListTallyDoc);

//        Log.d(TAG, getDoc);

//        prepareListData(url + "/api/receiving/get_document");

//        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);
//
//        expListView.setAdapter(listAdapter);
//
//        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
//            @Override
//            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
//                return false;
//            }
//        });
//
//        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
//            @Override
//            public void onGroupExpand(int groupPosition) {
//                receiving_doc.setText(listDataHeader.get(groupPosition));
//                if (lastExpandedPosition != -1
//                        && groupPosition != lastExpandedPosition) {
//                    expListView.collapseGroup(lastExpandedPosition);
//                }
//                lastExpandedPosition = groupPosition;
////                Toast.makeText(getApplicationContext(),listDataHeader.get(groupPosition) + "Expanded", Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
//            @Override
//            public void onGroupCollapse(int groupPosition) {
//                Toast.makeText(getApplicationContext(),listDataHeader.get(groupPosition) + "Collapsed", Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
//            @Override
//            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
//
//                Toast.makeText(getApplicationContext(),listDataHeader.get(groupPosition)
//                        + ":"
//                        + listDataChild.get(
//                        listDataHeader.get(groupPosition)).get(
//                        childPosition), Toast.LENGTH_SHORT).show();
//                return false;
//            }
//        });

        receiving_doc.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    postReceivingDoc();
                }
                return false;
            }
        });


        next.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        postReceivingDoc();
                    }
                }
        );
    }

    private void postReceivingDoc() {
            StringRequest strReq = new StringRequest(Request.Method.GET,  url + "/Api_receiving/get/" + String.valueOf(receiving_doc.getText()), new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d(TAG, response.toString());

                    try {
                        JSONObject data = new JSONObject(response);
                        Integer status = data.getInt("status");

                        if (status == 200) {
                            Intent intent = new Intent(getApplicationContext(), TallyReceivingItems.class);
                            intent.putExtra("receiving_document",String.valueOf(receiving_doc.getText()));
                            startActivity(intent);
//                            finish();
                        } else if (status == 401) {
                            Toast.makeText(getApplicationContext(), "Document Not Found.", Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
//Log.d(TAG,e.getMessage());
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                    String restoredText = prefs.getString("handheldsessioncodeKey", null);

                    String usernameKey = prefs.getString("usernameKey", "No name defined");
                    String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                    Log.d(TAG, usernameKey);
                    Log.d(TAG, handheldsessioncodeKey);

                    Map<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "RFID/application/x-www-form-urlencoded");
                    headers.put("User",usernameKey);
                    headers.put("Authorization", handheldsessioncodeKey);
                    return headers;


                }
            };

            controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
        }

    private void DataListDokument(String getDoc){
//        final ArrayList<String> items = new ArrayList<String>();

        RequestQueue requestQueue=Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, getDoc, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                try{
                    JSONObject jsonObject=new JSONObject(response);
                    if(jsonObject.getInt("status")==200){
                        JSONArray jsonArray1=jsonObject.getJSONArray("results");
                        for(int i=0;i<jsonArray1.length();i++){
                            JSONObject jsonObject1=jsonArray1.getJSONObject(i);
                            ReceivingDocumentGetterSetter item_dokumen = new ReceivingDocumentGetterSetter(
                                    jsonObject1.getString("kd_receiving"),
                                    jsonObject1.getString("qty"),
                                    jsonObject1.getString("source")
                            );
                            dokumenItemList.add(item_dokumen);
                        }
                        ReceivingDokumenListAdapter adapter = new ReceivingDokumenListAdapter(dokumenItemList, getApplicationContext());

                        listView.setAdapter(adapter);

                    }
//                    spinner.setAdapter(new ArrayAdapter<String>(TallyReceivingItemSerial.this, android.R.layout.simple_spinner_dropdown_item, QC));
                }catch (JSONException e){e.printStackTrace();}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;


            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

//    private void prepareListData(String getDoc){
//        listDataHeader = new ArrayList<>();
//        listDataChild = new HashMap<String, List<String>>();
//        RequestQueue requestQueue=Volley.newRequestQueue(getApplicationContext());
//        StringRequest stringRequest=new StringRequest(Request.Method.GET, getDoc, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.d(TAG, response.toString());
//                try{
//                    JSONObject jsonObject=new JSONObject(response);
//                    if(jsonObject.getInt("status")==200){
//                        JSONArray jsonArray1=jsonObject.getJSONArray("results");
//                        for(int i=0;i<jsonArray1.length();i++){
//                            JSONObject jsonObject1=jsonArray1.getJSONObject(i);
//                            String kd_receivings1=jsonObject1.getString("kd_receiving");
//                            listDataHeader.add(kd_receivings1);
//                            String qtys = jsonObject1.getString("qty");
//                            String source = jsonObject1.getString("source");
//                            expand_detail.add(qtys + "\n" + source);
//                            listDataChild.put(listDataHeader.get(i), new ArrayList<String>(Arrays.asList(expand_detail.get(i))));
//                        }
//
//                    }
////                    spinner.setAdapter(new ArrayAdapter<String>(TallyReceivingItemSerial.this, android.R.layout.simple_spinner_dropdown_item, QC));
//                }catch (JSONException e){e.printStackTrace();}
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                error.printStackTrace();
//            }
//        }) {
//
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
//                String restoredText = prefs.getString("handheldsessioncodeKey", null);
//
//                String usernameKey = prefs.getString("usernameKey", "No name defined");
//                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
//                Log.d(TAG, usernameKey);
//                Log.d(TAG, handheldsessioncodeKey);
//
//                Map<String, String> headers = new HashMap<>();
//                headers.put("User",usernameKey);
//                headers.put("Authorization", handheldsessioncodeKey);
//                return headers;
//
//
//            }
//        };
//        int socketTimeout = 30000;
//        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//        stringRequest.setRetryPolicy(policy);
//        requestQueue.add(stringRequest);
//
//    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }

    public void showMessage(String title, String Message){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setCancelable(true);

        builder.setTitle(title);

        builder.setMessage(Message);

        builder.show();

    }

    private BroadcastReceiver qrScan = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                receiving_doc.setText(intent.getStringExtra(param));
                postReceivingDoc();
            } catch (Exception e) {
//                e.printStackTrace();
            }
        }
    };
}
