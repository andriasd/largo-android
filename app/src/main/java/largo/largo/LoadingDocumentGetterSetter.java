package largo.largo;

import java.io.Serializable;

public class LoadingDocumentGetterSetter implements Serializable {
    String pl_name, license, picking, destination_name;

    public LoadingDocumentGetterSetter(String pl_name) {
        this.pl_name = pl_name;
    }

    public String getPl_name() {
        return pl_name;
    }

    public void setPl_name(String pl_name) {
        this.pl_name = pl_name;
    }

    public String getPicking() {
        return picking;
    }

    public void setPicking(String picking) {
        this.picking = picking;
    }

    public String getDestination_name() {
        return destination_name;
    }

    public void setDestination_name(String destination_name) {
        this.destination_name = destination_name;
    }

    public LoadingDocumentGetterSetter(String pl_name, String picking, String destination_name) {
        this.pl_name = pl_name;
        this.picking = picking;
        this.destination_name = destination_name;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public LoadingDocumentGetterSetter(String pl_name, String license, String picking, String destination_name) {
        this.pl_name = pl_name;
        this.license = license;
        this.picking = picking;
        this.destination_name = destination_name;
    }


    public String getPlName() {
        return pl_name;
    }
}
