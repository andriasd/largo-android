package largo.largo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.Image;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.util.ExceptionCatchingInputStream;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AsnItem extends AppCompatActivity {

    private Button cancelButton, nextButton, qrButton, rfidButton;
    private Intent intent;
    private ListView qrList;
    private List<AsnGetSet> asnArrayList;
    private List<String> skuList;
    private AsnGetSet asnGetSet;
    private AsnListAdapter adapter;
    private EditText qrField;
    private String param, receiving_document, TAG;
    private TextView qtyText;

    DatabaseHelper myDb;
    String url;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inbound_tally_receiving_document_by_asn_item);

        TAG = this.getClass().getSimpleName();
        myDb = new DatabaseHelper(this);
        getServerURL();

        intent = getIntent();
        param = this.getClass().getSimpleName();
        receiving_document = intent.getStringExtra("receiving_document");
        skuList = new ArrayList<String>();

        try {
            if(receiving_document.length() > 0) {
                getDocumentData(url + "/api/receiving/get_document_item/" + receiving_document);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }

        cancelButton = findViewById(R.id.cancel);
        nextButton = findViewById(R.id.next);
        qrButton = findViewById(R.id.qr_button);
        rfidButton = findViewById(R.id.rfid_button);
        qtyText = findViewById(R.id.qty_text);
        qrList = findViewById(R.id.QRList);
        asnArrayList = new ArrayList<>();
        qrField = findViewById(R.id.receiving_document);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        qrButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // intent = new Intent(getApplicationContext(), AsnScanQR.class);
                intent = new Intent(getApplicationContext(), AsnDocumentItem.class);
                intent.putExtra("skuList", (Serializable) skuList);
                intent.putExtra("receiving_document", receiving_document);
                intent.putExtra("asnFClass", "AsnScanQR");
                startActivity(intent);
            }
        });

        rfidButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // intent = new Intent(getApplicationContext(), AsnScanRFID.class);
                intent = new Intent(getApplicationContext(), AsnDocumentItem.class);
                intent.putExtra("skuList", (Serializable) skuList);
                intent.putExtra("receiving_document", receiving_document);
                intent.putExtra("asnFClass", "AsnScanRFID");
                startActivity(intent);
            }
        });
    }

    public void getDocumentData(String getDoc){
        RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, getDoc, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);
                try{
                    JSONObject jsonObject=new JSONObject(response);
                    if(jsonObject.getInt("status")==200){
                        JSONArray jsonArray1=jsonObject.getJSONArray("results");
                        float countReceived = 0;
                        float countTotal = 0;
                        for(int i=0;i<jsonArray1.length();i++){
                            JSONObject jsonObject1=jsonArray1.getJSONObject(i);
                            countReceived = countReceived + Float.parseFloat(jsonObject1.getString("received_qty"));
                            countTotal = countTotal + Float.parseFloat(jsonObject1.getString("qty"));

                            asnGetSet = new AsnGetSet(jsonObject1.getString("item_code"), jsonObject1.getString("received_qty") + "/" + jsonObject1.getString("qty"), "1");
                            skuList.add(jsonObject1.getString("item_code").toUpperCase());
                            asnArrayList.add(asnGetSet);
                        }

                        adapter = new AsnListAdapter(asnArrayList, getApplicationContext(), 2);
                        qrList.setAdapter(adapter);
                        qtyText.setText(countReceived + "/" + countTotal + " PCS");

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;


            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }

    private BroadcastReceiver refreshReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if(intent.getStringExtra("refresh").equals("true")) {
                    /*finish();
                    Intent intentRefresh = new Intent(getApplicationContext(), AsnItem.class);
                    intentRefresh.putExtra("receiving_document", receiving_document);
                    startActivity(intentRefresh);
                    getIntent().removeExtra("refresh");*/
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
}
