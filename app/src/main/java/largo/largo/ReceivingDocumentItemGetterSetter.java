package largo.largo;

import java.io.Serializable;

public class ReceivingDocumentItemGetterSetter implements Serializable {
    String receiving_code, item_ori, item_id, location, outbound_code, item_code, item_name, qty, nama_satuan, source, image, default_qty, received_qty, has_expdate, has_batch, has_qty;

    public ReceivingDocumentItemGetterSetter(String receiving_code, String item_code, String item_name, String qty, String nama_satuan, String source, String image, String default_qty, String received_qty, String has_expdate, String has_batch, String has_qty) {
        this.receiving_code = receiving_code;
        this.item_code = item_code;
        this.item_name = item_name;
        this.qty = qty;
        this.nama_satuan = nama_satuan;
        this.source = source;
        this.image = image;
        this.default_qty = default_qty;
        this.received_qty = received_qty;
        this.has_expdate = has_expdate;
        this.has_batch = has_batch;
        this.has_qty = has_qty;
    }

    public ReceivingDocumentItemGetterSetter(String receiving_code, String outbound_code, String item_code, String item_name, String qty, String nama_satuan, String source, String image, String default_qty, String received_qty, String has_expdate, String has_batch, String has_qty, String item_id) {
        this.receiving_code = receiving_code;
        this.outbound_code = outbound_code;
        this.item_code = item_code;
        this.item_name = item_name;
        this.qty = qty;
        this.nama_satuan = nama_satuan;
        this.source = source;
        this.image = image;
        this.default_qty = default_qty;
        this.received_qty = received_qty;
        this.has_expdate = has_expdate;
        this.has_batch = has_batch;
        this.has_qty = has_qty;
        this.item_id = item_id;
    }

    public ReceivingDocumentItemGetterSetter(String receiving_code, String outbound_code, String item_code, String item_name, String qty, String nama_satuan, String source, String image, String default_qty, String received_qty, String has_expdate, String has_batch, String has_qty, String item_id, String item_ori) {
        this.receiving_code = receiving_code;
        this.outbound_code = outbound_code;
        this.item_code = item_code;
        this.item_name = item_name;
        this.qty = qty;
        this.nama_satuan = nama_satuan;
        this.source = source;
        this.image = image;
        this.default_qty = default_qty;
        this.received_qty = received_qty;
        this.has_expdate = has_expdate;
        this.has_batch = has_batch;
        this.has_qty = has_qty;
        this.item_id = item_id;
        this.item_ori = item_ori;
    }

    public String getHas_qty() {
        return has_qty;
    }

    public void setHas_qty(String has_qty) {
        this.has_qty = has_qty;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getOutbound_code(){return  outbound_code;}

    public void setOutbound_code(String outbound_code){this.outbound_code = outbound_code;}

    public String getHas_expdate() {
        return has_expdate;
    }

    public void setHas_expdate(String has_expdate) {
        this.has_expdate = has_expdate;
    }

    public String getHas_batch() {
        return has_batch;
    }

    public void setHas_batch(String has_batch) {
        this.has_batch = has_batch;
    }

    public String getReceiving_code() {
        return receiving_code;
    }

    public void setReceiving_code(String receiving_code) {
        this.receiving_code = receiving_code;
    }

    public String getItem_code() {
        return item_code;
    }

    public void setItem_code(String item_code) {
        this.item_code = item_code;
    }

    public String getItem_ori() {
        return item_ori;
    }

    public void setItem_ori(String item_ori) {
        this.item_ori = item_ori;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getNama_satuan() {
        return nama_satuan;
    }

    public void setNama_satuan(String nama_satuan) {
        this.nama_satuan = nama_satuan;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDefault_qty() {
        return default_qty;
    }

    public void setDefault_qty(String default_qty) {
        this.default_qty = default_qty;
    }

    public String getReceived_qty() {
        return received_qty;
    }

    public void setReceived_qty(String received_qty) {
        this.received_qty = received_qty;
    }
}
