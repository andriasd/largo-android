package largo.largo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.database.Cursor;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class TallyReceivingItemSerial extends AppCompatActivity {
    DatabaseHelper myDb;

    String TAG = TallyReceivingItemSerial.class.getSimpleName();
    String param = this.getClass().getSimpleName();
    Button next, backButton, detail, tallied;
    ImageButton scanButton;
    ToggleButton toggle_license_plating;
    Spinner spinner;
    ArrayList<String> QC;
    String url,check_qty;

    EditText batch,expired_dates,serial_number,qty;
    TextView tvBatch,tvExpiredDate,tvQty,receiving_doc,name,tallied_sku,tallied_item_name,tallied_qty;

    public static final String MyPREFERENCES = "MyPrefs" ;

    Intent intent;
    Bundle bundle;

    final Calendar myCalendar = Calendar.getInstance();

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inbound_tally_receiving_item_serial);

        backButton = findViewById(R.id.back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        myDb = new DatabaseHelper(this);

        getServerURL();

        intent = getIntent();
        final String receiving_document = intent.getStringExtra("receiving_document");
        final String sku = intent.getStringExtra("sku");
        final String item_name = intent.getStringExtra("item_name");
        final String license_plating = intent.getStringExtra("license_plating");
        final String has_batch = intent.getStringExtra("has_batch");
        final String has_expdate = intent.getStringExtra("has_expdate");
        final String has_qty = intent.getStringExtra("has_qty");
        final String qty_ = intent.getStringExtra("qty");
        final String def_qty = intent.getStringExtra("def_qty");

        LocalBroadcastManager.getInstance(this).registerReceiver(qrScan, new IntentFilter("broadcastQrResult"));

        scanButton = findViewById(R.id.scanButton);
        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), QRScanner.class);
                intent.putExtra("param", param);
                startActivity(intent);
            }
        });

        tvBatch = findViewById(R.id.tvBatch);
        tvExpiredDate = findViewById(R.id.tvExpiredDate);
        tvQty = findViewById(R.id.tvQty);

        batch = findViewById(R.id.batch);
        batch.setShowSoftInputOnFocus(false);
        serial_number = findViewById(R.id.serial_number);
        serial_number.setShowSoftInputOnFocus(false);
        expired_dates = (EditText)findViewById(R.id.expired_date);
        expired_dates.setShowSoftInputOnFocus(false);
        qty = findViewById(R.id.etQty);
        qty.setShowSoftInputOnFocus(false);

        tallied_qty = findViewById(R.id.tallied_qty);

        if(has_batch.equals("0")){
            tvBatch.setVisibility(View.GONE);
            batch.setVisibility(View.GONE);
            batch.setInputType(InputType.TYPE_NULL);
        }

        if(has_expdate.equals("0")){
            tvExpiredDate.setVisibility(View.GONE);
            expired_dates.setVisibility(View.GONE);
            expired_dates.setInputType(InputType.TYPE_NULL);
        }

        getTalliedQty();

//        SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
//        final String item_name = prefs.getString("item_name", "No name defined");
//        Integer qtys = prefs.getInt("qty", 0);

        receiving_doc = (TextView)findViewById(R.id.receiving_doc);

        receiving_doc.setText(receiving_document);

        name = (TextView)findViewById(R.id.item_name);

        name.setText(item_name);

        detail = findViewById(R.id.detail);

        QC = new ArrayList<>();
        spinner = (Spinner)findViewById(R.id.qc);

        loadSpinnerData(url+"/Api_receiving/get_qc/");

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String qc =   spinner.getItemAtPosition(spinner.getSelectedItemPosition()).toString();
//                Toast.makeText(getApplicationContext(),qc,Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                Log.d("month",String.valueOf(monthOfYear));
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        expired_dates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(TallyReceivingItemSerial.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        toggle_license_plating = (ToggleButton) findViewById(R.id.toggle_license_plating);

        SharedPreferences sharedPrefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        toggle_license_plating.setChecked(sharedPrefs.getBoolean("license_plating_switch", false));

        toggle_license_plating.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(toggle_license_plating.isChecked()) {
                            SharedPreferences.Editor editor = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE).edit();
                            editor.putBoolean("license_plating_switch", true);
                            editor.commit();
                            Intent intent = new Intent(TallyReceivingItemSerial.this,LicensePlating.class);
                            intent.putExtra("receiving_document",receiving_document);
                            intent.putExtra("sku",sku);
                            startActivity(intent);
                        }
                        else{
                            SharedPreferences preferences = getSharedPreferences(MyPREFERENCES, 0);
                            preferences.edit().remove("license_plating_switch").commit();
                        }
                    }
                }
        );

        serial_number.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    checkQty();
                    if(has_qty.equals("1")) {
                        if(Float.parseFloat(check_qty) >= Float.parseFloat(intent.getStringExtra("qty"))) {
                            // showMessage("Alert", "Seluruh item sudah di proses tally !");
							Toast.makeText(getApplicationContext(), "Seluruh item sudah di proses tally !", Toast.LENGTH_SHORT).show();
                        }

                        else {

                            if (Float.parseFloat(qty.getText().toString()) <= 0) {
                                // showMessage("Error", "Quantity yang diterima harus lebih dari 0 !");
								Toast.makeText(getApplicationContext(), "Quantity yang diterima harus lebih dari 0 !", Toast.LENGTH_SHORT).show();
                            } else if (Float.parseFloat(qty.getText().toString()) > Float.parseFloat(intent.getStringExtra("def_qty"))) {
                                // showMessage("Error", "Quantity yang diterima melebihi quantity yang dikirim !");
								Toast.makeText(getApplicationContext(), "Quantity yang diterima melebihi quantity yang dikirim !", Toast.LENGTH_SHORT).show();
                            } else {
                                checkSerialNumber();
                            }
                        }
                    }else{
                        if(Float.parseFloat(check_qty) >= Float.parseFloat(intent.getStringExtra("qty"))) {
                            // showMessage("Alert", "Seluruh item sudah di proses tally !");
							Toast.makeText(getApplicationContext(), "Seluruh item sudah di proses tally !", Toast.LENGTH_SHORT).show();
                        }else {
                            checkSerialNumber();
                        }
                    }
                }
                return false;
            }
        });

        detail.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(),TallyReceivingItemSerialDetail.class);
                        intent.putExtra("receiving_document",receiving_document);
                        intent.putExtra("sku",sku);
                        intent.putExtra("item_name",item_name);
                        intent.putExtra("qty",qty_);
                        intent.putExtra("has_batch",has_batch);
                        intent.putExtra("has_expdate",has_expdate);
                        intent.putExtra("has_qty",has_qty);
                        intent.putExtra("def_qty",def_qty);
                        startActivity(intent);
                    }
                }
        );

//        tallied = findViewById(R.id.tallied);
//
//        tallied.setOnClickListener(
//                new View.OnClickListener(){
//                    @Override
//                    public void onClick(View v){
//                        Intent intent = new Intent(TallyReceivingItemSerial.this,TallyReceivingItemSerial.class);
//                        startActivity(intent);
//                    }
//                }
//        );
    }

//    public void toggleclick(View v){
//        if(toggle_license_plating.isChecked()) {
//            Toast.makeText(TallyReceivingItemSerial.this, "ON", Toast.LENGTH_SHORT).show();
//            Intent intent = new Intent(TallyReceivingItemSerial.this,LicensePlating.class);
//            startActivity(intent);
//        }
//        else{
//            Toast.makeText(TallyReceivingItemSerial.this, "OFF", Toast.LENGTH_SHORT).show();
//        }
//    }

    private void updateLabel() {
        String myFormat = "yyyy/MM/dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        expired_dates.setText(sdf.format(myCalendar.getTime()));
        expired_dates.setError(null);
    }

    private void loadSpinnerData(String getSpinner) {
        RequestQueue requestQueue=Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, getSpinner, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                Log.d(TAG, response.toString());
                try{
                    JSONObject jsonObject=new JSONObject(response);
                    if(jsonObject.getInt("status")==200){
                        JSONArray jsonArray=jsonObject.getJSONArray("results");
                        for(int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            String qc=jsonObject1.getString("kd_qc");
                            QC.add(qc);
                        }
                    }
                    spinner.setAdapter(new ArrayAdapter<String>(TallyReceivingItemSerial.this, android.R.layout.simple_spinner_dropdown_item, QC));
                }catch (JSONException e){e.printStackTrace();}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void checkSerialNumber() {
        StringRequest strReq = new StringRequest(Request.Method.GET, url + "/Api_receiving/get_serial/" + intent.getStringExtra("receiving_document") + "/" + serial_number.getText().toString(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());

                try {
                    JSONObject data = new JSONObject(response);

                    String status = data.getString("status");
                    String message = data.getString("message");

                    if(status.equals("AVAILABLE")){
                        postReceivingSerialNumber();
                    }else{
                        // showMessage("Alert",message);
						Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    // showMessage("Error",e.getMessage());
					Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                // showMessage("Error",error.getMessage());
				Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "RFID/application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;

            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);

    }

    private void postReceivingSerialNumber() {
            StringRequest strReq = new StringRequest(Request.Method.POST, url + "/Api_receiving/post/", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d(TAG, response.toString());

                    try {
                        final String has_batch = intent.getStringExtra("has_batch");
                        final String has_qty = intent.getStringExtra("has_qty");
//                        final String has_expdate = intent.getStringExtra("has_expdate");

                        batch = findViewById(R.id.batch);

                        expired_dates = (EditText) findViewById(R.id.expired_date);

                        qty = findViewById(R.id.etQty);

                        serial_number = findViewById(R.id.serial_number);

                        if (batch.getText().toString().equalsIgnoreCase("") && has_batch.equals("1")) {
                            batch.setHint("please enter batch");//it gives user to hint
                            batch.setError("please enter batch");//it gives user to info message //use any one //
                        } else if (expired_dates.getText().toString().equalsIgnoreCase("") && has_batch.equals("1")) {
                            expired_dates.setHint("please enter shelf life");//it gives user to hint
                            expired_dates.setError("please enter shelf life");//it gives user to info message //use any one //
                        } else if (qty.getText().toString().equalsIgnoreCase("") && has_qty.equals("1")) {
                            qty.setHint("please enter quantity");//it gives user to hint
                            qty.setError("please enter quantity");//it gives user to info message //use any one //
                        } else if (serial_number.getText().toString().equalsIgnoreCase("")) {
                            serial_number.setHint("please enter serial number");//it gives user to hint
                            serial_number.setError("please enter serial number");//it gives user to info message //use any one //
                        } else {
                            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    String qc = spinner.getItemAtPosition(spinner.getSelectedItemPosition()).toString();
//                Toast.makeText(getApplicationContext(),qc,Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {
                                    // DO Nothing here
                                }
                            });

                            JSONObject data = new JSONObject(response);
                            Integer status = data.getInt("status");
                            String message = data.getString("message");

//                            Log.d(TAG,status.toString());

                            if (status == 200) {

                                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);

                                String usernameKey = prefs.getString("usernameKey", "No name defined");

                            } else if (status == 401) {
                                // showMessage("Error",message);
								Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            }
                        }

                    } catch (JSONException e) {
                        // showMessage("Error",e.getMessage());
						Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                    // showMessage("Error",error.getMessage());
					Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }) {

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                    String usernameKey = prefs.getString("usernameKey", "No name defined");

                    final String receiving_document = intent.getStringExtra("receiving_document");
                    final String sku = intent.getStringExtra("sku");


                    if (intent.getStringExtra("license_plating") != null) {
                        String license_plating = intent.getStringExtra("license_plating");

                        Date cDate = new Date();
                        String fDate = new SimpleDateFormat("yyyy-MM-dd").format(cDate);

                        params.put("receiving_cod e", receiving_document);
                        params.put("item_code", sku);
                        params.put("serial_number", String.valueOf(serial_number.getText()));
                        params.put("tgl_exp", expired_dates.getText().toString());
                        params.put("qty", String.valueOf(qty.getText()));
                        params.put("qc_code", spinner.getItemAtPosition(spinner.getSelectedItemPosition()).toString());
                        params.put("tgl_in", fDate);
                        params.put("uname", usernameKey);
                        params.put("parent_code", license_plating);
                    } else {
                        String license_plating = "";

                        Date cDate = new Date();
                        String fDate = new SimpleDateFormat("yyyy-MM-dd").format(cDate);

                        params.put("receiving_code", receiving_document);
                        params.put("item_code", sku);
                        params.put("serial_number", String.valueOf(serial_number.getText()));
                        params.put("tgl_exp", expired_dates.getText().toString());
                        params.put("qty", String.valueOf(qty.getText()));
                        params.put("qc_code", spinner.getItemAtPosition(spinner.getSelectedItemPosition()).toString());
                        params.put("tgl_in", fDate);
                        params.put("uname", usernameKey);
                        params.put("parent_code", license_plating);
                    }

                    Log.d(TAG, params.toString());

                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                    String restoredText = prefs.getString("handheldsessioncodeKey", null);

                    String usernameKey = prefs.getString("usernameKey", "No name defined");
                    String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                    Log.d(TAG, usernameKey);
                    Log.d(TAG, handheldsessioncodeKey);

                    Map<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "application/x-www-form-urlencoded");
                    headers.put("User", usernameKey);
                    headers.put("Authorization", handheldsessioncodeKey);
                    return headers;

                }
            };

            controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);

    }

    private void checkQty(){
        Cursor res1 = myDb.getQtyTalliedItem(intent.getStringExtra("receiving_document"),intent.getStringExtra("sku"));

        check_qty = "0";

        while (res1.moveToNext()) {
//            buffer.append("Data :"+ res1.getString(0)+"\n");

            check_qty = res1.getString(0);
        }

    }

    private void getTalliedQty(){

        if(intent.getStringExtra("has_qty").equals("0")){
            tvQty.setVisibility(View.GONE);
            qty.setVisibility(View.GONE);
            qty.setInputType(InputType.TYPE_NULL);
            tallied_qty.setText("0/" + intent.getStringExtra("qty"));
        }
        else{
            qty.setText(intent.getStringExtra("def_qty"));
            tallied_qty.setText("0/" + intent.getStringExtra("def_qty"));
        }

        Cursor res1 = myDb.getQtyTalliedItem(intent.getStringExtra("receiving_document"),intent.getStringExtra("sku"));

        tallied_qty = findViewById(R.id.tallied_qty);

//        if(res1.getCount() == 0) {
//
//            // show message
//
//            showMessage("Error","Nothing Found");
//
//            return;
//
//        }

//        StringBuffer buffer = new StringBuffer();

//        showMessage("Error","error");

        while (res1.moveToNext()) {
//            buffer.append("Data :"+ res1.getString(0)+"\n");

            tallied_qty.setText(res1.getString(0) + "/" + intent.getStringExtra("qty"));
        }

//        showMessage("Data",buffer.toString());
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }

    public void showMessage(String title, String Message){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setCancelable(true);

        builder.setTitle(title);

        builder.setMessage(Message);

        builder.show();

    }

    private BroadcastReceiver qrScan = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                serial_number.setText(intent.getStringExtra(param));
            } catch (Exception e) {
//                e.printStackTrace();
            }
        }
    };
}
