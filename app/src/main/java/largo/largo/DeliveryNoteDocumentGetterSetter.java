package largo.largo;

import java.io.Serializable;

public class DeliveryNoteDocumentGetterSetter implements Serializable {
    String dn_name;
    public DeliveryNoteDocumentGetterSetter(String dn_name) {
        this.dn_name = dn_name;
    }

    public String getDn_name(){return dn_name;}
    public void setDn_name(String dn_name){ this.dn_name = dn_name;}
}
