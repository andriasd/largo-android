package largo.largo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.support.v7.widget.Toolbar;
import android.widget.ImageButton;
import android.widget.TextView;

public class Inbound extends AppCompatActivity {
    Button tally;
    Button putaway;
    Button asn;
    Button routing;
    ImageButton back;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
            setContentView(R.layout.inbound);

        routing = findViewById(R.id.routing);
        putaway = findViewById(R.id.putaway);
        asn = findViewById(R.id.asn);
//        routing = findViewById(R.id.routing);
        back = findViewById(R.id.back);

        back.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
//                        Intent intent = new Intent(Inbound.this,MenuActivity.class);
//                        startActivity(intent);
                        finish();
                    }
                }
        );

        routing.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        Intent intent = new Intent(Inbound.this,RoutingScanQR.class);
                        startActivity(intent);
                    }
                }
        );



        putaway.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        Intent intent = new Intent(Inbound.this,PutawaySerialNumber.class);
                        startActivity(intent);
                    }
                }
        );

        asn.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        Intent intent = new Intent(Inbound.this,Asn.class);
                        startActivity(intent);
                    }
                }
        );

//        routing.setOnClickListener(
//                new View.OnClickListener(){
//                    @Override
//                    public void onClick(View v){
//                        Intent intent = new Intent(Inbound.this,Asn.class);
//                        startActivity(intent);
//                    }
//                }
//        );
    }
}
