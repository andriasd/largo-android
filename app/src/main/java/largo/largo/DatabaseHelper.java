package largo.largo;

import android.content.ContentValues;

import android.content.Context;

import android.database.Cursor;

import android.database.sqlite.SQLiteDatabase;

import android.database.sqlite.SQLiteOpenHelper;

import org.w3c.dom.Text;


/**

 * Created by asus on 02/04/2017.

 */



public class DatabaseHelper extends SQLiteOpenHelper{

    //nama database

    public static final String DATABASE_NAME = "Largo.db";

    //nama table

    public static final String TABLE_NAME = "ms_tallied_items";

    public static final String TABLE_NAME2 = "ms_putaway_items";

    public static final String TABLE_NAME3 = "ms_picking_items";

    public static final String TABLE_NAME4 = "ms_server_api";

    public static final String TR_HISTORY_LICENSE_PLATING = "tr_history_license_plating";

    //versi database

    private static final int DATABASE_VERSION = 1;

    //table field

    public static final String DOC = "DOCUMENT";

    public static final String COL_1 = "SKU";

    public static final String COL_2 = "STATUS";

    public static final String COL_3 = "BATCH";

    public static final String COL_4 = "SHELF_LIFE";

    public static final String TALLIED_QTY = "TALLIED_QTY";

    public static final String COL_5 = "QTY";

    public static final String COL_6 = "SERIAL_NUMBER";

    public static final String COL_7 = "TALLIED_TIME";

    public static final String COL_8 = "USER_TALLIED";

    public static final String COL_9 = "STATE";

    //

    public static final String COL_10 = "SKU";

    public static final String ITEM_NAME = "ITEM_NAME";

    public static final String COL_11 = "SERIAL_NUMBER";

    public static final String COL_12 = "PUT_QTY";

    public static final String COL_13 = "LOC";

    public static final String COL_14 = "PUT_TIME";

    public static final String COL_15 = "USER_PUT";

    public static final String COL_16 = "STATE";

    //

    public static final String COL_17 = "SKU";

    public static final String COL_18 = "SERIAL_NUMBER";

    public static final String DEF_PICKING_QTY = "QTY";

    public static final String COL_19 = "PICK_QTY";

    public static final String COL_20 = "PICK_TIME";

    public static final String COL_21 = "PICK_USER";

    public static final String COL_22 = "STATE";

    //

    public static final String COL_23 = "URL";

    public static final String COL_24 = "STATE";

    //

    public static final String license_plat_doc = "DOC";

    public static final String license_plat_item_code = "SKU";

    public static final String license_plat_number = "LICENSE_PLAT_NUMBER";

    public static final String license_plat_state = "STATE";

    public DatabaseHelper(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        SQLiteDatabase db = this.getWritableDatabase();

    }



    @Override

    public void onCreate(SQLiteDatabase db) {

        db.execSQL("create table ms_tallied_items(document string not null," +

                "sku string not null," +

                "status text null," +

                "batch string null," +

                "shelf_life text null," +

                "tallied_qty int not null default 1," +

                "qty int not null," +

                "serial_number text null," +

                "tallied_time DATETIME DEFAULT (datetime('now','localtime'))," +

                "user_tallied string not null," +

                "state int not null default 1);");

        db.execSQL("create table ms_putaway_items(sku string not null," +

                "item_name string null," +

                "serial_number text null," +

                "put_qty integer not null  default 1," +

                "loc string null," +

                "put_time DATETIME DEFAULT (datetime('now','localtime'))," +

                "user_put string not null," +

                "state int not null default 1);");

        db.execSQL("create table ms_picking_items(sku string not null," +

                "serial_number text not null," +

                "pick_qty int not null," +

                "qty int not null default 1," +

                "pick_time DATETIME DEFAULT (datetime('now','localtime'))," +

                "pick_user string not null," +

                "state int not null default 1);");

        db.execSQL("create table ms_server_api(url string not null," +

                "state int not null default 1);");

        db.execSQL("create table tr_history_license_plating(doc string not null," +
                "item_code string not null," +

                "licence_plat_number string not null," +

                "state int not null default 1);");

    }


    @Override

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_NAME);

        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_NAME2);

        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_NAME3);

        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_NAME4);

        db.execSQL("DROP TABLE IF EXISTS "+ TR_HISTORY_LICENSE_PLATING);

        onCreate(db);

    }



    //metode untuk tambah data

    public boolean insertData(String doc, String sku, String status, String batch, String shelf_life, Integer qty, Integer tallied_qty, String serial_number, String user) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(DOC,doc);

        contentValues.put(COL_1,sku);

        contentValues.put(COL_2,status);

        if(batch != "") {
            contentValues.put(COL_3, batch);
        }else{
            contentValues.putNull(COL_3);
        }

        if(shelf_life != ""){
            contentValues.put(COL_4,shelf_life);
        }else{
            contentValues.putNull(COL_4);
        }

        contentValues.put(COL_5,qty);

        contentValues.put(COL_6,serial_number);

        contentValues.put(COL_8,user);

        contentValues.put(TALLIED_QTY,tallied_qty);

        db.execSQL("UPDATE ms_tallied_items SET state = 0 WHERE serial_number = '"+ serial_number + "'");

        long result = db.insert(TABLE_NAME, null, contentValues);

        if(result == -1)

            return false;

        else

            return true;

    }

    public boolean insertDataPutaway(String sku, String serial_number, String item_name, String loc, String user) {

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor qty = db.rawQuery("select put_qty from ms_putaway_items where sku= '" + sku + "' AND serial_number = '" + serial_number + "' and state = 1", null);

        ContentValues contentValues = new ContentValues();

        contentValues.put(COL_10,sku);

        contentValues.put(COL_11,serial_number);

        while (qty.moveToNext()) {
            contentValues.put(COL_12, qty.getInt(0) + 1);
        }

        contentValues.put(ITEM_NAME,item_name);

        contentValues.put(COL_13,loc);

        contentValues.put(COL_15,user);

        db.execSQL("UPDATE ms_putaway_items SET state = 0 WHERE serial_number = '"+ serial_number + "'");

        long result = db.insert(TABLE_NAME2, null, contentValues);

        if(result == -1)

            return false;

        else

            return true;

    }

    public boolean insertDataPicking(String sku, String serial_number, float pick_qty, String user) {

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor qty = db.rawQuery("select qty from ms_picking_items where sku= '" + sku + "' AND serial_number = '" + serial_number + "'", null);

        ContentValues contentValues = new ContentValues();

        contentValues.put(COL_17,sku);

        contentValues.put(COL_18,serial_number);

        while (qty.moveToNext()) {
            contentValues.put(DEF_PICKING_QTY, qty.getInt(0) + 1);
        }

        contentValues.put(COL_19,pick_qty);

        contentValues.put(COL_21,user);

        db.execSQL("UPDATE ms_picking_items SET state = 0 WHERE serial_number = '"+ serial_number + "'");

        long result = db.insert(TABLE_NAME3, null, contentValues);

        if(result == -1)

            return false;

        else

            return true;

    }

    public boolean insertLicensePlat(String doc, String sku, String serial_number) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(license_plat_doc,doc);

        contentValues.put(license_plat_item_code,sku);

        contentValues.put(license_plat_number,serial_number);

        long result = db.insert(TR_HISTORY_LICENSE_PLATING, null, contentValues);

        if(result == -1)

            return false;

        else

            return true;

    }



    //metode untuk mengambil data

    public Cursor getTalliedItem(String doc, String sku) {

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor res = db.rawQuery("select * from ms_tallied_items where document= '" + doc + "' AND sku = '" + sku + "' and state = 1", null);

        return res;

    }

    public Cursor getGoodTalliedItem(String doc, String sku) {

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor res = db.rawQuery("select * from ms_tallied_items where status = 'GOOD' AND document= '" + doc + "' AND sku = '" + sku + "'" , null);

        return res;

    }

    public Cursor getNgTalliedItem(String doc, String sku) {

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor res = db.rawQuery("select * from ms_tallied_items where status = 'NG' AND document= '" + doc + "' AND sku = '" + sku + "'", null);

        return res;

    }

    public Cursor getQcTalliedItem(String doc, String sku) {

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor res = db.rawQuery("select * from ms_tallied_items where status = 'QC HOLD' AND document= '" + doc + "' AND sku = '" + sku + "'", null);

        return res;

    }

    public Cursor getQtyTalliedItem(String doc, String sku) {

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor res = db.rawQuery("select coalesce(sum(qty),0) from ms_tallied_items where document= '" + doc + "' AND sku = '" + sku + "' and state = 1 group by document,sku", null);

        return res;

    }

    public Cursor getTotalTalliedItem(String doc, String sku) {

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor res = db.rawQuery("select count(*) from ms_tallied_items where document= '" + doc + "' AND sku = '" + sku + "' and state = 1", null);

        return res;

    }

    public Cursor getTotalGoodTalliedItem(String doc, String sku) {

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor res = db.rawQuery("select count(*) from ms_tallied_items where status = 'GOOD' AND document= '" + doc + "' AND sku = '" + sku + "' and state = 1", null);

        return res;

    }

    public Cursor getTotalNgTalliedItem(String doc, String sku) {

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor res = db.rawQuery("select count(*) from ms_tallied_items where status = 'NG' AND document= '" + doc + "' AND sku = '" + sku + "' and state = 1", null);

        return res;

    }

    public Cursor getTotalQcTalliedItem(String doc, String sku) {

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor res = db.rawQuery("select count(*) from ms_tallied_items where status = 'QC HOLD' AND document= '" + doc + "' AND sku = '" + sku + "' and state = 1", null);

        return res;

    }


    public Cursor getPutawayItem(String user) {

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor res = db.rawQuery("select * from ms_putaway_items where user_put ='"+user+"' AND state = 1", null);

        return res;

    }

    public Cursor getQtyPutawayItem(String user, String sku) {

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor res = db.rawQuery("select coalesce(ms_putaway_items.put_qty,0), coalesce(ms_tallied_items.qty,0) from ms_tallied_items left join ms_putaway_items on ms_tallied_items.sku = ms_putaway_items.sku and ms_tallied_items.state = 1 and ms_putaway_items.state = 1 where user_tallied = '"+ user + "' and ms_tallied_items.sku = '" + sku + "'", null);

        return res;

    }

    public Cursor getTotalSKUTalliedItem(String serial_number) {

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor res = db.rawQuery("select count(*) from ms_tallied_items where serial_number = " + serial_number, null);

        return res;

    }

    public Cursor getPickingItem(String user) {

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor res = db.rawQuery("select * from ms_picking_items where pick_user = '" + user + "' and state = 1", null);

        return res;

    }

    public Cursor getOldLoc(String serial_number) {

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor res = db.rawQuery("select loc from ms_picking_items where serial_number = " + serial_number + " and state = 1", null);

        return res;

    }

    public Cursor getSNTalliedItem(String sn) {

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor res = db.rawQuery("select * from ms_tallied_items where serial_number ='" + sn+ "'", null);

        return res;

    }

    public Cursor getSNPutawayItem(String sn) {

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor res = db.rawQuery("select * from ms_putaway_items where serial_number ='" + sn+ "' and state = 1", null);

        return res;

    }

    public boolean setServerAPI(String url) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(COL_23,url);

        String update = "UPDATE ms_server_api SET state = 0";

        long result = db.insert(TABLE_NAME4, null, contentValues);

        if(result == -1)

            return false;

        else

            return true;

    }

    public Cursor getServerAPI() {

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor res = db.rawQuery("select url from ms_server_api where state = 1", null);

        return res;

    }

    /* ASN */
    /*public boolean setAsnScanQR() {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(COL_23,url);

        String update = "UPDATE ms_server_api SET state = 0";

        long result = db.insert(TABLE_NAME4, null, contentValues);

        if(result == -1)
            return false;
        else
            return true;
    }*/

}
