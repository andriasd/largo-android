package largo.largo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class PickingItemsListAdapter extends ArrayAdapter<PickingItemsGetterSetter> {

    private List<PickingItemsGetterSetter> pickingItemList;

    private Context context;

    public PickingItemsListAdapter(List<PickingItemsGetterSetter> pickingItemList, Context context) {
        super(context, R.layout.list_view_picking_items, pickingItemList);
        this.pickingItemList = pickingItemList;
        this.context = context;
    }



    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);

        View listViewItem = inflater.inflate(R.layout.list_view_picking_items, null, true);

        TextView textViewPickingItem = listViewItem.findViewById(R.id.textViewPickingItem);
        TextView textViewQty = listViewItem.findViewById(R.id.textViewQty);
        TextView textViewSerialNumber = listViewItem.findViewById(R.id.textViewSerialNumber);


        PickingItemsGetterSetter pickingItem = pickingItemList.get(position);

        textViewPickingItem.setText(String.valueOf(pickingItem.getSku()));
        textViewQty.setText(pickingItem.getQty() +"/"+pickingItem.getPickQty());
        textViewSerialNumber.setText(pickingItem.getSerialNumber());

        return listViewItem;
    }
}