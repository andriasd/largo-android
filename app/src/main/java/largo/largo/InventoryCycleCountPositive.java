package largo.largo;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InventoryCycleCountPositive extends AppCompatActivity {

    private Button cancelButton, doneButton;
    private ImageButton scanOLButton;
    private EditText asnQr, asnQty, asnOL;
    private ListView qrList;
    private List<AsnGetSet> asnArrayList;
    private List<String> tagList;
    private AsnGetSet asnGetSet;
    private AsnListAdapter adapter;
    private String param, TAG, cc_document, warehouse_name, cc_type, type, serial_number, outer_label;
    private String fClass = this.getClass().getSimpleName();
    private Intent intent;
    private AsnScanRFID asnScanRFID;

    String type1 = "";
    String kd_qc = "CC HOLD";
    String tgl_exp = "";
    String tgl_in = "";
    String putaway_time = "";

    private JSONObject paramsJson;
    public static final Map<String, String> params = new HashMap<String, String>();
    public static final String MyPREFERENCES = "MyPrefs" ;

    AsnItem asnItem;
    DatabaseHelper myDb;
    String url;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cycle_count_post_positive);

        asnScanRFID = new AsnScanRFID();
        paramsJson = new JSONObject();

        TAG = this.getClass().getSimpleName();
        myDb = new DatabaseHelper(this);
        getServerURL();

        intent = getIntent();
        param = this.getClass().getSimpleName();

        tagList = new ArrayList<>();
        if(intent.getSerializableExtra("tagList") != null) {
            tagList = (List<String>) intent.getSerializableExtra("tagList");
        }
        cc_document = intent.getStringExtra("cc_document");
        warehouse_name = intent.getStringExtra("warehouse_name");
        cc_type = intent.getStringExtra("cc_type");
        type = intent.getStringExtra("type");
        serial_number = intent.getStringExtra("asnQR");
        outer_label = intent.getStringExtra("asnOL");

        cancelButton = findViewById(R.id.cancel);
        doneButton = findViewById(R.id.done);
        scanOLButton = findViewById(R.id.scanLocButton);

        asnQr = findViewById(R.id.asnQr);
        asnOL = findViewById(R.id.asnQrLoc);
        asnQr.setShowSoftInputOnFocus(false);
        asnOL.requestFocus();
        asnOL.setSelection(0);
        asnQr.setText(serial_number.toUpperCase());

        asnQty = findViewById(R.id.asnQty);
        asnQty.setText("1");

        qrList = findViewById(R.id.QRList);

        asnArrayList = new ArrayList<>();
        adapter = new AsnListAdapter(asnArrayList, getApplicationContext(), 2);
        qrList.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        if(intent.getSerializableExtra("asnGetSet") != null) {
            asnGetSet = (AsnGetSet) intent.getSerializableExtra("asnGetSet");
        }

        if(intent.getSerializableExtra("asnArrayList") != null) {
            asnArrayList = (List<AsnGetSet>) intent.getSerializableExtra("asnArrayList");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    asnQr.setText("");
                    asnQr.setFocusableInTouchMode(true);
                    asnQr.setFocusable(true);
                    asnQr.requestFocus();
                    asnQty.setText("1");
                    adapter = new AsnListAdapter(asnArrayList, getApplicationContext(), 2);
                    qrList.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }
            });
        }

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                SimpleDateFormat mdformat = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
                final String time = mdformat.format(calendar.getTime());
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                final String usernameKey = prefs.getString("usernameKey", "No name defined");

                postCycleCount(cc_document, asnOL.getText().toString(), asnScanRFID.hexToString(serial_number).substring(0, 12) , serial_number, "1", time, usernameKey, kd_qc, time, time, tgl_exp, outer_label);
            }

        });

        scanOLButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getApplicationContext(), QRScanner.class);
                intent.putExtra("param", "ccScanOL");
                intent.putExtra("fClass", fClass);
                intent.putExtra("asnArrayList", (Serializable) asnArrayList);
                intent.putExtra("asnGetSet", asnGetSet);
                intent.putExtra("tagList", (Serializable) tagList);
                intent.putExtra("cc_document", cc_document);
                intent.putExtra("warehouse_name", warehouse_name);
                intent.putExtra("type", type);
                intent.putExtra("cc_type", cc_type);
                intent.putExtra("tallyQty", asnQty.getText().toString());
                intent.putExtra("asnOL", asnOL.getText().toString());
                startActivity(intent);
            }
        });

        if(intent.getStringExtra("ccScanOL") != null) {
            asnArrayList = (List<AsnGetSet>) intent.getSerializableExtra("asnArrayList");
            asnGetSet = (AsnGetSet) intent.getSerializableExtra("asnGetSet");
            tagList = (List<String>) intent.getSerializableExtra("tagList");
            cc_document = intent.getStringExtra("cc_document");
            warehouse_name = intent.getStringExtra("warehouse_name");
            type = intent.getStringExtra("type");
            cc_type = intent.getStringExtra("cc_type");
            asnOL.setText(intent.getStringExtra("ccScanOL"));
            asnQr.requestFocus();
            asnQr.setFocusableInTouchMode(true);
            asnQr.setFocusable(true);
            asnQr.setText("");
            asnQty.setText("1");
            adapter.notifyDataSetChanged();
        }
    }

    private void postCC(final Map<String, String> parameters) {
        String postUrl = this.url + "/Api_cycle_count/tempPost";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Toast.makeText(getApplicationContext(), "Done", Toast.LENGTH_SHORT).show();
                        finish();
                        Intent intent2 = new Intent(getApplicationContext(), Inventory.class);
                        intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent2);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;

            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    private void parseQR(String asnQrText, String asnQtyText) {
        String qr = "";

        qr = asnQrText.toUpperCase();
        try {
            if(asnQtyText.length() > 0 && Float.parseFloat(asnQtyText) > 0) {
                if(qr.length() == 40 || qr.length() == 42 || qr.length() == 44) { // Hex scanned
                    if(tagList.size() > 0) {
                        if(tagList.contains(qr)) {
                            System.out.println("tagList : " + tagList);
                            Toast.makeText(getApplicationContext(), "This QR is already scanned.", Toast.LENGTH_SHORT).show();
                        } else {
                            tagList.add(qr.toUpperCase());
                            String qrText = asnScanRFID.hexToString(qr);
                            asnGetSet = new AsnGetSet(qr, qrText.substring(0, 20), qrText.substring(0, 12).toUpperCase(), "1", asnQtyText);
                            asnArrayList.add(asnGetSet);
                        }
                    } else {
                        tagList.add(qr.toUpperCase());
                        String qrText = asnScanRFID.hexToString(qr);
                        asnGetSet = new AsnGetSet(qr, qrText.substring(0, 20), qrText.substring(0, 12).toUpperCase(), "1", asnQtyText);
                        asnArrayList.add(asnGetSet);
                    }
                    System.out.println("tagList2 : " + tagList);
                } else {
                    if(qr.contains("OL")) {
                        params.clear();
                        params.put("cc_document", cc_document);
                        params.put("outer_label", qr);
                        getOuterLabelData(params);
                    } else {
                        String[] qrArr = splitStringEvery(qr, 17);

                        int qrArrLen = qrArr.length;

                        for(int i = 0; i < qrArr.length; i++) {
                            if(qrArr[i].substring(0,3).equals("SKU")) {
                                qrArr[i] = qrArr[i].substring(3, qrArr[i].length());
                            }
                            asnGetSet = new AsnGetSet(toHex(qrArr[i].substring(0, 20)).toUpperCase(), qrArr[i].substring(0, 20).toUpperCase(), qrArr[i].substring(0, 12).toUpperCase(), "1", asnQtyText);
                            asnArrayList.add(asnGetSet);
                        }
                    }
                }
            } else {
                // showMessage("Alert", "Qty must be not null and bigger than 0.");
                Toast.makeText(getApplicationContext(), "Qty must be not null and bigger than 0.", Toast.LENGTH_SHORT).show();
            }
        } catch (NumberFormatException nfe) {
            // showMessage("Alert", "Qty must be not null and bigger than 0.");
            Toast.makeText(getApplicationContext(), "Qty must be not null and bigger than 0.", Toast.LENGTH_SHORT).show();
        } catch (StringIndexOutOfBoundsException e) {
            Toast.makeText(getApplicationContext(), "Format barcode tidak sesuai.", Toast.LENGTH_SHORT).show();
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                asnQr.requestFocus();
                asnQr.setFocusableInTouchMode(true);
                asnQr.setFocusable(true);
                asnQr.setText("");
                asnQty.setText("1");
                adapter.notifyDataSetChanged();
            }
        });
    }

    public void postCycleCount(final String cc_code, final String type, final String type1, final String serial_number, final String qty, final String time, final String uname, final String kd_qc, final String tgl_in, final String putaway_time, final String tgl_exp, final String outer_label) {
        String postUrl = this.url + "/Api_cycle_count/post/";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Cycle Count getSerial : ", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getInt("status") == 200) {
                                Toast.makeText(getApplicationContext(),  jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                                parseQR(serial_number, "1");
                            } else {
                                Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("cc_code", cc_code);
                params.put("type", type);
                params.put("type1", type1);
                params.put("serial_number", serial_number.toUpperCase());
                params.put("qty", qty);
                params.put("time", time);
                params.put("kd_qc", kd_qc);
                params.put("uname", uname);
                params.put("tgl_in", tgl_in);
                params.put("putaway_time", putaway_time);
                params.put("tgl_exp", tgl_exp);
                params.put("outer_label", outer_label);
                System.out.println("Post Cycle Count params : " + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    public void postPositiveCycleCount(final String cc_code, final String type, final String type1, final String serial_number, final String qty, final String time, final String uname, final String kd_qc, final String tgl_in, final String putaway_time, final String tgl_exp) {
        String postUrl = this.url + "/Api_cycle_count/post/";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Cycle Count getSerial : ", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getInt("status") == 200) {
                                Toast.makeText(getApplicationContext(),  jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                                parseQR(serial_number, "1");
                            } else {
                                Toast.makeText(getApplicationContext(),  jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("cc_code", cc_code);
                params.put("type", type);
                params.put("type1", type1);
                params.put("serial_number", serial_number.toUpperCase());
                params.put("qty", qty);
                params.put("time", time);
                params.put("kd_qc", kd_qc);
                params.put("uname", uname);
                params.put("tgl_in", tgl_in);
                params.put("putaway_time", putaway_time);
                params.put("tgl_exp", tgl_exp);
                System.out.println("Post Cycle Count params : " + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    public void getOuterLabelData(final Map<String, String> parameters) {
        String postUrl = this.url + "/Api_receiving/outer_label_item/";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);

                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            String message = jsonResponse.getString("message");
                            JSONArray jsonMessage = new JSONArray(message);

                            asnArrayList.clear();
                            for(int i = 0; i < jsonMessage.length(); i++) {
                                String unique_code = jsonMessage.getString(i);
                                JSONObject jsonHex = new JSONObject(unique_code);
                                String hex = jsonHex.getString("unique_code");
                                String hexToText = asnScanRFID.hexToString(hex).toUpperCase();

                                asnGetSet = new AsnGetSet(hex, hexToText.substring(0, 20), hexToText.substring(0, 12), "1", "1");
                                asnArrayList.add(asnGetSet);
                            }
                        } catch (JSONException jsonEx) {
                            jsonEx.printStackTrace();
                        }

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                asnQr.setText("");
                                asnQr.requestFocus();
                                asnQr.setSelection(0);
                                adapter.notifyDataSetChanged();
                            }
                        });
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    public String toHex(String arg) {
        String result = String.format("%x", new BigInteger(1, arg.getBytes(/*YOUR_CHARSET?*/)));
        return result.toUpperCase();
    }

    public String[] splitStringEvery(String s, int interval) {
        int arrayLength = (int) Math.ceil(((s.length() / (double)interval)));
        String[] result = new String[arrayLength];

        int j = 0;
        int lastIndex = result.length - 1;
        for (int i = 0; i < lastIndex; i++) {
            result[i] = s.substring(j, j + interval);
            j += interval;
        } //Add the last bit
        result[lastIndex] = s.substring(j);

        return result;
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }
}