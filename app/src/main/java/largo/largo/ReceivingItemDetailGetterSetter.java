package largo.largo;

import java.io.Serializable;

public class ReceivingItemDetailGetterSetter implements Serializable {
    String serial_number, status;

    public ReceivingItemDetailGetterSetter(String serial_number, String status) {
        this.serial_number = serial_number;
        this.status = status;
    }


    public String getSerialNumber() {
        return serial_number;
    }

    public String getStatus() {
        return status;
    }
}
