package largo.largo;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

public class PackingDocumentListAdapter2 extends ArrayAdapter<PackingDocumentGetterSetter2> {

    private List<PackingDocumentGetterSetter2> packingDocumentItemList;

    private PackingDocumentGetterSetter2 packingDocument;

    private Context context;

    public PackingDocumentListAdapter2(List<PackingDocumentGetterSetter2> packingDocumentItemList, Context context) {
        super(context, R.layout.list_view_packing_document, packingDocumentItemList);
        this.packingDocumentItemList = packingDocumentItemList;
        this.context = context;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);

        View listViewItem = inflater.inflate(R.layout.list_view_packing_document, null, true);

        packingDocument = packingDocumentItemList.get(position);

        TextView pl_name = listViewItem.findViewById(R.id.pl_name);
        TextView kd_outbound = listViewItem.findViewById(R.id.kd_outbound);
        TextView qty = listViewItem.findViewById(R.id.qty);

        pl_name.setTypeface(Typeface.DEFAULT_BOLD);
        pl_name.setText(packingDocument.getPicking_code() + " (" + packingDocument.getOutbound_code() + ")");
        kd_outbound.setText(packingDocument.getItem_code() + " - " + packingDocument.getItem_name());
        qty.setText(packingDocument.getQty_packed() + "/" + packingDocument.getQty_picked() + " " + packingDocument.getUom());

        if(Integer.valueOf(packingDocument.getQty_packed().toString()) < Integer.valueOf(packingDocument.getQty_picked().toString()) && Integer.valueOf(packingDocument.getQty_packed().toString()) != 0){
            pl_name.setBackgroundColor(Color.YELLOW);
            kd_outbound.setBackgroundColor(Color.YELLOW);
            qty.setBackgroundColor(Color.YELLOW);
        }

        if(Integer.valueOf(packingDocument.getQty_packed()).equals(Integer.valueOf(packingDocument.getQty_picked()))){
            pl_name.setBackgroundColor(Color.GREEN);
            kd_outbound.setBackgroundColor(Color.GREEN);
            qty.setBackgroundColor(Color.GREEN);
        }

        return listViewItem;
    }
}
