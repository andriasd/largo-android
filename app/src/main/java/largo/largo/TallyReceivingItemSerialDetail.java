package largo.largo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import db.DBHelper;

public class TallyReceivingItemSerialDetail extends AppCompatActivity {
    DatabaseHelper myDb;

    String TAG = TallyReceivingItems.class.getSimpleName();

    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    List<String> expand_detail = new ArrayList<String>();
    HashMap<String, List<String>> listDataChild;

    ListView listView;
    private List<ReceivingItemDetailGetterSetter> skuDetailItemList;

    public static final String MyPREFERENCES = "MyPrefs" ;

    TextView doc,item,item_,all,good,ng,qc;

    LinearLayout all_header,good_header,ng_header,qc_header;

    Intent intent;

    private int lastExpandedPosition = -1;

    ImageButton backButton;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inbound_tally_receiving_item_serial_detail);

        intent = getIntent();

        final String receiving_document = intent.getStringExtra("receiving_document");
        final String sku = intent.getStringExtra("sku");
        final String item_name = intent.getStringExtra("item_name");
        final String qty = intent.getStringExtra("qty");
        final String has_batch = intent.getStringExtra("has_batch");
        final String has_expdate = intent.getStringExtra("has_expdate");
        final String has_qty = intent.getStringExtra("has_qty");
        final String def_qty = intent.getStringExtra("def_qty");

//        SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
//        final String item_name = prefs.getString("item_name", "No name defined");

        doc = findViewById(R.id.doc);
        doc.setText(receiving_document);

        item = findViewById(R.id.sku);
        item.setText(sku);

        item_ = findViewById(R.id.name);
//        item_.setText(item_name.substring(0, 30 - "...".length()).concat("..."));
        item_.setText(item_name);

        backButton = findViewById(R.id.back);

        backButton.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        Intent intent = new Intent(TallyReceivingItemSerialDetail.this,TallyReceivingItemSerial.class);
                        intent.putExtra("receiving_document",receiving_document);
                        intent.putExtra("sku",sku);
                        intent.putExtra("item_name",item_name);
                        intent.putExtra("qty",qty);
                        intent.putExtra("def_qty",def_qty);
                        intent.putExtra("has_batch",has_batch);
                        intent.putExtra("has_expdate",has_expdate);
                        intent.putExtra("has_qty",has_qty);
                        startActivity(intent);
                        finish();
                    }
                }
        );

        myDb = new DatabaseHelper(this);

//        expListView = (ExpandableListView)findViewById(R.id.ListItem);

        updateHeader();

        listView =  findViewById(R.id.ListItem);

        skuDetailItemList = new ArrayList<>();
        updateUI();


//        updateUI();

//        if(listDataHeader.size() == 0){
//            Toast.makeText(getApplicationContext(),"No Data.", Toast.LENGTH_SHORT).show();
//
//            return;
//        }else {
//
//            listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);
//
//            expListView.setAdapter(listAdapter);
//
//            expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
//                @Override
//                public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
//                    return false;
//                }
//            });
//
//            expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
//                @Override
//                public void onGroupExpand(int groupPosition) {
//                    if (lastExpandedPosition != -1
//                            && groupPosition != lastExpandedPosition) {
//                        expListView.collapseGroup(lastExpandedPosition);
//                    }
//                    lastExpandedPosition = groupPosition;
////                Toast.makeText(getApplicationContext(),listDataHeader.get(groupPosition) + "Expanded", Toast.LENGTH_SHORT).show();
//                }
//            });
//
//            expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
//                @Override
//                public void onGroupCollapse(int groupPosition) {
//                    Toast.makeText(getApplicationContext(), listDataHeader.get(groupPosition) + "Collapsed", Toast.LENGTH_SHORT).show();
//                }
//            });
//
//            expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
//                @Override
//                public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
//                    Toast.makeText(getApplicationContext(), listDataHeader.get(groupPosition)
//                            + ":"
//                            + listDataChild.get(
//                            listDataHeader.get(groupPosition)).get(
//                            childPosition), Toast.LENGTH_SHORT).show();
//                    return false;
//                }
//            });
//
//        }

        all_header = findViewById(R.id.all_header);

        all_header.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        skuDetailItemList.clear();
                        updateUIAll();
                    }
                }
        );

        good_header = findViewById(R.id.good_header);

        good_header.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        skuDetailItemList.clear();
                        updateUIGood();
                    }
                }
        );

        ng_header = findViewById(R.id.ng_header);

        ng_header.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        skuDetailItemList.clear();
                        updateUINg();
                    }
                }
        );

        qc_header = findViewById(R.id.qc_header);

        qc_header.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        skuDetailItemList.clear();
                        updateUIQc();
                    }
                }
        );

    }

    private void updateHeader(){
        Cursor res1 = myDb.getTotalTalliedItem(intent.getStringExtra("receiving_document"), intent.getStringExtra("sku"));

        Cursor res2 = myDb.getTotalGoodTalliedItem(intent.getStringExtra("receiving_document"), intent.getStringExtra("sku"));

        Cursor res3 = myDb.getTotalNgTalliedItem(intent.getStringExtra("receiving_document"), intent.getStringExtra("sku"));

        Cursor res4 = myDb.getTotalQcTalliedItem(intent.getStringExtra("receiving_document"), intent.getStringExtra("sku"));

        all = findViewById(R.id.all);

        good = findViewById(R.id.good);

        ng = findViewById(R.id.ng);

        qc = findViewById(R.id.qc);

//        if(res1.getCount() == 0) {
//
//            // show message
//
//            showMessage("Error","Nothing Found");
//
//            return;
//
//        }


        StringBuffer buffer = new StringBuffer();


        while (res1.moveToNext() ) {
            all.setText(res1.getString(0));

        }

        while (res2.moveToNext() ) {
            good.setText(res2.getString(0));

        }

        while (res3.moveToNext() ) {
            ng.setText(res3.getString(0));

        }

        while (res4.moveToNext() ) {
            qc.setText(res4.getString(0));

        }

    }

    private void updateUI(){
        Cursor res = myDb.getTalliedItem(intent.getStringExtra("receiving_document"), intent.getStringExtra("sku"));

        int i = 0;

        while (res.moveToNext() ) {

            ReceivingItemDetailGetterSetter item_sku_detail = new ReceivingItemDetailGetterSetter(
                    res.getString(7),
                    res.getString(2)
            );
            skuDetailItemList.add(item_sku_detail);

            i++;

        }

        ReceivingItemDetailListAdapter adapter = new ReceivingItemDetailListAdapter(skuDetailItemList, getApplicationContext());

        listView.setAdapter(adapter);
    }

//    private void updateUI(){
//        Cursor res = myDb.getTalliedItem(intent.getStringExtra("receiving_document"), intent.getStringExtra("sku"));
//
////        if(res.getCount() == 0) {
////
////            // show message
////
////            showMessage("Error","Noting Found");
////
////            return;
////
////        }
//
//        listDataHeader = new ArrayList<>();
//        listDataChild = new HashMap<String, List<String>>();
//
//        StringBuffer buffer = new StringBuffer();
//
//        int i = 0;
//
//        while (res.moveToNext() ) {
//
//            listDataHeader.add(res.getString(6));
//
//            expand_detail.add("Status: " + res.getString(2));
//            listDataChild.put(listDataHeader.get(i), new ArrayList<String>(Arrays.asList(expand_detail.get(i))));
//
////            buffer.append("Sku :"+ res.getString(0)+"\n");
////
////            buffer.append("Status :"+ res.getString(1)+"\n");
////
////            buffer.append("Batch :"+ res.getString(2)+"\n");
////
////            buffer.append("Shelf_Life :"+ res.getString(3)+"\n");
////
////            buffer.append("Qty :"+ res.getString(4)+"\n");
////
////            buffer.append("Serial_Number :"+ res.getString(5)+"\n\n");
//
//            i++;
//
//        }
//
//
//        // show all data
//
////        showMessage("Data",buffer.toString());
//    }

    private void updateUIAll(){

        all_header.setBackgroundColor(Color.parseColor("#BAE4E3"));
        good_header.setBackgroundColor(Color.parseColor("#FFFFFF"));
        ng_header.setBackgroundColor(Color.parseColor("#FFFFFF"));
        qc_header.setBackgroundColor(Color.parseColor("#FFFFFF"));

        Cursor res = myDb.getTalliedItem(intent.getStringExtra("receiving_document"), intent.getStringExtra("sku"));

        int i = 0;

        while (res.moveToNext() ) {

            ReceivingItemDetailGetterSetter item_sku_detail = new ReceivingItemDetailGetterSetter(
                    res.getString(7),
                    res.getString(2)
            );
            skuDetailItemList.add(item_sku_detail);

            i++;

        }

        ReceivingItemDetailListAdapter adapter = new ReceivingItemDetailListAdapter(skuDetailItemList, getApplicationContext());

        listView.setAdapter(adapter);

        adapter.notifyDataSetChanged();

    }

    private void updateUIGood(){

        listView.setAdapter(null);

        all_header.setBackgroundColor(Color.parseColor("#FFFFFF"));
        good_header.setBackgroundColor(Color.parseColor("#BAE4E3"));
        ng_header.setBackgroundColor(Color.parseColor("#FFFFFF"));
        qc_header.setBackgroundColor(Color.parseColor("#FFFFFF"));

        Cursor res = myDb.getGoodTalliedItem(intent.getStringExtra("receiving_document"), intent.getStringExtra("sku"));

        while (res.moveToNext() ) {

            ReceivingItemDetailGetterSetter item_sku_detail = new ReceivingItemDetailGetterSetter(
                    res.getString(7),
                    res.getString(2)
            );
            skuDetailItemList.add(item_sku_detail);

        }

        ReceivingItemDetailListAdapter adapter = new ReceivingItemDetailListAdapter(skuDetailItemList, getApplicationContext());

        listView.setAdapter(adapter);

        adapter.notifyDataSetChanged();

    }

    private void updateUINg(){

        all_header.setBackgroundColor(Color.parseColor("#FFFFFF"));
        good_header.setBackgroundColor(Color.parseColor("#FFFFFF"));
        ng_header.setBackgroundColor(Color.parseColor("#BAE4E3"));
        qc_header.setBackgroundColor(Color.parseColor("#FFFFFF"));

        Cursor res = myDb.getNgTalliedItem(intent.getStringExtra("receiving_document"), intent.getStringExtra("sku"));

        int i = 0;

        while (res.moveToNext() ) {

            ReceivingItemDetailGetterSetter item_sku_detail = new ReceivingItemDetailGetterSetter(
                    res.getString(7),
                    res.getString(2)
            );
            skuDetailItemList.add(item_sku_detail);

            i++;

        }

        ReceivingItemDetailListAdapter adapter = new ReceivingItemDetailListAdapter(skuDetailItemList, getApplicationContext());

        listView.setAdapter(adapter);

        adapter.notifyDataSetChanged();

    }

    private void updateUIQc(){

        all_header.setBackgroundColor(Color.parseColor("#FFFFFF"));
        good_header.setBackgroundColor(Color.parseColor("#FFFFFF"));
        ng_header.setBackgroundColor(Color.parseColor("#FFFFFF"));
        qc_header.setBackgroundColor(Color.parseColor("#BAE4E3"));

        Cursor res = myDb.getQcTalliedItem(intent.getStringExtra("receiving_document"), intent.getStringExtra("s"));

        int i = 0;

        while (res.moveToNext() ) {

            ReceivingItemDetailGetterSetter item_sku_detail = new ReceivingItemDetailGetterSetter(
                    res.getString(7),
                    res.getString(2)
            );
            skuDetailItemList.add(item_sku_detail);

            i++;

        }

        ReceivingItemDetailListAdapter adapter = new ReceivingItemDetailListAdapter(skuDetailItemList, getApplicationContext());

        listView.setAdapter(adapter);

        adapter.notifyDataSetChanged();

    }

    public void showMessage(String title, String Message){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setCancelable(true);

        builder.setTitle(title);

        builder.setMessage(Message);

        builder.show();

    }
}
