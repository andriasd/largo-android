package largo.largo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static largo.largo.LoginActivity.MyPREFERENCES;

public class SplitItemsQty extends AppCompatActivity {
    private InventoryQCGetSet inventoryQCGetSet;
    private List<InventoryQCGetSet> inventoryQCGetSetList = new ArrayList<>();
    private List<String> tagList = new ArrayList<>();

    String TAG = SplitItemsQty.class.getSimpleName();
    EditText serial_number, qty;
    Button backButton, saveButton;

    private TextView serial_number_text, sku_text, item_name_text, qty_text, bin_loc_text, exp_date_text, status_text;

    DatabaseHelper myDb;
    String url, new_serial_number;

    private Intent intent, intentBack;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.split_items_qty);

        intent = getIntent();
        intentBack = null;

        if(intent.getSerializableExtra("inventoryQCGetSet") != null) {
            intentBack = new Intent(this, InventoryQCOutLocation.class);
            inventoryQCGetSet = (InventoryQCGetSet) intent.getSerializableExtra("inventoryQCGetSet");
            inventoryQCGetSetList = (List<InventoryQCGetSet>) intent.getSerializableExtra("inventoryQCGetSetList");
            tagList = (List<String>) intent.getSerializableExtra("tagList");
        }

        new_serial_number = intent.getStringExtra("new_serial_number");

        serial_number_text = findViewById(R.id.serial_number_text);
        sku_text = findViewById(R.id.sku_text);
        item_name_text = findViewById(R.id.item_name_text);
        qty_text = findViewById(R.id.qty_text);
        bin_loc_text = findViewById(R.id.bin_loc_text);
        exp_date_text = findViewById(R.id.exp_date_text);
        status_text = findViewById(R.id.status_text);

        myDb = new DatabaseHelper(this);
        getServerURL();

        backButton = findViewById(R.id.back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        saveButton = findViewById(R.id.save);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postSplitItems(new_serial_number.toUpperCase(), serial_number_text.getText().toString(), qty.getText().toString());
            }
        });

        qty = findViewById(R.id.qty);
        serial_number = findViewById(R.id.serial_number);
        serial_number.setShowSoftInputOnFocus(false);
        serial_number.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    //if(serial_number.getText().toString().toUpperCase().substring(0,2).equals("IL")) {
                    postSplitItems(serial_number.getText().toString().toUpperCase(), serial_number_text.getText().toString(), qty.getText().toString());
                    /*} else {
                        Toast.makeText(getApplicationContext(), "Serial Number tidak sesuai dengan format.", Toast.LENGTH_SHORT).show();
                    }*/
                    serial_number.setFocusableInTouchMode(true);
                    serial_number.setFocusable(true);
                    serial_number.requestFocus();
                    serial_number.setText("");
                }
                return false;
            }
        });

        if(intent.getStringExtra("serial_number") != null) {
            getActiveStock(intent.getStringExtra("serial_number"));
        }
    }

    private void getActiveStock(final String serialNumber) {
        StringRequest strReq = new StringRequest(Request.Method.GET, url+"/Api_active_stock/get/" + serialNumber.toUpperCase(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);

                try {
                    JSONObject data = new JSONObject(response);
                    Integer status = data.getInt("status");

                    if (status == 200) {
                        serial_number.requestFocus();
                        serial_number.setText("");

                        JSONArray jsonArray = data.getJSONArray("results");
                        JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                        serial_number_text.setText(serialNumber.toUpperCase());
                        sku_text.setText(jsonObject1.getString("kd_barang"));
                        item_name_text.setText(jsonObject1.getString("nama_barang"));
                        qty_text.setText(jsonObject1.getString("last_qty") + " pcs");
                        bin_loc_text.setText(jsonObject1.getString("location"));
                        exp_date_text.setText(jsonObject1.getString("tgl_exp"));
                        if(jsonObject1.getString("tgl_exp").equals("0000-00-00")) {
                            exp_date_text.setText("-");
                        }
                        status_text.setText(jsonObject1.getString("kd_qc"));
                        qty.setText(jsonObject1.getString("def_qty"));
                    } else {
                        Toast.makeText(getApplicationContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
                    }

                    serial_number.setText("");
                    serial_number.requestFocus();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (StringIndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }

    private void postSplitItems(final String newSerialNumber, final String oldSerialNumber, final String qtyPars) {
        if(Float.parseFloat(qty.getText().toString()) > 0) {
            StringRequest strReq = new StringRequest(Request.Method.POST, url+"/Api_split_serial_number/post/", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d(TAG + "postSplitItems", response);

                    try {
                        JSONObject data = new JSONObject(response);
                        Integer status = data.getInt("status");
                        String message = data.getString("message");

                        if (status == 200) {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

                            if(intent.getSerializableExtra("inventoryQCGetSet") != null) {
                                try {
                                    for(int i = 0; i < inventoryQCGetSetList.size(); i++) {
                                        if(inventoryQCGetSetList.get(i).getSn().equals(oldSerialNumber)) {
                                            tagList.add(newSerialNumber);
                                            inventoryQCGetSet = new InventoryQCGetSet(newSerialNumber, inventoryQCGetSetList.get(i).getLoc(), inventoryQCGetSetList.get(i).getQc(), newSerialNumber, inventoryQCGetSetList.get(i).getPickTime(), "");
                                            inventoryQCGetSetList.add(inventoryQCGetSet);
                                        }
                                    }
                                } catch (ClassCastException e) {
                                    e.printStackTrace();
                                }

                                intentBack.putExtra("tagList", (Serializable) tagList);
                                intentBack.putExtra("inventoryQCGetSetList", (Serializable) inventoryQCGetSetList);
                                intentBack.putExtra("inventoryQCGetSet", inventoryQCGetSet);
                                intentBack.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intentBack);
                            } else {
                                intent = new Intent(SplitItemsQty.this, SplitItems.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                }
            }) {

                @Override
                protected Map<String, String> getParams(){
                    Map<String, String> params = new HashMap<String, String>();
                    SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                    String usernameKey = prefs.getString("usernameKey", "No name defined");

                    Calendar calendar = Calendar.getInstance();
                    SimpleDateFormat mdformat = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");

                    params.put("new_serial_number", newSerialNumber);
                    params.put("old_serial_number", oldSerialNumber);
                    params.put("qty", qtyPars);
                    params.put("user", usernameKey);
                    System.out.println("Params post split items : " + params);

                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                    String restoredText = prefs.getString("handheldsessioncodeKey", null);

                    String usernameKey = prefs.getString("usernameKey", "No name defined");
                    String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                    Log.d(TAG, usernameKey);
                    Log.d(TAG, handheldsessioncodeKey);

                    Map<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "application/x-www-form-urlencoded");
                    headers.put("User",usernameKey);
                    headers.put("Authorization", handheldsessioncodeKey);
                    return headers;


                }
            };

            controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
        } else {
            Toast.makeText(getApplicationContext(), "Qty tidak boleh 0.", Toast.LENGTH_SHORT);
        }
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }
}
