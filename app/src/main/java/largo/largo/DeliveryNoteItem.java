package largo.largo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Layout;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static largo.largo.LoadingScanQR.MyPREFERENCES;

public class DeliveryNoteItem extends AppCompatActivity {
    String TAG = this.getClass().getSimpleName();

    private Intent intent;
    private String param;
    private LinearLayout linearLayout12;
    private Button backButton, nextButton;
    private ListView listView, selectedListView;
    private List<String> docList;
    private EditText outbound;
    private List<DeliveryNoteItemGetterSetter> dokumenItemList;
    private DeliveryNoteItemGetterSetter dnDocGetSet;
    private List<DeliveryNoteItemGetterSetter> selectedDokumenItemList;
    private DeliveryNoteItemListAdapter adapter;
    private TextView header, textView13;
    private JSONObject paramsJson;
    public static final Map<String, String> params = new HashMap<String, String>();
    public static final String MyPREFERENCES = "MyPrefs" ;

    DatabaseHelper myDb;
    String url;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.delivery_note_item);

        intent = getIntent();
        param = this.getClass().getSimpleName();

        myDb = new DatabaseHelper(this);
        getServerURL();

        backButton = findViewById(R.id.back);
        nextButton = findViewById(R.id.next);
        header = findViewById(R.id.header_tally);
        textView13 = findViewById(R.id.textView13);
        listView = findViewById(R.id.ListTallyDoc);
        outbound = findViewById(R.id.outbound_location);

        header.setText("Delivery Note "+intent.getStringExtra("dn_name"));
        textView13.setText("Scan Outbound");

        nextButton.setVisibility(View.VISIBLE);
        nextButton.setText("Finish");
        nextButton.setEnabled(false);

        outbound.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 0){
                    System.out.println("masuk ktl");
                    postCheckOutboundLoc(outbound.getText().toString());
                }
                return false;
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(outbound.getText().length()>0){
                    for(int i=0;i<dokumenItemList.size();i++){
                        SharedPreferences prefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
                        String restoredText = prefs.getString("handheldsessioncodeKey", null);

                        String usernameKey = prefs.getString("usernameKey", "No name defined");
                        String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                        Log.d(TAG, usernameKey);
                        Log.d(TAG, handheldsessioncodeKey);

                        String pl_name = dokumenItemList.get(i).getPl_name(),
                                item_code = dokumenItemList.get(i).getItem_code(),
                                qty = dokumenItemList.get(i).getQty(),
                                code = dokumenItemList.get(i).getCode(),
                                user_id = dokumenItemList.get(i).getUser_id(),
                                user_name = dokumenItemList.get(i).getUser_name(),
                                loc = outbound.getText().toString();

                        String jsonData = "";
                        jsonData = "[";
                        try{
                            paramsJson.put("picking_name", pl_name);
                            paramsJson.put("item_code", item_code);
                            paramsJson.put("serial_number", code);
                            paramsJson.put("old_loc", "");
                            paramsJson.put("qty", qty);
                            paramsJson.put("user_pick", user_name);
                            paramsJson.put("pick_time", new SimpleDateFormat("YYYY-MM-dd HH:mm:ss").format(new Date()));
                            paramsJson.put("outbound_loc", loc);
                            paramsJson.put("user_put", usernameKey);
                            paramsJson.put("put_time", new SimpleDateFormat("YYYY-MM-dd HH:mm:ss").format(new Date()));

                            jsonData = jsonData + paramsJson.toString() + ",";
                        }catch(Exception e){
                            e.printStackTrace();
                        }

                        jsonData = jsonData.substring(0, jsonData.length()-1);
                        jsonData = jsonData + "]";
                        Log.d("JSON Param", jsonData);
                        Log.d("STRLEN", Integer.toString(jsonData.length()));
                        params.clear();
                        params.put("data", jsonData);

                        postConfirmPicking(params, i);
                        System.out.println("Post Confirm Picking !!");
                    }
                    completeDN(url+"/tray_list/completeTrayName");
                }else{
                    Toast.makeText(getApplicationContext(), "Please Check Outbound Location Field!", Toast.LENGTH_SHORT).show();
                }

            }
        });

        docList = new ArrayList<>();
        if(intent.getSerializableExtra("docList") != null) {
            docList = (List<String>) intent.getSerializableExtra("docList");
        }
        paramsJson = new JSONObject();

        dokumenItemList = new ArrayList<>();
        selectedDokumenItemList = new ArrayList<>();
        SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        String usernameKey = prefs.getString("usernameKey", "No name defined");
        DataListItem(url + "/tray_list/get_document_item");

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
////                Intent intent = new Intent(getApplicationContext(), DeliveryNotePl.class);
////                intent.putExtra("item_name",dokumenItemList.get(position).getItem_name());
////                intent.putExtra("code",dokumenItemList.get(position).getCode());
////                intent.putExtra("pl_name",dokumenItemList.get(position).getPl_name());
////                startActivity(intent);
//
//                if(receiving_document.getText().length()>0){
//                    SharedPreferences prefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
//                    String restoredText = prefs.getString("handheldsessioncodeKey", null);
//
//                    String usernameKey = prefs.getString("usernameKey", "No name defined");
//                    String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
//                    Log.d(TAG, usernameKey);
//                    Log.d(TAG, handheldsessioncodeKey);
//
//                    String pl_name = dokumenItemList.get(position).getPl_name(),
//                            item_code = dokumenItemList.get(position).getItem_code(),
//                            qty = dokumenItemList.get(position).getQty(),
//                            code = dokumenItemList.get(position).getCode(),
//                            loc = receiving_document.getText().toString();
//
//                    String jsonData = "";
//                    jsonData = "[";
//                    try{
//                        paramsJson.put("picking_name", pl_name);
//                        paramsJson.put("item_code", item_code);
//                        paramsJson.put("serial_number", code);
//                        paramsJson.put("old_loc", "");
//                        paramsJson.put("qty", qty);
//                        paramsJson.put("user_pick", usernameKey);
//                        paramsJson.put("pick_time", new SimpleDateFormat("YYYY-MM-dd HH:mm:ss").format(new Date()));
//                        paramsJson.put("outbound_loc", loc);
//                        paramsJson.put("user_put", usernameKey);
//                        paramsJson.put("put_time", new SimpleDateFormat("YYYY-MM-dd HH:mm:ss").format(new Date()));
//
//                        jsonData = jsonData + paramsJson.toString() + ",";
//                    }catch(Exception e){
//                        e.printStackTrace();
//                    }
//
//                    jsonData = jsonData.substring(0, jsonData.length()-1);
//                    jsonData = jsonData + "]";
//                    Log.d("JSON Param", jsonData);
//                    Log.d("STRLEN", Integer.toString(jsonData.length()));
//                    params.clear();
//                    params.put("data", jsonData);
//
//                    postConfirmPicking(params, position);
//                    System.out.println("Post Confirm Picking !!");
//                }else{
//                    Toast.makeText(getApplicationContext(), "Please Check Outbound Location Field!", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });

    }

    private void DataListItem(String getDoc){
        RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.POST, getDoc, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);
                try{
                    JSONObject jsonObject=new JSONObject(response);
                    if(jsonObject.getInt("status")==200){
                        JSONArray jsonArray1=jsonObject.getJSONArray("results");
                        for(int i=0;i<jsonArray1.length();i++){
                            JSONObject jsonObject1=jsonArray1.getJSONObject(i);
                            dnDocGetSet = new DeliveryNoteItemGetterSetter(
                                    jsonObject1.getString("item_code"),
                                    jsonObject1.getString("code"),
                                    jsonObject1.getString("item_code"),
                                    jsonObject1.getString("qty"),
                                    jsonObject1.getString("pl_name"),
                                    jsonObject1.getString("user_id"),
                                    jsonObject1.getString("user_name")
                            );
                            dokumenItemList.add(dnDocGetSet);
                            docList.add(jsonObject1.getString("pl_name"));
                        }
                        adapter = new DeliveryNoteItemListAdapter(dokumenItemList, getApplicationContext());

                        listView.setAdapter(adapter);

                        outbound.requestFocus();
                        outbound.setText("");
                    }
//                    spinner.setAdapter(new ArrayAdapter<String>(TallyReceivingItemSerial.this, android.R.layout.simple_spinner_dropdown_item, QC));
                }catch (JSONException e){e.printStackTrace();}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {

            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("dn_name",intent.getStringExtra("dn_name"));
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;


            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void postConfirmPicking(final Map<String, String> parameters, final Integer positiona) {
        StringRequest strReq = new StringRequest(Request.Method.POST,url+"/Api_picking/post_json/", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG + "postConfirmPicking", response);

                try {
                    JSONObject data = new JSONObject(response);

                    Integer status = data.getInt("status");
                    String results = data.getString("results");
                    JSONObject jsonObject = new JSONObject(results.substring(1, results.length()-1));
                    String message = jsonObject.getString("message");

                    if (status == 200) {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        listView.getChildAt(positiona).setBackgroundColor(Color.RED);

                    } else {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        outbound.setText("");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    String jsonError = new String(networkResponse.data);

                    try {
                        JSONObject data = new JSONObject(jsonError);

                        String message = data.getString("message");

                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        })  {

            @Override
            protected Map<String, String> getParams(){
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }

    private void completeDN(String getDoc){
        RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.POST, getDoc, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);
                try{
                    JSONObject jsonObject=new JSONObject(response);
                    if(jsonObject.getInt("status")==200){
                        Toast.makeText(getApplicationContext(), "Delivery Note is completed", Toast.LENGTH_SHORT).show();
                        Intent intent2 = new Intent(getApplicationContext(), DeliveryNote.class);
                        intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent2);
                    }else{
                        Toast.makeText(getApplicationContext(), "Please try again! Your command is not return anything", Toast.LENGTH_SHORT).show();
                    }
//                    spinner.setAdapter(new ArrayAdapter<String>(TallyReceivingItemSerial.this, android.R.layout.simple_spinner_dropdown_item, QC));
                }catch (JSONException e){e.printStackTrace();}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {

            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("dn_name",intent.getStringExtra("dn_name"));
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;


            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void postCheckOutboundLoc(final String locName) {
        System.out.println("masuk disini kawan!");
        StringRequest strReq = new StringRequest(Request.Method.GET,url+"/Api_picking/get_location/" + locName, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);

                try {
                    JSONObject data = new JSONObject(response);

                    Integer status = data.getInt("status");
                    String message = data.getString("message");
//                    JSONObject jsonObj1 = new JSONObject(results);
//                    String message = jsonObj1.getString("message");

                    if (status == 200) {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        nextButton.setEnabled(true);
                    } else {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        nextButton.setEnabled(false);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    String jsonError = new String(networkResponse.data);


                    try {
                        JSONObject data = new JSONObject(jsonError);

                        String message = data.getString("message");

                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        })  {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }

}
