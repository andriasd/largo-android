package largo.largo;

import java.io.Serializable;

public class LicensePlatingGetSet implements Serializable {
    String sn, hex, sku, ol = "";

    public LicensePlatingGetSet(String sn, String hex, String sku, String ol) {
        this.sn = sn;
        this.hex = hex;
        this.sku = sku;
        this.ol = ol;
    }

    public LicensePlatingGetSet(String sn, String hex, String ol) {
        this.sn = sn;
        this.hex = hex;
        this.ol = ol;
    }

    public LicensePlatingGetSet(String sn, String hex) {
        this.sn = sn;
        this.hex = hex;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getHex() {
        return hex;
    }

    public void setHex(String hex) {
        this.hex = hex;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getOl() {
        return ol;
    }

    public void setOl(String ol) {
        this.ol = ol;
    }
}
