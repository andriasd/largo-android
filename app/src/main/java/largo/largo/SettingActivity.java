package largo.largo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SettingActivity extends AppCompatActivity {
    DatabaseHelper myDb;

    String TAG = LoginActivity.class.getSimpleName();
    public static final String MyPREFERENCES = "MyPrefs" ;

    String fClass = this.getClass().getSimpleName();
    Intent intent;

    EditText url;
    String urls;
    TextView versionText;

    Button connect;

    ImageButton backButton, scanButton;

    String address;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting);

        versionText = findViewById(R.id.versionText);

        backButton = findViewById(R.id.back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent2 = new Intent(getApplicationContext(), LoginActivity.class);
                intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent2);
            }
        });

        intent = getIntent();

        myDb = new DatabaseHelper(this);

        url = findViewById(R.id.url);
//        url.setShowSoftInputOnFocus(false);

        url.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    setServerURL();
                }
                return false;
            }
        });

        connect = findViewById(R.id.connect);

        getServerURL();

        connect.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        setServerURL();
                    }
                }
        );

//        LocalBroadcastManager.getInstance(this).registerReceiver(qrScan, new IntentFilter("broadcastQrResult"));
        scanButton = findViewById(R.id.scanButton);
        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), QRScanner.class);
                intent.putExtra("param", "url");
                intent.putExtra("fClass", fClass);
                startActivity(intent);
            }
        });

        if(intent.getStringExtra("url") != null) {
            url.setText(intent.getStringExtra("url"));
            setServerURL();
            intent.removeExtra("url");
            intent.removeExtra("fClass");
        }
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        url = findViewById(R.id.url);

//        if(res1.getCount() == 0) {
//
//            // show message
//
//            showMessage("Error","Nothing Found");
//
//            return;
//
//        }


        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                address = res1.getString(0);
                url.setText(res1.getString(0));
            }
        }
    }

    private void setServerURL(){
        StringRequest strReq = new StringRequest(Request.Method.POST,url.getText().toString() + "/Api_setting/post_server", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());

                try {
                    JSONObject data = new JSONObject(response);
                    Integer status = data.getInt("status");
                    String message = data.getString("message");


                    if (status == 200) {
                        boolean isInserted = myDb.setServerAPI(
                                url.getText().toString()
                        );

                        if (isInserted == true) {
                            // showMessage("Notification",message);
							Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            checkAppVer(versionText.getText().toString());
//                            Toast.makeText(getApplicationContext(), "URL has been Updated", Toast.LENGTH_SHORT).show();
//                            Intent intent = new Intent(SettingActivity.this,LoginActivity.class);
//                            startActivity(intent);
                        }
                    }


                } catch (JSONException e) {

                }

            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),"Silakan lakukan update terhadap alamat server",Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams(){
                Map<String, String> params = new HashMap<String, String>();

                params.put("server_address",String.valueOf(url.getText()));

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);

    }

    private void checkAppVer(final String version) {
        StringRequest strReq = new StringRequest(Request.Method.POST,url.getText().toString()+"/Api_setting/checkAppVer/", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);

                try {
                    JSONObject data = new JSONObject(response);

                    Integer status = data.getInt("status");
                    String msg = data.getString("message");
                    JSONObject jsonObject = new JSONObject(msg);
                    String message = jsonObject.getString("version");

                    if (status == 200) {
                        Integer latestVersion = Integer.parseInt(message.replace(".", "").replace("V", "").trim());
                        Integer currentVersion = Integer.parseInt(version.replace(".", "").replace("V", "").trim());
                        if(latestVersion > currentVersion) {
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    String jsonError = new String(networkResponse.data);
                    try {
                        JSONObject data = new JSONObject(jsonError);
                        String message = data.getString("message");
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        })  {

            @Override
            protected Map<String, String> getParams(){
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }

    private void updateAppVer(){
        StringRequest strReq = new StringRequest(Request.Method.POST,url.getText().toString() + "/Api_setting/updateAppVer/", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());

                try {
                    JSONObject data = new JSONObject(response);
                    Integer status = data.getInt("status");
                    String message = data.getString("message");

                    if (status == 200) {

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                VolleyLog.d(TAG, "Error: " + error.getMessage());
//                Toast.makeText(getApplicationContext(),"Silahkan lakukan update terhadap alamat server.",Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams(){
                Map<String, String> params = new HashMap<String, String>();

                params.put("username", "");

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }

    private void showMessage(String title, String Message){

        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setCancelable(true);

            builder.setTitle(title);

            builder.setMessage(Message);

            builder.show();
        } catch (WindowManager.BadTokenException e) {
            e.printStackTrace();
        }

    }

    /*private BroadcastReceiver qrScan = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if(intent.getStringExtra(param + "setting") != null) {
                    String getParam = intent.getStringExtra(param + "setting");

                    url.setText(getParam);
                    setServerURL();
                    intent.removeExtra(param + "setting");
                }
            } catch (Exception e) {
//                e.printStackTrace();
            }
        }
    };*/
}
