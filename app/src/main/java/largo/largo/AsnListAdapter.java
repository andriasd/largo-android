package largo.largo;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class AsnListAdapter extends ArrayAdapter<AsnGetSet> {
    private AsnScanRFID asnScanRFID = new AsnScanRFID();

    private List<AsnGetSet> asnGetSetList;

    private Context context;

    private int column;

    public AsnListAdapter(List<AsnGetSet> asnGetSetList, Context context, int column) {
        super(context, R.layout.adapter_list_horizontal, asnGetSetList);
        this.asnGetSetList = asnGetSetList;
        this.context = context;
        this.column = column;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);

        View listViewItem = inflater.inflate(R.layout.adapter_list_horizontal, null, true);

        TextView upcText = listViewItem.findViewById(R.id.textView1);
        TextView skuText = listViewItem.findViewById(R.id.textView2);
        TextView pcsText = listViewItem.findViewById(R.id.textView3);
        TextView qtyText = listViewItem.findViewById(R.id.textView4);

        AsnGetSet asnGetSet = asnGetSetList.get(position);

        upcText.setText(asnGetSet.getUpc().toUpperCase());
        if(asnGetSet.getSn() != null) {
            upcText.setText(asnGetSet.getSn().toUpperCase());
        }
        skuText.setText(asnGetSet.getSku());
        pcsText.setText(asnGetSet.getPcs());
        qtyText.setText(asnGetSet.getQty());

        if(asnGetSet.getPcs().equals("0")) { // red
            upcText.setTextColor(Color.parseColor("#CD0015"));
            skuText.setTextColor(Color.parseColor("#CD0015"));
            qtyText.setTextColor(Color.parseColor("#CD0015"));
        }
        if(asnGetSet.getPcs().equals("2")) { // green
            upcText.setTextColor(Color.parseColor("#77B24C"));
            skuText.setTextColor(Color.parseColor("#77B24C"));
            qtyText.setTextColor(Color.parseColor("#77B24C"));
        }

        if(column == 1) {
            qtyText.setVisibility(View.GONE);
            pcsText.setVisibility(View.GONE);
            skuText.setVisibility(View.GONE);
        }

        if(column == 2) {
            pcsText.setVisibility(View.GONE);
            skuText.setVisibility(View.GONE);
        }

        if(column == 3) {
            pcsText.setVisibility(View.GONE);
        }

        if(column == 4) {
            pcsText.setVisibility(View.GONE);
            upcText.setVisibility(View.GONE);
        }

        return listViewItem;
    }
}
