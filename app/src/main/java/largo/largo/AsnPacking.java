package largo.largo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AsnPacking extends AppCompatActivity {
    String TAG = this.getClass().getSimpleName();

    private Button backButton, nextButton;
    private ImageButton scanButton;
    private EditText packingDocument;
    private String param;
    private String fClass = this.getClass().getSimpleName();
    private Intent intent;
    private ListView listView;
    private List<PackingDocumentGetterSetter> dokumenItemList;
    private PackingDocumentGetterSetter packDocGetSet;
    private List<String> docList;

    DatabaseHelper myDb;
    String url;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.outbound_packing_document_by_asn);

        intent = getIntent();
        param = this.getClass().getSimpleName();

        myDb = new DatabaseHelper(this);
        getServerURL();

        backButton = findViewById(R.id.back);
        nextButton = findViewById(R.id.next);
        listView = findViewById(R.id.ListPackingDoc);
        packingDocument = findViewById(R.id.picking_document);

        docList = new ArrayList<>();
        if(intent.getSerializableExtra("docList") != null) {
            docList = (List<String>) intent.getSerializableExtra("docList");
        }
        dokumenItemList = new ArrayList<>();
        DataListDokument(url + "/Api_packing/load/");

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(packingDocument.getText().toString().length() > 0) {
                    String[] strArr = packingDocument.getText().toString().split("#");
                    Intent intent = new Intent(getApplicationContext(), AsnPackingScanQR.class);
                    intent.putExtra("picking_code", strArr[0]);
                    intent.putExtra("outbound_code", strArr[1]);
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "Please choose document first.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        packingDocument.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 0) {
                    if(packingDocument.getText().toString().length() > 0) {
                        String[] strArr = packingDocument.getText().toString().split("#");
                        Intent intent = new Intent(getApplicationContext(), AsnPackingScanQR.class);
                        intent.putExtra("picking_code", strArr[0]);
                        intent.putExtra("outbound_code", strArr[1]);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getApplicationContext(), "Please choose document first.", Toast.LENGTH_SHORT).show();
                    }
                }
                return false;
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                packingDocument.setText(dokumenItemList.get(position).getPl_name() + "#" + dokumenItemList.get(position).getKd_outbound());
                String[] strArr = packingDocument.getText().toString().split("#");
                Intent intent = new Intent(getApplicationContext(), AsnPackingScanQR.class);
                intent.putExtra("picking_code", strArr[0]);
                intent.putExtra("outbound_code", strArr[1]);
                startActivity(intent);
            }
        });

        scanButton = findViewById(R.id.scanButton);
        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getApplicationContext(), QRScanner.class);
                intent.putExtra("param", "asnPickingDoc");
                intent.putExtra("fClass", fClass);
                intent.putExtra("docList", (Serializable) docList);
                intent.putExtra("docItemList", (Serializable) dokumenItemList);
                intent.putExtra("packDocGetSet", packDocGetSet);
                startActivity(intent);
            }
        });
        // LocalBroadcastManager.getInstance(this).registerReceiver(qrScan, new IntentFilter("broadcastQrResult"));
        if(intent.getStringExtra("asnPickingDoc") != null) {
            if(docList.contains(intent.getStringExtra("asnPickingDoc"))) {
                Intent intent2 = new Intent(getApplicationContext(), AsnPickingItem.class);
                docList = (List<String>) intent.getSerializableExtra("docList");
                dokumenItemList = (List<PackingDocumentGetterSetter>) intent.getSerializableExtra("docItemList");
                packDocGetSet = (PackingDocumentGetterSetter) intent.getSerializableExtra("pickDocGetSet");
                intent2.putExtra("packing_document", intent.getStringExtra("asnPackingDoc"));
                startActivity(intent2);
            } else {
                Toast.makeText(getApplicationContext(), "Document not available.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void DataListDokument(String getDoc){
//        final ArrayList<String> items = new ArrayList<String>();

        RequestQueue requestQueue=Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, getDoc, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);
                try{
                    JSONObject jsonObject=new JSONObject(response);
                    if(jsonObject.getInt("status")==200){
                        JSONArray jsonArray1=jsonObject.getJSONArray("results");
                        for(int i=0;i<jsonArray1.length();i++){
                            JSONObject jsonObject1=jsonArray1.getJSONObject(i);
                            packDocGetSet = new PackingDocumentGetterSetter(
                                    jsonObject1.getString("pl_name"),
                                    jsonObject1.getString("kd_outbound"),
                                    jsonObject1.getString("qty"),
                                    jsonObject1.getString("picked_qty"),
                                    jsonObject1.getString("destination_name"),
                                    5
                            );
                            dokumenItemList.add(packDocGetSet);
                            docList.add(jsonObject1.getString("pl_name"));
                        }
                        PackingDocumentListAdapter adapter = new PackingDocumentListAdapter(dokumenItemList, getApplicationContext());

                        listView.setAdapter(adapter);

                    }
//                    spinner.setAdapter(new ArrayAdapter<String>(TallyReceivingItemSerial.this, android.R.layout.simple_spinner_dropdown_item, QC));
                }catch (JSONException e){e.printStackTrace();}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;


            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }
}
