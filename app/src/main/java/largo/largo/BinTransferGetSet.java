package largo.largo;

import java.io.Serializable;

public class BinTransferGetSet implements Serializable {
    String docNum;
    String item_name, order_qty, unit_code, scanned_qty;

    public BinTransferGetSet(String docNum) {
        this.docNum = docNum;
    }

    public BinTransferGetSet(String item_name, String order_qty, String unit_code, String scanned_qty) {
        this.item_name = item_name;
        this.order_qty = order_qty;
        this.unit_code = unit_code;
        this.scanned_qty = scanned_qty;
    }

    public String getDocNum() {
        return docNum;
    }

    public void setDocNum(String docNum) {
        this.docNum = docNum;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getOrder_qty() {
        return order_qty;
    }

    public void setOrder_qty(String order_qty) {
        this.order_qty = order_qty;
    }

    public String getUnit_code() {
        return unit_code;
    }

    public void setUnit_code(String unit_code) {
        this.unit_code = unit_code;
    }

    public String getScanned_qty() {
        return scanned_qty;
    }

    public void setScanned_qty(String scanned_qty) {
        this.scanned_qty = scanned_qty;
    }
}
