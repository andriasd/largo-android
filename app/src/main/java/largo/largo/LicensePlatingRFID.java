package largo.largo;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.zebra.rfid.api3.Antennas;
import com.zebra.rfid.api3.ENUM_TRANSPORT;
import com.zebra.rfid.api3.ENUM_TRIGGER_MODE;
import com.zebra.rfid.api3.HANDHELD_TRIGGER_EVENT_TYPE;
import com.zebra.rfid.api3.InvalidUsageException;
import com.zebra.rfid.api3.MEMORY_BANK;
import com.zebra.rfid.api3.OperationFailureException;
import com.zebra.rfid.api3.RFIDReader;
import com.zebra.rfid.api3.ReaderDevice;
import com.zebra.rfid.api3.Readers;
import com.zebra.rfid.api3.RfidEventsListener;
import com.zebra.rfid.api3.RfidReadEvents;
import com.zebra.rfid.api3.RfidStatusEvents;
import com.zebra.rfid.api3.START_TRIGGER_TYPE;
import com.zebra.rfid.api3.STATUS_EVENT_TYPE;
import com.zebra.rfid.api3.STOP_TRIGGER_TYPE;
import com.zebra.rfid.api3.TAG_FIELD;
import com.zebra.rfid.api3.TagAccess;
import com.zebra.rfid.api3.TagData;
import com.zebra.rfid.api3.TagStorageSettings;
import com.zebra.rfid.api3.TriggerInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class LicensePlatingRFID extends AppCompatActivity {

    Button cancelButton, doneButton, scanButton, scan32Button;
    String scan32;
    private LicensePlatingGetSet licensePlatingGetSet;
    private List<LicensePlatingGetSet> licensePlatingGetSetList = new ArrayList<>();
    private static AsyncTask<Void, Void, Boolean> connectReader;
    private static Readers readers;
    private static ArrayList availableRFIDReaderList;
    private static ReaderDevice readerDevice;
    public static RFIDReader reader;
    private String param, TAG;
    private String fClass = this.getClass().getSimpleName();
    private List<String> tagList;
    private ListView rfidListView;
    private List<AsnGetSet> asnArrayList;
    private AsnGetSet asnGetSet;
    private AsnListAdapter adapter;
    private EventHandler eventHandler;
    private Intent intent;
    private TextView totalItems, textView40;

    private TagAccess tagAccess;
    private TagAccess.ReadAccessParams readAccessParams;

    private JSONObject paramsJson;
    public static final Map<String, String> params = new HashMap<String, String>();
    public static final String MyPREFERENCES = "MyPrefs" ;

    DatabaseHelper myDb;
    String url;
    private int checkLoad = 0;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.license_plating_rfid);

        TAG = this.getClass().getSimpleName();
        myDb = new DatabaseHelper(this);
        getServerURL();

        intent = getIntent();
        param = this.getClass().getSimpleName();

        rfidListView = findViewById(R.id.rfid_list);
        totalItems = findViewById(R.id.qty);
        totalItems.setText("0");
        textView40 = findViewById(R.id.textView40);

        tagList = new ArrayList<>();
        if(intent.getSerializableExtra("tagList") != null) {
            tagList = (List<String>) intent.getSerializableExtra("tagList");
            System.out.println("tagListAfter : " + tagList);
        }
        asnArrayList = new ArrayList<>();
        if(intent.getSerializableExtra("asnArrayList") != null) {
            asnArrayList = (List<AsnGetSet>) intent.getSerializableExtra("asnArrayList");
            totalItems.setText(Integer.toString(asnArrayList.size()));
            try {
                if(reader != null) {
                    reader.Config.setTriggerMode(ENUM_TRIGGER_MODE.RFID_MODE, true);
                }
            } catch (InvalidUsageException e) {
                e.printStackTrace();
            } catch (OperationFailureException e) {
                e.printStackTrace();
            }
        }

        tagAccess = new TagAccess();
        readAccessParams = tagAccess.new ReadAccessParams();
        readAccessParams.setAccessPassword(0);
        readAccessParams.setCount(8);
        readAccessParams.setMemoryBank(MEMORY_BANK.MEMORY_BANK_EPC);
        readAccessParams.setOffset(2);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter = new AsnListAdapter(asnArrayList, getApplicationContext(), 2);
                rfidListView.setAdapter(adapter);
            }
        });

        cancelButton = findViewById(R.id.cancel);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        scanButton = findViewById(R.id.scan);
        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(scanButton.getText().equals("SCAN")) {
                    scan32 = "1";
                    triggerStart();
                } else if(scanButton.getText().equals("STOP")) {
                    triggerStop();
                } else {
                    triggerStop();
                    triggerStart();
                }
            }
        });

        scan32Button = findViewById(R.id.scan32);
        scan32Button.setVisibility(View.GONE);
        scan32Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(scan32Button.getText().equals("SCAN32")) {
                    scan32 = "1";
                    triggerStart();
                } else if(scan32Button.getText().equals("STOP32")) {
                    triggerStop();
                } else {

                }
            }
        });

        doneButton = findViewById(R.id.done);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(doneButton.getText().equals("POST")) {
                    if(checkLoad == 0) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                doneButton.setEnabled(false);
                            }
                        });

                        for(int i = 0; i < asnArrayList.size(); i++) {
                            if(asnArrayList.get(i).getPcs().equals("1")) {
                                new validateSNASync(asnArrayList.get(i).getUpc(), i).execute();
                            }
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Sedang proses posting data...", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if(licensePlatingGetSetList.size() != asnArrayList.size()) {
                        try {
                            AlertDialog.Builder builder = new AlertDialog.Builder(LicensePlatingRFID.this);
                            builder.setTitle("Alert");
                            builder.setMessage("Serial number yang tidak terdaftar dalam sistem sebelumnya tidak akan berubah, lanjutkan ?");
                            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                    intent = new Intent(getApplicationContext(), LicensePlatingPC.class);
                                    intent.putExtra("tagList", (Serializable) tagList);
                                    intent.putExtra("licensePlatingGetSet", licensePlatingGetSet);
                                    intent.putExtra("licensePlatingGetSetList", (Serializable) licensePlatingGetSetList);
                                    startActivity(intent);
                                    try {
                                        if (reader != null) {
                                            triggerStop();
                                            reader.Config.setTriggerMode(ENUM_TRIGGER_MODE.BARCODE_MODE, true);
                                            reader.Events.removeEventsListener(eventHandler);
                                            reader.disconnect();
                                            Toast.makeText(getApplicationContext(), "Disconnecting reader", Toast.LENGTH_SHORT).show();
                                            reader = null;
                                            readers.Dispose();
                                            readers = null;
                                            scanButton.setText("Connect to Reader");
                                            scanButton.setBackgroundColor(Color.parseColor("#FFBEBE"));
                                            scan32Button.setText("Connect to Reader");
                                            scan32Button.setBackgroundColor(Color.parseColor("#FFBEBE"));
                                        }
                                    } catch (InvalidUsageException e) {
                                        e.printStackTrace();
                                    } catch (OperationFailureException e) {
                                        e.printStackTrace();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        } catch (WindowManager.BadTokenException bde) {
                            bde.printStackTrace();
                        } catch (IllegalStateException ise) {
                            ise.printStackTrace();
                        }
                    } else {
                        finish();
                        intent = new Intent(getApplicationContext(), LicensePlatingPC.class);
                        intent.putExtra("tagList", (Serializable) tagList);
                        intent.putExtra("licensePlatingGetSet", licensePlatingGetSet);
                        intent.putExtra("licensePlatingGetSetList", (Serializable) licensePlatingGetSetList);
                        startActivity(intent);
                        try {
                            if (reader != null) {
                                triggerStop();
                                reader.Config.setTriggerMode(ENUM_TRIGGER_MODE.BARCODE_MODE, true);
                                reader.Events.removeEventsListener(eventHandler);
                                reader.disconnect();
                                Toast.makeText(getApplicationContext(), "Disconnecting reader", Toast.LENGTH_SHORT).show();
                                reader = null;
                                readers.Dispose();
                                readers = null;
                                scanButton.setText("Connect to Reader");
                                scanButton.setBackgroundColor(Color.parseColor("#FFBEBE"));
                                scan32Button.setText("Connect to Reader");
                                scan32Button.setBackgroundColor(Color.parseColor("#FFBEBE"));
                            }
                        } catch (InvalidUsageException e) {
                            e.printStackTrace();
                        } catch (OperationFailureException e) {
                            e.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });

        // SDK
        if (readers == null) {
            readers = new Readers(this, ENUM_TRANSPORT.SERVICE_SERIAL);
        }

        connectReader = new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... voids) {
                try {
                    if (readers != null) {
                        if (readers.GetAvailableRFIDReaderList() != null) {
                            availableRFIDReaderList = readers.GetAvailableRFIDReaderList();
                            if (availableRFIDReaderList.size() != 0) {
                                // get first reader from list
                                readerDevice = (ReaderDevice) availableRFIDReaderList.get(0);
                                reader = readerDevice.getRFIDReader();
                                if (!reader.isConnected()) {
                                    // Establish connection to the RFID Reader
                                    reader.connect();
                                    ConfigureReader();
                                    return true;
                                }
                            }
                        }
                    }
                } catch (InvalidUsageException e) {
                    e.printStackTrace();
                } catch (OperationFailureException e) {
                    e.printStackTrace();
                    Log.d(TAG, "OperationFailureException " + e.getVendorMessage());
                }
                return false;
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                if (aBoolean) {
                    Toast.makeText(getApplicationContext(), "Reader Connected", Toast.LENGTH_SHORT).show();
                    scanButton.setText("SCAN");
                    scanButton.setBackgroundColor(Color.parseColor("#32C5D2"));
                    scan32Button.setText("SCAN32");
                    scan32Button.setBackgroundColor(Color.parseColor("#32C5D2"));
                }
            }
        };

        Toast.makeText(LicensePlatingRFID.this, "Connecting to Reader...", Toast.LENGTH_SHORT).show();
        connectReader.execute();
    }

    private void triggerStop() {
        try {
            reader.Actions.Inventory.stop();
        } catch (InvalidUsageException e) {
            e.printStackTrace();
        } catch (OperationFailureException e) {
            e.printStackTrace();
        }
        scanButton.setText("SCAN");
        scanButton.setBackgroundColor(Color.parseColor("#32C5D2"));
        scan32Button.setText("SCAN32");
        scan32Button.setBackgroundColor(Color.parseColor("#32C5D2"));
    }

    private void triggerStart() {
        try {
            reader.Actions.Inventory.perform();
        } catch (InvalidUsageException e) {
            e.printStackTrace();
        } catch (OperationFailureException e) {
            e.printStackTrace();
        }
        scanButton.setText("STOP");
        scanButton.setBackgroundColor(Color.parseColor("#FFBEBE"));
        scan32Button.setText("STOP32");
        scan32Button.setBackgroundColor(Color.parseColor("#FFBEBE"));
    }

    private void ConfigureReader() {
        if (reader.isConnected()) {
            TriggerInfo triggerInfo = new TriggerInfo();
            triggerInfo.StartTrigger.setTriggerType(START_TRIGGER_TYPE.START_TRIGGER_TYPE_IMMEDIATE);
            triggerInfo.StopTrigger.setTriggerType(STOP_TRIGGER_TYPE.STOP_TRIGGER_TYPE_IMMEDIATE);

            TagStorageSettings tagStorageSettings = new TagStorageSettings();
            tagStorageSettings.setTagFields(TAG_FIELD.ALL_TAG_FIELDS);

            try {
                Antennas.AntennaRfConfig antennaRfConfig = reader.Config.Antennas.getAntennaRfConfig(1);
                antennaRfConfig.setrfModeTableIndex(0);
                antennaRfConfig.setTari(0);
                antennaRfConfig.setTransmitPowerIndex(150);
                // set the configuration
                reader.Config.Antennas.setAntennaRfConfig(1, antennaRfConfig);

                // receive events from reader
                if (eventHandler == null)
                    eventHandler = new EventHandler();
                reader.Events.addEventsListener(eventHandler);
                // HH event
                reader.Events.setHandheldEvent(true);
                // tag event with tag data
                reader.Events.setTagReadEvent(true);
                // application will collect tag using getReadTags API
                reader.Events.setAttachTagDataWithReadEvent(false);
                // set trigger mode as rfid so scanner beam will not come
                reader.Config.setTriggerMode(ENUM_TRIGGER_MODE.RFID_MODE, true);
                // set start and stop triggers
                reader.Config.setStartTrigger(triggerInfo.StartTrigger);
                reader.Config.setStopTrigger(triggerInfo.StopTrigger);
                reader.Config.setUniqueTagReport(true);
                reader.Config.setTagStorageSettings(tagStorageSettings);

            } catch (InvalidUsageException e) {
                e.printStackTrace();
            } catch (OperationFailureException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (reader != null) {
                triggerStop();
                reader.Config.setTriggerMode(ENUM_TRIGGER_MODE.BARCODE_MODE, true);
                reader.Events.removeEventsListener(eventHandler);
                reader.disconnect();
                Toast.makeText(getApplicationContext(), "Disconnecting reader", Toast.LENGTH_SHORT).show();
                reader = null;
                readers.Dispose();
                readers = null;
                scanButton.setText("Connect to Reader");
                scanButton.setBackgroundColor(Color.parseColor("#FFBEBE"));
                scan32Button.setText("Connect to Reader");
                scan32Button.setBackgroundColor(Color.parseColor("#FFBEBE"));
            }
        } catch (InvalidUsageException e) {
            e.printStackTrace();
        } catch (OperationFailureException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Read/Status Notify handler
    // Implement the RfidEventsLister class to receive event notifications
    public class EventHandler implements RfidEventsListener {
        // Read Event Notification
        public void eventReadNotify(RfidReadEvents e) {
            try {
                // Recommended to use new method getReadTagsEx for better performance in case of large tag population
                final TagData[] myTags = reader.Actions.getReadTags(500);
                String rfidTagHex = "";
                String rfidTagText = "";

                if (myTags != null) {
                    for (int index = 0; index < myTags.length; index++) {
                        rfidTagHex = myTags[index].getTagID().toUpperCase();
                        if(rfidTagHex.equals("111122223333444455559999")) {
                            rfidTagHex = "36314a4130304d32323038313930303031".toUpperCase();
                        } else if(rfidTagHex.equals("36314A4130304D3000000009")) {
                            rfidTagHex = "36314a4130304d32323038313930303032".toUpperCase();
                        } else if(rfidTagHex.equals("0000000000000029")) {
                            rfidTagHex = "36314a4130304d32323038313930303033".toUpperCase();
                        } else if(rfidTagHex.equals("00000000010101122825000000000028")) {
                            rfidTagHex = "36314a4130304d32323038313930303034".toUpperCase();
                        }
                        rfidTagText = hexToString(rfidTagHex).toUpperCase();
                        System.out.println(rfidTagHex);

                        try {
                            if(tagList != null) {
                                if(!tagList.contains(rfidTagHex)) {
                                    final String finalRfidTagHex = rfidTagHex;
                                    final String finalRfidTagText = rfidTagText;
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                tagList.add(finalRfidTagHex);
                                                asnGetSet = new AsnGetSet(finalRfidTagHex, finalRfidTagText.substring(0, 20), finalRfidTagText.substring(0, 12), "1", "1");
                                                asnArrayList.add(asnGetSet);
                                                adapter.notifyDataSetChanged();
                                                totalItems.setText(Integer.toString(asnArrayList.size()));
                                            } catch(IndexOutOfBoundsException ex) {
                                                ex.printStackTrace();
                                            }
                                        }
                                    });
                                }
                            } else {
                                final String finalRfidTagHex1 = rfidTagHex;
                                final String finalRfidTagText1 = rfidTagText;
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            tagList.add(finalRfidTagHex1);
                                            asnGetSet = new AsnGetSet(finalRfidTagHex1, finalRfidTagText1.substring(0, 20), finalRfidTagText1.substring(0, 12), "1", "1");
                                            asnArrayList.add(asnGetSet);
                                            adapter.notifyDataSetChanged();
                                            totalItems.setText(Integer.toString(asnArrayList.size()));
                                        } catch(IndexOutOfBoundsException ex) {
                                            ex.printStackTrace();
                                        }
                                    }
                                });
                            }
                        } catch (StringIndexOutOfBoundsException ex) {
                            Toast.makeText(getApplicationContext(), "Format barcode tidak sesuai.", Toast.LENGTH_SHORT).show();
                        } catch (IndexOutOfBoundsException ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            } catch(IllegalStateException ex) {
                ex.printStackTrace();
            }
        }

        // Status Event Notification
        public void eventStatusNotify(RfidStatusEvents rfidStatusEvents) {
            Log.d(TAG, "Status Notification: " + rfidStatusEvents.StatusEventData.getStatusEventType());
            if (rfidStatusEvents.StatusEventData.getStatusEventType() == STATUS_EVENT_TYPE.HANDHELD_TRIGGER_EVENT) {
                if (rfidStatusEvents.StatusEventData.HandheldTriggerEventData.getHandheldEvent() == HANDHELD_TRIGGER_EVENT_TYPE.HANDHELD_TRIGGER_PRESSED) {
                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... voids) {
                            try {
                                reader.Actions.Inventory.perform();
                            } catch (InvalidUsageException e) {
                                e.printStackTrace();
                            } catch (OperationFailureException e) {
                                e.printStackTrace();
                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            scanButton.setText("STOP");
                            scanButton.setBackgroundColor(Color.parseColor("#FFBEBE"));
                            scan32Button.setText("STOP32");
                            scan32Button.setBackgroundColor(Color.parseColor("#FFBEBE"));
                        }
                    }.execute();
                }
                if (rfidStatusEvents.StatusEventData.HandheldTriggerEventData.getHandheldEvent() == HANDHELD_TRIGGER_EVENT_TYPE.HANDHELD_TRIGGER_RELEASED) {
                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... voids) {
                            try {
                                reader.Actions.Inventory.stop();
                            } catch (InvalidUsageException e) {
                                e.printStackTrace();
                            } catch (OperationFailureException e) {
                                e.printStackTrace();
                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            scanButton.setText("SCAN");
                            scanButton.setBackgroundColor(Color.parseColor("#32C5D2"));
                            scan32Button.setText("SCAN32");
                            scan32Button.setBackgroundColor(Color.parseColor("#32C5D2"));
                        }
                    }.execute();
                }
            }
        }
    }

    private class validateSNASync extends AsyncTask<Void, Void, Void> {
        String serialNumberParam = "";
        int check;

        validateSNASync(String serialNumberParam, int check) {
            super();
            this.serialNumberParam = serialNumberParam;
            this.check = check;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            checkLoad = check;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            StringRequest strReq = new StringRequest(Request.Method.POST,url+"/Api_license_plating/validateSN/", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if(check == checkLoad) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                checkLoad = 0;
                                doneButton.setEnabled(true);
                                doneButton.setText("NEXT");
                            }
                        });
                    }

                    try {
                        Log.d("validateSN", response);
                        JSONObject data = new JSONObject(response);

                        Integer status = data.getInt("status");
                        String message = data.getString("message");

                        if(message.contains("is valid")) {
                            licensePlatingGetSet = new LicensePlatingGetSet(hexToString(serialNumberParam), serialNumberParam);
                            licensePlatingGetSetList.add(licensePlatingGetSet);
                        } else {
                            for(int i = 0; i < asnArrayList.size(); i++) {
                                if(asnArrayList.get(i).getUpc().toUpperCase().equals(serialNumberParam.toUpperCase())) {
                                    asnGetSet = new AsnGetSet(serialNumberParam, serialNumberParam.substring(0, 20), serialNumberParam.substring(0, 12), "0", "1");
                                    asnArrayList.set(i, asnGetSet);
                                    adapter.notifyDataSetChanged();
                                }
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    NetworkResponse networkResponse = error.networkResponse;
                    if (networkResponse != null && networkResponse.data != null) {
                        String jsonError = new String(networkResponse.data);

                        try {
                            JSONObject data = new JSONObject(jsonError);

                            String message = data.getString("message");

                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            })  {

                @Override
                protected Map<String, String> getParams(){
                    Map<String, String> params = new HashMap<String, String>();

                    params.put("kd_unik", serialNumberParam.toUpperCase());

                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    SharedPreferences prefs = getSharedPreferences(LoginActivity.MyPREFERENCES, MODE_PRIVATE);
                    String restoredText = prefs.getString("handheldsessioncodeKey", null);

                    String usernameKey = prefs.getString("usernameKey", "No name defined");
                    String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                    Log.d(TAG, usernameKey);
                    Log.d(TAG, handheldsessioncodeKey);

                    Map<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "application/x-www-form-urlencoded");
                    headers.put("User",usernameKey);
                    headers.put("Authorization", handheldsessioncodeKey);
                    return headers;
                }
            };

            controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
            return null;
        }
    }

    public static String hexToString(final String tagID) {
        if(tagID != null) {
            List<String> forbiddenChar = new ArrayList<>();
            forbiddenChar.add("");

            StringBuilder sb = new StringBuilder();
            char[] hexData = tagID.toCharArray();
            for (int count = 0; count < hexData.length - 1; count += 2) {
                int firstDigit = Character.digit(hexData[count], 16);
                int lastDigit = Character.digit(hexData[count + 1], 16);
                int decimal = firstDigit * 16 + lastDigit;
                if(!forbiddenChar.contains((char)decimal)) {
                    sb.append((char)decimal);
                }
            }
            return sb.toString();
        } else {
            return "";
        }
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }
}