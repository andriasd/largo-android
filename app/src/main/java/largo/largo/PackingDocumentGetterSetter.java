package largo.largo;

import java.io.Serializable;

public class PackingDocumentGetterSetter implements Serializable {
    String pl_name, kd_outbound, qty, picked_qty, destination_name;
    boolean is_checked;
    int column;

    public String getDestination_name() {
        return destination_name;
    }

    public void setDestination_name(String destination_name) {
        this.destination_name = destination_name;
    }

    public boolean isIs_checked() {
        return is_checked;
    }

    public void setIs_checked(boolean is_checked) {
        this.is_checked = is_checked;
    }

    public PackingDocumentGetterSetter(String pl_name, String kd_outbound, String qty, String picked_qty, String destination_name, int column) {
        this.pl_name = pl_name;
        this.kd_outbound = kd_outbound;
        this.qty = qty;
        this.picked_qty = picked_qty;
        this.column = column;
        this.destination_name = destination_name;
    }

    public String getPl_name() {
        return pl_name;
    }

    public void setPl_name(String pl_name) {
        this.pl_name = pl_name;
    }

    public String getKd_outbound() {
        return kd_outbound;
    }

    public void setKd_outbound(String kd_outbound) {
        this.kd_outbound = kd_outbound;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getPicked_qty() {
        return picked_qty;
    }

    public void setPicked_qty(String picked_qty) {
        this.picked_qty = picked_qty;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public boolean isChecked() {
        return is_checked;
    }

    public void set_checked(boolean is_checked) {
        this.is_checked = is_checked;
    }
}
