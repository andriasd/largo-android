package largo.largo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AsnPackingScanQR extends AppCompatActivity {
    String TAG = this.getClass().getSimpleName();
    private Button backButton, doneButton, nextPack;
    private EditText asnQr;
    private Intent intent;
    private ListView listView;
    private List<PackingDocumentGetterSetter> dokumenItemList;
    private PackingDocumentGetterSetter packDocGetSet;
    private List<String> docList;
    private String outbound_code, picking_code, packing_code, inf_text, pac_text;
    private PackingDocumentListAdapter adapter;
    private TextView information_text, pack_text;

    public static final Map<String, String> params = new HashMap<String, String>();
    public static final String MyPREFERENCES = "MyPrefs" ;

    DatabaseHelper myDb;
    String url;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.outbound_packing_by_asn_qr);

        intent = getIntent();

        outbound_code = intent.getStringExtra("outbound_code");
        picking_code = intent.getStringExtra("picking_code");

        myDb = new DatabaseHelper(this);
        getServerURL();

        listView = findViewById(R.id.QRList);
        information_text = findViewById(R.id.information_text);
        pack_text = findViewById(R.id.pack_text);

        if(intent.getStringExtra("inf_text") != null) {
            inf_text = intent.getStringExtra("inf_text");
            pac_text = intent.getStringExtra("pac_text");
            packing_code = intent.getStringExtra("packing_code");
            information_text.setText(inf_text);
            pack_text.setText(pac_text);
        } else {
            get_pc_code(url + "/Api_packing/get_pc_code/" + outbound_code + "/" + picking_code);
        }

        backButton = findViewById(R.id.back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        nextPack = findViewById(R.id.next_pack);
        nextPack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject jsonObject = null;
                String tmp_packing_code = "";
                int tmp_inc = 0;
                try {
                    jsonObject = new JSONObject(packing_code);
                    tmp_packing_code = jsonObject.getString("pc_code");
                    tmp_inc = jsonObject.getInt("inc");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                params.clear();
                params.put("id_outbound", outbound_code);
                params.put("picking_code", picking_code);
                params.put("pc_code", tmp_packing_code);
                params.put("inc", String.valueOf(tmp_inc));
                get_next_pc_code(params, packing_code);
            }
        });

        doneButton = findViewById(R.id.next);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AsnPackingScanQR.this,AsnPacking.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        asnQr = findViewById(R.id.asnQr);
        asnQr.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 0) {
                    params.clear();
                    params.put("serial_number", asnQr.getText().toString());
                    params.put("outbound_code", outbound_code);
                    params.put("packing_code", packing_code);
                    postPacking(params);
                }
                return false;
            }
        });

        docList = new ArrayList<>();
        if(intent.getSerializableExtra("docList") != null) {
            docList = (List<String>) intent.getSerializableExtra("docList");
        }
        dokumenItemList = new ArrayList<>();
        adapter = new PackingDocumentListAdapter(dokumenItemList, getApplicationContext());
        listView.setAdapter(adapter);

        params.clear();
        params.put("outbound_code", outbound_code);
        params.put("picking_code", picking_code);
        getItems(params);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                String[] split = dokumenItemList.get(position).getPl_name().split(" - ");
                Intent intent = new Intent(AsnPackingScanQR.this,AsnPackingItemCheck.class);
                intent.putExtra("outbound_code", outbound_code);
                intent.putExtra("picking_code", picking_code);
                intent.putExtra("packing_code", packing_code);
                intent.putExtra("inf_text", inf_text);
                intent.putExtra("pac_text", pac_text);
                intent.putExtra("item_info", dokumenItemList.get(position).getPl_name());
                intent.putExtra("item_code", split[1]);
                startActivity(intent);
            }
        });
    }

    private void get_next_pc_code_BAK(final Map<String, String> parameters, final String jsonPacking){
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(jsonPacking);
            String tmp_packing_code = jsonObject.getString("pc_code");
            int tmp_inc = jsonObject.getInt("inc");
            String tmp_pc_string = jsonObject.getString("pc_string");
            tmp_inc = tmp_inc + 1;

            inf_text = picking_code + ", " + outbound_code + "\n";
            inf_text = inf_text + "Packing Label: " + tmp_packing_code;
            pac_text = "COLLY\n" + tmp_inc;
            information_text.setText(inf_text);
            pack_text.setText(pac_text);

            packing_code = "{\"pc_code\":\"" +tmp_packing_code+ "\",\"inc\":" +tmp_inc+1+ ",\"pc_string\":\"<b>"+tmp_pc_string+"<\\/b>\"}";
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void get_next_pc_code(final Map<String, String> parameters, final String jsonPacking){
        String postUrl = this.url + "/Api_packing/check_pc_code/";
        System.out.println("get_next_pc_codeParams() : " + parameters);

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, response);
                        try{
                            JSONObject jsonResult = new JSONObject(response);
                            if(jsonResult.getString("status").equals("OK")) {
                                JSONObject jsonObject = new JSONObject(jsonPacking);
                                String tmp_packing_code = jsonObject.getString("pc_code");
                                int tmp_inc = jsonObject.getInt("inc");
                                String tmp_pc_string = jsonObject.getString("pc_string");
                                tmp_inc = tmp_inc + 1;

                                inf_text = picking_code + ", " + outbound_code + "\n";
                                inf_text = inf_text + "Packing Label: " + tmp_packing_code;
                                pac_text = "Colly\n" + tmp_inc;
                                information_text.setText(inf_text);
                                pack_text.setText(pac_text);

                                packing_code = "{\"pc_code\":\"" +tmp_packing_code+ "\",\"inc\":" +tmp_inc+ ",\"pc_string\":\"<b>"+tmp_pc_string+"<\\/b>\"}";
                            } else {
                                String message = jsonResult.getString("message");
                                Toast.makeText(AsnPackingScanQR.this, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e){ e.printStackTrace(); }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;

            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    private void get_pc_code(String getDoc){
        RequestQueue requestQueue=Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, getDoc, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);
                try{
                    JSONObject jsonObject=new JSONObject(response);
                    packing_code = jsonObject.toString();
                    inf_text = picking_code + ", " + outbound_code + "\n";
                    inf_text = inf_text + "Packing Label: " + jsonObject.getString("pc_code");
                    pac_text = "Colly\n" + jsonObject.getString("inc");
                    information_text.setText(inf_text);
                    pack_text.setText(pac_text);
                }catch (JSONException e){e.printStackTrace();}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;


            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void getItems(final Map<String, String> parameters) {
        String postUrl = this.url + "/Api_packing/get_items/";
        System.out.println("postPickingParams() : " + parameters);

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, response);
                        try{
                            JSONObject jsonObject=new JSONObject(response);
                            if(jsonObject.getInt("status")==200){
                                JSONArray jsonArray1=jsonObject.getJSONArray("results");
                                dokumenItemList.clear();
                                for(int i=0;i<jsonArray1.length();i++){
                                    JSONObject jsonObject1=jsonArray1.getJSONObject(i);
                                    packDocGetSet = new PackingDocumentGetterSetter(
                                            jsonObject1.getString("kd_barang"),
                                            "",
                                            jsonObject1.getString("doc_qty"),
                                            jsonObject1.getString("act_qty"),
                                            "",
                                            3
                                    );
                                    dokumenItemList.add(packDocGetSet);
                                    docList.add(jsonObject1.getString("kd_barang"));
                                }
                                adapter.notifyDataSetChanged();
                            }
                        } catch (JSONException e){ e.printStackTrace(); }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;

            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    private void postPacking(final Map<String, String> parameters) {
        String postUrl = this.url + "/Api_packing/post/";
        System.out.println("postPickingParams() : " + parameters);

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if(jsonObject.getInt("status") == 200){
                                params.clear();
                                params.put("outbound_code", outbound_code);
                                params.put("picking_code", picking_code);
                                getItems(params);
                                asnQr.requestFocus();
                            } else {
                                String message = jsonObject.getString("message");
                                Toast.makeText(AsnPackingScanQR.this, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) { e.printStackTrace(); }
                        asnQr.setText("");
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;

            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }
}
