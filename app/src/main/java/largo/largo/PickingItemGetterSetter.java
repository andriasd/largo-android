package largo.largo;

import java.io.Serializable;

public class PickingItemGetterSetter implements Serializable {
    String sku, picked_qty, has_qty, qty, multi_qty, location;
    boolean check;

    public PickingItemGetterSetter(String sku, String picked_qty, String has_qty, String qty, String multi_qty, String location, boolean check) {
        this.sku = sku;
        this.picked_qty = picked_qty;
        this.has_qty = has_qty;
        this.qty = qty;
        this.multi_qty = qty;
        this.location = location;
        this.check = check;
    }

    public PickingItemGetterSetter(String sku, String picked_qty, String has_qty, String qty, String multi_qty, String location) {
        this.sku = sku;
        this.picked_qty = picked_qty;
        this.has_qty = has_qty;
        this.qty = qty;
        this.multi_qty = qty;
        this.location = location;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getPickedQty() {
        return picked_qty;
    }

    public void setPickedQty(String picked_qty) {
        this.picked_qty = picked_qty;
    }

    public String getHasQty() {
        return has_qty;
    }

    public void setHasQty(String has_qty) {
        this.has_qty = has_qty;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getMultiQty() {
        return multi_qty;
    }

    public void setMultiQty(String multi_qty) {
        this.multi_qty = multi_qty;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public boolean getCheckBox(){
        return check;
    }

    public void setCheckBox(boolean check){
        this.check = check;
    }
}
