package largo.largo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AsnPickingOBDoc extends AppCompatActivity {
    String TAG = this.getClass().getSimpleName();

    private Button backButton, nextButton;
    private Intent intent;
    private ListView listView;
    private List<PickingItemGetterSetter> itemList;
    private List<String> docList;
    private EditText asnQr;
    private String pickingDocument;
    private PickingItemListAdapter adapter;
    private PickingItemGetterSetter pickingItemGetSet;
    private TextView header_text;
    int checkPost;

    private JSONObject paramsJson;
    public static final Map<String, String> params = new HashMap<String, String>();
    public static final String MyPREFERENCES = "MyPrefs" ;

    DatabaseHelper myDb;
    String url;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.outbound_picking_document_by_asn_obdoc);

        intent = getIntent();
        checkPost = 0;

        paramsJson = new JSONObject();

        myDb = new DatabaseHelper(this);
        getServerURL();

        backButton = findViewById(R.id.back);
        nextButton = findViewById(R.id.next);
        listView = findViewById(R.id.ListPickingDoc);
        header_text = findViewById(R.id.header_picking);

        docList = new ArrayList<>();
        itemList = new ArrayList<>();
        pickingDocument = "";

        if(intent.getStringExtra("picking_document") != null) {
            pickingDocument = intent.getStringExtra("picking_document");
            getPicking(url + "/Api_picking/get_picking_by_area/" + pickingDocument);
        }

        String header_string = header_text.getText().toString();
        header_text.setText(header_string + " - " + pickingDocument);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    finish();
                    // Intent intent2 = new Intent(getApplicationContext(), AsnPickingItem.class);
                    Intent intent2 = new Intent(getApplicationContext(), AsnPicking.class);
                    intent2.putExtra("picking_document", pickingDocument);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(true) {
//                    intent = new Intent(getApplicationContext(), AsnPickingScanQR.class);
//                    intent.putExtra("picking_document", pickingDocument);
//                    intent.putExtra("outbound_code", asnQr.getText().toString());


                    intent = new Intent(getApplicationContext(), PickingDocumentItem.class);
                    intent.putExtra("picking_document", pickingDocument);
//                    intent.putExtra("outbound_code", asnQr.getText().toString());
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "Please input outbound document first.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void getPicking(String getDoc){
        if(itemList != null) {
            itemList.clear();
        }

        RequestQueue requestQueue=Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, getDoc, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG + "getPicking", response);
                try{
                    JSONObject jsonObject = new JSONObject(response);
                    if(jsonObject.getInt("status")==200){
                        JSONArray jsonArray1 = jsonObject.getJSONArray("results");
                        for(int i = 0; i < jsonArray1.length(); i++){
                            JSONObject jsonObject1=jsonArray1.getJSONObject(i);
                            System.out.println(jsonObject1);
                            pickingItemGetSet = new PickingItemGetterSetter(
                                    jsonObject1.getString("outbound_code"),
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    false
                            );
                            itemList.add(pickingItemGetSet);
                            docList.add(jsonObject1.getString("outbound_code"));
                        }

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                adapter = new PickingItemListAdapter(itemList, getApplicationContext());
                                listView.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                            }
                        });

                    }
//                    spinner.setAdapter(new ArrayAdapter<String>(TallyReceivingItemSerial.this, android.R.layout.simple_spinner_dropdown_item, QC));
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;


            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }
}