package largo.largo;

import java.io.Serializable;

public class CycleCountDocumentGetterSetter implements Serializable {
    String cc_code, cc_type, source, source2;

    public CycleCountDocumentGetterSetter(String cc_code, String cc_type, String source) {
        this.cc_code = cc_code;
        this.cc_type = cc_type;
        this.source = source;
    }

    public CycleCountDocumentGetterSetter(String cc_code, String cc_type, String source, String source2) {
        this.cc_code = cc_code;
        this.cc_type = cc_type;
        this.source = source;
        this.source2 = source2;
    }

    public String getCc_code() {
        return cc_code;
    }

    public String getCc_type() {
        return cc_type;
    }

    public String getSource() {
        return source;
    }

    public String getSource2() {
        return source2;
    }
}
