package largo.largo;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class InventoryTransferListAdapter extends ArrayAdapter<InventoryTransferGetSet> {
    private List<InventoryTransferGetSet> inventoryTransferGetSetList;

    private Context context;

    private int column;

    public InventoryTransferListAdapter(List<InventoryTransferGetSet> inventoryTransferGetSetList, Context context, int column) {
        super(context, R.layout.adapter_list_horizontal, inventoryTransferGetSetList);
        this.inventoryTransferGetSetList = inventoryTransferGetSetList;
        this.context = context;
        this.column = column;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);

        View listViewItem = inflater.inflate(R.layout.adapter_list_horizontal, null, true);

        TextView snText = listViewItem.findViewById(R.id.textView1);
        TextView locText = listViewItem.findViewById(R.id.textView2);
        TextView hexText = listViewItem.findViewById(R.id.textView3);
        TextView flagText = listViewItem.findViewById(R.id.textView4);

        InventoryTransferGetSet inventoryTransferGetSet = inventoryTransferGetSetList.get(position);

        snText.setText(inventoryTransferGetSet.getSn());
        locText.setText(inventoryTransferGetSet.getLoc());
        hexText.setText(inventoryTransferGetSet.getHex());

        if(inventoryTransferGetSet.getFlag().length() > 0) {
            snText.setTextColor(Color.parseColor("#77B24C"));
            locText.setTextColor(Color.parseColor("#77B24C"));
        }

        if(column == 2) {
            flagText.setVisibility(View.GONE);
            hexText.setVisibility(View.GONE);
        }

        return listViewItem;
    }
}
