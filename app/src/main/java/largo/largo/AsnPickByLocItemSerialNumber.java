package largo.largo;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AsnPickByLocItemSerialNumber extends AppCompatActivity {
    String TAG = this.getClass().getSimpleName();

    private Button backButton, nextButton, split_items;
    private Intent intent;
    private ListView listView;
    private List<PickingItemGetterSetter> itemList;
    private List<String> docList;
    private EditText asnQr, asnQty, tray_location;
    private String pickingDocument, item_code, location, outboundCode;
    private PickingItemListAdapter adapter;
    private PickingItemGetterSetter pickingItemGetSet;
    private TextView skuText, pickedText, header_text;
    private int trayCheck = 0;
    int checkPost;

    private String[] uomList;
    private Spinner uomSpinner;

    private JSONObject paramsJson;
    public static final Map<String, String> params = new HashMap<String, String>();
    public static final String MyPREFERENCES = "MyPrefs" ;

    DatabaseHelper myDb;
    String url;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.outbound_picking_by_loc_item_serial_number);

        intent = getIntent();
        checkPost = 0;

        paramsJson = new JSONObject();

        myDb = new DatabaseHelper(this);
        getServerURL();

        backButton = findViewById(R.id.back);
        nextButton = findViewById(R.id.next);
        asnQr = findViewById(R.id.asnQr);
        asnQty = findViewById(R.id.asnQty);
        tray_location = findViewById(R.id.tray_location);
        listView = findViewById(R.id.QRList);
        skuText = findViewById(R.id.textView15);
        pickedText = findViewById(R.id.textView16);
        header_text = findViewById(R.id.header_picking);
        uomSpinner = findViewById(R.id.uomSpinner);

        docList = new ArrayList<>();
        itemList = new ArrayList<>();
        pickingDocument = "";

        skuText.setText(intent.getStringExtra("sku"));

        if(intent.getStringExtra("picking_document") != null) {
            pickingDocument = intent.getStringExtra("picking_document");
            outboundCode = intent.getStringExtra("outbound_code");
            item_code = intent.getStringExtra("item_code");
            location = intent.getStringExtra("location");
            getPicking(url + "/Api_picking/get/" + pickingDocument + "/" + outboundCode + "/" + URLEncoder.encode(item_code.replace("*", "|")));
            getPickingItem(url + "/Api_picking/get_sn_by_location/" + location + "/" + URLEncoder.encode(item_code.replace("*", "|")));
        }

        String header_string = header_text.getText().toString();
        header_text.setText(header_string + " - " + pickingDocument + ", " + outboundCode);
        header_text.setSelected(true);

        split_items = findViewById(R.id.split_items);
        split_items.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        // Intent intent = new Intent(MenuActivity.this,LicensePlatingOption.class);
                        Intent intent = new Intent(AsnPickByLocItemSerialNumber.this,SplitItems.class);
                        startActivity(intent);
                    }
                }
        );

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    finish();
                    // Intent intent2 = new Intent(getApplicationContext(), AsnPickingItem.class);
                    // Intent intent2 = new Intent(getApplicationContext(), AsnPickByLocItem.class);
                    Intent intent2 = new Intent(getApplicationContext(), AsnPickingScanQR.class);
                    intent2.putExtra("picking_document", pickingDocument);
                    intent2.putExtra("outbound_code", outboundCode);
                    intent2.putExtra("item_code", item_code);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getApplicationContext(), AsnPickingLocation.class);
                intent.putExtra("picking_document", pickingDocument);
                intent.putExtra("outbound_code", outboundCode);
                startActivity(intent);
            }
        });

        tray_location.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                trayCheck = 0;
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 0) {
                    getTrayLocation(url + "/Api_picking/get_tray_location/" + tray_location.getText());
                }
                return false;
            }
        });

        asnQr.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 0) {
                    asnQty.setFocusableInTouchMode(true);
                    asnQty.setFocusable(true);
                    asnQty.requestFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(asnQty, InputMethodManager.SHOW_IMPLICIT);

                    params.clear();
                    params.put("picking_code", pickingDocument);
                    params.put("serial_number", asnQr.getText().toString());
                    getUomBySN(params);
                }
                return false;
            }
        });

        asnQty.setOnKeyListener(new View.OnKeyListener() {
            SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
            String usernameKey = prefs.getString("usernameKey", "No name defined");

            @Override
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 0) {
                    try {
                        if (asnQr.length() != 0 && Float.parseFloat(asnQty.getText().toString()) > 0) {
                            if(checkPost == 0) {
                                params.clear();
                                params.put("picking_code", intent.getStringExtra("picking_document"));
                                params.put("tray_location", tray_location.getText().toString());
                                params.put("outbound_code", outboundCode);
                                params.put("serial_number", asnQr.getText().toString());
                                params.put("qty", asnQty.getText().toString());
                                params.put("kit_time", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                                params.put("pick_time", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                                params.put("user", usernameKey);
                                params.put("item_code", item_code);
                                try {
                                    params.put("unit_code", uomList[uomSpinner.getSelectedItemPosition()]);
                                } catch (NullPointerException e) {
                                    params.put("unit_code", "");
                                }
                                checkPost = 1;
                                try {
                                    Thread.sleep(100);
                                    postPicking(params);
                                    System.out.println(Calendar.getInstance().getTime() + " asnQty.setOnKeyListener, params = " + params);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            if(asnQr.length() != 0) {
                                Toast.makeText(getApplicationContext(), "Qty harus diisi dan harus lebih besar dari 0.", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getApplicationContext(), "Masukkan Serial Number terlebih dahulu.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (NumberFormatException nfe) {
                        // showMessage("Alert", "Qty harus diisi dengan angka.");
                        Toast.makeText(getApplicationContext(), "Qty harus diisi dengan angka.", Toast.LENGTH_SHORT).show();
                        nfe.printStackTrace();
                    }
                }
                return false;
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String[] split = itemList.get(position).getLocation().split("<90>");
                asnQr.setText(split[0]);
                asnQty.setText(split[1]);
                asnQty.setFocusableInTouchMode(true);
                asnQty.setFocusable(true);
                asnQty.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(asnQty, InputMethodManager.SHOW_IMPLICIT);

                params.clear();
                params.put("picking_code", pickingDocument);
                params.put("serial_number", asnQr.getText().toString());
                params.put("item_code", item_code);
                getUomBySN(params);
            }
        });
    }

    private void getTrayLocation(final String getDoc){
        RequestQueue requestQueue=Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, getDoc, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);
                try{
                    JSONObject jsonObject=new JSONObject(response);
                    if(jsonObject.getInt("status") == 200){
                        asnQr.requestFocus();
                        trayCheck = 1;
                    } else {
                        Toast.makeText(getApplicationContext(), "Tray tidak terdaftar pada sistem.", Toast.LENGTH_SHORT).show();
                        trayCheck = 0;
                        tray_location.requestFocus();
                    }
                } catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void getUomBySN(final Map<String, String> parameters) {
        String postUrl = this.url + "/Api_picking/get_uom_by_serial_number/";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);

                        try {
                            JSONObject data = new JSONObject(response);
                            Integer status = data.getInt("status");
                            String results = data.getString("results");
                            if(status == 200) {
                                JSONObject unit_code = new JSONObject(results.substring(1, results.length()-1));
                                uomList = new String[unit_code.length()];
                                for(int i = 0; i < unit_code.length(); i++) {
                                    uomList[i] = unit_code.get("unit_code").toString();
                                }

                                CustomSpinnerAdapter customAdapter = new CustomSpinnerAdapter(getApplicationContext(), uomList);
                                uomSpinner.setAdapter(customAdapter);
                            }

                            asnQty.setFocusableInTouchMode(true);
                            asnQty.setFocusable(true);
                            asnQty.requestFocus();
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.showSoftInput(asnQty, InputMethodManager.SHOW_IMPLICIT);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    private void getPickingItem(String getDoc){
        if(itemList != null) {
            System.out.println("here");
            itemList.clear();
        }

        RequestQueue requestQueue=Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, getDoc, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG + "getPickingItem", response);
                try{
                    JSONObject jsonObject = new JSONObject(response);
                    if(jsonObject.getInt("status")==200){
                        JSONArray jsonArray1 = jsonObject.getJSONArray("results");
                        for(int i = 0; i < jsonArray1.length(); i++){
                            JSONObject jsonObject1=jsonArray1.getJSONObject(i);
                            System.out.println(jsonObject1);
                            pickingItemGetSet = new PickingItemGetterSetter(
                                    "","","","","",jsonObject1.getString("serial_number") + "<90>" + jsonObject1.getString("qty")
                            );
                            itemList.add(pickingItemGetSet);
                            docList.add(jsonObject1.getString("serial_number"));
                        }

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                adapter = new PickingItemListAdapter(itemList, getApplicationContext());
                                listView.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                            }
                        });

                    }
//                    spinner.setAdapter(new ArrayAdapter<String>(TallyReceivingItemSerial.this, android.R.layout.simple_spinner_dropdown_item, QC));
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;


            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void postPicking(final Map<String, String> parameters) {
//        System.out.println("PostPicking, " + params);
//        checkPost = 0;
        String postUrl = this.url + "/Api_picking/post_serial/";
        System.out.println("postPickingParams() : " + parameters);

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        checkPost = 0;

                        try {
                            JSONObject data = new JSONObject(response);
                            Integer status = data.getInt("status");
                            String message = data.getString("message");
                            if(status == 200) {
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
//                                getPicking(url + "/Api_picking/get/" + pickingDocument);
//                                getPickingItem(url + "/Api_picking/get_sn_by_location/" + location + "/" + item_code);
                                getPicking(url + "/Api_picking/get/" + pickingDocument + "/" + outboundCode + "/" + URLEncoder.encode(item_code.replace("*", "|")));
                                getPickingItem(url + "/Api_picking/get_sn_by_location/" + location + "/" + URLEncoder.encode(item_code.replace("*", "|")));
                            } else {
                                try {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(AsnPickByLocItemSerialNumber.this);
                                    builder.setTitle("Notification");
                                    builder.setMessage(Html.fromHtml(message));
                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                        }
                                    });
                                    AlertDialog dialog = builder.create();
                                    dialog.show();
                                } catch (WindowManager.BadTokenException bde) {
                                    bde.printStackTrace();
                                } catch (IllegalStateException ise) {
                                    ise.printStackTrace();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        asnQr.setText("");
                        asnQty.setText("0");
                        asnQr.requestFocus();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        System.out.println("postPicking() error! Params: " + parameters);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;

            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    private void getPicking(String getDoc){
        if(itemList != null) {
            System.out.println("hello");
            itemList.clear();
        }

        RequestQueue requestQueue=Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, getDoc, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG + "getPicking", response);
                try{
                    JSONObject jsonObject = new JSONObject(response);
                    if(jsonObject.getInt("status")==200){
                        JSONArray jsonArray1 = jsonObject.getJSONArray("locations");
                        for(int i = 0; i < jsonArray1.length(); i++){
                            JSONObject jsonObject1=jsonArray1.getJSONObject(i);
                            System.out.println(jsonObject1);
                            String[] item_code_result = jsonObject1.getString("kd_barang").split(" - ");
                            System.out.println(item_code_result[1]);
                            if(item_code_result[1].equals(item_code)) {
                                skuText.setText(jsonObject1.getString("kd_barang"));
                                pickedText.setText(jsonObject1.getString("picked_qty") + "/" + jsonObject1.getString("qty"));
                            }
                        }
                    }
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;


            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }
}
