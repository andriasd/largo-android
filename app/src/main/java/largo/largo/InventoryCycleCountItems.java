package largo.largo;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class InventoryCycleCountItems extends AppCompatActivity {

    private Button cancelButton, qrButton, rfidButton;
    private Intent intent;
    private String param, ccDocument, warehouse_name, cc_type, type, TAG, location;

    DatabaseHelper myDb;
    String url;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cycle_count_item);

        TAG = this.getClass().getSimpleName();
        myDb = new DatabaseHelper(this);
        getServerURL();

        intent = getIntent();
        param = this.getClass().getSimpleName();
        ccDocument = "";
        ccDocument = intent.getStringExtra("cc_document");
        warehouse_name = intent.getStringExtra("warehouse_name");
        cc_type = intent.getStringExtra("cc_type");
        type = intent.getStringExtra("type");
        location = intent.getStringExtra("location");

        cancelButton = findViewById(R.id.cancel);
        qrButton = findViewById(R.id.qr_button);
        rfidButton = findViewById(R.id.rfid_button);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        qrButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        intent = new Intent(getApplicationContext(), InventoryCycleCountScanQR.class);
        intent.putExtra("cc_document", ccDocument);
        intent.putExtra("warehouse_name", warehouse_name);
        intent.putExtra("cc_type", cc_type);
        intent.putExtra("type", type);
        intent.putExtra("location",location);
        startActivity(intent);

        System.out.println(ccDocument);

        rfidButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getApplicationContext(), InventoryCycleCountScanRFID.class);
                intent.putExtra("cc_document", ccDocument);
                intent.putExtra("warehouse_name", warehouse_name);
                intent.putExtra("cc_type", cc_type);
                intent.putExtra("type", type);
                startActivity(intent);
            }
        });
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }

}
