package largo.largo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class Outbound extends AppCompatActivity {
    Button picking;
    Button loading;
    Button packing;
    Button dn;
    ImageButton backButton;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.outbound);

        backButton = findViewById(R.id.back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        picking = findViewById(R.id.picking);
        loading = findViewById(R.id.loading);
        packing = findViewById(R.id.packing);
        dn = findViewById(R.id.dn);

        picking.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        Intent intent = new Intent(getApplicationContext(), AsnPicking.class);
                        startActivity(intent);
                    }
                }
        );

        loading.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        Intent intent = new Intent(Outbound.this,Loading.class);
                        startActivity(intent);
                    }
                }
        );

        packing.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        Intent intent = new Intent(Outbound.this,Packing.class);
                        startActivity(intent);
                    }
                }
        );

        dn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Outbound.this, DeliveryNote.class);
                        startActivity(intent);
                    }
                }
        );
    }
}
