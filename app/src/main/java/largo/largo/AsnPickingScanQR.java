package largo.largo;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AsnPickingScanQR extends AppCompatActivity {
    String TAG = this.getClass().getSimpleName();

    private Button backButton, nextButton, split_items, pick_by_loc, next_item, prev_item, check_sub;
    private ImageButton scanButton;
    private String param;
    private String fClass = this.getClass().getSimpleName();
    private Intent intent;
    private ListView listView;
    private List<PickingItemGetterSetter> itemList;
    private List<String> docList;
    private EditText asnQr, asnQty, tray_location, asnSku;
    private String pickingDocument, outboundCode, item_code, item_ori;
    private PickingItemListAdapter adapter;
    private PickingItemGetterSetter pickingItemGetSet;
    private TextView header_text, textView1, textView2, textView5, textView6, textView7;
    private int trayCheck = 0, positionx = 0;
    private int checkSku = 0, qty;
    int checkPost;
    LinearLayout layout50;

    private String[] uomList;
    private Spinner uomSpinner;

    private List<ReceivingDocumentItemGetterSetter> dokumenItemList;

    private JSONObject paramsJson;
    public static final Map<String, String> params = new HashMap<String, String>();
    public static final String MyPREFERENCES = "MyPrefs" ;

    DatabaseHelper myDb;
    String url;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.outbound_picking_by_asn_qr);

        intent = getIntent();
        param = this.getClass().getSimpleName();
        checkPost = 0;

        paramsJson = new JSONObject();

        myDb = new DatabaseHelper(this);
        getServerURL();

        backButton = findViewById(R.id.back);
        nextButton = findViewById(R.id.next);
        asnQr = findViewById(R.id.asnQr);
        asnQty = findViewById(R.id.asnQty);
        asnSku = findViewById(R.id.asnSku);
        asnQty.setShowSoftInputOnFocus(false);
        tray_location = findViewById(R.id.tray_location);
        listView = findViewById(R.id.QRList);
        header_text = findViewById(R.id.header_picking);
        check_sub = findViewById(R.id.checkSub);
        uomSpinner = findViewById(R.id.uomSpinner);

        next_item = findViewById(R.id.next_item);
        prev_item = findViewById(R.id.prev_item);
        textView1 = findViewById(R.id.textView1);
        textView2 = findViewById(R.id.textView2);
        textView5 = findViewById(R.id.textView5);
        textView6 = findViewById(R.id.textView6);
        textView7 = findViewById(R.id.pickId);

        layout50 = findViewById(R.id.linear50);

        asnQty.setText("0");

        docList = new ArrayList<>();
        itemList = new ArrayList<>();
        pickingDocument = "";
        outboundCode = "";
        item_code = "";
        item_ori = "";

        if(intent.getStringExtra("picking_document") != null) {
            dokumenItemList = (List<ReceivingDocumentItemGetterSetter>) intent.getSerializableExtra("dokumenItemList");
            pickingDocument = dokumenItemList.get(positionx).getReceiving_code();
            outboundCode = intent.getStringExtra("outbound_code");
            item_code = intent.getStringExtra("item_code");
            item_ori = dokumenItemList.get(positionx).getItem_ori();
            System.out.println(item_code);
            System.out.println(url + "/Api_picking/get/" + pickingDocument + "/" + outboundCode + "/" + item_code);
            textView1.setText("LOCATION : " + dokumenItemList.get(positionx).getSource());
            textView2.setText(dokumenItemList.get(positionx).getItem_name() + " (" + dokumenItemList.get(positionx).getItem_code() + ")");
            textView6.setText(dokumenItemList.get(positionx).getReceiving_code());
            textView5.setText(": " + dokumenItemList.get(positionx).getReceived_qty()+"/"+dokumenItemList.get(positionx).getQty()+" "+ dokumenItemList.get(positionx).getNama_satuan());
            checkSku = 0;
            if(Integer.parseInt(dokumenItemList.get(positionx).getReceived_qty())>0){
                textView1.setBackgroundColor(Color.GREEN);
                textView2.setBackgroundColor(Color.GREEN);
                textView6.setBackgroundColor(Color.GREEN);
                textView5.setBackgroundColor(Color.GREEN);
                textView7.setBackgroundColor(Color.GREEN);
            }else{
                textView1.setBackgroundColor(Color.WHITE);
                textView2.setBackgroundColor(Color.WHITE);
                textView6.setBackgroundColor(Color.WHITE);
                textView5.setBackgroundColor(Color.WHITE);
                textView7.setBackgroundColor(Color.WHITE);
            }

//            getPicking(url + "/Api_picking/get/" + pickingDocument + "/" + URLEncoder.encode(outboundCode.replace("/", "|")) + "/" + URLEncoder.encode(item_code.replace("*", "|")));
//            URLEncoder.encode(receiving_document.replace("/", "|")
        }

        if(dokumenItemList.size() == 1){
            next_item.setVisibility(View.GONE);
            prev_item.setVisibility(View.GONE);
        }

        if(positionx == 0){
            prev_item.setVisibility(View.GONE);
        }

        System.out.println("here" + item_code);
        String header_string = header_text.getText().toString();
        header_text.setText(header_string + " - " + pickingDocument + ", " + outboundCode);
        header_text.setSelected(true);

        check_sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                params.put("item_ori",item_ori);
                checkSub(params);
            }
        });

        next_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                positionx = positionx + 1;
                checkSku = 0;
                textView1.setText("LOCATION : " + dokumenItemList.get(positionx).getSource());
                textView2.setText(dokumenItemList.get(positionx).getItem_name() + " (" + dokumenItemList.get(positionx).getItem_code() + ")");
                textView6.setText(dokumenItemList.get(positionx).getReceiving_code());
                textView5.setText(": " + dokumenItemList.get(positionx).getReceived_qty()+"/"+dokumenItemList.get(positionx).getQty()+" "+ dokumenItemList.get(positionx).getNama_satuan());
                header_text.setText(header_text.getText().toString() + " - " + dokumenItemList.get(positionx).getReceiving_code() + ", " + outboundCode);
                header_text.setSelected(true);

                pickingDocument = dokumenItemList.get(positionx).getReceiving_code();
                outboundCode = dokumenItemList.get(positionx).getOutbound_code();
                item_code = dokumenItemList.get(positionx).getItem_code();
                item_ori = dokumenItemList.get(positionx).getItem_ori();

                asnSku.setText("");
                asnQr.setText("");

                if(Integer.parseInt(dokumenItemList.get(positionx).getReceived_qty())>0){
                    textView1.setBackgroundColor(Color.GREEN);
                    textView2.setBackgroundColor(Color.GREEN);
                    textView6.setBackgroundColor(Color.GREEN);
                    textView5.setBackgroundColor(Color.GREEN);
                    textView7.setBackgroundColor(Color.GREEN);
                }else{
                    textView1.setBackgroundColor(Color.WHITE);
                    textView2.setBackgroundColor(Color.WHITE);
                    textView6.setBackgroundColor(Color.WHITE);
                    textView5.setBackgroundColor(Color.WHITE);
                    textView7.setBackgroundColor(Color.WHITE);
                }


                if(positionx == 0) {
                    prev_item.setVisibility(View.GONE);
                } else {
                    prev_item.setVisibility(View.VISIBLE);
                }
                if(positionx+1 == dokumenItemList.size()) {
                    next_item.setVisibility(View.GONE);
                } else {
                    next_item.setVisibility(View.VISIBLE);
                }
            }
        });

        prev_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                positionx = positionx - 1;
                checkSku = 0;
                textView1.setText("LOCATION : " + dokumenItemList.get(positionx).getSource());
                textView2.setText(dokumenItemList.get(positionx).getItem_name() + " (" + dokumenItemList.get(positionx).getItem_code() + ")");
                textView6.setText(dokumenItemList.get(positionx).getReceiving_code());
                textView5.setText(": " + dokumenItemList.get(positionx).getReceived_qty()+"/"+dokumenItemList.get(positionx).getQty()+" "+ dokumenItemList.get(positionx).getNama_satuan());
                header_text.setText(header_text.getText().toString() + " - " + dokumenItemList.get(positionx).getReceiving_code() + ", " + outboundCode);
                header_text.setSelected(true);

                pickingDocument = dokumenItemList.get(positionx).getReceiving_code();
                outboundCode = dokumenItemList.get(positionx).getOutbound_code();
                item_code = dokumenItemList.get(positionx).getItem_code();
                item_ori = dokumenItemList.get(positionx).getItem_ori();

                asnSku.setText("");
                asnQr.setText("");

                if(Integer.parseInt(dokumenItemList.get(positionx).getReceived_qty())>0){
                    textView1.setBackgroundColor(Color.GREEN);
                    textView2.setBackgroundColor(Color.GREEN);
                    textView6.setBackgroundColor(Color.GREEN);
                    textView5.setBackgroundColor(Color.GREEN);
                    textView7.setBackgroundColor(Color.GREEN);
                }else{
                    textView1.setBackgroundColor(Color.WHITE);
                    textView2.setBackgroundColor(Color.WHITE);
                    textView6.setBackgroundColor(Color.WHITE);
                    textView5.setBackgroundColor(Color.WHITE);
                    textView7.setBackgroundColor(Color.WHITE);
                }

                if(positionx == 0) {
                    prev_item.setVisibility(View.GONE);
                } else {
                    prev_item.setVisibility(View.VISIBLE);
                }

                next_item.setVisibility(View.VISIBLE);
            }
        });

        split_items = findViewById(R.id.split_items);
        split_items.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        // Intent intent = new Intent(MenuActivity.this,LicensePlatingOption.class);
                        Intent intent = new Intent(AsnPickingScanQR.this,SplitItems.class);
                        startActivity(intent);
                    }
                }
        );

        pick_by_loc = findViewById(R.id.pick_by_loc);
        pick_by_loc.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        // Intent intent = new Intent(AsnPickingScanQR.this,AsnPickByLocItem.class);
                        Intent intent = new Intent(AsnPickingScanQR.this,AsnPickByLocItemLocation.class);
                        intent.putExtra("picking_document", pickingDocument);
                        intent.putExtra("outbound_code", outboundCode);
                        intent.putExtra("item_code", item_code);
                        intent.putExtra("item_ori", item_ori);
                        intent.putExtra("sku", item_code);
                        startActivity(intent);
                    }
                }
        );

//        backButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                try {
//                    finish();
//                    // Intent intent2 = new Intent(getApplicationContext(), AsnPickingItem.class);
//                    Intent intent2 = new Intent(getApplicationContext(), AsnPicking.class);
//                    Intent intent2 = new Intent(getApplicationContext(), PickingDocumentItem.class);
//                    intent2.putExtra("picking_document", pickingDocument);
//                    intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    startActivity(intent2);
//                    Thread.sleep(1000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        });

        backButton.setOnClickListener(
                new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                finish();
                intent = new Intent(getApplicationContext(), AsnPicking.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getApplicationContext(), AsnPicking.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        tray_location.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                trayCheck = 0;
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String usernameKey = prefs.getString("usernameKey", "No name defined");
                System.out.print(usernameKey);
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 0) {
                    getTrayLocation(url + "/Api_picking/get_tray_location/" + tray_location.getText() + "/" + outboundCode + "/" + usernameKey);
                }
                return false;
            }
        });

        asnSku.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                if(asnSku.getText().length() > 0 && asnQr.getText().length()>0){
//                    checkPartNumber(url+"/Api_picking/check_part_number/"+URLEncoder.encode(dokumenItemList.get(positionx).getItem_code().toString().replace("/", "|")) + "/" + URLEncoder.encode(asnSku.getText().toString().replace("/", "|")));
//                }else{
//                    checkSku = 0;
//                    Toast.makeText(getApplicationContext(), "Please Filled all the field above.", Toast.LENGTH_SHORT).show();
//                }
                if(keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 0){
                    checkPartNumber(url+"/Api_picking/check_part_number/"+URLEncoder.encode(dokumenItemList.get(positionx).getItem_id().toString().replace("/", "|")) + "/" + URLEncoder.encode(asnSku.getText().toString().replace("/", "|")));
                }
                return false;
            }
        });

        asnQr.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String usernameKey = prefs.getString("usernameKey", "No name defined");

                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 0) {
                    /*if(checkPost == 0) {
                        params.clear();
                        params.put("picking_code", intent.getStringExtra("picking_document"));
                        params.put("serial_number", asnQr.getText().toString());
                        params.put("qty", asnQty.getText().toString());
                        params.put("kit_time", new SimpleDateFormat("YYYY-MM-dd HH:mm:ss").format(new Date()));
                        params.put("pick_time", new SimpleDateFormat("YYYY-MM-dd HH:mm:ss").format(new Date()));
                        params.put("user", usernameKey);
                        checkPost = 1;
                        try {
                            Thread.sleep(100);
                            postPicking(params);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }*/
                    System.out.print("here");
                    getActiveStock(asnQr.getText().toString());

                    params.clear();
                    params.put("picking_code", pickingDocument);
                    params.put("serial_number", asnQr.getText().toString());
                    params.put("item_code", item_code);
                    getUomBySN(params);
                }
                return false;
            }
        });

        asnQty.setOnKeyListener(new View.OnKeyListener() {
            SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
            String usernameKey = prefs.getString("usernameKey", "No name defined");

            @Override
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 0) {
                    if(checkSku != 0) {
                        try {
                            if (asnQr.length() != 0 && Float.parseFloat(asnQty.getText().toString()) > 0) {
                                if(checkPost == 0) {
                                    params.clear();
                                    params.put("picking_code", pickingDocument);
                                    params.put("tray_location", tray_location.getText().toString());
                                    params.put("outbound_code", outboundCode);
                                    params.put("item_code", item_code);
                                    params.put("item_ori",item_ori);
                                    params.put("serial_number", asnQr.getText().toString());
                                    params.put("qty", asnQty.getText().toString());
                                    params.put("kit_time", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                                    params.put("pick_time", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                                    params.put("user", usernameKey);
                                    params.put("sku",asnSku.getText().toString());
                                    System.out.println("===<><>"+item_ori);
                                    try {
                                        params.put("unit_code", uomList[uomSpinner.getSelectedItemPosition()]);
                                    } catch (NullPointerException e) {
                                        params.put("unit_code", "");
                                    }
                                    checkPost = 1;
                                    try {
                                        Thread.sleep(100);
                                        postPicking(params);
                                        System.out.println(Calendar.getInstance().getTime() + " asnQty.setOnKeyListener, params = " + params);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                            } else {
                                if(asnQr.length() != 0) {
                                    Toast.makeText(getApplicationContext(), "Qty harus diisi dan harus lebih besar dari 0.", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getApplicationContext(), "Masukkan Serial Number terlebih dahulu.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (NumberFormatException nfe) {
                            // showMessage("Alert", "Qty harus diisi dengan angka.");
                            Toast.makeText(getApplicationContext(), "Qty harus diisi dengan angka.", Toast.LENGTH_SHORT).show();
                            nfe.printStackTrace();
                        }
                    }else{
                        Toast.makeText(getApplicationContext(), "Please confirm your part number first!", Toast.LENGTH_SHORT).show();
                    }
                }
                return false;
            }
        });
    }

    private void getTrayLocation(final String getDoc){
        RequestQueue requestQueue=Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, getDoc, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);
                try{
                    JSONObject jsonObject=new JSONObject(response);
                    if(jsonObject.getInt("status") == 200){
                        Toast.makeText(getApplicationContext(), "Tray dapat digunakan.", Toast.LENGTH_SHORT).show();
                        asnSku.requestFocus();
                        trayCheck = 1;
                    } else {
                        Toast.makeText(getApplicationContext(), "Tray sedang digunakan atau tidak terdaftar pada sistem.", Toast.LENGTH_SHORT).show();
                        trayCheck = 0;
                        tray_location.requestFocus();
                    }
                } catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void getActiveStock(final String serialNumber) {
        StringRequest strReq = new StringRequest(Request.Method.GET, url+"/Api_active_stock/get/" + serialNumber.toUpperCase(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);

                try {
                    JSONObject data = new JSONObject(response);
                    Integer status = data.getInt("status");

                    if (status == 200) {
                        JSONArray jsonArray = data.getJSONArray("results");
                        JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                        asnQty.setText(jsonObject1.getString("last_qty"));
                        asnQty.requestFocus();
                        item_code = jsonObject1.getString("item_id");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (StringIndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }

    private void getUomBySN(final Map<String, String> parameters) {
        String postUrl = this.url + "/Api_picking/get_uom_by_serial_number/";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);

                        try {
                            JSONObject data = new JSONObject(response);
                            Integer status = data.getInt("status");
                            String results = data.getString("results");
                            if(status == 200) {
                                JSONObject unit_code = new JSONObject(results.substring(1, results.length()-1));
                                uomList = new String[unit_code.length()];
                                for(int i = 0; i < unit_code.length(); i++) {
                                    uomList[i] = unit_code.get("unit_code").toString();
                                }

                                CustomSpinnerAdapter customAdapter = new CustomSpinnerAdapter(getApplicationContext(), uomList);
                                uomSpinner.setAdapter(customAdapter);
                            }else if(status == 402){
                                System.out.println("here");
                                System.out.println(status);
                                Toast.makeText(getApplicationContext(), "Items Was Expired.", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    private void postPicking(final Map<String, String> parameters) {
//        System.out.println("PostPicking, " + params);
//        checkPost = 0;
        String postUrl = this.url + "/Api_picking/post_serial/";
        System.out.println("postPickingParams() : " + parameters);

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        checkPost = 0;

                        try {
                            JSONObject data = new JSONObject(response);
                            System.out.println(data);
                            Integer status = data.getInt("status");
                            String message = data.getString("message");
//                            String item_code = data.getString("item_code");
                            System.out.println("hey" + item_code);
                            System.out.println(outboundCode);
                            if (status == 200) {
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                textView1.setText("LOCATION : " + dokumenItemList.get(positionx).getSource());
                                textView2.setText(dokumenItemList.get(positionx).getItem_name() + " (" + dokumenItemList.get(positionx).getItem_code() + ")");
                                qty = Integer.valueOf(dokumenItemList.get(positionx).getReceived_qty()) + Integer.valueOf(asnQty.getText().toString());
                                dokumenItemList.get(positionx).setReceived_qty(String.valueOf(qty));
                                textView5.setText(": " +  dokumenItemList.get(positionx).getReceived_qty().toString() +"/"+dokumenItemList.get(positionx).getQty()+" "+ dokumenItemList.get(positionx).getNama_satuan());


                                    textView1.setBackgroundColor(Color.GREEN);;
                                    textView2.setBackgroundColor(Color.GREEN);
                                    textView6.setBackgroundColor(Color.GREEN);
                                    textView5.setBackgroundColor(Color.GREEN);
                                    textView7.setBackgroundColor(Color.GREEN);
                                    asnQr.setText("");
                                    asnQty.setText("0");
                                    asnSku.setText("");
                                    asnQr.requestFocus();
//                                System.out.println("here"+item_code);
//                                getPicking(url + "/Api_picking/get/" + pickingDocument + "/" + URLEncoder.encode(outboundCode.replace("/", "|")) + "/" + URLEncoder.encode(item_code.replace("*", "|")));
                            } else {
                                try {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(AsnPickingScanQR.this);
                                    builder.setTitle("Notification");
                                    builder.setMessage(Html.fromHtml(message));
                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                        }
                                    });
                                    AlertDialog dialog = builder.create();
                                    dialog.show();
                                } catch (WindowManager.BadTokenException bde) {
                                    bde.printStackTrace();
                                } catch (IllegalStateException ise) {
                                    ise.printStackTrace();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        asnQr.setText("");
                        asnQty.setText("0");
                        asnSku.setText("");
                        asnQr.requestFocus();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;

            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    private void checkSub(final Map<String, String> parameters) {
//        System.out.println("PostPicking, " + params);
//        checkPost = 0;
        System.out.println("Sudah masuk sini");
        String postUrl = this.url + "/Api_picking/check_sub/";
        System.out.println("checkSub() : " + parameters);

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);

                        try {
                            JSONObject data = new JSONObject(response);
                            System.out.println(data);
                            Integer status = data.getInt("status");
                            String message = data.getString("message");
//                            String item_code = data.getString("item_code");
                            System.out.println("hey" + item_code);
                            System.out.println(outboundCode);
                            try {
                                AlertDialog.Builder builder = new AlertDialog.Builder(AsnPickingScanQR.this);
                                builder.setTitle("List Item Sub");
                                builder.setMessage(Html.fromHtml(message));
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                });
                                AlertDialog dialog = builder.create();
                                dialog.show();
                            } catch (WindowManager.BadTokenException bde) {
                                bde.printStackTrace();
                            } catch (IllegalStateException ise) {
                                ise.printStackTrace();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;

            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    private void getPicking(String getDoc){
        if(itemList != null) {
            itemList.clear();
        }

        RequestQueue requestQueue=Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, getDoc, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG + "getPicking", response);
                try{
                    JSONObject jsonObject = new JSONObject(response);
                    if(jsonObject.getInt("status")==200){
                        JSONArray jsonArray1 = jsonObject.getJSONArray("locations");
                        for(int i = 0; i < jsonArray1.length(); i++){
                            JSONObject jsonObject1=jsonArray1.getJSONObject(i);
                            System.out.println(jsonObject1);
                            pickingItemGetSet = new PickingItemGetterSetter(
                                    jsonObject1.getString("kd_barang"),
                                    jsonObject1.getString("picked_qty"),
                                    jsonObject1.getString("has_qty"),
                                    jsonObject1.getString("qty"),
                                    jsonObject1.getString("available_qty"),
                                    jsonObject1.getString("loc_name")
                            );
                            itemList.add(pickingItemGetSet);
                            docList.add(jsonObject1.getString("pl_name"));
                        }

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                adapter = new PickingItemListAdapter(itemList, getApplicationContext());
                                listView.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                            }
                        });

                    }
//                    spinner.setAdapter(new ArrayAdapter<String>(TallyReceivingItemSerial.this, android.R.layout.simple_spinner_dropdown_item, QC));
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;


            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void checkPartNumber(String getDoc){
        RequestQueue requestQueue=Volley.newRequestQueue(getApplicationContext());
        System.out.println("+>URL=>"+getDoc);
        StringRequest stringRequest=new StringRequest(Request.Method.GET, getDoc, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);
                try{
                    JSONObject jsonObject=new JSONObject(response);
                    if(jsonObject.getInt("status")==200){
                        if(jsonObject.getString("results")=="true"){
                            checkSku = 1;
                            asnQr.requestFocus();
                            Toast.makeText(getApplicationContext(), "Part Number sesuai", Toast.LENGTH_SHORT).show();
                        }else{
                            checkSku = 0;
                            Toast.makeText(getApplicationContext(), "Part Number tidak sesuai!", Toast.LENGTH_SHORT).show();
                            asnSku.setText("");
                            asnSku.setFocusableInTouchMode(true);
                            asnSku.setFocusable(true);
                            asnSku.requestFocus();
                            asnSku.setSelection(0);
                        }
                    }else{
                        checkSku = 0;
                        Toast.makeText(getApplicationContext(), "Part Number tidak sesuai!", Toast.LENGTH_SHORT).show();
                        asnSku.setText("");
                        asnSku.setFocusableInTouchMode(true);
                        asnSku.setFocusable(true);
                        asnSku.requestFocus();
                        asnSku.setSelection(0);
                    }
                }catch (Exception e){e.printStackTrace();}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }
}
