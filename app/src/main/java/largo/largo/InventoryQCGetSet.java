package largo.largo;

import java.io.Serializable;

public class InventoryQCGetSet implements Serializable {
    String sn, loc, qc, hex, pickTime, flag;

    public InventoryQCGetSet(String sn, String loc, String qc, String hex, String pickTime, String flag) {
        this.sn = sn;
        this.loc = loc;
        this.qc = qc;
        this.hex = hex;
        this.pickTime = pickTime;
        this.flag = flag;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getPickTime() {
        return pickTime;
    }

    public void setPickTime(String pickTime) {
        this.pickTime = pickTime;
    }

    public String getHex() {
        return hex;
    }

    public void setHex(String hex) {
        this.hex = hex;
    }

    public String getQc() {
        return qc;
    }

    public void setQc(String qc) {
        this.qc = qc;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }


}
