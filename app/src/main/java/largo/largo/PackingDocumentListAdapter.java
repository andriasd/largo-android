package largo.largo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class PackingDocumentListAdapter extends ArrayAdapter<PackingDocumentGetterSetter> {

    private List<PackingDocumentGetterSetter> packingDocumentItemList;

    private PackingDocumentGetterSetter packingDocument;

    private Context context;

    public PackingDocumentListAdapter(List<PackingDocumentGetterSetter> packingDocumentItemList, Context context) {
        super(context, R.layout.list_view_packing_document, packingDocumentItemList);
        this.packingDocumentItemList = packingDocumentItemList;
        this.context = context;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);

        View listViewItem = inflater.inflate(R.layout.list_view_packing_document, null, true);

        TextView pl_name = listViewItem.findViewById(R.id.pl_name);
        TextView kd_outbound = listViewItem.findViewById(R.id.kd_outbound);
        TextView qty = listViewItem.findViewById(R.id.qty);
        TextView picked_qty = listViewItem.findViewById(R.id.picked_qty);
        TextView destination_name = listViewItem.findViewById(R.id.destination_name);
        final CheckBox checkBox = listViewItem.findViewById(R.id.checkbox_packing);
        LinearLayout checkBox_layout = listViewItem.findViewById(R.id.checkbox_layout);

        packingDocument = packingDocumentItemList.get(position);

        pl_name.setText(String.valueOf(packingDocument.getPl_name()));
        kd_outbound.setText(packingDocument.getKd_outbound());
        qty.setText(packingDocument.getQty());
        picked_qty.setText(packingDocument.getPicked_qty());
        destination_name.setText(packingDocument.getDestination_name());

        if(packingDocument.getColumn() == 5) {
            qty.setVisibility(View.GONE);
            picked_qty.setVisibility(View.GONE);
            checkBox_layout.setVisibility(View.GONE);
            destination_name.setVisibility(View.VISIBLE);
        }

        if(packingDocument.getColumn() == 3) {
            kd_outbound.setText(packingDocument.getPicked_qty() + "/" + packingDocument.getQty());
            qty.setVisibility(View.GONE);
            picked_qty.setVisibility(View.GONE);
            checkBox_layout.setVisibility(View.GONE);
            destination_name.setVisibility(View.GONE);
        }

        if(packingDocument.getColumn() == 2) {
            qty.setVisibility(View.GONE);
            picked_qty.setVisibility(View.GONE);
            checkBox_layout.setVisibility(View.GONE);
            destination_name.setVisibility(View.GONE);
        }

        if(packingDocument.getColumn() == 1) {
            pl_name.setText(String.valueOf(packingDocument.getPl_name()) + "\n" + packingDocument.getKd_outbound());
            kd_outbound.setVisibility(View.GONE);
            qty.setVisibility(View.GONE);
            picked_qty.setVisibility(View.GONE);
            destination_name.setVisibility(View.GONE);
            checkBox_layout.setVisibility(View.GONE);
        }

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                try {
                    packingDocumentItemList.get(position).set_checked(buttonView.isChecked());
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });

        return listViewItem;
    }
}
