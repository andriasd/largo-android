package largo.largo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LicensePlating extends AppCompatActivity {
    String TAG = LicensePlating.class.getSimpleName();

    private LicensePlatingGetSet licensePlatingGetSet;
    private List<LicensePlatingGetSet> licensePlatingGetSetList = new ArrayList<>();
    private List<String> tagList = new ArrayList<>();
    private LicensePlatingAdapter licensePlatingAdapter;
    private ListView lpList;
    private AsnScanRFID asnScanRFID = new AsnScanRFID();
    private Map<String, String> params = new HashMap<String, String>();
    private Intent intent;
    private ImageButton scanButton;
    private String fClass = this.getClass().getSimpleName();

    private TextView totalScanned;

    private Calendar calendar;
    private SimpleDateFormat mdformat;
    private SharedPreferences prefs;
    private String usernameKey;

    DatabaseHelper myDb;

    String url;

    EditText serial_number;
    Button nextButton,backButton;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.license_plating_sn);

        myDb = new DatabaseHelper(this);

        getServerURL();

        totalScanned = findViewById(R.id.totalScanned);

        intent = getIntent();

        calendar = Calendar.getInstance();
        mdformat = new SimpleDateFormat("YYYY-MM-dd");
        prefs = getSharedPreferences(LoginActivity.MyPREFERENCES, MODE_PRIVATE);
        usernameKey = prefs.getString("usernameKey", "No name defined");
        intent = getIntent();

        lpList = findViewById(R.id.listLicensePlatingSN);
        licensePlatingAdapter = new LicensePlatingAdapter(licensePlatingGetSetList, getApplicationContext(), 2);
        lpList.setAdapter(licensePlatingAdapter);

        if(intent.getSerializableExtra("licensePlatingGetSetList") != null) {
            licensePlatingGetSetList = (List<LicensePlatingGetSet>) intent.getSerializableExtra("licensePlatingGetSetList");
            licensePlatingGetSet = (LicensePlatingGetSet) intent.getSerializableExtra("licensePlatingGetSet");
            tagList = (List<String>) intent.getSerializableExtra("tagList");
            licensePlatingAdapter.notifyDataSetChanged();
            totalScanned.setText(Integer.toString(licensePlatingGetSetList.size()));
        }

        serial_number = findViewById(R.id.serial_number);
        serial_number.setShowSoftInputOnFocus(false);

        serial_number.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    if(serial_number.getText().toString().contains("OL")) {
                        params.put("outer_label", serial_number.getText().toString().toUpperCase());
                        getOuterLabelData(params);
                    } else {
                        new validateSNASync(
                                serial_number.getText().toString().toUpperCase()
                        ).execute();
                    }
                }
                return false;
            }
        });

        backButton = findViewById(R.id.back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        nextButton = findViewById(R.id.next);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                intent = new Intent(getApplicationContext(), LicensePlatingPC.class);
                intent.putExtra("tagList", (Serializable) tagList);
                intent.putExtra("licensePlatingGetSet", licensePlatingGetSet);
                intent.putExtra("licensePlatingGetSetList", (Serializable) licensePlatingGetSetList);
                startActivity(intent);
            }
        });

        scanButton = findViewById(R.id.scanButton);
        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getApplicationContext(), QRScanner.class);
                intent.putExtra("param", "licensePlating");
                intent.putExtra("fClass", fClass);
                intent.putExtra("licensePlatingGetSetList", (Serializable) licensePlatingGetSetList);
                intent.putExtra("tagList", (Serializable) tagList);
                intent.putExtra("licensePlatingGetSet", licensePlatingGetSet);
                startActivity(intent);
            }
        });

        if(intent.getStringExtra("licensePlating") != null) {
            //licensePlatingGetSetList = (List<LicensePlatingGetSet>) intent.getSerializableExtra("licensePlatingGetSetList");
            //licensePlatingGetSet = (LicensePlatingGetSet) intent.getSerializableExtra("licensePlatingGetSet");
            //tagList = (List<String>) intent.getSerializableExtra("tagList");

            if(intent.getStringExtra("licensePlating").contains("OL")) {
                params.put("outer_label", intent.getStringExtra("licensePlating").toUpperCase());
                getOuterLabelData(params);
            } else {
                new validateSNASync(
                        intent.getStringExtra("licensePlating").toUpperCase()
                ).execute();
            }
        }
    }

    public void getOuterLabelData(final Map<String, String> parameters) {
        String postUrl = this.url + "/Api_transfer/getOuterLabelItem/";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);

                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            if(jsonResponse.getInt("status") == 200) {
                                String message = jsonResponse.getString("message");
                                JSONArray jsonMessage = new JSONArray(message);

                                for(int i = 0; i < jsonMessage.length(); i++) {
                                    String unique_code = jsonMessage.getString(i);
                                    JSONObject jsonHex = new JSONObject(unique_code);
                                    String hex = jsonHex.getString("unique_code");

                                    new validateSNASync(
                                            hex.toUpperCase()
                                    ).execute();
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), jsonResponse.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException jsonEx) {
                            jsonEx.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(LoginActivity.MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    private class validateSNASync extends AsyncTask<Void, Void, Void> {
        String serialNumberParam = "";

        validateSNASync(String serialNumberParam) {
            super();
            this.serialNumberParam = serialNumberParam;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            StringRequest strReq = new StringRequest(Request.Method.POST,url+"/Api_license_plating/validateSN/", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        Log.d("validateSN", response);
                        JSONObject data = new JSONObject(response);

                        Integer status = data.getInt("status");
                        String message = data.getString("message");

                        if(message.contains("is valid")) {
                            if(tagList.contains(serialNumberParam)) {
                                Toast.makeText(getApplicationContext(), serialNumberParam + " is already scanned.", Toast.LENGTH_SHORT).show();
                            } else {
                                tagList.add(serialNumberParam);
                                licensePlatingGetSet = new LicensePlatingGetSet(serialNumberParam, serialNumberParam);
                                licensePlatingGetSetList.add(licensePlatingGetSet);
                                licensePlatingAdapter.notifyDataSetChanged();
                                totalScanned.setText(Integer.toString(licensePlatingGetSetList.size()));
                                serial_number.requestFocus();
                                serial_number.hasFocus();
                                serial_number.setText("");
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    NetworkResponse networkResponse = error.networkResponse;
                    if (networkResponse != null && networkResponse.data != null) {
                        String jsonError = new String(networkResponse.data);

                        try {
                            JSONObject data = new JSONObject(jsonError);

                            String message = data.getString("message");

                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            })  {

                @Override
                protected Map<String, String> getParams(){
                    Map<String, String> params = new HashMap<String, String>();

                    params.put("kd_unik", serialNumberParam.toUpperCase());

                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    SharedPreferences prefs = getSharedPreferences(LoginActivity.MyPREFERENCES, MODE_PRIVATE);
                    String restoredText = prefs.getString("handheldsessioncodeKey", null);

                    String usernameKey = prefs.getString("usernameKey", "No name defined");
                    String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                    Log.d(TAG, usernameKey);
                    Log.d(TAG, handheldsessioncodeKey);

                    Map<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "application/x-www-form-urlencoded");
                    headers.put("User",usernameKey);
                    headers.put("Authorization", handheldsessioncodeKey);
                    return headers;
                }
            };

            controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
            return null;
        }
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }
}
