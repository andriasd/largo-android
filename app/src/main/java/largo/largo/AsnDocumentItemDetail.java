package largo.largo;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

public class AsnDocumentItemDetail extends AppCompatActivity {
    String TAG = this.getClass().getSimpleName();

    private Button backButton, nextButton;
    private Intent intent;
    private String param;
    private TextView header_text;
    private String receiving_document, inbound_document, item_code, doc_qty, nama_satuan, has_batch, has_expdate, is_multi;
    private TextView item_detail, asnExpDateText, asnBatchText;
    private EditText asnQty, asnExpDate, asnBatch;
    private List<ReceivingDocumentItemGetterSetter> dokumenItemList;
    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;
    private SimpleDateFormat lastExpiredDate;
    private Date lastExpDate;

    private Spinner statusSpinner;
    private String statusQC = "";

    private String[] statusArray = {"GOOD", "QC HOLD"};

    DatabaseHelper myDb;
    String url;

    public static final Map<String, String> params = new HashMap<String, String>();
    public static final String MyPREFERENCES = "MyPrefs" ;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inbound_tally_receiving_document_item_detail);

        intent = getIntent();
        param = this.getClass().getSimpleName();

        myDb = new DatabaseHelper(this);
        getServerURL();

        has_batch = "0";
        has_expdate = "0";

        backButton = findViewById(R.id.cancel);
        nextButton = findViewById(R.id.done);
        item_detail = findViewById(R.id.detail_text);
        asnQty = findViewById(R.id.asnQty);
        asnQty.setText("1");

        asnBatch = findViewById(R.id.asnBatch);
        asnBatchText = findViewById(R.id.asnBatchText);
        asnExpDate = findViewById(R.id.asnExpDate);
        asnExpDateText = findViewById(R.id.asnExpDateText);

        statusSpinner = findViewById(R.id.statusSpinner);
        CustomSpinnerAdapter customAdapter = new CustomSpinnerAdapter(getApplicationContext(), statusArray);
        statusSpinner.setAdapter(customAdapter);

        dateFormatter = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        lastExpiredDate = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        asnExpDate.setFocusable(false);
        asnExpDate.setShowSoftInputOnFocus(false);
        asnExpDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDateDialog();
            }
        });

        receiving_document = intent.getStringExtra("receiving_document");
        inbound_document = intent.getStringExtra("inbound_document");
        item_code = intent.getStringExtra("item_code");
        dokumenItemList = (List<ReceivingDocumentItemGetterSetter>) intent.getSerializableExtra("dokumenItemList");

        header_text = findViewById(R.id.header_tally);
        String header_string = header_text.getText().toString();
        header_text.setText(header_string + " - " + receiving_document + ", " + inbound_document);
        header_text.setSelected(true);

        getLastExpired(item_code);

        for(int i = 0; i < dokumenItemList.size(); i++) {
            if(dokumenItemList.get(i).getItem_code().equals(item_code)) {
                String detText = dokumenItemList.get(i).getItem_code() + " - " + dokumenItemList.get(i).getItem_name();
                String qtyText = dokumenItemList.get(i).getDefault_qty();

                item_detail.setText(detText);
                item_detail.setTypeface(Typeface.DEFAULT_BOLD);
                doc_qty = dokumenItemList.get(i).getQty();
                nama_satuan = dokumenItemList.get(i).getNama_satuan();
                has_expdate = dokumenItemList.get(i).getHas_expdate();
                has_batch = dokumenItemList.get(i).getHas_batch();
                is_multi = dokumenItemList.get(i).getHas_qty();

                if(is_multi.equals("1")) {
                    asnQty.setText(qtyText);
                } else {
                    asnQty.setText("1");
                    asnQty.setEnabled(false);
                }
            }
        }

        if(Float.parseFloat(has_expdate) == 1 && Float.parseFloat(has_batch) == 1) {
            asnExpDate.setVisibility(View.VISIBLE);
            asnExpDateText.setVisibility(View.VISIBLE);
            asnBatch.setVisibility(View.VISIBLE);
            asnBatchText.setVisibility(View.VISIBLE);
        } else if(Float.parseFloat(has_expdate) == 1){
            asnExpDate.setVisibility(View.VISIBLE);
            asnExpDateText.setVisibility(View.VISIBLE);
            asnBatch.setVisibility(View.GONE);
            asnBatchText.setVisibility(View.GONE);
        } else if(Float.parseFloat(has_batch) == 1){
            asnExpDate.setVisibility(View.GONE);
            asnExpDateText.setVisibility(View.GONE);
            asnBatch.setVisibility(View.VISIBLE);
            asnBatchText.setVisibility(View.VISIBLE);
        }
        if(Float.parseFloat(has_expdate) == 0 && Float.parseFloat(has_batch) == 0) {
            asnExpDate.setVisibility(View.GONE);
            asnExpDateText.setVisibility(View.GONE);
            asnBatch.setVisibility(View.GONE);
            asnBatchText.setVisibility(View.GONE);
        }

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AsnScanQR.class);
                intent.putExtra("receiving_document", receiving_document);
                intent.putExtra("inbound_document", inbound_document);
                intent.putExtra("item_code", item_code);
                intent.putExtra("dokumenItemList", (Serializable) dokumenItemList);
                intent.putExtra("asnBatch", asnBatch.getText().toString());
                intent.putExtra("asnExpDate", asnExpDate.getText().toString());
                intent.putExtra("asnQty", asnQty.getText().toString());
                intent.putExtra("status", statusArray[statusSpinner.getSelectedItemPosition()]);

                if (Float.parseFloat(asnQty.getText().toString()) > 0 && Float.parseFloat(asnQty.getText().toString()) <= Float.parseFloat(doc_qty)) {
                    if (Float.parseFloat(has_expdate) == 1 && Float.parseFloat(has_batch) == 1) {
                        if (asnExpDate.getText().toString().length() > 0 && asnBatch.getText().toString().length() > 0) {
                            startActivity(intent);
                        } else {
                            Toast.makeText(getApplicationContext(), "Item ini harus memiliki expiry date dan batch.", Toast.LENGTH_SHORT).show();
                        }
                    } else if (Float.parseFloat(has_expdate) == 1) {
                        if (asnExpDate.getText().toString().length() > 0) {
                            startActivity(intent);
                        } else {
                            Toast.makeText(getApplicationContext(), "Item ini harus memiliki expiry date.", Toast.LENGTH_SHORT).show();
                        }
                    } else if (Float.parseFloat(has_batch) == 1) {
                        if (asnBatch.getText().toString().length() > 0) {
                            startActivity(intent);
                        } else {
                            Toast.makeText(getApplicationContext(), "Item ini harus memiliki batch.", Toast.LENGTH_SHORT).show();
                        }
                    }
                    if (Float.parseFloat(has_expdate) == 0 && Float.parseFloat(has_batch) == 0) {
                        startActivity(intent);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Qty tidak boleh 0 dan lebih dari document qty (" + doc_qty + " " + nama_satuan + ").", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void getLastExpired(final String item_code) {
        String postUrl = this.url + "/Api_receiving/get_minimal_expired_date";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, response);
                        try {
                            lastExpDate = null;
                            JSONObject results = new JSONObject(response);
                            JSONObject data = new JSONObject(results.getString("results"));

                            if(data.length() > 0) {
                                lastExpDate = lastExpiredDate.parse(data.getString("tgl_exp"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                params.clear();
                params.put("item_code", item_code);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;

            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }

    private void showDateDialog(){
        Calendar newCalendar = Calendar.getInstance(TimeZone.getTimeZone("UTC+7"));

        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                view.setMinDate(System.currentTimeMillis() + 1000);
//                view.setMaxDate();

                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                if(lastExpDate != null) {
                    if(newDate.getTime().compareTo(lastExpDate) > 0) {
                        asnExpDate.setText(dateFormatter.format(newDate.getTime()));
                    } else {
                        Toast.makeText(getApplicationContext(), "Expiry Date ini tidak diperbolehkan masuk.", Toast.LENGTH_LONG).show();
                    }
                } else {
                    asnExpDate.setText(dateFormatter.format(newDate.getTime()));
                }
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

        datePickerDialog.show();
    }
}
