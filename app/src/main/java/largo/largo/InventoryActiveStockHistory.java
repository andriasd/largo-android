package largo.largo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class InventoryActiveStockHistory extends AppCompatActivity {
    String TAG = InventoryQCInSerialNumber.class.getSimpleName();
    ImageButton backButton;

    private InventoryActiveStockGetSet inventoryActiveStockGetSet;
    private List<InventoryActiveStockGetSet> inventoryActiveStockGetSetList = new ArrayList<>();
    private InventoryActiveStockListAdapter inventoryActiveStockListAdapter;
    private ListView historyList;
    private Intent intent;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.active_stock_item_history);

        backButton = findViewById(R.id.back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        historyList = findViewById(R.id.historyList);

        intent = getIntent();

        inventoryActiveStockGetSetList = (List<InventoryActiveStockGetSet>) intent.getSerializableExtra("inventoryActiveStockGetSetList");
        inventoryActiveStockGetSet = (InventoryActiveStockGetSet) intent.getSerializableExtra("inventoryActiveStockGetSet");
        inventoryActiveStockListAdapter = new InventoryActiveStockListAdapter(inventoryActiveStockGetSetList, getApplicationContext(), 4);
        historyList.setAdapter(inventoryActiveStockListAdapter);
    }
}
