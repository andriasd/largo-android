package largo.largo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static largo.largo.LoadingScanQR.MyPREFERENCES;

public class DeliveryNote extends AppCompatActivity {
    String TAG = this.getClass().getSimpleName();

    private Intent intent;
    private String param;
    private Button backButton, nextButton, free;
    private ListView listView, selectedListView;
    private List<String> docList;
    private EditText receiving_document;
    private List<DeliveryNoteDocumentGetterSetter> dokumenItemList;
    private DeliveryNoteDocumentGetterSetter dnDocGetSet;
    private List<DeliveryNoteDocumentGetterSetter> selectedDokumenItemList;
    private DeliveryNoteDocumentListAdapter adapter;

    DatabaseHelper myDb;
    String url;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.delivery_note_document);

        intent = getIntent();
        param = this.getClass().getSimpleName();

        myDb = new DatabaseHelper(this);
        getServerURL();

        backButton = findViewById(R.id.back);
        nextButton = findViewById(R.id.next);
        free = findViewById(R.id.free);
        listView = findViewById(R.id.ListTallyDoc);
        receiving_document = findViewById(R.id.receiving_document);

        docList = new ArrayList<>();
        if(intent.getSerializableExtra("docList") != null) {
            docList = (List<String>) intent.getSerializableExtra("docList");
        }

        dokumenItemList = new ArrayList<>();
        selectedDokumenItemList = new ArrayList<>();
        SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        String usernameKey = prefs.getString("usernameKey", "No name defined");
        DataListDokument(url + "/tray_list/get_document");

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        receiving_document.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 0){
                    Intent intent = new Intent(getApplicationContext(), DeliveryNoteItem.class);
                    intent.putExtra("dn_name",receiving_document.getText().toString());
                    startActivity(intent);
                }
                return false;
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), DeliveryNoteItem.class);
                intent.putExtra("dn_name",dokumenItemList.get(position).getDn_name());
                startActivity(intent);
            }
        });

        free.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), FreeTray.class);
                startActivity(intent);
            }
        });

    }

    private void DataListDokument(String getDoc){
        RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, getDoc, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);
                try{
                    JSONObject jsonObject=new JSONObject(response);
                    if(jsonObject.getInt("status")==200){
                        JSONArray jsonArray1=jsonObject.getJSONArray("results");
                        for(int i=0;i<jsonArray1.length();i++){
                            JSONObject jsonObject1=jsonArray1.getJSONObject(i);
                            dnDocGetSet = new DeliveryNoteDocumentGetterSetter(
                                    jsonObject1.getString("dn_name")
                            );
                            dokumenItemList.add(dnDocGetSet);
                            docList.add(jsonObject1.getString("dn_name"));
                        }
                        adapter = new DeliveryNoteDocumentListAdapter(dokumenItemList, getApplicationContext());

                        listView.setAdapter(adapter);
                        receiving_document.setFocusableInTouchMode(true);
                        receiving_document.setFocusable(true);
                        receiving_document.requestFocus();
                        receiving_document.setText("");
                        receiving_document.setSelection(0);
                    }
//                    spinner.setAdapter(new ArrayAdapter<String>(TallyReceivingItemSerial.this, android.R.layout.simple_spinner_dropdown_item, QC));
                }catch (JSONException e){e.printStackTrace();}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;


            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }

}
