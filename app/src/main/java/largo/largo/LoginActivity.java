package largo.largo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.database.Cursor;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.os.Bundle;
import android.content.Intent;
import android.widget.EditText;
import com.android.volley.*;
import com.android.volley.toolbox.*;

import java.util.*;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

import android.widget.ImageButton;
import android.widget.Toast;
import android.content.SharedPreferences;

public class LoginActivity extends AppCompatActivity {
    DatabaseHelper myDb;

    String TAG = LoginActivity.class.getSimpleName();
    String fClass = LoginActivity.class.getSimpleName();
    Button login,info,setting;
    ImageButton scanUserButton, scanPassButton;
    EditText username;
    EditText password;
    Intent intent;
    String appsVersion;

    String url;

    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String user_name = "usernameKey";
    public static final String handheld_session_code = "handheldsessioncodeKey";
    SharedPreferences sharedpreferences;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        finishAffinity();
        System.exit(0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        myDb = new DatabaseHelper(this);

//        intent = new Intent("broadcastQrResult");
        intent = getIntent();
        appsVersion = getString(R.string.version);

        username = (EditText)findViewById(R.id.username);
//        username.setShowSoftInputOnFocus(false);
        password = (EditText)findViewById(R.id.password);
//        password.setShowSoftInputOnFocus(false);
        scanUserButton = findViewById(R.id.scanUserButton);
        scanPassButton = findViewById(R.id.scanPassButton);

        getServerURL();

        login = findViewById(R.id.login);
        info = findViewById(R.id.info);
        setting = findViewById(R.id.setting);

        password.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    postLogin();
                }
                return false;
            }
        });

        login.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        postLogin();
                    }
                }
        );

        info.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        Intent intent = new Intent(LoginActivity.this,AboutActivity.class);
                        startActivity(intent);

                    }
                }
        );

        setting.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        Intent intent = new Intent(LoginActivity.this,SettingActivity.class);
                        startActivity(intent);
                    }
                }
        );

        scanUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(getApplicationContext(), QRScanner.class);
                intent.putExtra("param", "username");
                intent.putExtra("fClass", fClass);
                startActivity(intent);
            }
        });

        scanPassButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(getApplicationContext(), QRScanner.class);
                intent.putExtra("param", "password");
                intent.putExtra("fClass", fClass);
                if (username.getText().length() > 0) {
                    intent.putExtra("usernameText", username.getText().toString());
                }
                startActivity(intent);
            }
        });

//        LocalBroadcastManager.getInstance(this).registerReceiver(qrScan, new IntentFilter("broadcastQrResult"));

        if (intent.getStringExtra("username") != null) {
            username.setText(intent.getStringExtra("username"));
            intent.removeExtra("username");
            intent.removeExtra("fClass");
        }

        if (intent.getStringExtra("password") != null) {
            password.setText(intent.getStringExtra("password"));
            if(intent.getStringExtra("usernameText") != null) {
                username.setText(intent.getStringExtra("usernameText"));
            }
            if (username.getText().length() > 0) {
                postLogin();
            }
            intent.removeExtra("password");
            intent.removeExtra("usernameText");
            intent.removeExtra("fClass");
        }
    }

    private void postLogin(){
        final String[] message = new String[1];
        StringRequest strReq = new StringRequest(Request.Method.POST,url + "/Api_setting/login/", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                System.out.println("postLogin param : " + appsVersion);

                try {
                    JSONObject data = new JSONObject(response);
                    Integer status = data.getInt("status");
                    message[0] = data.getString("message");

                    //save session
                    JSONArray results = data.getJSONArray("results");
                    JSONObject session = results.getJSONObject(0);
                    String un = session.getString("user_name");
                    String handheld = session.getString("handheld_session_code");
//                    Log.d(TAG, user_name);
//                    Log.d(TAG, handheld_session_code);
                    sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

                    SharedPreferences.Editor editor = sharedpreferences.edit();

                    editor.putString(user_name, un);
                    editor.putString(handheld_session_code, handheld);
                    editor.apply();
                    // end save session

                    Log.d(TAG,status.toString());

                    if (status == 200) {
                        Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
                        startActivity(intent);
                        username.setText("");
                        password.setText("");
                        // finish();
                    } else {
                        Toast.makeText(getApplicationContext(), message[0], Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    // showMessage("Alert","Password yang anda masukan salah !");
                    Toast.makeText(getApplicationContext(), message[0], Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),"Silahkan lakukan update terhadap alamat server",Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams(){
                Map<String, String> params = new HashMap<String, String>();

                params.put("username",String.valueOf(username.getText()));
                params.put("password",String.valueOf(password.getText()));
                params.put("app_version", appsVersion);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        if(res1.getCount() == 0) {

            // showMessage("Alert","Harap setting terlebih dahulu server api di dalam menu setting");
            Toast.makeText(getApplicationContext(), "Harap setting terlebih dahulu server api di dalam menu setting", Toast.LENGTH_SHORT).show();

            return;

        }

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }
}
