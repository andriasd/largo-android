package largo.largo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static largo.largo.LoginActivity.MyPREFERENCES;

public class InventoryQCInLocation extends AppCompatActivity {

    private InventoryQCGetSet inventoryQCGetSet;
    private List<InventoryQCGetSet> inventoryQCGetSetList = new ArrayList<>();
    private List<String> tagList = new ArrayList<>();
    private InventoryQCListAdapter inventoryQCListAdapter;
    private ListView qcList;
    private AsnScanRFID asnScanRFID = new AsnScanRFID();
    private Map<String, String> params = new HashMap<String, String>();
    private Intent intent;
    private ImageButton scanButton, scanLocButton;
    private String fClass = this.getClass().getSimpleName();

    String TAG = InventoryQCInLocation.class.getSimpleName();
    EditText location,serial_number;

    DatabaseHelper myDb;
    String url,user_tallied,tallied_time,put_time,user_putaway;

    Button backButton,next_loc;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qc_in_receiving_item_location);

        final Calendar calendar = Calendar.getInstance();
        final SimpleDateFormat mdformat = new SimpleDateFormat("YYYY-MM-dd");
        final SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        final String usernameKey = prefs.getString("usernameKey", "No name defined");

        qcList = findViewById(R.id.listQCInItem);
        inventoryQCListAdapter = new InventoryQCListAdapter(inventoryQCGetSetList, getApplicationContext(), 2);
        qcList.setAdapter(inventoryQCListAdapter);

        intent = getIntent();
        if(intent.getSerializableExtra("inventoryQCGetSetList") != null) {
            inventoryQCGetSetList = (List<InventoryQCGetSet>) intent.getSerializableExtra("inventoryQCGetSetList");
            inventoryQCGetSet = (InventoryQCGetSet) intent.getSerializableExtra("inventoryQCGetSet");
            tagList = (List<String>) intent.getSerializableExtra("tagList");
            inventoryQCListAdapter = new InventoryQCListAdapter(inventoryQCGetSetList, getApplicationContext(), 2);
            qcList.setAdapter(inventoryQCListAdapter);
            inventoryQCListAdapter.notifyDataSetChanged();
        }

        myDb = new DatabaseHelper(this);

        getServerURL();

        intent = getIntent();

        location = findViewById(R.id.location);
        location.setShowSoftInputOnFocus(false);

        location.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    getQCLocation(location.getText().toString());
                }
                return false;
            }
        });

        serial_number = findViewById(R.id.serial_number);
        serial_number.setShowSoftInputOnFocus(false);

        serial_number.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    if(location.getText().toString().length() > 0) {
                        if(tagList.contains(serial_number.getText().toString())) {
                            for (int i = 0; i < inventoryQCGetSetList.size(); i++) {
                                System.out.println("inventoryQCGetSetList : " + inventoryQCGetSetList.get(i).getHex());
                                if(inventoryQCGetSetList.get(i).getHex().toUpperCase().equals(serial_number.getText().toString().toUpperCase())) {
                                    params.put("serial_number", inventoryQCGetSetList.get(i).getHex().toUpperCase());
                                    params.put("loc_name_old", inventoryQCGetSetList.get(i).getLoc());
                                    params.put("user_pick", usernameKey);
                                    params.put("pick_time", inventoryQCGetSetList.get(i).getPickTime());
                                    params.put("loc_name_new", location.getText().toString());
                                    params.put("user_put", usernameKey);
                                    params.put("put_time", mdformat.format(calendar.getTime()));
                                    //params.put("qc", inventoryQCGetSetList.get(i).getQc());
                                    params.put("qc", "QC HOLD");
                                    postQCIn(params);
                                }
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), serial_number.getText().toString() + " tidak terdapat dalam list.", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Harap isi lokasi terlebih dahulu.", Toast.LENGTH_SHORT);
                    }
                }
                return false;
            }
        });

        backButton = findViewById(R.id.back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(inventoryQCGetSetList.size() > 0) {
                    for(int i = 0; i < inventoryQCGetSetList.size(); i++) {
                        if(inventoryQCGetSetList.get(i).getFlag().equals("")) {
                            params.put("serial_number", inventoryQCGetSetList.get(i).getHex().toUpperCase());
                            params.put("loc_name_old", inventoryQCGetSetList.get(i).getLoc());
                            params.put("user_pick", usernameKey);
                            params.put("old_qc", inventoryQCGetSetList.get(i).getQc());
                            params.put("pick_time", mdformat.format(calendar.getTime()));
                            //new postQCCancelASync(params).execute();
                            try {
                                postQCCancel(params);
                                Thread.sleep(100);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                finish();
            }
        });

        next_loc = findViewById(R.id.next);
        next_loc.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        location.setText("");
                        serial_number.setText("");
                    }
                }
        );

        scanLocButton = findViewById(R.id.scanLocButton);
        scanLocButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getApplicationContext(), QRScanner.class);
                intent.putExtra("param", "inventoryQCInLoc");
                intent.putExtra("fClass", fClass);
                intent.putExtra("inventoryQCGetSetList", (Serializable) inventoryQCGetSetList);
                intent.putExtra("tagList", (Serializable) tagList);
                intent.putExtra("inventoryQCGetSet", inventoryQCGetSet);
                startActivity(intent);
            }
        });

        if(intent.getStringExtra("inventoryQCInLoc") != null) {
            location.setText(intent.getStringExtra("inventoryQCInLoc"));
            getQCLocation(intent.getStringExtra("inventoryQCInLoc"));
            inventoryQCGetSetList = (List<InventoryQCGetSet>) intent.getSerializableExtra("inventoryQCGetSetList");
            inventoryQCGetSet = (InventoryQCGetSet) intent.getSerializableExtra("inventoryQCGetSet");
            tagList = (List<String>) intent.getSerializableExtra("tagList");
        }

        scanButton = findViewById(R.id.scanButton);
        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getApplicationContext(), QRScanner.class);
                intent.putExtra("param", "inventoryQCInLocQR");
                intent.putExtra("fClass", fClass);
                intent.putExtra("inventoryQCGetSetList", (Serializable) inventoryQCGetSetList);
                intent.putExtra("tagList", (Serializable) tagList);
                intent.putExtra("inventoryQCGetSet", inventoryQCGetSet);
                intent.putExtra("location", location.getText().toString());
                startActivity(intent);
            }
        });

        if(intent.getStringExtra("inventoryQCInLocQR") != null) {
            inventoryQCGetSetList = (List<InventoryQCGetSet>) intent.getSerializableExtra("inventoryQCGetSetList");
            inventoryQCGetSet = (InventoryQCGetSet) intent.getSerializableExtra("inventoryQCGetSet");
            tagList = (List<String>) intent.getSerializableExtra("tagList");

            if(intent.getStringExtra("location").length() > 0) {
                location.setText(intent.getStringExtra("location"));
                if(location.getText().toString().length() > 0) {
                    if(tagList.contains(intent.getStringExtra("inventoryQCInLocQR"))) {
                        for (int i = 0; i < inventoryQCGetSetList.size(); i++) {
                            if(inventoryQCGetSetList.get(i).getHex().equals(intent.getStringExtra("inventoryQCInLocQR").toUpperCase())) {
                                params.put("serial_number", inventoryQCGetSetList.get(i).getHex().toUpperCase());
                                params.put("loc_name_old", inventoryQCGetSetList.get(i).getLoc());
                                params.put("user_pick", usernameKey);
                                params.put("pick_time", inventoryQCGetSetList.get(i).getPickTime());
                                params.put("loc_name_new", location.getText().toString().toUpperCase());
                                params.put("user_put", usernameKey);
                                params.put("put_time", mdformat.format(calendar.getTime()));
                                //params.put("qc", inventoryQCGetSetList.get(i).getQc());
                                params.put("qc", "QC HOLD");
                                postQCIn(params);
                            }
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), intent.getStringExtra("inventoryQCInLocQR") + " tidak terdapat dalam list.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Harap isi lokasi terlebih dahulu.", Toast.LENGTH_SHORT);
                }
            }

        }
    }

    private void getQCLocation(final String locParam) {
        StringRequest strReq = new StringRequest(Request.Method.GET, url+"/Api_QC/get_location/" + locParam, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());

                try {
                    JSONObject data = new JSONObject(response);
                    Integer status = data.getInt("status");
                    String message = data.getString("message");

                    if (status == 200) {
                        serial_number.requestFocus();
                    }else{
                        location.setText("");
                        location.requestFocus();
                    }

                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }

    private void postQCIn(final Map<String, String> parameters) {
        StringRequest strReq = new StringRequest(Request.Method.POST, url + "/Api_QC/post/", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("postQCIn", response.toString());

                try {
                    JSONObject data = new JSONObject(response);
                    Integer status = data.getInt("status");
                    String message = data.getString("message");
                    String sn = data.getString("param");

                    if(status == 200) {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        for(int i = 0; i < inventoryQCGetSetList.size(); i++) {
                            if(inventoryQCGetSetList.get(i).getHex().toUpperCase().equals(sn.toUpperCase())) {
                                inventoryQCGetSet = new InventoryQCGetSet(
                                        inventoryQCGetSetList.get(i).getSn(),
                                        //inventoryQCGetSetList.get(i).getLoc(),
                                        location.getText().toString(),
                                        inventoryQCGetSetList.get(i).getQc(),
                                        inventoryQCGetSetList.get(i).getHex(),
                                        inventoryQCGetSetList.get(i).getPickTime(),
                                        "done"
                                );
                                inventoryQCGetSetList.set(i, inventoryQCGetSet);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        inventoryQCListAdapter.notifyDataSetChanged();
                                        serial_number.setText("");
                                        serial_number.requestFocus();
                                        serial_number.hasFocus();
                                    }
                                });
                                continue;
                            }
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // showMessage("Error","Silahkan lakukan picking / putaway terlebih dahulu !");
                Toast.makeText(getApplicationContext(), "Silahkan lakukan picking / putaway terlebih dahulu !", Toast.LENGTH_SHORT).show();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;

            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);

    }

    private class postQCCancelASync extends AsyncTask<Void, Void, Void> {
        final Map<String, String> parameters;

        postQCCancelASync(Map<String, String> parameters) {
            super();
            this.parameters = parameters;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    postQCCancel(parameters);
                }
            });
            return null;
        }
    }

    private void postQCCancel(final Map<String, String> parameters) {
        StringRequest strReq = new StringRequest(Request.Method.POST,url+"/Api_QC/cancel/", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("postQCCancel", response);
                    JSONObject data = new JSONObject(response);

                    Integer status = data.getInt("status");
                    String message = data.getString("message");

                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    String jsonError = new String(networkResponse.data);

                    try {
                        JSONObject data = new JSONObject(jsonError);

                        String message = data.getString("message");

                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        })  {

            @Override
            protected Map<String, String> getParams(){
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }
}
