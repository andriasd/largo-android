package largo.largo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static largo.largo.LoginActivity.MyPREFERENCES;

public class InventoryTransferSN extends AppCompatActivity {
    String TAG = InventoryTransferSN.class.getSimpleName();
    EditText serial_number;
    Button next,backButton;

    private InventoryTransferGetSet inventoryTransferGetSet;
    private List<InventoryTransferGetSet> inventoryTransferGetSetList = new ArrayList<>();
    private List<String> tagList = new ArrayList<>();
    private InventoryTransferListAdapter inventoryTransferListAdapter;
    private ListView snList;
    private AsnScanRFID asnScanRFID = new AsnScanRFID();
    private Map<String, String> params = new HashMap<String, String>();
    private Intent intent;
    private ImageButton scanButton;
    private String fClass = this.getClass().getSimpleName();
    private String bin_transfer_code = "", item_code = "";
    private String[] splitItemCode;
    private TextView header_transfer;

    private TextView totalScanned;

    private Calendar calendar;
    private SimpleDateFormat mdformat;
    private SharedPreferences prefs;
    private String usernameKey;

    DatabaseHelper myDb;

    String url;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inventory_transfer_sn);

        calendar = Calendar.getInstance();
        mdformat = new SimpleDateFormat("YYYY-MM-dd");
        prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        usernameKey = prefs.getString("usernameKey", "No name defined");
        intent = getIntent();
        header_transfer = findViewById(R.id.header_transfer);

        if(intent.getStringExtra("bin_transfer_code") != null) {
            bin_transfer_code = intent.getStringExtra("bin_transfer_code");
            item_code = intent.getStringExtra("item_code");
            splitItemCode = item_code.split(" - ");
            header_transfer.setText(header_transfer.getText() + " - " + bin_transfer_code + ", " + item_code);
            header_transfer.setSelected(true);
        }

        totalScanned = findViewById(R.id.totalScanned);

        snList = findViewById(R.id.listTransfer);
        inventoryTransferListAdapter = new InventoryTransferListAdapter(inventoryTransferGetSetList, getApplicationContext(), 2);
        snList.setAdapter(inventoryTransferListAdapter);

        myDb = new DatabaseHelper(this);

        getServerURL();

        serial_number = findViewById(R.id.serial_number);
        serial_number.setShowSoftInputOnFocus(false);

        serial_number.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    if(bin_transfer_code.length() > 0) {
                        new getBinTransferSerialNumberASync(
                                serial_number.getText().toString().toUpperCase(),
                                splitItemCode[0],
                                bin_transfer_code
                        ).execute();
                    } else {
                        if (serial_number.getText().toString().contains("OL")) {
                            params.put("outer_label", serial_number.getText().toString().toUpperCase());
                            getOuterLabelData(params);
                        } else {
                            new getTransferSerialNumberASync(
                                    serial_number.getText().toString().toUpperCase(),
                                    usernameKey,
                                    new SimpleDateFormat("YYYY-MM-dd HH:mm:ss").format(new Date())
                            ).execute();
                        }
                    }
                }
                return false;
            }
        });

        backButton = findViewById(R.id.back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(bin_transfer_code.length() > 0) {
                    if (inventoryTransferGetSetList != null) {
                        try {
                            String header = "{\"serial_number\":[";
                            String serial_numbers = "";
                            String ender = "]}";
                            for (int i = 0; i < inventoryTransferGetSetList.size(); i++) {
                                serial_numbers = serial_numbers + "\"" + inventoryTransferGetSetList.get(i).getHex().toUpperCase() + "\",";
                            }
                            String snJSON = header + serial_numbers.substring(0, serial_numbers.length() - 1) + ender;
                            params.clear();
                            params.put("serial_number", snJSON);
                            params.put("bin_transfer_code", bin_transfer_code);
                            postBinTransferCancel(params);
                        } catch(StringIndexOutOfBoundsException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    if (inventoryTransferGetSetList != null) {
                        for (int i = 0; i < inventoryTransferGetSetList.size(); i++) {
                            params.put("serial_number", inventoryTransferGetSetList.get(i).getHex().toUpperCase());
                            params.put("loc_name_old", inventoryTransferGetSetList.get(i).getLoc());
                            params.put("user_pick", usernameKey);
                            params.put("pick_time", mdformat.format(calendar.getTime()));
                            try {
                                postTransferCancel(params);
                                Thread.sleep(100);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

                finish();
            }
        });

        next = findViewById(R.id.next);
        next.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        finish();
                        if(bin_transfer_code.length() > 0) {
                            Intent intent = new Intent(getApplicationContext(), InventoryTransferLocation.class);
                            intent.putExtra("inventoryTransferGetSet", inventoryTransferGetSet);
                            intent.putExtra("inventoryTransferGetSetList", (Serializable) inventoryTransferGetSetList);
                            intent.putExtra("tagList", (Serializable) tagList);
                            intent.putExtra("bin_transfer_code", bin_transfer_code);
                            intent.putExtra("item_code", item_code);
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(getApplicationContext(), InventoryTransferLocation.class);
                            intent.putExtra("inventoryTransferGetSet", inventoryTransferGetSet);
                            intent.putExtra("inventoryTransferGetSetList", (Serializable) inventoryTransferGetSetList);
                            intent.putExtra("tagList", (Serializable) tagList);
                            startActivity(intent);
                        }
                    }
                }
        );

        scanButton = findViewById(R.id.scanButton);
        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getApplicationContext(), QRScanner.class);
                intent.putExtra("param", "inventoryTransferSN");
                intent.putExtra("fClass", fClass);
                intent.putExtra("inventoryTransferGetSetList", (Serializable) inventoryTransferGetSetList);
                intent.putExtra("tagList", (Serializable) tagList);
                intent.putExtra("inventoryTransferGetSet", inventoryTransferGetSet);
                startActivity(intent);
            }
        });

        if(intent.getStringExtra("inventoryTransferSN") != null) {
            System.out.println(intent.getStringExtra("inventoryTransferSN"));
            inventoryTransferGetSetList = (List<InventoryTransferGetSet>) intent.getSerializableExtra("inventoryTransferGetSetList");
            inventoryTransferGetSet = (InventoryTransferGetSet) intent.getSerializableExtra("inventoryTransferGetSet");
            tagList = (List<String>) intent.getSerializableExtra("tagList");

            if(intent.getStringExtra("inventoryTransferSN").contains("OL")) {
                params.put("outer_label", intent.getStringExtra("inventoryTransferSN").toUpperCase());
                getOuterLabelData(params);
            } else {
                new getTransferSerialNumberASync(
                        intent.getStringExtra("inventoryTransferSN").toUpperCase(),
                        usernameKey,
                        new SimpleDateFormat("YYYY-MM-dd HH:mm:ss").format(new Date())
                ).execute();
            }
        }
    }

    private class getTransferSerialNumberASync extends AsyncTask<Void, Void, Void> {
        String timeParam = "";
        String userParam = "";
        String serialNumberParam = "";

        getTransferSerialNumberASync(String serial_number, String user, String time) {
            super();
            this.serialNumberParam = serial_number;
            this.userParam = user;
            this.timeParam = time;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            StringRequest strReq = new StringRequest(Request.Method.POST,url+"/Api_transfer/get/", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        Log.d("getTransferSerialNumber", response);
                        JSONObject data = new JSONObject(response);

                        Integer status = data.getInt("status");
                        String message = data.getString("message");

                        if (status == 200) {
                            if(message.contains(" is available.")) {
                                JSONArray jsonArray = data.getJSONArray("results");

                                for(int i=0;i<jsonArray.length();i++) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    String loc_name = jsonObject1.getString("loc_name");
                                    if(tagList != null) {
                                        if(tagList.contains(serialNumberParam)) {
                                            Toast.makeText(getApplicationContext(), serialNumberParam + " already scanned.", Toast.LENGTH_SHORT).show();
                                        } else {
                                            tagList.add(serialNumberParam);
                                            inventoryTransferGetSet = new InventoryTransferGetSet(serialNumberParam, loc_name, serialNumberParam, mdformat.format(calendar.getTime()), "");
                                            inventoryTransferGetSetList.add(inventoryTransferGetSet);
                                        }
                                    } else {
                                        tagList.add(serialNumberParam);
                                        inventoryTransferGetSet = new InventoryTransferGetSet(serialNumberParam, loc_name, serialNumberParam, mdformat.format(calendar.getTime()), "");
                                        inventoryTransferGetSetList.add(inventoryTransferGetSet);
                                    }
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            }
                        } else if (status == 401) {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            inventoryTransferListAdapter = new InventoryTransferListAdapter(inventoryTransferGetSetList, getApplicationContext(), 2);
                            snList.setAdapter(inventoryTransferListAdapter);
                            inventoryTransferListAdapter.notifyDataSetChanged();
                            totalScanned.setText(Integer.toString(inventoryTransferGetSetList.size()));
                            serial_number.setText("");
                            serial_number.hasFocus();
                        }
                    });
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    NetworkResponse networkResponse = error.networkResponse;
                    if (networkResponse != null && networkResponse.data != null) {
                        String jsonError = new String(networkResponse.data);

                        try {
                            JSONObject data = new JSONObject(jsonError);

                            String message = data.getString("message");

                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            })  {

                @Override
                protected Map<String, String> getParams(){
                    Map<String, String> params = new HashMap<String, String>();
                    SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                    String usernameKey = prefs.getString("usernameKey", "No name defined");

                    params.put("serial_number", serialNumberParam);
                    params.put("user", userParam);
                    params.put("time", timeParam);

                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                    String restoredText = prefs.getString("handheldsessioncodeKey", null);

                    String usernameKey = prefs.getString("usernameKey", "No name defined");
                    String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                    Log.d(TAG, usernameKey);
                    Log.d(TAG, handheldsessioncodeKey);

                    Map<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "application/x-www-form-urlencoded");
                    headers.put("User",usernameKey);
                    headers.put("Authorization", handheldsessioncodeKey);
                    return headers;
                }
            };

            controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
            return null;
        }
    }

    private class getBinTransferSerialNumberASync extends AsyncTask<Void, Void, Void> {
        String serialNumberParam = "";
        String itemCodeParam = "";
        String binTransferCodeParam = "";

        getBinTransferSerialNumberASync(String serialNumberParam, String itemCodeParam, String binTransferCodeParam) {
            super();
            this.serialNumberParam = serialNumberParam;
            this.itemCodeParam = itemCodeParam;
            this.binTransferCodeParam = binTransferCodeParam;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            StringRequest strReq = new StringRequest(Request.Method.POST,url+"/Api_bin_transfer/get_serial/", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        Log.d("getTransferSerialNumber", response);
                        JSONObject data = new JSONObject(response);

                        Integer status = data.getInt("status");
                        String message = data.getString("message");
                        String old_location = data.getString("old_location");

                        if (status == 200) {
                            if(tagList != null) {
                                if(tagList.contains(serialNumberParam)) {
                                    Toast.makeText(getApplicationContext(), serialNumberParam + " already scanned.", Toast.LENGTH_SHORT).show();
                                } else {
                                    tagList.add(serialNumberParam);
                                    inventoryTransferGetSet = new InventoryTransferGetSet(serialNumberParam, old_location, serialNumberParam, mdformat.format(calendar.getTime()), "");
                                    inventoryTransferGetSetList.add(inventoryTransferGetSet);
                                }
                            } else {
                                tagList.add(serialNumberParam);
                                inventoryTransferGetSet = new InventoryTransferGetSet(serialNumberParam, old_location, serialNumberParam, mdformat.format(calendar.getTime()), "");
                                inventoryTransferGetSetList.add(inventoryTransferGetSet);
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            inventoryTransferListAdapter = new InventoryTransferListAdapter(inventoryTransferGetSetList, getApplicationContext(), 2);
                            snList.setAdapter(inventoryTransferListAdapter);
                            inventoryTransferListAdapter.notifyDataSetChanged();
                            totalScanned.setText(Integer.toString(inventoryTransferGetSetList.size()));
                            serial_number.setText("");
                            serial_number.hasFocus();
                        }
                    });
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    NetworkResponse networkResponse = error.networkResponse;
                    if (networkResponse != null && networkResponse.data != null) {
                        String jsonError = new String(networkResponse.data);

                        try {
                            JSONObject data = new JSONObject(jsonError);

                            String message = data.getString("message");

                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            })  {

                @Override
                protected Map<String, String> getParams(){
                    Map<String, String> params = new HashMap<String, String>();
                    SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                    String usernameKey = prefs.getString("usernameKey", "No name defined");

                    params.put("serial_number", serialNumberParam);
                    params.put("item_code", itemCodeParam);
                    params.put("bin_transfer_code", binTransferCodeParam);

                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                    String restoredText = prefs.getString("handheldsessioncodeKey", null);

                    String usernameKey = prefs.getString("usernameKey", "No name defined");
                    String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                    Log.d(TAG, usernameKey);
                    Log.d(TAG, handheldsessioncodeKey);

                    Map<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "application/x-www-form-urlencoded");
                    headers.put("User",usernameKey);
                    headers.put("Authorization", handheldsessioncodeKey);
                    return headers;
                }
            };

            controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
            return null;
        }
    }

    public void postTransferCancel(final Map<String, String> parameters) {
        StringRequest strReq = new StringRequest(Request.Method.POST,url+"/Api_transfer/cancel/", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("postTransferCancel", response);
                    JSONObject data = new JSONObject(response);

                    Integer status = data.getInt("status");
                    String message = data.getString("message");

                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    String jsonError = new String(networkResponse.data);

                    try {
                        JSONObject data = new JSONObject(jsonError);

                        String message = data.getString("message");

                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        })  {

            @Override
            protected Map<String, String> getParams(){
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }

    public void postBinTransferCancel(final Map<String, String> parameters) {
        StringRequest strReq = new StringRequest(Request.Method.POST,url+"/Api_bin_transfer/cancel/", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("postTransferCancel", response);
                    JSONObject data = new JSONObject(response);

                    Integer status = data.getInt("status");
                    String message = data.getString("message");

                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    String jsonError = new String(networkResponse.data);

                    try {
                        JSONObject data = new JSONObject(jsonError);

                        String message = data.getString("message");

                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        })  {

            @Override
            protected Map<String, String> getParams(){
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }

    public void getOuterLabelData(final Map<String, String> parameters) {
        String postUrl = this.url + "/Api_transfer/getOuterLabelItem/";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        String message = "";

                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            message = jsonResponse.getString("message");
                            JSONArray jsonMessage = new JSONArray(message);

                            for(int i = 0; i < jsonMessage.length(); i++) {
                                String unique_code = jsonMessage.getString(i);
                                JSONObject jsonHex = new JSONObject(unique_code);
                                String hex = jsonHex.getString("unique_code");

                                new getTransferSerialNumberASync(
                                        hex.toUpperCase(),
                                        usernameKey,
                                        new SimpleDateFormat("YYYY-MM-dd HH:mm:ss").format(new Date())
                                ).execute();
                            }
                        } catch (JSONException jsonEx) {
                            jsonEx.printStackTrace();
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }
}
