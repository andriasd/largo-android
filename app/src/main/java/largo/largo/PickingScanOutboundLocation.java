package largo.largo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.util.Calendar;

import static largo.largo.LoginActivity.MyPREFERENCES;

public class PickingScanOutboundLocation extends AppCompatActivity {
    DatabaseHelper myDb;

    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    List<String> expand_detail = new ArrayList<String>();
    HashMap<String, List<String>> listDataChild;

    ListView listView;
    private List<PickingItemsGetterSetter> pickingItemList;

    String url;

    String TAG = PickingScanOutboundLocation.class.getSimpleName();

    Button backButton,confirm;
    EditText outbound_location;

    private int lastExpandedPosition = -1;

    Intent intent;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.outbound_picking_location);

        backButton = findViewById(R.id.back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        myDb = new DatabaseHelper(this);

        getServerURL();

        confirm = findViewById(R.id.confirm);

        outbound_location = findViewById(R.id.outbound_location);
        outbound_location.setShowSoftInputOnFocus(false);

        outbound_location.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    postCheckOutboundLoc();
                }
                return false;
            }
        });

        myDb = new DatabaseHelper(this);

        listView =  findViewById(R.id.ListItem);

        pickingItemList = new ArrayList<>();
        updateUI();

//        if(listDataHeader.size() == 0){
//            Toast.makeText(getApplicationContext(),"No Data.", Toast.LENGTH_SHORT).show();
//
//            return;
//        }else {
//
//            listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);
//
//            expListView.setAdapter(listAdapter);
//
//            expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
//                @Override
//                public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
//                    return false;
//                }
//            });
//
//            expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
//                @Override
//                public void onGroupExpand(int groupPosition) {
//                    if (lastExpandedPosition != -1
//                            && groupPosition != lastExpandedPosition) {
//                        expListView.collapseGroup(lastExpandedPosition);
//                    }
//                    lastExpandedPosition = groupPosition;
////                Toast.makeText(getApplicationContext(),listDataHeader.get(groupPosition) + "Expanded", Toast.LENGTH_SHORT).show();
//                }
//            });
//
//            expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
//                @Override
//                public void onGroupCollapse(int groupPosition) {
//                    Toast.makeText(getApplicationContext(), listDataHeader.get(groupPosition) + "Collapsed", Toast.LENGTH_SHORT).show();
//                }
//            });
//
//            expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
//                @Override
//                public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
//                    Toast.makeText(getApplicationContext(), listDataHeader.get(groupPosition)
//                            + ":"
//                            + listDataChild.get(
//                            listDataHeader.get(groupPosition)).get(
//                            childPosition), Toast.LENGTH_SHORT).show();
//                    return false;
//                }
//            });
//
//        }

        confirm.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        Intent intent = new Intent(PickingScanOutboundLocation.this, PickingDocument.class);
//                        startActivity(intent);
                        postConfirmPicking();
                    }
                }
        );
    }

    private void updateUI(){
        SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        String usernameKey = prefs.getString("usernameKey", "No name defined");

        Cursor res = myDb.getPickingItem(usernameKey);

//        StringBuffer buffer = new StringBuffer();

        while (res.moveToNext() ) {

//            buffer.append("Data 1 :"+ res.getString(0)+"\n");
//
//            buffer.append("Data 2 :"+ res.getString(1)+"\n");
//
//            buffer.append("Data 3 :"+ res.getString(2)+"\n");
//
//            buffer.append("Data 4 :"+ res.getString(3)+"\n");
//
//            buffer.append("Data 5 :"+ res.getString(4)+"\n");
//
//            buffer.append("Data 6 :"+ res.getString(5)+"\n");
//
//            buffer.append("Data 7 :"+ res.getString(6)+"\n\n");

            PickingItemsGetterSetter item_picking_list = new PickingItemsGetterSetter(
                    res.getString(0),
                    res.getString(3),
                    res.getString(2),
                    res.getString(1)
            );
            pickingItemList.add(item_picking_list);

        }

//        showMessage("Data",buffer.toString());

        PickingItemsListAdapter adapter = new PickingItemsListAdapter(pickingItemList, getApplicationContext());

        listView.setAdapter(adapter);
    }

    private void postCheckOutboundLoc() {
        StringRequest strReq = new StringRequest(Request.Method.GET,url+"/Api_picking/get_location/" + String.valueOf(outbound_location.getText()), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);

                try {
                    JSONObject data = new JSONObject(response);

                    Integer status = data.getInt("status");
                    String message = data.getString("message");

                    if (status == 200) {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        outbound_location.setText("");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    String jsonError = new String(networkResponse.data);


                    try {
                        JSONObject data = new JSONObject(jsonError);

                        String message = data.getString("message");

                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        })  {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "RFID/application/x-www-form-urlencoded");
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;


            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }

    private void postConfirmPicking() {
        StringRequest strReq = new StringRequest(Request.Method.POST,url+"/Api_picking/post/", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);

                try {
                    JSONObject data = new JSONObject(response);

                    Integer status = data.getInt("status");
                    String message = data.getString("message");

                    if (status == 200) {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(PickingScanOutboundLocation.this,PickingDocument.class);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        outbound_location.setText("");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    String jsonError = new String(networkResponse.data);


                    try {
                        JSONObject data = new JSONObject(jsonError);

                        String message = data.getString("message");

                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        })  {

            @Override
            protected Map<String, String> getParams(){
                Map<String, String> params = new HashMap<String, String>();
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String usernameKey = prefs.getString("usernameKey", "No name defined");

                Calendar calendar = Calendar.getInstance();
                SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm:ss");

                intent = getIntent();
                String picking_document = intent.getStringExtra("picking_document");
                String item_code = intent.getStringExtra("item_code");
                String serial_number = intent.getStringExtra("serial_number");
                String old_loc = intent.getStringExtra("old_loc");
                String qty = intent.getStringExtra("qty");

                StringBuffer buffer = new StringBuffer();

//                Cursor res = myDb.getPutawayItem(usernameKey);
//
//                while (res.moveToNext()) {

                    params.put("picking_name",picking_document);
                    params.put("item_code",item_code);
                    params.put("serial_number",serial_number);
                    params.put("old_loc",old_loc);
                    params.put("qty",qty);
                    params.put("user_pick",usernameKey);
                    params.put("pick_time",mdformat.format(calendar.getTime()));
                    params.put("outbound_loc",String.valueOf(outbound_location.getText()));
                    params.put("user_put",usernameKey);
                    params.put("put_time",mdformat.format(calendar.getTime()));

//                }

                Log.d("params:",params.toString());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "RFID/application/x-www-form-urlencoded");
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;


            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);

        SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);

        String usernameKey = prefs.getString("usernameKey", "No name defined");

        intent = getIntent();
    }

    private void postConfirmMultiplePicking() {
        StringRequest strReq = new StringRequest(Request.Method.POST,url+"/Api_picking/post_array/", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);

                try {
                    JSONObject data = new JSONObject(response);

                    Integer status = data.getInt("status");
                    String message = data.getString("message");

                    if (status == 200) {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        outbound_location.setText("");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    String jsonError = new String(networkResponse.data);


                    try {
                        JSONObject data = new JSONObject(jsonError);

                        String message = data.getString("message");

                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        })  {

            @Override
            protected Map<String, String> getParams(){
                Map<String, String> params = new HashMap<String, String>();
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String usernameKey = prefs.getString("usernameKey", "No name defined");

                Calendar calendar = Calendar.getInstance();
                SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm:ss");

                intent = getIntent();
                String picking_document = intent.getStringExtra("picking_document");

                params.put("picking_name",picking_document);
                params.put("item_code",picking_document);
                params.put("serial_number",picking_document);
                params.put("old_loc",String.valueOf(outbound_location.getText()));
                params.put("qty",picking_document);
                params.put("user_pick",usernameKey);
                params.put("pick_time",mdformat.format(calendar.getTime()));
                params.put("outbound_loc",String.valueOf(outbound_location.getText()));
                params.put("user_put",usernameKey);
                params.put("put_time",mdformat.format(calendar.getTime()));

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "RFID/application/x-www-form-urlencoded");
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;


            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }

    public void showMessage(String title, String Message){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setCancelable(true);

        builder.setTitle(title);

        builder.setMessage(Message);

        builder.show();

    }
}
