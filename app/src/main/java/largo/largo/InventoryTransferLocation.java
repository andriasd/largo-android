package largo.largo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static largo.largo.LoginActivity.MyPREFERENCES;

public class InventoryTransferLocation extends AppCompatActivity {

    private InventoryTransferGetSet inventoryTransferGetSet;
    private List<InventoryTransferGetSet> inventoryTransferGetSetList = new ArrayList<>();
    private List<String> tagList = new ArrayList<>();
    private InventoryTransferListAdapter inventoryTransferListAdapter;
    private ListView snList;
    private AsnScanRFID asnScanRFID = new AsnScanRFID();
    private Map<String, String> params = new HashMap<String, String>();
    private Intent intent;
    private ImageButton scanButton, scanLocButton;
    private String fClass = this.getClass().getSimpleName();
    private String bin_transfer_code = "", item_code = "";
    private String[] splitItemCode;

    private TextView totalScanned, header_transfer;

    String TAG = InventoryTransferLocation.class.getSimpleName();
    EditText location,serial_number;

    DatabaseHelper myDb;
    String url,user_tallied,tallied_time,put_time,user_putaway;

    Button backButton,next_loc;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inventory_transfer_location);

        final Calendar calendar = Calendar.getInstance();
        final SimpleDateFormat mdformat = new SimpleDateFormat("YYYY-MM-dd");
        final SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        final String usernameKey = prefs.getString("usernameKey", "No name defined");

        totalScanned = findViewById(R.id.totalScanned);
        header_transfer = findViewById(R.id.header_transfer);

        snList = findViewById(R.id.listTransfer);
        inventoryTransferListAdapter = new InventoryTransferListAdapter(inventoryTransferGetSetList, getApplicationContext(), 2);
        snList.setAdapter(inventoryTransferListAdapter);

        intent = getIntent();
        if(intent.getSerializableExtra("inventoryTransferGetSetList") != null) {
            inventoryTransferGetSetList = (List<InventoryTransferGetSet>) intent.getSerializableExtra("inventoryTransferGetSetList");
            inventoryTransferGetSet = (InventoryTransferGetSet) intent.getSerializableExtra("inventoryTransferGetSet");
            tagList = (List<String>) intent.getSerializableExtra("tagList");
            inventoryTransferListAdapter = new InventoryTransferListAdapter(inventoryTransferGetSetList, getApplicationContext(), 2);
            snList.setAdapter(inventoryTransferListAdapter);
            inventoryTransferListAdapter.notifyDataSetChanged();
            totalScanned.setText(Integer.toString(inventoryTransferGetSetList.size()));
        }

        if(intent.getStringExtra("bin_transfer_code") != null) {
            bin_transfer_code = intent.getStringExtra("bin_transfer_code");
            item_code = intent.getStringExtra("item_code");
            splitItemCode = item_code.split(" - ");
            header_transfer.setText(header_transfer.getText() + " - " + bin_transfer_code + ", " + item_code);
            header_transfer.setSelected(true);
        }

        myDb = new DatabaseHelper(this);

        getServerURL();

        intent = getIntent();

        location = findViewById(R.id.location);
        location.setShowSoftInputOnFocus(false);

        location.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    getTransferLocation(location.getText().toString());
                }
                return false;
            }
        });

        serial_number = findViewById(R.id.serial_number);
        serial_number.setShowSoftInputOnFocus(false);

        serial_number.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    if(location.getText().toString().length() > 0) {
                        if(bin_transfer_code.length() > 0) {
                            if (tagList.contains(serial_number.getText().toString())) {
                                new postBinTransferASync(
                                        serial_number.getText().toString(),
                                        splitItemCode[0],
                                        bin_transfer_code,
                                        location.getText().toString()
                                ).execute();
                            } else {
                                Toast.makeText(getApplicationContext(), serial_number.getText().toString() + " tidak terdapat dalam list.", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            if (serial_number.getText().toString().contains("OL")) {
                                params.put("outer_label", serial_number.getText().toString().toUpperCase());
                                getOuterLabelData(params);
                            } else {
                                if (tagList.contains(serial_number.getText().toString())) {
                                    for (int i = 0; i < inventoryTransferGetSetList.size(); i++) {
                                        if (inventoryTransferGetSetList.get(i).getHex().equals(serial_number.getText().toString().toUpperCase())) {
                                            params.put("serial_number", inventoryTransferGetSetList.get(i).getHex());
                                            params.put("loc_name_old", inventoryTransferGetSetList.get(i).getLoc());
                                            params.put("user_pick", usernameKey);
                                            params.put("pick_time", inventoryTransferGetSetList.get(i).getPickTime());
                                            params.put("loc_name_new", location.getText().toString());
                                            params.put("user_put", usernameKey);
                                            params.put("put_time", mdformat.format(calendar.getTime()));
                                            postTransfer(params);
                                        }
                                    }
                                } else {
                                    Toast.makeText(getApplicationContext(), serial_number.getText().toString() + " tidak terdapat dalam list.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Harap isi lokasi terlebih dahulu.", Toast.LENGTH_SHORT);
                    }
                }
                return false;
            }
        });

        backButton = findViewById(R.id.back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(inventoryTransferGetSetList.size() > 0) {
                    if(bin_transfer_code.length() > 0) {
                        for(int i = 0; i < inventoryTransferGetSetList.size(); i++) {
                            if(inventoryTransferGetSetList.get(i).getFlag().equals("")) {
                                String header = "{\"serial_number\":[";
                                String serial_numbers = "";
                                String ender = "]}";
                                for (int j = 0; j < inventoryTransferGetSetList.size(); j++) {
                                    serial_numbers = serial_numbers + "\"" + inventoryTransferGetSetList.get(j).getHex().toUpperCase() + "\",";
                                }
                                String snJSON = header + serial_numbers.substring(0, serial_numbers.length()-1) + ender;
                                params.clear();
                                params.put("serial_number" , snJSON);
                                params.put("bin_transfer_code" , bin_transfer_code);
                                postBinTransferCancel(params);
                            }
                        }
                        intent = new Intent(InventoryTransferLocation.this, BinTransferDocItem.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("bin_transfer_code", bin_transfer_code);
                        startActivity(intent);
                    } else {
                        for(int i = 0; i < inventoryTransferGetSetList.size(); i++) {
                            if(inventoryTransferGetSetList.get(i).getFlag().equals("")) {
                                params.put("serial_number", inventoryTransferGetSetList.get(i).getHex().toUpperCase());
                                params.put("loc_name_old", inventoryTransferGetSetList.get(i).getLoc());
                                params.put("user_pick", usernameKey);
                                params.put("pick_time", mdformat.format(calendar.getTime()));
                                try {
                                    postTransferCancel(params);
                                    Thread.sleep(100);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }
                finish();
            }
        });

        next_loc = findViewById(R.id.next);
        next_loc.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        location.setText("");
                        serial_number.setText("");
                    }
                }
        );

        scanLocButton = findViewById(R.id.scanLocButton);
        scanLocButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getApplicationContext(), QRScanner.class);
                intent.putExtra("param", "inventoryTransferLoc");
                intent.putExtra("fClass", fClass);
                intent.putExtra("inventoryTransferGetSetList", (Serializable) inventoryTransferGetSetList);
                intent.putExtra("tagList", (Serializable) tagList);
                intent.putExtra("inventoryTransferGetSet", inventoryTransferGetSet);
                startActivity(intent);
            }
        });

        if(intent.getStringExtra("inventoryTransferLoc") != null) {
            System.out.println("inventoryTransferLoc : " + intent.getStringExtra("inventoryTransferLoc"));
            location.setText(intent.getStringExtra("inventoryTransferLoc"));
            getTransferLocation(intent.getStringExtra("inventoryTransferLoc"));
            inventoryTransferGetSetList = (List<InventoryTransferGetSet>) intent.getSerializableExtra("inventoryTransferGetSetList");
            inventoryTransferGetSet = (InventoryTransferGetSet) intent.getSerializableExtra("inventoryTransferGetSet");
            tagList = (List<String>) intent.getSerializableExtra("tagList");
        }

        scanButton = findViewById(R.id.scanButton);
        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getApplicationContext(), QRScanner.class);
                intent.putExtra("param", "inventoryTransferLocQR");
                intent.putExtra("fClass", fClass);
                intent.putExtra("inventoryTransferGetSetList", (Serializable) inventoryTransferGetSetList);
                intent.putExtra("tagList", (Serializable) tagList);
                intent.putExtra("inventoryTransferGetSet", inventoryTransferGetSet);
                intent.putExtra("location", location.getText().toString());
                startActivity(intent);
            }
        });

        if(intent.getStringExtra("inventoryTransferLocQR") != null) {
            inventoryTransferGetSetList = (List<InventoryTransferGetSet>) intent.getSerializableExtra("inventoryTransferGetSetList");
            inventoryTransferGetSet = (InventoryTransferGetSet) intent.getSerializableExtra("inventoryTransferGetSet");
            tagList = (List<String>) intent.getSerializableExtra("tagList");

            if(intent.getStringExtra("location").length() > 0) {
                location.setText(intent.getStringExtra("location"));
                if(location.getText().toString().length() > 0) {
                    if(intent.getStringExtra("inventoryTransferLocQR").toUpperCase().contains("OL")) {
                        params.put("outer_label", intent.getStringExtra("inventoryTransferLocQR").toUpperCase());
                        getOuterLabelData(params);
                    } else {
                        if(tagList.contains(intent.getStringExtra("inventoryTransferLocQR").toUpperCase())) {
                            for (int i = 0; i < inventoryTransferGetSetList.size(); i++) {
                                if(inventoryTransferGetSetList.get(i).getHex().equals(intent.getStringExtra("inventoryTransferLocQR").toUpperCase())) {
                                    params.put("serial_number", inventoryTransferGetSetList.get(i).getHex());
                                    params.put("loc_name_old", inventoryTransferGetSetList.get(i).getLoc());
                                    params.put("user_pick", usernameKey);
                                    params.put("pick_time", inventoryTransferGetSetList.get(i).getPickTime());
                                    params.put("loc_name_new", location.getText().toString());
                                    params.put("user_put", usernameKey);
                                    params.put("put_time", mdformat.format(calendar.getTime()));
                                    postTransfer(params);
                                }
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), serial_number.getText().toString() + " tidak terdapat dalam list.", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Harap isi lokasi terlebih dahulu.", Toast.LENGTH_SHORT);
                }
            }

        }
    }

    public void getOuterLabelData(final Map<String, String> parameters) {
        String postUrl = this.url + "/Api_transfer/getOuterLabelItem/";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        final Calendar calendar = Calendar.getInstance();
                        final SimpleDateFormat mdformat = new SimpleDateFormat("YYYY-MM-dd");
                        final SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                        final String usernameKey = prefs.getString("usernameKey", "No name defined");

                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            String message = jsonResponse.getString("message");
                            JSONArray jsonMessage = new JSONArray(message);

                            for(int i = 0; i < jsonMessage.length(); i++) {
                                String unique_code = jsonMessage.getString(i);
                                JSONObject jsonHex = new JSONObject(unique_code);
                                String hex = jsonHex.getString("unique_code");

                                for (int j = 0; j < inventoryTransferGetSetList.size(); j++) {
                                    if (inventoryTransferGetSetList.get(j).getHex().equals(hex.toUpperCase())) {
                                        new postTransferASync(
                                                inventoryTransferGetSetList.get(j).getHex(),
                                                inventoryTransferGetSetList.get(j).getLoc(),
                                                usernameKey,
                                                inventoryTransferGetSetList.get(j).getPickTime(),
                                                location.getText().toString(),
                                                usernameKey,
                                                mdformat.format(calendar.getTime())
                                        ).execute();
                                    }
                                }
                            }
                        } catch (JSONException jsonEx) {
                            jsonEx.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    private void getTransferLocation(final String locParam) {
        StringRequest strReq = new StringRequest(Request.Method.GET, url+"/Api_transfer/get_location/" + locParam.trim(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());

                try {
                    JSONObject data = new JSONObject(response);
                    Integer status = data.getInt("status");
                    String message = data.getString("message");

                    if (status == 200) {
                        serial_number.requestFocus();
                    }else{
                        location.setText("");
                        location.requestFocus();
                    }

                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }

    private void postTransfer(final Map<String, String> parameters) {
        StringRequest strReq = new StringRequest(Request.Method.POST, url + "/Api_transfer/post/", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("postTransfer", response.toString());

                try {
                    JSONObject data = new JSONObject(response);
                    Integer status = data.getInt("status");
                    String message = data.getString("message");
                    String sn = data.getString("param");

                    if(status == 200) {
                        //Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        for(int i = 0; i < inventoryTransferGetSetList.size(); i++) {
                            if(inventoryTransferGetSetList.get(i).getHex().toUpperCase().equals(sn.toUpperCase())) {
                                inventoryTransferGetSet = new InventoryTransferGetSet(
                                        inventoryTransferGetSetList.get(i).getSn(),
                                        //inventoryTransferGetSetList.get(i).getLoc(),
                                        location.getText().toString(),
                                        inventoryTransferGetSetList.get(i).getHex(),
                                        inventoryTransferGetSetList.get(i).getPickTime(),
                                        "done"
                                );
                                inventoryTransferGetSetList.set(i, inventoryTransferGetSet);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        inventoryTransferListAdapter.notifyDataSetChanged();
                                        totalScanned.setText(Integer.toString(inventoryTransferGetSetList.size()) + " pcs");
                                        serial_number.setText("");
                                        serial_number.requestFocus();
                                        serial_number.hasFocus();
                                    }
                                });
                                continue;
                            }
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // showMessage("Error","Silahkan lakukan picking / putaway terlebih dahulu !");
                Toast.makeText(getApplicationContext(), "Silahkan lakukan picking / putaway terlebih dahulu !", Toast.LENGTH_SHORT).show();
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;

            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }

    private class postTransferASync extends AsyncTask<Void, Void, Void> {
        String serial_number_param, loc_name_old_param, user_pick_param, pick_time_param, loc_name_new_param, user_put_param, put_time;

        postTransferASync(
                String serial_number_param,
                String loc_name_old_param,
                String user_pick_param,
                String pick_time_param,
                String loc_name_new_param,
                String user_put_param,
                String put_time)
        {
            super();
            this.serial_number_param = serial_number_param;
            this.loc_name_old_param = loc_name_old_param;
            this.user_pick_param = user_pick_param;
            this.pick_time_param = pick_time_param;
            this.loc_name_new_param = loc_name_new_param;
            this.user_put_param = user_put_param;
            this.put_time = put_time;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            StringRequest strReq = new StringRequest(Request.Method.POST, url + "/Api_transfer/post/", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("postTransfer", response.toString());

                    try {
                        JSONObject data = new JSONObject(response);
                        Integer status = data.getInt("status");
                        String message = data.getString("message");
                        String sn = data.getString("param");

                        if(status == 200) {
                            //Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            for(int i = 0; i < inventoryTransferGetSetList.size(); i++) {
                                if(inventoryTransferGetSetList.get(i).getHex().toUpperCase().equals(sn.toUpperCase())) {
                                    inventoryTransferGetSet = new InventoryTransferGetSet(
                                            inventoryTransferGetSetList.get(i).getSn(),
                                            //inventoryTransferGetSetList.get(i).getLoc(),
                                            location.getText().toString(),
                                            inventoryTransferGetSetList.get(i).getHex(),
                                            inventoryTransferGetSetList.get(i).getPickTime(),
                                            "done"
                                    );
                                    inventoryTransferGetSetList.set(i, inventoryTransferGetSet);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            inventoryTransferListAdapter.notifyDataSetChanged();
                                            totalScanned.setText(Integer.toString(inventoryTransferGetSetList.size()) + " pcs");
                                            serial_number.setText("");
                                            serial_number.requestFocus();
                                            serial_number.hasFocus();
                                        }
                                    });
                                    continue;
                                }
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // showMessage("Error","Silahkan lakukan picking / putaway terlebih dahulu !");
                    Toast.makeText(getApplicationContext(), "Silahkan lakukan picking / putaway terlebih dahulu !", Toast.LENGTH_SHORT).show();
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("serial_number", serial_number_param.toUpperCase());
                    params.put("loc_name_old", loc_name_old_param);
                    params.put("user_pick", user_pick_param);
                    params.put("pick_time", pick_time_param);
                    params.put("loc_name_new", loc_name_new_param);
                    params.put("user_put", user_put_param);
                    params.put("put_time", put_time);
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                    String restoredText = prefs.getString("handheldsessioncodeKey", null);

                    String usernameKey = prefs.getString("usernameKey", "No name defined");
                    String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                    Log.d(TAG, usernameKey);
                    Log.d(TAG, handheldsessioncodeKey);

                    Map<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "application/x-www-form-urlencoded");
                    headers.put("User", usernameKey);
                    headers.put("Authorization", handheldsessioncodeKey);
                    return headers;

                }
            };

            controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
            return null;
        }
    }

    private class postBinTransferASync extends AsyncTask<Void, Void, Void> {
        String serialNumberParam = "";
        String itemCodeParam = "";
        String binTransferCodeParam = "";
        String locationCodeParam = "";

        postBinTransferASync(String serialNumberParam, String itemCodeParam, String binTransferCodeParam, String locationCodeParam) {
            super();
            this.serialNumberParam = serialNumberParam;
            this.itemCodeParam = itemCodeParam;
            this.binTransferCodeParam = binTransferCodeParam;
            this.locationCodeParam = locationCodeParam;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            StringRequest strReq = new StringRequest(Request.Method.POST,url+"/Api_bin_transfer/post/", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("postBinTransferASync", response);
                    try {
                        JSONObject data = new JSONObject(response);
                        Integer status = data.getInt("status");
                        String message = data.getString("message");
                        String sn = data.getString("param");

                        if(status == 200) {
                            //Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            for(int i = 0; i < inventoryTransferGetSetList.size(); i++) {
                                if(inventoryTransferGetSetList.get(i).getHex().toUpperCase().equals(serialNumberParam.toUpperCase())) {
                                    inventoryTransferGetSet = new InventoryTransferGetSet(
                                            inventoryTransferGetSetList.get(i).getSn(),
                                            location.getText().toString(),
                                            inventoryTransferGetSetList.get(i).getHex(),
                                            inventoryTransferGetSetList.get(i).getPickTime(),
                                            "done"
                                    );
                                    inventoryTransferGetSetList.set(i, inventoryTransferGetSet);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            inventoryTransferListAdapter.notifyDataSetChanged();
                                            totalScanned.setText(Integer.toString(inventoryTransferGetSetList.size()));
                                            serial_number.setText("");
                                            serial_number.requestFocus();
                                            serial_number.hasFocus();
                                        }
                                    });
                                    continue;
                                }
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    NetworkResponse networkResponse = error.networkResponse;
                    if (networkResponse != null && networkResponse.data != null) {
                        String jsonError = new String(networkResponse.data);

                        try {
                            JSONObject data = new JSONObject(jsonError);

                            String message = data.getString("message");

                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            })  {

                @Override
                protected Map<String, String> getParams(){
                    Map<String, String> params = new HashMap<String, String>();
                    SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                    String usernameKey = prefs.getString("usernameKey", "No name defined");

                    params.put("serial_number", serialNumberParam);
                    params.put("item_code", itemCodeParam);
                    params.put("bin_transfer_code", binTransferCodeParam);
                    params.put("location_code", locationCodeParam);

                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                    String restoredText = prefs.getString("handheldsessioncodeKey", null);

                    String usernameKey = prefs.getString("usernameKey", "No name defined");
                    String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                    Log.d(TAG, usernameKey);
                    Log.d(TAG, handheldsessioncodeKey);

                    Map<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "application/x-www-form-urlencoded");
                    headers.put("User",usernameKey);
                    headers.put("Authorization", handheldsessioncodeKey);
                    return headers;
                }
            };

            controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
            return null;
        }
    }

    private void postTransferCancel(final Map<String, String> parameters) {
        StringRequest strReq = new StringRequest(Request.Method.POST,url+"/Api_transfer/cancel/", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("postTransfer", response);
                    JSONObject data = new JSONObject(response);

                    Integer status = data.getInt("status");
                    String message = data.getString("message");

                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    String jsonError = new String(networkResponse.data);

                    try {
                        JSONObject data = new JSONObject(jsonError);

                        String message = data.getString("message");

                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        })  {

            @Override
            protected Map<String, String> getParams(){
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }

    public void postBinTransferCancel(final Map<String, String> parameters) {
        StringRequest strReq = new StringRequest(Request.Method.POST,url+"/Api_bin_transfer/cancel/", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("postTransferCancel", response);
                    JSONObject data = new JSONObject(response);

                    Integer status = data.getInt("status");
                    String message = data.getString("message");

                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    String jsonError = new String(networkResponse.data);

                    try {
                        JSONObject data = new JSONObject(jsonError);

                        String message = data.getString("message");

                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        })  {

            @Override
            protected Map<String, String> getParams(){
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }
}
