package largo.largo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static largo.largo.TallyReceivingDocument.MyPREFERENCES;

public class TallyReceivingItems extends AppCompatActivity {
    String TAG = TallyReceivingItems.class.getSimpleName();
    ImageButton scanButton;
    Button next,backButton;
    EditText sku;

    DatabaseHelper myDb;

    String url;
    String param = this.getClass().getSimpleName();

    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    List<String> expand_detail = new ArrayList<String>();
    HashMap<String, List<String>> listDataChild;

    Intent intent;
    Bundle bundle;
    TextView receiving_doc;

    ListView listView;
    private List<ReceivingItemGetterSetter> skuItemList;

    private int lastExpandedPosition = -1;

    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String item_name = "item_name";
    public static final String qty = "qty";
    SharedPreferences sharedpreferences;

    String receiving_document;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inbound_tally_receiving_items);

        backButton = findViewById(R.id.back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        scanButton = findViewById(R.id.scanButton);

        intent = getIntent();
        final String receiving_document = intent.getStringExtra("receiving_document");

        LocalBroadcastManager.getInstance(this).registerReceiver(qrScan, new IntentFilter("broadcastQrResult"));

        myDb = new DatabaseHelper(this);

        getServerURL();

        next = findViewById(R.id.next);

        receiving_doc = (TextView)findViewById(R.id.receiving_doc);

        receiving_doc.setText(receiving_document);

        sku = findViewById(R.id.sku);
        sku.setShowSoftInputOnFocus(false);

        listView =  findViewById(R.id.ListTallyItems);

        skuItemList = new ArrayList<>();
        DataListSku(url + "/api/receiving/get_document_item/");

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                sku.setText(skuItemList.get(position).getSku());
            }
        });

        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), QRScanner.class);
                intent.putExtra("param", param);
                startActivity(intent);
            }
        });

//        expListView = (ExpandableListView)findViewById(R.id.ListTallySerialNumber);
//
//        prepareListData(url+"/api/receiving/get_document_item/");
//
//        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);
//
//        expListView.setAdapter(listAdapter);
//
//        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
//            @Override
//            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
//                return false;
//            }
//        });
//
//        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
//            @Override
//            public void onGroupExpand(int groupPosition) {
//                sku.setText(listDataHeader.get(groupPosition));
//                if (lastExpandedPosition != -1
//                        && groupPosition != lastExpandedPosition) {
//                    expListView.collapseGroup(lastExpandedPosition);
//                }
//                lastExpandedPosition = groupPosition;
////                Toast.makeText(getApplicationContext(),listDataHeader.get(groupPosition) + "Expanded", Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
//            @Override
//            public void onGroupCollapse(int groupPosition) {
//                Toast.makeText(getApplicationContext(),listDataHeader.get(groupPosition) + "Collapsed", Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
//            @Override
//            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
//                Toast.makeText(getApplicationContext(),listDataHeader.get(groupPosition)
//                        + ":"
//                        + listDataChild.get(
//                        listDataHeader.get(groupPosition)).get(
//                        childPosition), Toast.LENGTH_SHORT).show();
//                return false;
//            }
//        });

        sku.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    getItemDetail();
                }
                return false;
            }
        });

        next.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        getItemDetail();
                    }
                }
        );
    }

    private void DataListSku(String getItem){
//        final ArrayList<String> items = new ArrayList<String>();

        RequestQueue requestQueue=Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, getItem + receiving_doc.getText(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                try{
                    JSONObject jsonObject=new JSONObject(response);
                    if(jsonObject.getInt("status")==200){
                        JSONArray jsonArray1=jsonObject.getJSONArray("results");
                        for(int i=0;i<jsonArray1.length();i++){
                            JSONObject jsonObject1=jsonArray1.getJSONObject(i);

                            String nama_barang = jsonObject1.getString("item_name");
                            sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedpreferences.edit();
                            editor.putString(item_name, nama_barang);
                            editor.apply();

                            ReceivingItemGetterSetter item_sku = new ReceivingItemGetterSetter(
                                    jsonObject1.getString("item_code"),
                                    jsonObject1.getString("item_name"),
                                    jsonObject1.getString("qty"),
                                    jsonObject1.getString("source")
                            );
                            skuItemList.add(item_sku);
                        }
                        ReceivingItemListAdapter adapter = new ReceivingItemListAdapter(skuItemList, getApplicationContext());

                        listView.setAdapter(adapter);

                    }
//                    spinner.setAdapter(new ArrayAdapter<String>(TallyReceivingItemSerial.this, android.R.layout.simple_spinner_dropdown_item, QC));
                }catch (JSONException e){e.printStackTrace();}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;


            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

//    private void prepareListData(String getItem){
//        listDataHeader = new ArrayList<>();
//        listDataChild = new HashMap<String, List<String>>();
//        RequestQueue requestQueue=Volley.newRequestQueue(getApplicationContext());
//        StringRequest stringRequest=new StringRequest(Request.Method.GET, getItem + String.valueOf(receiving_doc.getText()), new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.d(TAG, response.toString());
//                try{
//                    JSONObject jsonObject=new JSONObject(response);
//                    if(jsonObject.getInt("status")==200){
//                        JSONArray jsonArray1=jsonObject.getJSONArray("results");
//                        for(int i=0;i<jsonArray1.length();i++){
//                            JSONObject jsonObject1=jsonArray1.getJSONObject(i);
//                            String kd_barang=jsonObject1.getString("item_code");
//                            listDataHeader.add(kd_barang);
//                            String nama_barang = jsonObject1.getString("item_name");
//                            Integer qtys = jsonObject1.getInt("qty");
//                            String source = jsonObject1.getString("source");
//
//                            sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
//                            SharedPreferences.Editor editor = sharedpreferences.edit();
//                            editor.putString(item_name, nama_barang);
//                            editor.putInt(qty, qtys);
//                            editor.apply();
//
////                            if(nama_barang.length() > 30){
////                                nama_barang = nama_barang.substring(0, 30 - "...".length()).concat("...");
////                            }
//
//                            expand_detail.add(nama_barang + "\n" + qtys + "\n" + source);
//                            listDataChild.put(listDataHeader.get(i), new ArrayList<String>(Arrays.asList(expand_detail.get(i))));
//                        }
//
//                    }else if(jsonObject.getInt("status")==401){
//                        Toast.makeText(getApplicationContext(),jsonObject.getString("message"),Toast.LENGTH_SHORT).show();
//                    }
////                    spinner.setAdapter(new ArrayAdapter<String>(TallyReceivingItemSerial.this, android.R.layout.simple_spinner_dropdown_item, QC));
//                }catch (JSONException e){e.printStackTrace();}
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                error.printStackTrace();
//            }
//        }) {
//
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
//                String restoredText = prefs.getString("handheldsessioncodeKey", null);
//
//                String usernameKey = prefs.getString("usernameKey", "No name defined");
//                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
//                Log.d(TAG, usernameKey);
//                Log.d(TAG, handheldsessioncodeKey);
//
//                Map<String, String> headers = new HashMap<>();
//                headers.put("User",usernameKey);
//                headers.put("Authorization", handheldsessioncodeKey);
//                return headers;
//
//            }
//        };
//        int socketTimeout = 30000;
//        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//        stringRequest.setRetryPolicy(policy);
//        requestQueue.add(stringRequest);
//
//    }

    private void getItemDetail() {
        StringRequest strReq = new StringRequest(Request.Method.GET, url+"/api/receiving/get_document_item/"+ intent.getStringExtra("receiving_document") + "/" + String.valueOf(sku.getText()), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());

                try {
                    JSONObject data = new JSONObject(response);

                    Integer status = data.getInt("status");
                    JSONArray jsonArray1=data.getJSONArray("results");
                    for(int i=0;i<jsonArray1.length();i++){
                        JSONObject jsonObject1=jsonArray1.getJSONObject(i);
                        String item_name = jsonObject1.getString("item_name");
                        String def_qty = jsonObject1.getString("default_qty");
                        String qty = jsonObject1.getString("qty");
                        String has_qty = jsonObject1.getString("has_qty");
                        String has_batch = jsonObject1.getString("has_batch");
                        String has_expdate = jsonObject1.getString("has_expdate");
                        if (status == 200) {
                            Intent intent = new Intent(getApplicationContext(), TallyReceivingItemSerial.class);
                            intent.putExtra("sku",String.valueOf(sku.getText()));
                            intent.putExtra("item_name",item_name);
                            intent.putExtra("receiving_document",String.valueOf(receiving_doc.getText()));
                            intent.putExtra("has_qty",has_qty);
                            intent.putExtra("qty",qty);
                            intent.putExtra("def_qty",def_qty);
                            intent.putExtra("has_batch",has_batch);
                            intent.putExtra("has_expdate",has_expdate);
                            startActivity(intent);
//                            finish();
                        } else if (status == 401) {
                            Toast.makeText(getApplicationContext(), "Document Not Found.", Toast.LENGTH_SHORT).show();
                        }
                    }

                } catch (JSONException e) {
//                    Log.d(TAG,e.getMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;

            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }

    private BroadcastReceiver qrScan = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                sku.setText(intent.getStringExtra(param));
                getItemDetail();
            } catch (Exception e) {
//                e.printStackTrace();
            }
        }
    };
}
