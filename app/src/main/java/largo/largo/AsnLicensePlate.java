package largo.largo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AsnLicensePlate extends AppCompatActivity {
    private Button cancelButton, doneButton;
    private ImageButton scanButton;
    private EditText license_plat;
    private TextView item_detail, totalItems;
    private ListView qrList;
    private Intent intent;
    private List<ReceivingDocumentItemGetterSetter> dokumenItemList;
    private List<AsnGetSet> asnArrayList;
    private AsnGetSet asnGetSet;
    private AsnListAdapter adapter;
    private JSONObject paramsJson;
    public static final Map<String, String> params = new HashMap<String, String>();
    private SharedPreferences prefs;
    private final String MyPREFERENCES = "MyPrefs";
    private final String TAG = this.getClass().getSimpleName();
    private String param, receiving_document, item_code, asnQty, asnBatch, asnExpDate;
    private String docType = "";
    DatabaseHelper myDb;
    String url;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inbound_license_plating_by_asn);

        myDb = new DatabaseHelper(this);
        getServerURL();

        param = this.getClass().getSimpleName();

        cancelButton = findViewById(R.id.back);
        doneButton = findViewById(R.id.done);
        scanButton = findViewById(R.id.scanButton);
        license_plat = findViewById(R.id.license_plat);
        qrList = findViewById(R.id.QRList);
        item_detail = findViewById(R.id.detail_text);
        totalItems = findViewById(R.id.qty);
        totalItems.setText("0");

        paramsJson = new JSONObject();

        intent = getIntent();
        if(intent.getSerializableExtra("asnArrayList") != null) { asnArrayList = (List<AsnGetSet>) intent.getSerializableExtra("asnArrayList"); }
        if(intent.getStringExtra("receiving_document") != null) { receiving_document = intent.getStringExtra("receiving_document"); }
        if(intent.getStringExtra("item_code") != null) { item_code = intent.getStringExtra("item_code"); }
        if(intent.getStringExtra("asnBatch") != null) { asnBatch = intent.getStringExtra("asnBatch"); }
        if(intent.getStringExtra("asnExpDate") != null) { asnExpDate = intent.getStringExtra("asnExpDate"); }
        if(intent.getStringExtra("asnQty") != null) { asnQty = intent.getStringExtra("asnQty"); }
        if(intent.getSerializableExtra("dokumenItemList") != null) { dokumenItemList = (List<ReceivingDocumentItemGetterSetter>) intent.getSerializableExtra("dokumenItemList"); }

        for(int i = 0; i < dokumenItemList.size(); i++) {
            if(dokumenItemList.get(i).getItem_code().equals(item_code)) {
                String detText = dokumenItemList.get(i).getItem_code() + " - " + dokumenItemList.get(i).getItem_name();
                detText = detText + "\n";
                detText = detText + "Batch : " + asnBatch;
                detText = detText + "\n";
                detText = detText + "Exp Date : " + asnExpDate;
                detText = detText + "\n";
                detText = detText + "Qty : " + asnQty + " " + dokumenItemList.get(i).getNama_satuan();
                item_detail.setText(detText);
            }
        }

        if(asnArrayList != null) {
            adapter = new AsnListAdapter(asnArrayList, getApplicationContext(), 1);
            qrList.setAdapter(adapter);
            totalItems.setText(Integer.toString(asnArrayList.size()));
        }

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(getApplicationContext(), Inbound.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });

        license_plat.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 0) {
                    if (license_plat.length() != 0) {
                        if(license_plat.getText().toString().contains("OL")) {
                            getDocType(receiving_document);
                        } else {
                            Toast.makeText(getApplicationContext(), "Outer label format should start with OL.", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    doneButton.setEnabled(false);
                }
                return false;
            }
        });

        doneButton.setEnabled(false);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (license_plat.length() != 0) {
                    if(license_plat.getText().toString().contains("OL")) {
                        try {
                            int arraySize;
                            SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                            String usernameKey = prefs.getString("usernameKey", "No name defined");
                            Long tsLong = System.currentTimeMillis() / 1000;
                            String jsonData = "";

                            String outer_label = license_plat.getText().toString();

                            try {
                                arraySize = asnArrayList.size();
                            } catch (Exception e) {
                                arraySize = 0;
                            }

                            if (arraySize > 0) {
                                jsonData = "[";
                                for (int i = 0; i < arraySize; i++) {
                                    if (!asnArrayList.get(i).getPcs().equals("0")) {
                                        paramsJson.put("receiving_code", receiving_document);
                                        paramsJson.put("item_code", item_code);
                                        paramsJson.put("serial_number", asnArrayList.get(i).getUpc());
                                        if(asnExpDate.length() > 0) {
                                            paramsJson.put("tgl_exp", new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("dd MMM yyyy").parse(asnExpDate)));
                                        } else {
                                            paramsJson.put("tgl_exp", "0000-00-00");
                                        }
                                        paramsJson.put("qty", asnQty);
                                        paramsJson.put("batch_code", asnBatch);
                                        paramsJson.put("qc_code", "GOOD");
                                        paramsJson.put("tgl_in", new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
                                        paramsJson.put("uname", usernameKey);
                                        paramsJson.put("parent_code", outer_label);

                                        jsonData = jsonData + paramsJson.toString() + ",";
                                    }
                                }

                                jsonData = jsonData.substring(0, jsonData.length() - 1);
                                jsonData = jsonData + "]";

                                Log.d("JSON Param", jsonData);
                                Log.d("STRLEN", Integer.toString(jsonData.length()));
                                params.clear();
                                params.put("data", jsonData);
                                System.out.println("postReceivingParam : " + jsonData);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        postReceiving(params);
                                    }
                                });
                            }
                        } catch(Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Outer label format should start with OL.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getApplicationContext(), QRScanner.class);
                intent.putExtra("fClass", param);
                intent.putExtra("param", "asnLicensePlateQR");
                intent.putExtra("asnArrayList", (Serializable) asnArrayList);
                intent.putExtra("dokumenItemList", (Serializable) dokumenItemList);
                intent.putExtra("receiving_document", receiving_document);
                intent.putExtra("item_code", item_code);
                intent.putExtra("asnQty", asnQty);
                intent.putExtra("asnBatch", asnBatch);
                intent.putExtra("asnExpDate", asnExpDate);
                startActivity(intent);
            }
        });

        if (intent.getStringExtra("asnLicensePlateQR") != null) {
            if (intent.getStringExtra("asnLicensePlateQR").contains("OL")) {
                license_plat.setText(intent.getStringExtra("asnLicensePlateQR"));
            } else {
                Toast.makeText(getApplicationContext(), "Outer label format should start with OL.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void getOuterLabelData(final Map<String, String> parameters) {
        String postUrl = this.url + "/Api_receiving/getValidOuterLabel/";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);

                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            String status = jsonResponse.getString("status");
                            if(status.equals("400")) {
                                license_plat.requestFocus();
                                license_plat.hasFocus();
                                license_plat.setText("");
                                Toast.makeText(getApplicationContext(), "Outer label ini sudah ada.", Toast.LENGTH_SHORT).show();
                            } else {
                                doneButton.setEnabled(true);
                                Toast.makeText(getApplicationContext(), "Outer label ini bisa digunakan.", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException jsonEx) {
                            jsonEx.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    public void getDocType(final String parameters) {
        String postUrl = this.url + "/Api_receiving/get_doc_type/" + parameters;

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.GET, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);

                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            String message = jsonResponse.getString("message");
                            if(message.equals("3") || message.equals("14")) {
                                docType = "ok";
                            } else {
                                params.put("outer_label", license_plat.getText().toString());
                                params.put("receiving_code", receiving_document);
                                params.put("item_code", item_code);
                                getOuterLabelData(params);
                            }
                        } catch (JSONException jsonEx) {
                            jsonEx.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    private void postReceiving(final Map<String, String> parameters) {
        String postUrl = this.url + "/Api_receiving/post_json";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);

                        try {
                            JSONObject data = new JSONObject(response);
                            String results = data.getString("results");
                            JSONArray resultsArray = new JSONArray(results);
                            JSONObject resultsObject = new JSONObject(resultsArray.get(0).toString());
                            int status = resultsObject.getInt("status");
                            String message = resultsObject.getString("message");

                            System.out.println("message : " + message);
                            if(status == 200) {
                                intent = new Intent(getApplicationContext(), AsnDocumentItem.class);
                                intent.putExtra("receiving_document", receiving_document);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                intent.removeExtra("receiving_document");
                            } else {
                                // showMessage("Alert", message);
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;

            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }
}