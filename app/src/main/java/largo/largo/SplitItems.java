package largo.largo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SplitItems extends AppCompatActivity {
    private InventoryQCGetSet inventoryQCGetSet;
    private List<InventoryQCGetSet> inventoryQCGetSetList = new ArrayList<>();
    private List<String> tagList = new ArrayList<>();

    String TAG = SplitItems.class.getSimpleName();

    private Intent intent, intentBack;

    DatabaseHelper myDb;

    String url;

    EditText serial_number;
    Button nextButton,backButton;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.split_items_sn);

        myDb = new DatabaseHelper(this);

        getServerURL();

        intent = getIntent();

        if(intent.getSerializableExtra("inventoryQCGetSet") != null) {
            intentBack = new Intent(this, InventoryQCOutLocation.class);
            inventoryQCGetSet = (InventoryQCGetSet) intent.getSerializableExtra("inventoryQCGetSet");
            inventoryQCGetSetList = (List<InventoryQCGetSet>) intent.getSerializableExtra("inventoryQCGetSetList");
            tagList = (List<String>) intent.getSerializableExtra("tagList");
        }

        serial_number = findViewById(R.id.serial_number);
        serial_number.setShowSoftInputOnFocus(false);

        serial_number.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    new validateSNASync(
                            serial_number.getText().toString().toUpperCase()
                    ).execute();
                }
                return false;
            }
        });

        backButton = findViewById(R.id.back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(intent.getSerializableExtra("inventoryQCGetSet") != null) {
                    intentBack.putExtra("tagList", (Serializable) tagList);
                    intentBack.putExtra("inventoryQCGetSetList", (Serializable) inventoryQCGetSetList);
                    intentBack.putExtra("inventoryQCGetSet", inventoryQCGetSet);
                    intentBack.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentBack);
                } else {
                    finish();
                }
            }
        });

        nextButton = findViewById(R.id.next);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }

    private class validateSNASync extends AsyncTask<Void, Void, Void> {
        String serialNumberParam = "";

        validateSNASync(String serialNumberParam) {
            super();
            this.serialNumberParam = serialNumberParam;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            StringRequest strReq = new StringRequest(Request.Method.GET, url+"/Api_split_serial_number/get/" + serialNumberParam, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        Log.d("validateSN", response);
                        JSONObject data = new JSONObject(response);

                        Integer status = data.getInt("status");
                        String message = data.getString("message");

                        if(message.contains("is available")) {
                            RandomString randomString = new RandomString();

                            intent = new Intent(SplitItems.this, SplitItemsQty.class);
                            intent.putExtra("serial_number", serialNumberParam);
                            intent.putExtra("new_serial_number", serialNumberParam + "-S" + randomString.generateAlphaNumeric(2));
                            intent.putExtra("tagList", (Serializable) tagList);
                            intent.putExtra("inventoryQCGetSetList", (Serializable) inventoryQCGetSetList);
                            intent.putExtra("inventoryQCGetSet", inventoryQCGetSet);
                            startActivity(intent);
                        } else {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        }
                        serial_number.requestFocus();
                        serial_number.setText("");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    SharedPreferences prefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
                    String restoredText = prefs.getString("handheldsessioncodeKey", null);

                    String usernameKey = prefs.getString("usernameKey", "No name defined");
                    String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                    Log.d(TAG, usernameKey);
                    Log.d(TAG, handheldsessioncodeKey);

                    Map<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "application/x-www-form-urlencoded");
                    headers.put("User",usernameKey);
                    headers.put("Authorization", handheldsessioncodeKey);
                    return headers;


                }
            };

            controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
            return null;
        }
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }
}
