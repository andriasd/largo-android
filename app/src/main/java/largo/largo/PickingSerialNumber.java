package largo.largo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static largo.largo.LoginActivity.MyPREFERENCES;

public class PickingSerialNumber extends AppCompatActivity {
    DatabaseHelper myDb;

    String TAG = PickingDocument.class.getSimpleName();

    Button next,backButton;
    EditText serial_number,etQty;
    TextView tvQty,picking_doc;

    String url;

    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    List<String> picked_item = new ArrayList<String>();
    HashMap<String, List<String>> listDataChild;

    ListView listView;
    private List<PickingItemGetterSetter> pickingItemList;

    Intent intent;

    private int lastExpandedPosition = -1;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.outbound_picking_serial_number);

        backButton = findViewById(R.id.back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        myDb = new DatabaseHelper(this);

        getServerURL();

        next = findViewById(R.id.next);
        serial_number = findViewById(R.id.picking_serial_number);
        serial_number.setShowSoftInputOnFocus(false);

        tvQty = findViewById(R.id.tvQty);
        etQty = findViewById(R.id.etQty);
        etQty.setShowSoftInputOnFocus(false);


        picking_doc = findViewById(R.id.picking_doc);

        listView =  findViewById(R.id.ListPickingDoc);

        pickingItemList = new ArrayList<>();
        DataListPickingItem();

        intent = getIntent();
        final String picking_document = intent.getStringExtra("picking_document");
        final String kd_barang = intent.getStringExtra("kd_barang");
        final String loc_name = intent.getStringExtra("loc_name");
        final String has_qty = intent.getStringExtra("has_qty");
        final String qty = intent.getStringExtra("qty");
        final String multi_qty = intent.getStringExtra("multi_qty");

        if(has_qty.equals("0")){
            tvQty.setVisibility(View.GONE);
            etQty.setVisibility(View.GONE);
            etQty.setInputType(InputType.TYPE_NULL);
        }

        picking_doc.setText(picking_document);

        serial_number.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    postSerialNumber();
                }
                return false;
            }
        });

//        expListView = (ExpandableListView)findViewById(R.id.ListPickingDoc);
//
//        prepareListData(url+"/Api_picking/get/");
//
//        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);
//
//        expListView.setAdapter(listAdapter);
//
//        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
//            @Override
//            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
//                return false;
//            }
//        });
//
//        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
//            @Override
//            public void onGroupExpand(int groupPosition) {
//                picking_doc.setText(listDataHeader.get(groupPosition));
//                if (lastExpandedPosition != -1
//                        && groupPosition != lastExpandedPosition) {
//                    expListView.collapseGroup(lastExpandedPosition);
//                }
//                lastExpandedPosition = groupPosition;
////                Toast.makeText(getApplicationContext(),listDataHeader.get(groupPosition) + "Expanded", Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
//            @Override
//            public void onGroupCollapse(int groupPosition) {
//                Toast.makeText(getApplicationContext(),listDataHeader.get(groupPosition) + "Collapsed", Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
//            @Override
//            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
//                Toast.makeText(getApplicationContext(),listDataHeader.get(groupPosition)
//                        + ":"
//                        + listDataChild.get(
//                        listDataHeader.get(groupPosition)).get(
//                        childPosition), Toast.LENGTH_SHORT).show();
//                return false;
//            }
//        });

        next.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Calendar calendar = Calendar.getInstance();
                        SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm:ss");

                        Intent intent = new Intent(PickingSerialNumber.this, PickingScanOutboundLocation.class);
                        intent.putExtra("picking_document",picking_document);
                        intent.putExtra("item_code",kd_barang);
                        intent.putExtra("serial_number",String.valueOf(serial_number.getText()));
                        intent.putExtra("old_loc",loc_name);
                        intent.putExtra("qty",has_qty == "0" ? "1" : multi_qty);
                        intent.putExtra("put_time",mdformat.format(calendar.getTime()));
                        startActivity(intent);
//                        postReceivingDoc();
                    }
                }
        );
    }

    private void postSerialNumber() {
        StringRequest strReq = new StringRequest(Request.Method.POST,url+"/Api_picking/post_serial/" , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);

                try {
                    JSONObject data = new JSONObject(response);

                    Integer status = data.getInt("status");
                    String message = data.getString("message");

                    if (status == 200) {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        serial_number.setText("");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    String jsonError = new String(networkResponse.data);


                    try {
                        JSONObject data = new JSONObject(jsonError);

                        String message = data.getString("message");

                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        })  {

            @Override
            protected Map<String, String> getParams(){
                Map<String, String> params = new HashMap<String, String>();
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String usernameKey = prefs.getString("usernameKey", "No name defined");

                Calendar calendar = Calendar.getInstance();
                SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm:ss");

                intent = getIntent();
                String picking_document = intent.getStringExtra("picking_document");
                String has_qty = intent.getStringExtra("has_qty");
                String qty = intent.getStringExtra("qty");
                String multi_qty = intent.getStringExtra("multi_qty");

                params.put("picking_code",picking_document);
                params.put("serial_number",String.valueOf(serial_number.getText()));
                params.put("qty",has_qty == "0" ? "1" : multi_qty);
                params.put("kit_time",mdformat.format(calendar.getTime()));
                params.put("pick_time",mdformat.format(calendar.getTime()));
                params.put("serial_code","");

                Log.d("params :",params.toString());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "RFID/application/x-www-form-urlencoded");
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;

            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }

    private void DataListPickingItem(){

        intent = getIntent();
        final String picking_document = intent.getStringExtra("picking_document");
        final String kd_barang = intent.getStringExtra("kd_barang");
        final String picked_qty = intent.getStringExtra("picked_qty");
        final String loc_name = intent.getStringExtra("loc_name");
        final String has_qty = intent.getStringExtra("has_qty");
        final String qty = intent.getStringExtra("qty");
        final String multi_qty = intent.getStringExtra("multi_qty");

        PickingItemGetterSetter picking_item = new PickingItemGetterSetter(
                kd_barang,
                picked_qty,
                has_qty,
                qty,
                multi_qty,
                loc_name
        );

        pickingItemList.add(picking_item);

        PickingItemListAdapter adapter = new PickingItemListAdapter(pickingItemList, getApplicationContext());

        listView.setAdapter(adapter);
    }

//    private void prepareListData(String getItem){
//        listDataHeader = new ArrayList<>();
//        listDataChild = new HashMap<String, List<String>>();
//        RequestQueue requestQueue=Volley.newRequestQueue(getApplicationContext());
//        StringRequest stringRequest=new StringRequest(Request.Method.GET, getItem + intent.getStringExtra("picking_document"), new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.d(TAG, response.toString());
//                try{
//                    JSONObject jsonObject=new JSONObject(response);
//                    if(jsonObject.getInt("status")==200){
//                        JSONArray jsonArray1=jsonObject.getJSONArray("locations");
//                        for(int i=0;i<jsonArray1.length();i++){
//                            JSONObject jsonObject1=jsonArray1.getJSONObject(i);
//                            String kd_barang=jsonObject1.getString("kd_barang");
//                            listDataHeader.add(kd_barang);
//                            if(jsonObject1.getInt("has_qty") == 1) {
//                                String qtys = jsonObject1.getString("multi_qty");
//                                String loc = jsonObject1.getString("loc_name");
//                                String batch = jsonObject1.getString("batch");
//                                picked_item.add(qtys + "/" + qtys + "\n" + loc + "\n" + batch);
//                                listDataChild.put(listDataHeader.get(i), new ArrayList<String>(Arrays.asList(picked_item.get(i))));
//                            }else{
//                                String qtys = jsonObject1.getString("qty");
//                                String loc = jsonObject1.getString("loc_name");
//                                String batch = jsonObject1.getString("batch");
//                                picked_item.add(qtys + "/" + qtys + "\n" + loc + "\n" + batch);
//                                listDataChild.put(listDataHeader.get(i), new ArrayList<String>(Arrays.asList(picked_item.get(i))));
//                            }
//                        }
//
//                    }
////                    spinner.setAdapter(new ArrayAdapter<String>(TallyReceivingItemSerial.this, android.R.layout.simple_spinner_dropdown_item, QC));
//                }catch (JSONException e){e.printStackTrace();}
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                error.printStackTrace();
//            }
//        }) {
//
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
//                String restoredText = prefs.getString("handheldsessioncodeKey", null);
//
//                String usernameKey = prefs.getString("usernameKey", "No name defined");
//                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
//                Log.d(TAG, usernameKey);
//                Log.d(TAG, handheldsessioncodeKey);
//
//                Map<String, String> headers = new HashMap<>();
//                headers.put("User",usernameKey);
//                headers.put("Authorization", handheldsessioncodeKey);
//                return headers;
//
//
//            }
//        };
//        int socketTimeout = 30000;
//        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//        stringRequest.setRetryPolicy(policy);
//        requestQueue.add(stringRequest);
//
//    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }
}
