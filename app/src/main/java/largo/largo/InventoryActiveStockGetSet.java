package largo.largo;

import java.io.Serializable;

public class InventoryActiveStockGetSet implements Serializable {
    String loc_name_old, user_name_old, pick_time, loc_name_new, user_name_new, put_time, process_name;

    public InventoryActiveStockGetSet(String loc_name_old, String user_name_old, String pick_time, String loc_name_new, String user_name_new, String put_time, String process_name) {
        this.loc_name_old = loc_name_old;
        this.user_name_old = user_name_old;
        this.pick_time = pick_time;
        this.loc_name_new = loc_name_new;
        this.user_name_new = user_name_new;
        this.put_time = put_time;
        this.process_name = process_name;
    }

    public String getLoc_name_old() {
        return loc_name_old;
    }

    public void setLoc_name_old(String loc_name_old) {
        this.loc_name_old = loc_name_old;
    }

    public String getUser_name_old() {
        return user_name_old;
    }

    public void setUser_name_old(String user_name_old) {
        this.user_name_old = user_name_old;
    }

    public String getPick_time() {
        return pick_time;
    }

    public void setPick_time(String pick_time) {
        this.pick_time = pick_time;
    }

    public String getLoc_name_new() {
        return loc_name_new;
    }

    public void setLoc_name_new(String loc_name_new) {
        this.loc_name_new = loc_name_new;
    }

    public String getUser_name_new() {
        return user_name_new;
    }

    public void setUser_name_new(String user_name_new) {
        this.user_name_new = user_name_new;
    }

    public String getPut_time() {
        return put_time;
    }

    public void setPut_time(String put_time) {
        this.put_time = put_time;
    }

    public String getProcess_name() {
        return process_name;
    }

    public void setProcess_name(String process_name) {
        this.process_name = process_name;
    }
}
