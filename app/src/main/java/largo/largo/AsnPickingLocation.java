package largo.largo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static largo.largo.LoginActivity.MyPREFERENCES;

public class AsnPickingLocation extends AppCompatActivity {
    DatabaseHelper myDb;

    ListView listView;
    private List<PickingItemGetterSetter> itemList;
    private PickingItemGetterSetter pickingItemGetSet;
    private List<String> docList;
    private String pickingDocument;
    private ImageButton scanButton;
    private String param;
    private String fClass = this.getClass().getSimpleName();
    private PickingItemListAdapter adapter;
    int checkPost;

    private JSONObject paramsJson;
    public static final Map<String, String> params = new HashMap<String, String>();
    public static final String MyPREFERENCES = "MyPrefs" ;

    String url;

    String TAG = this.getClass().getSimpleName();

    Button backButton,confirm;
    EditText outbound_location;

    Intent intent;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.outbound_picking_location);

        intent = getIntent();
        param = this.getClass().getSimpleName();
        checkPost = 0;

        backButton = findViewById(R.id.back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        myDb = new DatabaseHelper(this);

        getServerURL();

        confirm = findViewById(R.id.confirm);
        confirm.setEnabled(true);

        outbound_location = findViewById(R.id.outbound_location);
        outbound_location.setShowSoftInputOnFocus(false);

        outbound_location.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    if(outbound_location.length() > 0) {
                        postCheckOutboundLoc(outbound_location.getText().toString());
                    }
                }
                return false;
            }
        });

        myDb = new DatabaseHelper(this);

        listView =  findViewById(R.id.ListItem);
        itemList = new ArrayList<>();
        docList = new ArrayList<>();
        pickingDocument = "";
        if(intent.getStringExtra("picking_document") != null) {
            pickingDocument = intent.getStringExtra("picking_document");
            getPicking(url + "/Api_picking/get/" + pickingDocument);
        }
        paramsJson = new JSONObject();

        confirm.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        postConfirmPicking();
                        if(outbound_location.length() > 0) {
                            if(checkPost == 0) {
                                try {
                                    checkPost = 1;
                                    confirm.setEnabled(false);
                                    Thread.sleep(100);
                                    getSNByPL(outbound_location.getText().toString());
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                System.out.println("Post confirm picking wait.");
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Please input outbound location first.", Toast.LENGTH_SHORT).show();
                        }

                    }
                }
        );

        scanButton = findViewById(R.id.scanButton);
        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getApplicationContext(), QRScanner.class);
                intent.putExtra("param", "asnPickingLoc");
                intent.putExtra("fClass", fClass);
                intent.putExtra("docList", (Serializable) docList);
                intent.putExtra("itemList", (Serializable) itemList);
                intent.putExtra("pickingItemGetSet", pickingItemGetSet);
                intent.putExtra("picking_document", pickingDocument);
                startActivity(intent);
            }
        });
        // LocalBroadcastManager.getInstance(this).registerReceiver(qrScan, new IntentFilter("broadcastQrResult"));
        if(intent.getStringExtra("asnPickingLoc") != null) {
            if (checkPost == 0) {
                postCheckOutboundLoc(intent.getStringExtra("asnPickingLoc"));
            }
        }
    }

    private BroadcastReceiver qrScan = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getStringExtra(param) != null) {
                String getParam = intent.getStringExtra(param);
                intent.removeExtra(param);

                if(checkPost == 0) {
                    postCheckOutboundLoc(getParam);
                }
            }
        }
    };

    private void getSNByPL(final String locName) {
        SharedPreferences prefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);

        String usernameKey = prefs.getString("usernameKey", "No name defined");

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, url + "/Api_picking/get_sn_by_pl/" + usernameKey + "/" + pickingDocument, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);
                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    System.out.println("response : " + jsonResponse);
                    String results = jsonResponse.getString("results");
                    JSONArray jsonResult = new JSONArray(results);
                    System.out.println("resultsArr : " + jsonResult);
                    if(jsonResult.length() > 0) {
                        for(int i = 0; i < jsonResult.length(); i++) {
                            JSONObject jsonObj = new JSONObject(jsonResult.get(i).toString());
                            String serial_number = jsonObj.getString("unique_code");
                            String qty = jsonObj.getString("qty");
                            String sku = jsonObj.getString("item_code");

                            int arraySize;
                            SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                            String usernameKey = prefs.getString("usernameKey", "No name defined");
                            Long tsLong = System.currentTimeMillis()/1000;
                            String jsonData = "";

                            try {
                                arraySize = itemList.size();
                            } catch (Exception e) {
                                arraySize = 0;
                            }

                            if (arraySize > 0) {
                                jsonData = "[";
                                for (int j = 0; j < arraySize; j++) {
                                    paramsJson.put("picking_name", pickingDocument);
                                    paramsJson.put("item_code", sku);
                                    paramsJson.put("serial_number", serial_number.toUpperCase());
                                    paramsJson.put("old_loc", "");
                                    paramsJson.put("qty", qty);
                                    paramsJson.put("user_pick", usernameKey);
                                    paramsJson.put("pick_time", new SimpleDateFormat("YYYY-MM-dd HH:mm:ss").format(new Date()));
                                    paramsJson.put("outbound_loc", locName);
                                    paramsJson.put("user_put", usernameKey);
                                    paramsJson.put("put_time", new SimpleDateFormat("YYYY-MM-dd HH:mm:ss").format(new Date()));

                                    jsonData = jsonData + paramsJson.toString() + ",";
                                }

                                jsonData = jsonData.substring(0, jsonData.length()-1);
                                jsonData = jsonData + "]";

                                Log.d("JSON Param", jsonData);
                                Log.d("STRLEN", Integer.toString(jsonData.length()));
                                params.clear();
                                params.put("data", jsonData);

                            }
                        }

//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
                        postConfirmPicking(params);
                        System.out.println("Post Confirm Picking !!");
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
//                            }
//                        });
                    } else {
                        Toast.makeText(getApplicationContext(), "User picking tidak sesuai.", Toast.LENGTH_SHORT).show();
                        checkPost = 0;
                        confirm.setEnabled(true);
                    }
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;


            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void getPicking(String getDoc){
//        final ArrayList<String> items = new ArrayList<String>();

        RequestQueue requestQueue=Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, getDoc, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG + "getPicking", response);
                try{
                    JSONObject jsonObject = new JSONObject(response);
                    if(jsonObject.getInt("status")==200){
                        JSONArray jsonArray1 = jsonObject.getJSONArray("locations");
                        for(int i = 0; i < jsonArray1.length(); i++){
                            JSONObject jsonObject1=jsonArray1.getJSONObject(i);
                            System.out.println(jsonObject1);
                            pickingItemGetSet = new PickingItemGetterSetter(
                                    jsonObject1.getString("kd_barang"),
                                    jsonObject1.getString("picked_qty"),
                                    jsonObject1.getString("has_qty"),
                                    jsonObject1.getString("qty"),
                                    jsonObject1.getString("available_qty"),
                                    jsonObject1.getString("loc_name")
                            );
                            itemList.add(pickingItemGetSet);
                            docList.add(jsonObject1.getString("pl_name"));
                        }

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                adapter = new PickingItemListAdapter(itemList, getApplicationContext());
                                listView.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                            }
                        });

                    }
//                    spinner.setAdapter(new ArrayAdapter<String>(TallyReceivingItemSerial.this, android.R.layout.simple_spinner_dropdown_item, QC));
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;


            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void postCheckOutboundLoc(final String locName) {
        StringRequest strReq = new StringRequest(Request.Method.GET,url+"/Api_picking/get_location/" + locName, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);

                try {
                    JSONObject data = new JSONObject(response);

                    Integer status = data.getInt("status");
                    String message = data.getString("message");
//                    JSONObject jsonObj1 = new JSONObject(results);
//                    String message = jsonObj1.getString("message");

                    if (status == 200) {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        outbound_location.setText(locName);
                    } else {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        outbound_location.setText("");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    String jsonError = new String(networkResponse.data);


                    try {
                        JSONObject data = new JSONObject(jsonError);

                        String message = data.getString("message");

                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        })  {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }

    private void postConfirmPicking(final Map<String, String> parameters) {
        StringRequest strReq = new StringRequest(Request.Method.POST,url+"/Api_picking/post_json/", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG + "postConfirmPickingResponse", response);

                try {
                    JSONObject data = new JSONObject(response);

                    Integer status = data.getInt("status");
                    String results = data.getString("results");
                    JSONObject jsonObject = new JSONObject(results.substring(1, results.length()-1));
                    String message = jsonObject.getString("message");

                    if (status == 200) {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        finish();
                        Intent intent2 = new Intent(getApplicationContext(), AsnPicking.class);
                        intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent2);
                    } else {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        outbound_location.setText("");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    String jsonError = new String(networkResponse.data);

                    try {
                        JSONObject data = new JSONObject(jsonError);

                        String message = data.getString("message");

                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        })  {

            @Override
            protected Map<String, String> getParams(){
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }

    private void showMessage(String title, String Message){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setCancelable(true);

        builder.setTitle(title);

        builder.setMessage(Message);

        builder.show();

    }
}
