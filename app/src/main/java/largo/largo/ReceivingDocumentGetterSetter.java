package largo.largo;

import java.io.Serializable;

public class ReceivingDocumentGetterSetter implements Serializable {
    String kd_receiving, qty, source, qty_item;
    String inbound_code = "", id_inbound = "";

    public String getInbound_code() {
        return inbound_code;
    }

    public void setInbound_code(String inbound_code) {
        this.inbound_code = inbound_code;
    }

    public String getId_inbound() {
        return id_inbound;
    }

    public void setId_inbound(String id_inbound) {
        this.id_inbound = id_inbound;
    }

    public ReceivingDocumentGetterSetter(String inbound_code, String id_inbound) {
        this.inbound_code = inbound_code;
        this.id_inbound = id_inbound;
    }

    public ReceivingDocumentGetterSetter(String kd_receiving, String qty, String source) {
        this.kd_receiving = kd_receiving;
        this.qty = qty;
        this.source = source;
    }

    public ReceivingDocumentGetterSetter(String kd_receiving, String inbound_code, String qty, String source, String qty_item) {
        this.kd_receiving = kd_receiving;
        this.inbound_code = inbound_code;
        this.qty = qty;
        this.source = source;
        this.qty_item = qty_item;
    }


    public String getKdReceiving() {
        return kd_receiving;
    }

    public String getQty() {
        return qty;
    }

    public String getSource() {
        return source;
    }

    public String getQtyItem() {
        return qty_item;
    }
}
