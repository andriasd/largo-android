package largo.largo;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class AsnScanQR_1 extends AppCompatActivity {

    private Button cancelButton, doneButton;
    private ImageButton scanButton;
    private EditText asnQr, asnQty, asnExpDate, asnBatch;
    private TextView totalItems;
    private ListView qrList;
    private List<AsnGetSet> asnArrayList;
    private List<String> tagList, skuList;
    private AsnGetSet asnGetSet;
    private AsnListAdapter adapter;
    private String param, TAG, receiving_document;
    private String fClass = this.getClass().getSimpleName();
    private Intent intent;
    private AsnScanRFID asnScanRFID;

    private final Calendar myCalendar = Calendar.getInstance();
    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;

    private JSONObject paramsJson;
    public static final Map<String, String> params = new HashMap<String, String>();
    public static final String MyPREFERENCES = "MyPrefs" ;

    AsnItem asnItem;
    DatabaseHelper myDb;
    String url;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inbound_tally_receiving_document_by_asn_scan_qr);

        asnScanRFID = new AsnScanRFID();
        paramsJson = new JSONObject();

        TAG = this.getClass().getSimpleName();
        myDb = new DatabaseHelper(this);
        getServerURL();

        intent = getIntent();
        param = this.getClass().getSimpleName();

        tagList = new ArrayList<>();
        skuList = new ArrayList<>();

        if(intent.getSerializableExtra("skuList") != null) {
            skuList = (List<String>) intent.getSerializableExtra("skuList");
        }
        if(intent.getSerializableExtra("tagList") != null) {
            tagList = (List<String>) intent.getSerializableExtra("tagList");
        }
        receiving_document = intent.getStringExtra("receiving_document");

        cancelButton = findViewById(R.id.cancel);
        doneButton = findViewById(R.id.done);
        scanButton = findViewById(R.id.scanButton);

        asnQr = findViewById(R.id.asnQr);
        asnQr.setShowSoftInputOnFocus(false);
        asnQr.requestFocus();
        asnQr.setSelection(0);

        asnQty = findViewById(R.id.asnQty);
        asnQty.setText("1");

        asnBatch = findViewById(R.id.asnBatch);
        asnExpDate = findViewById(R.id.asnExpDate);
        dateFormatter = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        asnExpDate.setFocusable(false);
        asnExpDate.setShowSoftInputOnFocus(false);
        asnExpDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDateDialog();
            }
        });

        qrList = findViewById(R.id.QRList);
        totalItems = findViewById(R.id.qty);
        totalItems.setText("0");

        asnArrayList = new ArrayList<>();
        adapter = new AsnListAdapter(asnArrayList, getApplicationContext(), 3);
        qrList.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        if(intent.getSerializableExtra("asnGetSet") != null) {
            asnGetSet = (AsnGetSet) intent.getSerializableExtra("asnGetSet");
        }

        if(intent.getSerializableExtra("asnArrayList") != null) {
            asnArrayList = (List<AsnGetSet>) intent.getSerializableExtra("asnArrayList");
            System.out.println("asnArrayList : " + asnArrayList.size());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    asnQr.setText("");
                    asnQr.setFocusableInTouchMode(true);
                    asnQr.setFocusable(true);
                    asnQr.requestFocus();
                    asnQty.setText("1");
                    totalItems.setText(Integer.toString(asnArrayList.size()));
                    adapter = new AsnListAdapter(asnArrayList, getApplicationContext(), 3);
                    qrList.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }
            });
        }

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<String> skuExistList = new ArrayList<>();

                if(asnArrayList.size() > 0) {
                    for(int i = 0; i < asnArrayList.size(); i++) {
                        if(!skuExistList.contains(asnArrayList.get(i).getSku())) {
                            skuExistList.add(asnArrayList.get(i).getSku());
                        }
                    }
                    if(skuExistList.size() > 1) {
                        try {
                            AlertDialog.Builder builder = new AlertDialog.Builder(AsnScanQR_1.this);
                            builder.setTitle("Alert");
                            builder.setMessage("Terdapat 2 SKU dalam tally ini, lanjutkan ?");
                            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent2 = new Intent(getApplicationContext(), AsnLicensePlate.class);
                                    intent2.putExtra("asnArrayList", (Serializable) asnArrayList);
                                    intent2.putExtra("asnGetSet", asnGetSet);
                                    intent2.putExtra("skuList", (Serializable) skuList);
                                    intent2.putExtra("tagList", (Serializable) tagList);
                                    intent2.putExtra("receiving_document", receiving_document);
                                    intent2.putExtra("class", fClass);
                                    startActivity(intent2);
                                }
                            });
                            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        } catch (WindowManager.BadTokenException bde) {
                            bde.printStackTrace();
                        } catch (IllegalStateException ise) {
                            ise.printStackTrace();
                        }
                    } else {
                        Intent intent2 = new Intent(getApplicationContext(), AsnLicensePlate.class);
                        intent2.putExtra("asnArrayList", (Serializable) asnArrayList);
                        intent2.putExtra("asnGetSet", asnGetSet);
                        intent2.putExtra("skuList", (Serializable) skuList);
                        intent2.putExtra("tagList", (Serializable) tagList);
                        intent2.putExtra("receiving_document", receiving_document);
                        intent2.putExtra("class", fClass);
                        startActivity(intent2);
                    }
                }
            }
        });

        asnQr.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 0) {
                    if (asnQty.length() != 0 && Float.parseFloat(asnQty.getText().toString()) > 0) {
                        parseQR(asnQr.getText().toString(), asnQty.getText().toString());
                    } else {
                        Toast.makeText(getApplicationContext(), "QTY must be not null and bigger than 0.", Toast.LENGTH_SHORT).show();
                    }
                }
                return false;
            }
        });

        asnQty.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 0) {
                    if (asnQr.length() != 0) {
                        parseQR(asnQr.getText().toString(), asnQty.getText().toString());
                    } else {
                        Toast.makeText(getApplicationContext(), "Please insert serial number first.", Toast.LENGTH_SHORT).show();
                    }

                }
                return false;
            }
        });

        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getApplicationContext(), QRScanner.class);
                intent.putExtra("param", "asnScanQR");
                intent.putExtra("fClass", fClass);
                intent.putExtra("asnArrayList", (Serializable) asnArrayList);
                intent.putExtra("asnGetSet", asnGetSet);
                intent.putExtra("skuList", (Serializable) skuList);
                intent.putExtra("tagList", (Serializable) tagList);
                intent.putExtra("receiving_document", receiving_document);
                intent.putExtra("tallyQty", asnQty.getText().toString());
                intent.putExtra("tallyBatch", asnBatch.getText().toString());
                intent.putExtra("tallyExpDate", asnExpDate.getText().toString());
                startActivity(intent);
            }
        });

        // LocalBroadcastManager.getInstance(this).registerReceiver(qrScan, new IntentFilter("broadcastQrResult"));
        if(intent.getStringExtra("asnScanQR") != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    asnBatch.setText(intent.getStringExtra("tallyBatch"));
                    asnExpDate.setText(intent.getStringExtra("tallyExpDate"));
                    parseQR(intent.getStringExtra("asnScanQR"), intent.getStringExtra("tallyQty"));
                    asnArrayList = (List<AsnGetSet>) intent.getSerializableExtra("asnArrayList");
                    asnGetSet = (AsnGetSet) intent.getSerializableExtra("asnGetSet");
                    skuList = (List<String>) intent.getSerializableExtra("skuList");
                    tagList = (List<String>) intent.getSerializableExtra("tagList");
                    receiving_document = intent.getStringExtra("receiving_document");
                }
            });
        }
    }

    private void parseQR(String asnQrText, String asnQtyText) {
        String qr, manufacture, vendor, type, pType, design, kit, techFeatures, size, pSize, serialNumber, checksum;
        qr = manufacture = vendor = type = pType = design = kit = techFeatures = size = pSize = serialNumber = checksum = "";

        qr = asnQrText.toUpperCase();
        try {
            if(asnQtyText.length() > 0 && Float.parseFloat(asnQtyText) > 0 && asnBatch.getText().toString().length() > 0 && asnExpDate.getText().toString().length() > 0) {
                if(qr.length() == 40 || qr.length() == 42 || qr.length() == 44) { // Hex scanned
                    if(tagList.size() > 0) {
                        if(tagList.contains(qr)) {
                            System.out.println("tagList : " + tagList);
                            Toast.makeText(getApplicationContext(), "This QR is already scanned.", Toast.LENGTH_SHORT).show();
                        } else {
                            tagList.add(qr.toUpperCase());
                            String qrText = asnScanRFID.hexToString(qr);
                            if(skuList.contains(qrText.substring(0, 12).toUpperCase())){
                                asnGetSet = new AsnGetSet(qr, qrText.substring(0, 20), qrText.substring(0, 12).toUpperCase(), "1", asnQtyText, asnBatch.getText().toString(), asnExpDate.getText().toString());
//                        asnGetSet = new AsnGetSet(qr, qrText.substring(0, 12).toUpperCase(), "1", asnQtyText);
                            } else {
                                asnGetSet = new AsnGetSet(qr, qrText.substring(0, 20), qrText.substring(0, 12).toUpperCase(), "0", asnQtyText, asnBatch.getText().toString(), asnExpDate.getText().toString());
//                        asnGetSet = new AsnGetSet(qr, qrText.substring(0, 12).toUpperCase(), "0", asnQtyText);
                            }
                            asnArrayList.add(asnGetSet);
                        }
                    } else {
                        tagList.add(qr.toUpperCase());
                        String qrText = asnScanRFID.hexToString(qr);
                        if(skuList.contains(qrText.substring(0, 12).toUpperCase())){
                            asnGetSet = new AsnGetSet(qr, qrText.substring(0, 20), qrText.substring(0, 12).toUpperCase(), "1", asnQtyText, asnBatch.getText().toString(), asnExpDate.getText().toString());
//                    asnGetSet = new AsnGetSet(qr, qrText.substring(0, 12).toUpperCase(), "1", asnQtyText);
                        } else {
                            asnGetSet = new AsnGetSet(qr, qrText.substring(0, 20), qrText.substring(0, 12).toUpperCase(), "0", asnQtyText, asnBatch.getText().toString(), asnExpDate.getText().toString());
//                    asnGetSet = new AsnGetSet(qr, qrText.substring(0, 12).toUpperCase(), "0", asnQtyText);
                        }
                        asnArrayList.add(asnGetSet);
                    }
                    System.out.println("skuList2 : " + skuList);
                    System.out.println("tagList2 : " + tagList);
                } else {
                    if(qr.contains("OL")) {
                        params.clear();
                        params.put("receiving_code", receiving_document);
                        params.put("outer_label", qr);
                        getOuterLabelData(params);
                    } else {
                        String[] qrArr = splitStringEvery(qr, 20);

                        int qrArrLen = qrArr.length;

                        for(int i = 0; i < qrArr.length; i++) {
                            if(qrArr[i].substring(0,3).equals("SKU")) {
                                qrArr[i] = qrArr[i].substring(3, qrArr[i].length());
                            }

                            if(skuList.contains(qrArr[i].substring(0, 12))){
                                asnGetSet = new AsnGetSet(toHex(qrArr[i].substring(0, 20)).toUpperCase(), qrArr[i].substring(0, 20).toUpperCase(), qrArr[i].substring(0, 12).toUpperCase(), "1", asnQtyText, asnBatch.getText().toString(), asnExpDate.getText().toString());
//                        asnGetSet = new AsnGetSet(toHex(qrArr[i].substring(0, 20)).toUpperCase(), qrArr[i].substring(0, 12).toUpperCase(), "1", asnQtyText);
                            } else {
                                asnGetSet = new AsnGetSet(toHex(qrArr[i].substring(0, 20)).toUpperCase(), qrArr[i].substring(0, 20).toUpperCase(), qrArr[i].substring(0, 12).toUpperCase(), "0", asnQtyText, asnBatch.getText().toString(), asnExpDate.getText().toString());
//                        asnGetSet = new AsnGetSet(toHex(qrArr[i].substring(0, 20)).toUpperCase(), qrArr[i].substring(0, 12).toUpperCase(), "0", asnQtyText);
                            }
                            asnArrayList.add(asnGetSet);
                        }
                    }
                }
            } else {
                // showMessage("Alert", "Qty must be not null and bigger than 0.");
				Toast.makeText(getApplicationContext(), "Qty, Batch dan Exp Date tidak boleh kosong.", Toast.LENGTH_SHORT).show();
            }
        } catch (NumberFormatException nfe) {
            // showMessage("Alert", "Qty must be not null and bigger than 0.");
			Toast.makeText(getApplicationContext(), "Qty, Batch dan Exp Date tidak boleh kosong.", Toast.LENGTH_SHORT).show();
        } catch (StringIndexOutOfBoundsException e) {
            Toast.makeText(getApplicationContext(), "Format barcode tidak sesuai.", Toast.LENGTH_SHORT).show();
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                asnQr.setText("");
                asnQr.setFocusableInTouchMode(true);
                asnQr.setFocusable(true);
                asnQr.requestFocus();
                asnQty.setText("1");
                totalItems.setText(Integer.toString(asnArrayList.size()));
                adapter.notifyDataSetChanged();
            }
        });
    }

    public void getOuterLabelData(final Map<String, String> parameters) {
        String postUrl = this.url + "/Api_receiving/outer_label_item/";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);

                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            String message = jsonResponse.getString("message");
                            JSONArray jsonMessage = new JSONArray(message);

                            asnArrayList.clear();
                            for(int i = 0; i < jsonMessage.length(); i++) {
                                String unique_code = jsonMessage.getString(i);
                                JSONObject jsonHex = new JSONObject(unique_code);
                                String hex = jsonHex.getString("unique_code");
                                String hexToText = asnScanRFID.hexToString(hex).toUpperCase();

                                if(skuList.contains(hexToText.substring(0, 12))){
                                    asnGetSet = new AsnGetSet(hex, hexToText.substring(0, 20), hexToText.substring(0, 12), "1", "1");
//                                    asnGetSet = new AsnGetSet(hex, hexToText.substring(0, 12), "1");
                                } else {
                                    asnGetSet = new AsnGetSet(hex, hexToText.substring(0, 20), hexToText.substring(0, 12), "0", "1");
//                                    asnGetSet = new AsnGetSet(hex, hexToText.substring(0, 12), "0");
                                }
                                asnArrayList.add(asnGetSet);
                            }
                        } catch (JSONException jsonEx) {
                            jsonEx.printStackTrace();
                        }

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                totalItems.setText(Integer.toString(asnArrayList.size()));
                                asnQr.setText("");
                                asnQr.requestFocus();
                                asnQr.setSelection(0);
                                adapter.notifyDataSetChanged();
                            }
                        });
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    public String toHex(String arg) {
        String result = String.format("%x", new BigInteger(1, arg.getBytes(/*YOUR_CHARSET?*/)));
        return result.toUpperCase();
    }

    public String[] splitStringEvery(String s, int interval) {
        int arrayLength = (int) Math.ceil(((s.length() / (double)interval)));
        String[] result = new String[arrayLength];

        int j = 0;
        int lastIndex = result.length - 1;
        for (int i = 0; i < lastIndex; i++) {
            result[i] = s.substring(j, j + interval);
            j += interval;
        } //Add the last bit
        result[lastIndex] = s.substring(j);

        return result;
    }

    private void showDateDialog(){
        Calendar newCalendar = Calendar.getInstance();

        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                view.setMinDate(System.currentTimeMillis() - 1000);

                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                asnExpDate.setText(dateFormatter.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

        datePickerDialog.show();
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }
}