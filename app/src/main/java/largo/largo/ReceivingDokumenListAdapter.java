package largo.largo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class ReceivingDokumenListAdapter extends ArrayAdapter<ReceivingDocumentGetterSetter> {

    private List<ReceivingDocumentGetterSetter> dokumenItemList;

    private Context context;

    public ReceivingDokumenListAdapter(List<ReceivingDocumentGetterSetter> dokumenItemList, Context context) {
        super(context, R.layout.list_view_receiving_dokumen, dokumenItemList);
        this.dokumenItemList = dokumenItemList;
        this.context = context;
    }



    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);

        View listViewItem = inflater.inflate(R.layout.list_view_receiving_dokumen, null, true);

        TextView textViewKdReceiving = listViewItem.findViewById(R.id.textViewDokumen);
        TextView textViewQty = listViewItem.findViewById(R.id.textViewQty);
        TextView textViewSource = listViewItem.findViewById(R.id.textViewSource);


        ReceivingDocumentGetterSetter dokumenItem = dokumenItemList.get(position);

        if(dokumenItem.getInbound_code() != "") {
            textViewKdReceiving.setText(dokumenItem.getInbound_code());
            textViewQty.setVisibility(View.GONE);
            textViewSource.setVisibility(View.GONE);
        } else {
            textViewKdReceiving.setText(dokumenItem.getKdReceiving());
            try {
                if (dokumenItem.getQtyItem().length() > 0) {
                    textViewQty.setText(dokumenItem.getQty() + ", " + dokumenItem.getQtyItem() + " pcs");
                }
            } catch (Exception e) {
                textViewQty.setText(dokumenItem.getQty());
            }

            textViewSource.setText(dokumenItem.getSource());
        }

        return listViewItem;
    }
}

