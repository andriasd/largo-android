package largo.largo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PickingDocumentItem extends AppCompatActivity {
    String TAG = this.getClass().getSimpleName();
    private List<PickingDocumentGetterSetter> selectedDokumenItemList;
    private Button backButton, nextButton;
    private ImageButton scanButton;
    private TextView header_text;
    private EditText receivingDocument;
    private String param, pl_list;
    private String fClass = this.getClass().getSimpleName();
    private String receiving_document, item_area;
    private Intent intent;
    private ListView listView;
    private ReceivingDocumentItemGetterSetter receivingDocumentItemGetterSetter;
    private List<ReceivingDocumentItemGetterSetter> dokumenItemList;
    private List<String> docList;

    DatabaseHelper myDb;
    String url;

    public static final Map<String, String> params = new HashMap<String, String>();
    public static final String MyPREFERENCES = "MyPrefs" ;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inbound_tally_receiving_document_item);

        intent = getIntent();
        param = this.getClass().getSimpleName();

        myDb = new DatabaseHelper(this);
        getServerURL();

        backButton = findViewById(R.id.back);
        nextButton = findViewById(R.id.next);
        listView = findViewById(R.id.ListTallyDoc);
        receivingDocument = findViewById(R.id.receiving_document);

        docList = new ArrayList<>();
        dokumenItemList = new ArrayList<>();
        receiving_document = intent.getStringExtra("picking_document");
        item_area = intent.getStringExtra("item_area");
        selectedDokumenItemList = (List<PickingDocumentGetterSetter>) intent.getSerializableExtra("picking_code");

        DataListDokument(url + "/api/picking/get_document_item/");

        header_text = findViewById(R.id.header_tally);
        String header_string = header_text.getText().toString();
        header_text.setText("Picking" + " - " + item_area);
        header_text.setSelected(true);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(receivingDocument.getText().toString().length() > 0) {
//                    Intent intent = new Intent(getApplicationContext(), AsnDocumentItemDetail.class);
//                    intent.putExtra("receiving_document", receiving_document);
//                    intent.putExtra("inbound_document", inbound_document);
                    Intent intent = new Intent(getApplicationContext(), AsnPickingScanQR.class);
                    intent.putExtra("picking_document", receiving_document);
                    intent.putExtra("dokumenItemList", (Serializable) dokumenItemList);
                    startActivity(intent);
                } else {
                    Toast.makeText(PickingDocumentItem.this, "Please input item code first.", Toast.LENGTH_SHORT).show();
                }
            }
        });


        receivingDocument.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 0) {
                    if(receivingDocument.getText().toString().length() > 0) {
//                    Intent intent = new Intent(getApplicationContext(), AsnDocumentItemDetail.class);
//                    intent.putExtra("receiving_document", receiving_document);
//                    intent.putExtra("inbound_document", inbound_document);
                        Intent intent = new Intent(getApplicationContext(), AsnPickingScanQR.class);
                        intent.putExtra("picking_document", receiving_document);
                        intent.putExtra("item_code", receivingDocument.getText().toString());
                        intent.putExtra("dokumenItemList", (Serializable) dokumenItemList);
                        startActivity(intent);
                    } else {
                        Toast.makeText(PickingDocumentItem.this, "Please input item code first.", Toast.LENGTH_SHORT).show();
                    }
                }
                return false;
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                receivingDocument.setText(dokumenItemList.get(position).getItem_code());
//                    Intent intent = new Intent(getApplicationContext(), AsnDocumentItemDetail.class);
//                    intent.putExtra("receiving_document", receiving_document);
//                    intent.putExtra("inbound_document", inbound_document);
                Intent intent = new Intent(getApplicationContext(), AsnPickingScanQR.class);
                intent.putExtra("picking_document", dokumenItemList.get(position).getReceiving_code());
                intent.putExtra("outbound_code", dokumenItemList.get(position).getOutbound_code());
                intent.putExtra("item_code", dokumenItemList.get(position).getItem_code());
                intent.putExtra("item_area",item_area);
                intent.putExtra("dokumenItemList", (Serializable) dokumenItemList);
                startActivity(intent);
            }
        });

        scanButton = findViewById(R.id.scanButton);
        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getApplicationContext(), QRScanner.class);
                intent.putExtra("param", "item_code");
//                intent.putExtra("receiving_document", receiving_document);
//                intent.putExtra("inbound_document", inbound_document);
                intent.putExtra("picking_document", receiving_document);
                intent.putExtra("fClass", fClass);
                intent.putExtra("docList", (Serializable) docList);
                startActivity(intent);
            }
        });

        if(intent.getStringExtra("item_code") != null) {
            if (docList.contains(intent.getStringExtra("item_code"))) {
//                Intent intent2 = new Intent(getApplicationContext(), AsnDocumentItemDetail.class);
                Intent intent2 = new Intent(getApplicationContext(), AsnPickingScanQR.class);
                intent2.putExtra("item_code", intent.getStringExtra("item_code"));
//                intent2.putExtra("receiving_document", intent.getStringExtra("receiving_document"));
//                intent2.putExtra("inbound_document", intent.getStringExtra("inbound_document"));
                intent2.putExtra("picking_document", intent.getStringExtra("picking_document"));
                intent2.putExtra("outbund_document", intent.getStringExtra("outbund_document"));
                intent2.putExtra("dokumenItemList", (Serializable) dokumenItemList);
                startActivity(intent2);
                intent2.removeExtra("picking_document");
                intent2.removeExtra("outbund_document");
                intent2.removeExtra("item_code");
                intent2.removeExtra("dokumentItemList");
            } else {
                Toast.makeText(getApplicationContext(), "Document not available.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void DataListDokument(String getDoc){
        RequestQueue requestQueue=Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.POST, getDoc, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);
                try{
                    JSONObject jsonObject=new JSONObject(response);
                    if(jsonObject.getInt("status")==200){
                        JSONArray jsonArray1=jsonObject.getJSONArray("results");
                        if(docList.size() > 0) {
                            docList.clear();
                        }
                        for(int i=0;i<jsonArray1.length();i++){
                            JSONObject jsonObject1=jsonArray1.getJSONObject(i);
                            ReceivingDocumentItemGetterSetter item_dokumen = new ReceivingDocumentItemGetterSetter(
                                    jsonObject1.getString("receiving_code"),
                                    jsonObject1.getString("outbound_code"),
                                    jsonObject1.getString("item_code"),
                                    jsonObject1.getString("item_name"),
                                    jsonObject1.getString("qty"),
                                    jsonObject1.getString("nama_satuan"),
                                    jsonObject1.getString("location"),
                                    "",
                                    "",
                                    jsonObject1.getString("received_qty"),
                                    "",
                                    "",
                                    "",
                                    jsonObject1.getString("item_id"),
                                    jsonObject1.getString("item_ori")
                            );
                            dokumenItemList.add(item_dokumen);
                            docList.add(jsonObject1.getString("item_code"));
                        }
                        ReceivingDokumenItemListAdapter adapter = new ReceivingDokumenItemListAdapter(dokumenItemList, getApplicationContext());

                        listView.setAdapter(adapter);

                        Intent intent = new Intent(getApplicationContext(), AsnPickingScanQR.class);
                        intent.putExtra("picking_document", dokumenItemList.get(0).getReceiving_code());
                        intent.putExtra("outbound_code", dokumenItemList.get(0).getOutbound_code());
                        intent.putExtra("item_code", dokumenItemList.get(0).getItem_code());
                        intent.putExtra("item_area",item_area);
                        intent.putExtra("dokumenItemList", (Serializable) dokumenItemList);
                        startActivity(intent);
                    }
                }catch (Exception e){e.printStackTrace();}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getParams(){
                Map<String,String> params = new HashMap<>();
                List<String> itemCheck = new ArrayList<>();
                String itemCodeParam = "";

                try {
                    for(int i = 0; i < selectedDokumenItemList.size(); i++) {
                        if(itemCheck != null) {
                            if(!itemCheck.contains(selectedDokumenItemList.get(i).getPlName())) {
                                itemCodeParam = itemCodeParam + "\"" + selectedDokumenItemList.get(i).getPlName() + "\",";
                                itemCheck.add(selectedDokumenItemList.get(i).getPlName());
                            }
                        } else {
                            itemCodeParam = itemCodeParam + selectedDokumenItemList.get(i).getPlName() + "\",";
                            itemCheck.add(selectedDokumenItemList.get(i).getPlName());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                itemCodeParam = "[" + itemCodeParam.substring(0, itemCodeParam.length()-1) + "]";
                params.put("pickingCode",itemCodeParam);
                params.put("item_area",item_area);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }

    private BroadcastReceiver qrScan = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getStringExtra(param) != null) {
                String getParam = intent.getStringExtra(param);
                intent.removeExtra(param);

                if(docList.contains(getParam)) {
                    Intent intent2 = new Intent(getApplicationContext(), AsnItem.class);
                    intent2.putExtra("receiving_document", getParam);
                    startActivity(intent2);
                } else {
                    Toast.makeText(getApplicationContext(), "Document not available.", Toast.LENGTH_SHORT).show();
                }
            }
        }
    };
}
