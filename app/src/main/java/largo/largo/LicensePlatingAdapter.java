package largo.largo;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class LicensePlatingAdapter extends ArrayAdapter<LicensePlatingGetSet> {
    private List<LicensePlatingGetSet> licensePlatingGetSetList;

    private Context context;

    private int column;

    public LicensePlatingAdapter(List<LicensePlatingGetSet> licensePlatingGetSetList, Context context, int column) {
        super(context, R.layout.adapter_list_horizontal, licensePlatingGetSetList);
        this.licensePlatingGetSetList = licensePlatingGetSetList;
        this.context = context;
        this.column = column;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);

        View listViewItem = inflater.inflate(R.layout.adapter_list_horizontal, null, true);

        TextView snText = listViewItem.findViewById(R.id.textView1);
        TextView hexText = listViewItem.findViewById(R.id.textView2);
        TextView skuText = listViewItem.findViewById(R.id.textView3);
        TextView olText = listViewItem.findViewById(R.id.textView4);

        LicensePlatingGetSet licensePlatingGetSet = licensePlatingGetSetList.get(position);

        snText.setText(licensePlatingGetSet.getSn());
        hexText.setText(licensePlatingGetSet.getHex());
        skuText.setText(licensePlatingGetSet.getSku());
        olText.setText(licensePlatingGetSet.getOl());

        if(column == 2) {
            hexText.setVisibility(View.GONE);
            skuText.setVisibility(View.GONE);
            olText.setVisibility(View.GONE);
        }

        return listViewItem;
    }
}
