package largo.largo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static largo.largo.LoadingScanQR.MyPREFERENCES;

public class AsnPicking extends AppCompatActivity {
    String TAG = this.getClass().getSimpleName();

    private Button backButton, nextButton;
    private EditText pickingDocument;
    private String param, gate_id;
    private String fClass = this.getClass().getSimpleName();
    private Intent intent;
    private ListView listView, selectedListView;
    private List<PickingDocumentGetterSetter> dokumenItemList;
    private List<PickingDocumentGetterSetter> selectedDokumenItemList;
    private PickingDocumentGetterSetter pickDocGetSet;
    private PickingDocumentGetterSetter selectedPickDocGetSet;
    private List<String> docList;
    private TextView area;
    private PickingDocumentListAdapter adapter;
    private PickingDocumentListAdapter selectedAdapter;

    DatabaseHelper myDb;
    String url;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.outbound_picking_document_by_asn);

        intent = getIntent();
        param = this.getClass().getSimpleName();

        gate_id = "";

        myDb = new DatabaseHelper(this);
        getServerURL();

        backButton = findViewById(R.id.back);
        nextButton = findViewById(R.id.next);
        listView = findViewById(R.id.ListPickingDoc);
        selectedListView = findViewById(R.id.ListSelectedPickingDoc);
        pickingDocument = findViewById(R.id.picking_document);
        area = findViewById(R.id.area);

        docList = new ArrayList<>();
        if(intent.getSerializableExtra("docList") != null) {
            docList = (List<String>) intent.getSerializableExtra("docList");
        }
        dokumenItemList = new ArrayList<>();
        selectedDokumenItemList = new ArrayList<>();
        SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        String usernameKey = prefs.getString("usernameKey", "No name defined");
        DataListDokument(url + "/api/picking/get_document/" + usernameKey);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selectedDokumenItemList.size() > 0) {
                    Intent intent = new Intent(getApplicationContext(), PickingDocumentItem.class);
                    intent.putExtra("item_area", area.getText().toString());
                    intent.putExtra("picking_code", (Serializable) selectedDokumenItemList);
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "Please input picking document first.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        pickingDocument.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                boolean check_area = false;
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 0) {
                    boolean check = false;
                    for(int x=0;x<dokumenItemList.size();x++) {
                        if(dokumenItemList.get(x).getPlName().toUpperCase().trim().equals(pickingDocument.getText().toString().toUpperCase().trim())) {
                            try {
                                if(selectedDokumenItemList.size() > 0) {
                                    for(int i=0;i<selectedDokumenItemList.size();i++) {
                                        if(!selectedDokumenItemList.get(i).getItem_area().equals(dokumenItemList.get(x).getItem_area())){
                                            Toast.makeText(getApplicationContext(), "Item Area harus sama", Toast.LENGTH_SHORT).show();
                                            pickingDocument.setText("");
                                            pickingDocument.requestFocus();
                                            return false;
                                        }

                                        if(!selectedDokumenItemList.get(i).getGate_id().equals(dokumenItemList.get(x).getGate_id())){
                                            Toast.makeText(getApplicationContext(), "Gate harus sama!", Toast.LENGTH_SHORT).show();
                                            pickingDocument.setText("");
                                            pickingDocument.requestFocus();
                                            return false;
                                        }

                                        if(selectedDokumenItemList.get(i).getPlName().equals(dokumenItemList.get(x).getPlName())) {
                                            Toast.makeText(getApplicationContext(), "Picking Doc. sudah ada di List!", Toast.LENGTH_SHORT).show();
                                            pickingDocument.setText("");
                                            pickingDocument.requestFocus();
                                            return false;
                                        }
                                    }
                                }

                                selectedPickDocGetSet = new PickingDocumentGetterSetter(
                                        dokumenItemList.get(x).getPlName(),
                                        dokumenItemList.get(x).getQty(),
                                        dokumenItemList.get(x).getSource(),
                                        dokumenItemList.get(x).getItem_area(),
                                        dokumenItemList.get(x).getGate_id()
                                );
                                selectedDokumenItemList.add(selectedPickDocGetSet);
                                selectedAdapter = new PickingDocumentListAdapter(selectedDokumenItemList, getApplicationContext());
                                selectedListView.setAdapter(selectedAdapter);
//                                pickingDocument.setFocusableInTouchMode(true);
//                                pickingDocument.requestFocus();
                                pickingDocument.setText("");
                                pickingDocument.setFocusableInTouchMode(true);
                                pickingDocument.setFocusable(true);
                                pickingDocument.requestFocus();
                                pickingDocument.setSelection(0);
                                area.setText(dokumenItemList.get(x).getItem_area());
                                gate_id = dokumenItemList.get(x).getGate_id();
                                check = true;
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            if(!check) {
                                check = false;
                            }
                        }
                    }

                    if(!check) {
                        Toast.makeText(getApplicationContext(), "PL tersebut tidak ada dalam list.", Toast.LENGTH_SHORT).show();
//                        pickingDocument.setFocusableInTouchMode(true);
//                        pickingDocument.requestFocus();
                        pickingDocument.setText("");
                        pickingDocument.setFocusableInTouchMode(true);
                        pickingDocument.setFocusable(true);
                        pickingDocument.requestFocus();
                        pickingDocument.setSelection(0);
                    }

                    /*Intent intent = new Intent(getApplicationContext(), AsnPickingOBDoc.class);
                    intent.putExtra("picking_document", pickingDocument.getText().toString());
                    startActivity(intent);*/
                }
                return false;
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                boolean check_area = false;
                try {
                    if(!area.getText().equals("-")) {

                        if (dokumenItemList.get(position).getItem_area().equals(area.getText().toString()) && dokumenItemList.get(position).getGate_id().equals(gate_id)) {
                            System.out.println(dokumenItemList.get(position).getItem_area()+"<--");
                            check_area = true;
                        } else {
                            check_area = false;
                        }
                    } else {
                        check_area = true;
                    }

                    if(check_area) {
                        if (selectedDokumenItemList.size() > 0) {
                            for (int i = 0; i < selectedDokumenItemList.size(); i++) {
                                if (selectedDokumenItemList.get(i).getPlName().equals(dokumenItemList.get(position).getPlName())) {
                                    return;
                                }
                            }
                        }

                        selectedPickDocGetSet = new PickingDocumentGetterSetter(
                                dokumenItemList.get(position).getPlName(),
                                dokumenItemList.get(position).getQty(),
                                dokumenItemList.get(position).getSource(),
                                dokumenItemList.get(position).getItem_area(),
                                dokumenItemList.get(position).getGate_id()
                        );
                        selectedDokumenItemList.add(selectedPickDocGetSet);
                        selectedAdapter = new PickingDocumentListAdapter(selectedDokumenItemList, getApplicationContext());
                        selectedListView.setAdapter(selectedAdapter);

                        area.setText(dokumenItemList.get(position).getItem_area());
                        gate_id = dokumenItemList.get(position).getGate_id();
                    } else {
                        Toast.makeText(getApplicationContext(), "Item area dan Gate harus sama.", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                /*pickingDocument.setText(dokumenItemList.get(position).getPlName());
                Intent intent = new Intent(getApplicationContext(), AsnPickingOBDoc.class);
                intent.putExtra("picking_document", dokumenItemList.get(position).getPlName());
                startActivity(intent);*/
            }
        });

        selectedListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedDokumenItemList.remove(position);
                selectedAdapter.notifyDataSetChanged();
                if(selectedDokumenItemList.size() == 0) {
                    area.setText("-");
                    gate_id = "";
                }
            }
        });
    }

    private void DataListDokument(String getDoc){
        RequestQueue requestQueue=Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, getDoc, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);
                try{
                    JSONObject jsonObject=new JSONObject(response);
                    if(jsonObject.getInt("status")==200){
                        JSONArray jsonArray1=jsonObject.getJSONArray("results");
                        for(int i=0;i<jsonArray1.length();i++){
                            JSONObject jsonObject1=jsonArray1.getJSONObject(i);
                            pickDocGetSet = new PickingDocumentGetterSetter(
                                    jsonObject1.getString("pl_name"),
                                    "",
                                    jsonObject1.getString("destination"),
                                    jsonObject1.getString("item_area"),
                                    jsonObject1.getString("gate_id")
                            );
                            dokumenItemList.add(pickDocGetSet);
                            docList.add(jsonObject1.getString("pl_name"));
                        }
                        adapter = new PickingDocumentListAdapter(dokumenItemList, getApplicationContext());

                        listView.setAdapter(adapter);
                        pickingDocument.setFocusableInTouchMode(true);
                        pickingDocument.setFocusable(true);
                        pickingDocument.requestFocus();
                        pickingDocument.setText("");
                        pickingDocument.setSelection(0);
                    }
//                    spinner.setAdapter(new ArrayAdapter<String>(TallyReceivingItemSerial.this, android.R.layout.simple_spinner_dropdown_item, QC));
                }catch (JSONException e){e.printStackTrace();}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;


            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }
}
