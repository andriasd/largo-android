package largo.largo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static largo.largo.LoginActivity.MyPREFERENCES;

public class PutawaySerialNumber extends AppCompatActivity {
    DatabaseHelper myDb;

    private String TAG = PutawaySerialNumber.class.getSimpleName();
    private String fClass = this.getClass().getSimpleName();
    Button next,backButton;
    EditText serial_number;
    TextView sku,qty,totalItems;
    private List<AsnGetSet> asnArrayList = new ArrayList<>();
    private List<String> tagList;
    private AsnGetSet asnGetSet;
    private AsnPutawayListAdapter asnAdapter;
    private AsnScanRFID asnScanRFID;
    private AsnScanQR asnScanQR;
    private Intent intent;
    int checkPost;

    private ImageButton scanButton;
    private String param;

    private JSONObject paramsJson;
    public static final Map<String, String> params = new HashMap<String, String>();

    String url, itemCode, total_qty;

    ListView listView;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inbound_putaway_serial_number);

        checkPost = 0;

        backButton = findViewById(R.id.back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        asnScanQR = new AsnScanQR();
        asnScanRFID = new AsnScanRFID();

        param = this.getClass().getSimpleName();
        myDb = new DatabaseHelper(this);
        getServerURL();
        intent = getIntent();

        next = findViewById(R.id.next);
        serial_number = findViewById(R.id.serial_number);
        serial_number.setShowSoftInputOnFocus(false);
        sku = findViewById(R.id.sku);
        qty = findViewById(R.id.etQty);
        listView =  findViewById(R.id.ListItem);
        totalItems = findViewById(R.id.qty);
        totalItems.setText("0 pcs");

        tagList = new ArrayList<>();

        serial_number.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    if(serial_number.getText().length() > 0) {
                        new checkSerialNumberASync(serial_number.getText().toString().toUpperCase()).execute();
                    }
                }
                return false;
            }
        });

        next.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(asnArrayList.size() > 0) {
                            Calendar calendar = Calendar.getInstance();
                            SimpleDateFormat mdformat = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");

                            intent = new Intent(getApplicationContext(), PutawayLocation.class);
                            intent.putExtra("asnArrayList", (Serializable) asnArrayList);
                            intent.putExtra("pick_time", mdformat.format(calendar.getTime()));
                            startActivity(intent);
                        } else {
                            Toast.makeText(getApplicationContext(), "Please input serial number first.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
        );

        scanButton = findViewById(R.id.scanButton);
        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getApplicationContext(), QRScanner.class);
                intent.putExtra("param", "putawayQR");
                intent.putExtra("fClass", fClass);
                intent.putExtra("asnArrayList", (Serializable) asnArrayList);
                intent.putExtra("tagList", (Serializable) tagList);
                intent.putExtra("asnGetSet", asnGetSet);
                startActivity(intent);
            }
        });
        // LocalBroadcastManager.getInstance(this).registerReceiver(qrScan, new IntentFilter("broadcastQrResult"));
        if(intent.getStringExtra("putawayQR") != null) {
            //checkSerialNumber(intent.getStringExtra("putawayQR"));
            asnArrayList = (List<AsnGetSet>) intent.getSerializableExtra("asnArrayList");
            asnGetSet = (AsnGetSet) intent.getSerializableExtra("asnGetSet");
            tagList = (List<String>) intent.getSerializableExtra("tagList");
            new checkSerialNumberASync(intent.getStringExtra("putawayQR").toUpperCase()).execute();

        }
    }

    private void parseQR(String asnQrText, String itemCodeParam, String lastLocParam) {
        String qr, manufacture, vendor, type, pType, design, kit, techFeatures, size, pSize, serialNumber, checksum;
        qr = manufacture = vendor = type = pType = design = kit = techFeatures = size = pSize = serialNumber = checksum = "";
        String[] skuNotFound = {};

        qr = asnQrText.toUpperCase();
        if(qr.contains("OL")) { // Outer label scanned
            params.clear();
            params.put("outer_label", qr.toUpperCase());
            getOuterLabelData(params);
        } else {
            if(tagList.size() > 0) {
                if(tagList.contains(qr.toUpperCase())) {
                    Toast.makeText(getApplicationContext(), "This QR is already scanned.", Toast.LENGTH_SHORT).show();
                } else {
                    tagList.add(qr.toUpperCase());
//                    asnGetSet = new AsnGetSet(qr, qr, "1", itemCodeParam, lastLocParam, "");
                    asnGetSet = new AsnGetSet(qr, itemCodeParam, "1", itemCodeParam, lastLocParam, "");
                    asnArrayList.add(asnGetSet);

                }
            } else {
                tagList.add(qr.toUpperCase());
//                asnGetSet = new AsnGetSet(qr, qr, "1", itemCodeParam, lastLocParam, "");
                asnGetSet = new AsnGetSet(qr, itemCodeParam, "1", itemCodeParam, lastLocParam, "");
                asnArrayList.add(asnGetSet);
            }
        }

        asnAdapter = new AsnPutawayListAdapter(asnArrayList, getApplicationContext(), 2);
        listView.setAdapter(asnAdapter);
        asnAdapter.notifyDataSetChanged();
        serial_number.setText("");
        serial_number.requestFocus();
        serial_number.setSelection(0);
        totalItems.setText(Integer.toString(asnArrayList.size()));
    }

    public void getOuterLabelData(final Map<String, String> parameters) {
        String postUrl = this.url + "/Api_putaway/getOuterLabelItem";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("ResponsegetOuterLabelData", response);

                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            String message = jsonResponse.getString("message");
                            JSONArray jsonMessage = new JSONArray(message);

//                            asnArrayList.clear();
                            for(int i = 0; i < jsonMessage.length(); i++) {
                                String unique_code = jsonMessage.getString(i);
                                JSONObject jsonHex = new JSONObject(unique_code);
                                String hex = jsonHex.getString("unique_code");
                                String item_code = jsonHex.getString("kd_barang");
                                if(tagList.size() > 0) {
                                    if(tagList.contains(hex)) {
                                        // Toast.makeText(getApplicationContext(), hex + " is already scanned.", Toast.LENGTH_SHORT).show();
                                        asnGetSet = new AsnGetSet(hex, parameters.get("outer_label"), "1", item_code);
                                        for(int j = 0; j < asnArrayList.size(); j++) {
                                            System.out.println("asnArrayList" + j + ":" + asnArrayList.get(j).getUpc() + " => " + hex);
                                            if(asnArrayList.get(j).getUpc().equals(hex)) {
                                                asnArrayList.set(j, asnGetSet);
                                            }
                                        }
                                    } else {
                                        tagList.add(hex);
                                        asnGetSet = new AsnGetSet(hex, parameters.get("outer_label"), "1", item_code);
                                        asnArrayList.add(asnGetSet);
                                    }
                                } else {
                                    tagList.add(hex.toUpperCase());
                                    asnGetSet = new AsnGetSet(hex, parameters.get("outer_label"), "1", item_code);
                                    asnArrayList.add(asnGetSet);
                                }
                            }
                        } catch (JSONException jsonEx) {
                            jsonEx.printStackTrace();
                        }

                        asnAdapter = new AsnPutawayListAdapter(asnArrayList, getApplicationContext(), 2);
                        listView.setAdapter(asnAdapter);
                        serial_number.setText("");
                        serial_number.requestFocus();
                        serial_number.setSelection(0);
                        totalItems.setText(Integer.toString(asnArrayList.size()));
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    private class checkSerialNumberASync extends AsyncTask<Void, Void, Void> {
        String qr = "";

        checkSerialNumberASync(String qr) {
            super();
            this.qr = qr;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            checkSerialNumber(qr);
            return null;
        }
    }

    private void checkSerialNumber(final String qr) {
        StringRequest strReq = new StringRequest(Request.Method.POST,url+"/Api_putaway/get/", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG + "checkSerialNumber", response);

                try {
                    JSONObject data = new JSONObject(response);

                    Integer status = data.getInt("status");
                    String message = data.getString("message");

                    if (message.contains("not available")) {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    } else if (status.equals(200)) {
                        JSONArray resultsArr = new JSONArray(data.getString("results"));
                        JSONObject results = new JSONObject(resultsArr.getString(0));
                        String item_code = results.getString("kd_barang");
                        JSONArray lastLocArray = new JSONArray(data.getString("last_location"));
                        JSONObject lastLocJObj = new JSONObject(lastLocArray.getString(0));
                        String last_location = lastLocJObj.getString("location_name");

                        if(message.contains("has been stored") || message.contains("has been putaway")) {
                            // showMessage("Alert",message);
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        } else {
                            if(checkPost == 0) {
                                //postPutawaySerialNumber(qr.toUpperCase());
                                new postPutawaySerialNumberASync(qr.toUpperCase(), item_code, last_location).execute();
                                checkPost = 1;
                                //Thread.sleep(100);
                            }
                        }
                    } else {
                        // showMessage("Alert",message);
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                serial_number.requestFocus();
                serial_number.setText("");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    String jsonError = new String(networkResponse.data);

                    try {
                        JSONObject data = new JSONObject(jsonError);

                        String message = data.getString("message");

                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        })  {

            @Override
            protected Map<String, String> getParams(){
                Map<String, String> params = new HashMap<String, String>();
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String usernameKey = prefs.getString("usernameKey", "No name defined");

                params.put("serial_number",String.valueOf(qr).toUpperCase());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }

    private class postPutawaySerialNumberASync extends AsyncTask<Void, Void, Void> {
        String qrParam = "";
        String itemCodeParam = "";
        String lastLocParam = "";

        postPutawaySerialNumberASync(String qrParam, String itemCodeParam, String lastLocParam) {
            super();
            System.out.println(qrParam);
            System.out.println(itemCodeParam);
            System.out.println(lastLocParam);
            this.qrParam = qrParam;
            this.itemCodeParam = itemCodeParam;
            this.lastLocParam = lastLocParam;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            postPutawaySerialNumber(qrParam, itemCodeParam, lastLocParam);
            return null;
        }
    }

    private void postPutawaySerialNumber(final String qrParam, final String itemCodeParam, final String lastLocParam) {
        StringRequest strReq = new StringRequest(Request.Method.POST,url+"/api/receiving/post_putaway_serial", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG + "postPutawaySerialNumber", response);
                checkPost = 0;

                try {
                    parseQR(qrParam, itemCodeParam, lastLocParam);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null && networkResponse.data != null) {
                    String jsonError = new String(networkResponse.data);

                    try {
                        JSONObject data = new JSONObject(jsonError);

                        String message = data.getString("message");

                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        })  {

            @Override
            protected Map<String, String> getParams(){
                Map<String, String> params = new HashMap<String, String>();
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String usernameKey = prefs.getString("usernameKey", "No name defined");

                params.put("serial_number", qrParam.toUpperCase());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }
}