package largo.largo;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AsnScanQR extends AppCompatActivity {
    private Button backButton, nextButton;
    private Intent intent;
    private String param, asnBatch = "", status = "", asnExpDate = "", asnQty = "";
    private String receiving_document, inbound_document, item_code, serial_number;
    private TextView item_detail, totalItems, header_tally;
    private List<ReceivingDocumentItemGetterSetter> dokumenItemList;
    private EditText asnQr;
    private ImageButton scanButton;
    private ListView qrList;
    private List<String> tagList;
    private List<AsnGetSet> asnArrayList;
    private AsnGetSet asnGetSet;
    private AsnListAdapter adapter;
    private JSONObject paramsJson;
    public static final Map<String, String> params = new HashMap<String, String>();
    private SharedPreferences prefs;
    private final String MyPREFERENCES = "MyPrefs";
    private final String TAG = this.getClass().getSimpleName();

    private String usernameKey;

    DatabaseHelper myDb;
    String url;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inbound_tally_receiving_document_by_asn_scan_qr);

        prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        usernameKey = prefs.getString("usernameKey", "No name defined");

        intent = getIntent();
        param = this.getClass().getSimpleName();
        paramsJson = new JSONObject();

        myDb = new DatabaseHelper(this);
        getServerURL();

        backButton = findViewById(R.id.cancel);
        nextButton = findViewById(R.id.done);
        item_detail = findViewById(R.id.detail_text);
        asnQr = findViewById(R.id.asnQr);
        asnQr.setShowSoftInputOnFocus(false);
        scanButton = findViewById(R.id.scanButton);
        header_tally = findViewById(R.id.header_tally);

        qrList = findViewById(R.id.QRList);
        totalItems = findViewById(R.id.qty);
        totalItems.setText("0");

        tagList = new ArrayList<>();
        asnArrayList = new ArrayList<>();
        adapter = new AsnListAdapter(asnArrayList, getApplicationContext(), 2);
        qrList.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(backButton.getText().toString().equals("BACK")) { finish(); }
                else {
                    intent = new Intent(getApplicationContext(), AsnDocumentItem.class);
                    intent.putExtra("receiving_document", receiving_document);
                    intent.putExtra("inbound_document", inbound_document);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    intent.removeExtra("receiving_document");
                    intent.removeExtra("inbound_document");
                }
            }
        });

        if(intent.getStringExtra("receiving_document") != null) { receiving_document = intent.getStringExtra("receiving_document"); }
        if(intent.getStringExtra("inbound_document") != null) { inbound_document = intent.getStringExtra("inbound_document"); }
        if(intent.getStringExtra("item_code") != null) { item_code = intent.getStringExtra("item_code"); }
        if(intent.getStringExtra("asnBatch") != null) { asnBatch = intent.getStringExtra("asnBatch"); }
        if(intent.getStringExtra("status") != null) { status = intent.getStringExtra("status"); }
        if(intent.getStringExtra("asnExpDate") != null) { asnExpDate = intent.getStringExtra("asnExpDate"); }
        if(intent.getStringExtra("asnQty") != null) { asnQty = intent.getStringExtra("asnQty"); }
        if(intent.getSerializableExtra("dokumenItemList") != null) { dokumenItemList = (List<ReceivingDocumentItemGetterSetter>) intent.getSerializableExtra("dokumenItemList"); }
        if(intent.getSerializableExtra("asnArrayList") != null) {
            asnArrayList = (List<AsnGetSet>) intent.getSerializableExtra("asnArrayList");
            System.out.println("asnArrayList : " + asnArrayList.size());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    asnQr.setText("");
                    asnQr.setFocusableInTouchMode(true);
                    asnQr.setFocusable(true);
                    asnQr.requestFocus();
                    totalItems.setText(Integer.toString(asnArrayList.size()));
                    adapter = new AsnListAdapter(asnArrayList, getApplicationContext(), 2);
                    qrList.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }
            });
        }
        if(intent.getSerializableExtra("tagList") != null) { tagList = (List<String>) intent.getSerializableExtra("tagList"); }

        for(int i = 0; i < dokumenItemList.size(); i++) {
            if(dokumenItemList.get(i).getItem_code().equals(item_code)) {
                String detText = "<b>" + dokumenItemList.get(i).getItem_code() + " - " + dokumenItemList.get(i).getItem_name() + "</b>";
                detText = Html.fromHtml(detText) + "\n";
                if(asnBatch.length() > 0) {
                    detText = detText + "Batch : " + asnBatch;
                    detText = detText + "\n";
                }
                if(status.length() > 0) {
                    detText = detText + "Status : " + status;
                    detText = detText + "\n";
                }
                if(asnExpDate.length() > 0) {
                    detText = detText + "Exp Date : " + asnExpDate;
                    detText = detText + "\n";
                }
                detText = detText + "Qty : " + asnQty + " " + dokumenItemList.get(i).getNama_satuan();
                item_detail.setText(detText);
            }
        }

        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getApplicationContext(), QRScanner.class);
                intent.putExtra("fClass", param);
                intent.putExtra("param", "asnScanQR");
                intent.putExtra("asnArrayList", (Serializable) asnArrayList);
                intent.putExtra("asnGetSet", asnGetSet);
                intent.putExtra("dokumenItemList", (Serializable) dokumenItemList);
                intent.putExtra("tagList", (Serializable) tagList);
                intent.putExtra("receiving_document", receiving_document);
                intent.putExtra("inbound_document", inbound_document);
                intent.putExtra("item_code", item_code);
                intent.putExtra("asnQty", asnQty);
                intent.putExtra("asnBatch", asnBatch);
                intent.putExtra("status", status);
                intent.putExtra("asnExpDate", asnExpDate);
                startActivity(intent);
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(asnArrayList.size() > 0) {
                    try {
                        AlertDialog.Builder builder = new AlertDialog.Builder(AsnScanQR.this);
                        builder.setTitle("Alert");
                        builder.setMessage("Apakah Anda yakin ingin menggunakan Outer Label?");
                        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                intent = new Intent(getApplicationContext(), AsnLicensePlate.class);
                                intent.putExtra("asnArrayList", (Serializable) asnArrayList);
                                intent.putExtra("dokumenItemList", (Serializable) dokumenItemList);
                                intent.putExtra("receiving_document", receiving_document);
                                intent.putExtra("inbound_document", inbound_document);
                                intent.putExtra("item_code", item_code);
                                intent.putExtra("asnQty", asnQty);
                                intent.putExtra("asnBatch", asnBatch);
                                intent.putExtra("status", status);
                                intent.putExtra("asnExpDate", asnExpDate);
                                startActivity(intent);
                            }
                        });
                        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    } catch (WindowManager.BadTokenException bde) {
                        bde.printStackTrace();
                    } catch (IllegalStateException ise) {
                        ise.printStackTrace();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Harap lakukan receiving item terlebih dahulu.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        String header_string = header_tally.getText().toString();
        header_tally.setText(header_string + " - " + receiving_document + ", " + inbound_document);
        header_tally.setSelected(true);

        if(intent.getStringExtra("asnScanQR") != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    asnQr.setText(intent.getStringExtra("asnScanQR"));
                    //if(serial_number.toUpperCase().substring(0,2).equals("IL")) {
                        try {
                            paramsJson.put("receiving_code", receiving_document);
                            paramsJson.put("inbound_code", inbound_document);
                            paramsJson.put("item_code", item_code);
                            paramsJson.put("serial_number", intent.getStringExtra("asnScanQR"));
                            if(asnExpDate.length() > 0) {
                                paramsJson.put("tgl_exp", new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("dd MMM yyyy").parse(asnExpDate)));
                            } else {
                                paramsJson.put("tgl_exp", "0000-00-00");
                            }
                            paramsJson.put("qty", asnQty);
                            paramsJson.put("batch_code", asnBatch);
                            paramsJson.put("status", status);
                            paramsJson.put("qc_code", "GOOD");
                            paramsJson.put("tgl_in", new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
                            paramsJson.put("uname", usernameKey);
                            paramsJson.put("parent_code", "");
                        } catch(Exception e) {
                            e.printStackTrace();
                        }

                        String jsonData = "[" + paramsJson.toString() + "]";
                        params.put("data", jsonData);
                        System.out.println("postReceivingParams : " + params);
                        postReceiving(params);
                    /*} else {
                        Toast.makeText(getApplicationContext(), "Serial Number tidak sesuai dengan format.", Toast.LENGTH_SHORT).show();
                    }*/
                }
            });
        }

        asnQr.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 0) {
                    serial_number = asnQr.getText().toString();
                    try {
                        //if(serial_number.toUpperCase().substring(0,2).equals("IL")) {
                            new postReceivingASync(serial_number.toUpperCase()).execute();
                        /*} else {
                            Toast.makeText(getApplicationContext(), "Serial Number tidak sesuai dengan format.", Toast.LENGTH_SHORT).show();
                        }*/
                        asnQr.setFocusableInTouchMode(true);
                        asnQr.setFocusable(true);
                        asnQr.requestFocus();
                        asnQr.setText("");
                    } catch(Exception e) {
                        System.out.println("Serial Number : " + serial_number);
                        e.printStackTrace();
                    }
                }
                return false;
            }
        });
    }

    private class postReceivingASync extends AsyncTask<Void, Void, Void> {
        String qr = "";

        postReceivingASync(String qr) {
            super();
            this.qr = qr;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                paramsJson.put("receiving_code", receiving_document);
                paramsJson.put("inbound_code", inbound_document);
                paramsJson.put("item_code", item_code);
                paramsJson.put("serial_number", qr);
                if(asnExpDate.length() > 0) {
                    paramsJson.put("tgl_exp", new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("dd MMM yyyy").parse(asnExpDate)));
                } else {
                    paramsJson.put("tgl_exp", "0000-00-00");
                }
                paramsJson.put("qty", asnQty);
                paramsJson.put("batch_code", asnBatch);
                paramsJson.put("status", status);
                paramsJson.put("qc_code", "GOOD");
                paramsJson.put("tgl_in", new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
                paramsJson.put("uname", usernameKey);
                paramsJson.put("parent_code", "");
            } catch(Exception e) {
                e.printStackTrace();
            }

            String jsonData = "[" + paramsJson.toString() + "]";
            params.put("data", jsonData);
            System.out.println("postReceivingParams : " + paramsJson.toString());
            postReceiving(params);
            return null;
        }
    }

    private void postReceiving(final Map<String, String> parameters) {
        String postUrl = this.url + "/Api_receiving/post_json";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);

                        try {
                            JSONObject data = new JSONObject(response);
                            String results = data.getString("results");
                            JSONArray resultsArray = new JSONArray(results);
                            JSONObject resultsObject = new JSONObject(resultsArray.get(0).toString());
                            int status = resultsObject.getInt("status");
                            String message = resultsObject.getString("message");

                            System.out.println("message : " + message);
                            if(status == 200) {
                                if(message.contains("already exist")) {
                                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                    // showToast(getApplicationContext(), message);
                                } else {
                                    if(tagList.size() == 0) {
                                        if(message.contains("returned")) {
                                            Toast.makeText(getApplicationContext(), "Serial Number ini sudah diterima pada receiving yang sama.", Toast.LENGTH_SHORT).show();
                                            // showToast(getApplicationContext(), "Serial Number ini sudah diterima pada receiving yang sama.");
                                        } else {
                                            tagList.add(serial_number);
                                            asnGetSet = new AsnGetSet(serial_number, serial_number, item_code, "1", asnQty, asnBatch, asnExpDate);
                                            asnArrayList.add(asnGetSet);
                                            backButton.setText("DONE");
                                            Toast.makeText(getApplicationContext(), "Serial Number berhasil dimasukkan.", Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        if (!tagList.contains(serial_number)) {
                                            if(message.contains("returned")) {
                                                Toast.makeText(getApplicationContext(), "Serial Number ini sudah diterima pada receiving yang sama.", Toast.LENGTH_SHORT).show();
                                                // showToast(getApplicationContext(), "Serial Number ini sudah diterima pada receiving yang sama.");
                                            } else {
                                                tagList.add(serial_number);
                                                asnGetSet = new AsnGetSet(serial_number, serial_number, item_code, "1", asnQty, asnBatch, asnExpDate);
                                                asnArrayList.add(asnGetSet);
                                                backButton.setText("DONE");
                                                Toast.makeText(getApplicationContext(), "Serial Number berhasil dimasukkan.", Toast.LENGTH_SHORT).show();
                                            }
                                        } else {
                                            Toast.makeText(getApplicationContext(), "Serial Number ini sudah di scan.", Toast.LENGTH_SHORT).show();
                                            // showToast(getApplicationContext(), "Serial Number ini sudah di scan.");
                                        }
                                    }
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                // showToast(getApplicationContext(), message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                asnQr.setText("");
                                asnQr.setFocusableInTouchMode(true);
                                asnQr.setFocusable(true);
                                asnQr.requestFocus();
                                totalItems.setText(Integer.toString(asnArrayList.size()));
                                adapter.notifyDataSetChanged();
                            }
                        });
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;

            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    private void showToast(final Context context, final String text) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_layout, (ViewGroup) findViewById(R.id.toast_layout_container));
        TextView textToast = layout.findViewById(R.id.toast_layout_text);
        textToast.setText(text);

        Toast toast = new Toast(context);
        toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }
}