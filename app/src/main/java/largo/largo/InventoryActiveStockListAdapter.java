package largo.largo;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class InventoryActiveStockListAdapter extends ArrayAdapter<InventoryActiveStockGetSet> {
    private List<InventoryActiveStockGetSet> inventoryActiveStockGetSetList;

    private Context context;

    private int column;

    public InventoryActiveStockListAdapter(List<InventoryActiveStockGetSet> inventoryActiveStockGetSetList, Context context, int column) {
        super(context, R.layout.adapter_list_vertical, inventoryActiveStockGetSetList);
        this.inventoryActiveStockGetSetList = inventoryActiveStockGetSetList;
        this.context = context;
        this.column = column;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);

        View listViewItem = inflater.inflate(R.layout.adapter_list_vertical, null, true);

        TextView loc_name_old = listViewItem.findViewById(R.id.textView7);
        TextView user_name_old = listViewItem.findViewById(R.id.textView2);
        TextView pick_time = listViewItem.findViewById(R.id.textView3);
        TextView loc_name_new = listViewItem.findViewById(R.id.textView4);
        TextView user_name_new = listViewItem.findViewById(R.id.textView5);
        TextView put_time = listViewItem.findViewById(R.id.textView6);
        TextView process_name = listViewItem.findViewById(R.id.textView1);

        InventoryActiveStockGetSet inventoryActiveStockGetSet = inventoryActiveStockGetSetList.get(position);

        loc_name_old.setText(inventoryActiveStockGetSet.getLoc_name_old());
        user_name_old.setText(inventoryActiveStockGetSet.getUser_name_old());
        pick_time.setText(inventoryActiveStockGetSet.getPick_time());
        pick_time.setGravity(Gravity.LEFT);
        loc_name_new.setText(inventoryActiveStockGetSet.getLoc_name_new());
        user_name_new.setText(inventoryActiveStockGetSet.getUser_name_new());
        put_time.setText(inventoryActiveStockGetSet.getPut_time());
        put_time.setGravity(Gravity.LEFT);
        process_name.setText(inventoryActiveStockGetSet.getProcess_name());

        if(column == 4) {
            loc_name_old.setVisibility(View.VISIBLE);
            user_name_old.setVisibility(View.VISIBLE);
            pick_time.setVisibility(View.VISIBLE);
            loc_name_new.setVisibility(View.GONE);
            user_name_new.setVisibility(View.GONE);
            put_time.setVisibility(View.GONE);
            process_name.setVisibility(View.VISIBLE);
        }

        return listViewItem;
    }
}
