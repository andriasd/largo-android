package largo.largo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class LicensePlatingOption extends AppCompatActivity {
    private Button cancelButton, qrButton, rfidButton;
    private Intent intent;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.license_plating_option);

        cancelButton = findViewById(R.id.cancel);
        qrButton = findViewById(R.id.qr_button);
        rfidButton = findViewById(R.id.rfid_button);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        qrButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getApplicationContext(), LicensePlating.class);
                startActivity(intent);
            }
        });

        rfidButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getApplicationContext(), LicensePlatingRFID.class);
                startActivity(intent);
            }
        });
    }
}
