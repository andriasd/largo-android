package largo.largo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InventoryCycleCountScanQR extends AppCompatActivity {

    private Button cancelButton, doneButton;
    private ImageButton scanButton, scanOLButton;
    private EditText asnQr, asnQty, asnOL;
    private TextView totalItems;
    private ListView qrList;
    private List<AsnGetSet> asnArrayList;
    private List<String> tagList;
    private AsnGetSet asnGetSet;
    private AsnListAdapter adapter;
    private String param, TAG, cc_document, warehouse_name, cc_type, type;
    private String fClass = this.getClass().getSimpleName();
    private Intent intent;
    private AsnScanRFID asnScanRFID;

    String type1 = "";
    String kd_qc = "";
    String tgl_exp = "";
    String tgl_in = "";
    String putaway_time = "";
    String location = "";

    private JSONObject paramsJson;
    public static final Map<String, String> params = new HashMap<String, String>();
    public static final String MyPREFERENCES = "MyPrefs" ;

    AsnItem asnItem;
    DatabaseHelper myDb;
    String url;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cycle_count_scan_qr);

        asnScanRFID = new AsnScanRFID();
        paramsJson = new JSONObject();

        TAG = this.getClass().getSimpleName();
        myDb = new DatabaseHelper(this);
        getServerURL();

        intent = getIntent();
        param = this.getClass().getSimpleName();

        tagList = new ArrayList<>();
        if(intent.getSerializableExtra("tagList") != null) {
            tagList = (List<String>) intent.getSerializableExtra("tagList");
        }
        cc_document = intent.getStringExtra("cc_document");
        warehouse_name = intent.getStringExtra("warehouse_name");
        cc_type = intent.getStringExtra("cc_type");
        type = intent.getStringExtra("type");
        location = intent.getStringExtra("location");
        System.out.println("this is the location= "+location);

        cancelButton = findViewById(R.id.cancel);
        doneButton = findViewById(R.id.done);
        scanButton = findViewById(R.id.scanButton);
        scanOLButton = findViewById(R.id.scanOLButton);

        asnQr = findViewById(R.id.asnQr);
        asnOL = findViewById(R.id.asnOL);
        asnQr.setShowSoftInputOnFocus(false);
        asnOL.requestFocus();
        asnOL.setSelection(0);

        asnQty = findViewById(R.id.asnQty);
        asnQty.setText("");

        qrList = findViewById(R.id.QRList);
        totalItems = findViewById(R.id.qty);
        totalItems.setText("0");

        asnArrayList = new ArrayList<>();
        adapter = new AsnListAdapter(asnArrayList, getApplicationContext(), 2);
        qrList.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        if(intent.getSerializableExtra("asnGetSet") != null) {
            asnGetSet = (AsnGetSet) intent.getSerializableExtra("asnGetSet");
        }

        if(intent.getSerializableExtra("asnArrayList") != null) {
            asnArrayList = (List<AsnGetSet>) intent.getSerializableExtra("asnArrayList");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    asnQr.setText("");
                    asnQr.setFocusableInTouchMode(true);
                    asnQr.setFocusable(true);
                    asnQr.requestFocus();
                    asnQty.setText("1");
                    totalItems.setText(Integer.toString(asnArrayList.size()));
                    adapter = new AsnListAdapter(asnArrayList, getApplicationContext(), 2);
                    qrList.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }
            });
        }

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (asnArrayList.size() > 0) {
                        int arraySize;
                        SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                        String usernameKey = prefs.getString("usernameKey", "No name defined");
                        Long tsLong = System.currentTimeMillis() / 1000;
                        String jsonData = "";

                        try {
                            arraySize = asnArrayList.size();
                        } catch (Exception e) {
                            arraySize = 0;
                        }

                        if (arraySize > 0) {
                            jsonData = "[";
                            paramsJson.put("serial_number", "Serial Number");
                            paramsJson.put("sku", "SKU");
                            paramsJson.put("warehouse_name", "Warehouse");

                            jsonData = jsonData + paramsJson.toString() + ",";
                            for (int i = 0; i < arraySize; i++) {
                                if (!asnArrayList.get(i).getPcs().equals("0")) {
                                    paramsJson.put("serial_number", asnArrayList.get(i).getUpc().toUpperCase());
                                    paramsJson.put("sku", asnArrayList.get(i).getSku());
                                    paramsJson.put("warehouse_name", warehouse_name);

                                    jsonData = jsonData + paramsJson.toString() + ",";
                                }
                            }

                            jsonData = jsonData.substring(0, jsonData.length() - 1);
                            jsonData = jsonData + "]";

                            Log.d("JSON Param", jsonData);
                            Log.d("STRLEN", Integer.toString(jsonData.length()));
                            params.clear();
                            params.put("json", jsonData);
                            params.put("cc_document", cc_document);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
//                                    postCC(params);
                                }
                            });
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(getApplicationContext(), InventoryCycleCountType.class);
                intent.putExtra("cc_document", cc_document);
                intent.putExtra("warehouse_name", warehouse_name);
                intent.putExtra("cc_type", cc_type);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }

        });

        asnQr.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 0) {
//                    if (asnQty.length() != 0 && Float.parseFloat(asnQty.getText().toString()) > 0) {
//                        if(asnQr.getText().toString().contains("OL")) {
//                            params.put("outer_label", asnQr.getText().toString().toUpperCase());
//                            params.put("cc_code", cc_document);
//                            getOuterLabelData(params);
//                        } else {
//                            // getSerial(cc_document, cc_type, type, asnQr.getText().toString(), asnOL.getText().toString());
//                            String jsonParam = "{\"cc_code\":\"" + cc_document + "\",\"location\":\"" + type + "\",\"outer_label\":\"" + asnOL.getText().toString() + "\",\"qty\":\"" + asnQty.getText().toString() + "\",\"serial_number\":[\"" + asnQr.getText().toString() + "\"]}";
//                            new postBulkASync(jsonParam).execute();
//                        }
//                        //parseQR(asnQr.getText().toString(), asnQty.getText().toString());
//                    } else {
//                        Toast.makeText(getApplicationContext(), "QTY must be not null and bigger than 0.", Toast.LENGTH_SHORT).show();
//                    }
//                }
                return false;
            }
        });

        asnQty.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 0) {
                    if (asnQr.getText().toString().length() != 0) {
                        if(asnQr.getText().toString().contains("OL")) {
                            params.put("outer_label", asnQr.getText().toString().toUpperCase());
                            params.put("cc_code", cc_document);
                            getOuterLabelData(params);
                        } else {
                            System.out.println("sudah masuk sini guys");
                            // getSerial(cc_document, cc_type, type, asnQr.getText().toString(), asnOL.getText().toString());
//                            String jsonParam = "{\"cc_code\":\"" + cc_document + "\",\"location\":\"" + type + "\",\"outer_label\":\"" + asnOL.getText().toString() + "\",\"serial_number\":[\"" + asnQr.getText().toString() + "\"]}";
                            String jsonParam = "{\"cc_code\":\"" + cc_document + "\",\"location\":\"" + location + "\",\"outer_label\":\"" + asnOL.getText().toString() + "\",\"qty\":\"" + asnQty.getText().toString() + "\",\"serial_number\":[\"" + asnQr.getText().toString() + "\"]}";
                            new postBulkASync(jsonParam).execute();
                            //                            new postBulkASync(jsonParam);
                        }
                        //parseQR(asnQr.getText().toString(), asnQty.getText().toString());
                    } else {
                        Toast.makeText(getApplicationContext(), "Please insert serial number first.", Toast.LENGTH_SHORT).show();
                    }

                }
                return false;
            }
        });

        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getApplicationContext(), QRScanner.class);
                intent.putExtra("param", "ccScanQR");
                intent.putExtra("fClass", fClass);
                intent.putExtra("asnArrayList", (Serializable) asnArrayList);
                intent.putExtra("asnGetSet", asnGetSet);
                intent.putExtra("tagList", (Serializable) tagList);
                intent.putExtra("cc_document", cc_document);
                intent.putExtra("warehouse_name", warehouse_name);
                intent.putExtra("type", type);
                intent.putExtra("cc_type", cc_type);
                intent.putExtra("tallyQty", asnQty.getText().toString());
                intent.putExtra("asnOL", asnOL.getText().toString());
                startActivity(intent);
            }
        });

        scanOLButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getApplicationContext(), QRScanner.class);
                intent.putExtra("param", "ccScanOL");
                intent.putExtra("fClass", fClass);
                intent.putExtra("asnArrayList", (Serializable) asnArrayList);
                intent.putExtra("asnGetSet", asnGetSet);
                intent.putExtra("tagList", (Serializable) tagList);
                intent.putExtra("cc_document", cc_document);
                intent.putExtra("warehouse_name", warehouse_name);
                intent.putExtra("type", type);
                intent.putExtra("cc_type", cc_type);
                intent.putExtra("tallyQty", asnQty.getText().toString());
                intent.putExtra("asnOL", asnOL.getText().toString());
                startActivity(intent);
            }
        });

        // LocalBroadcastManager.getInstance(this).registerReceiver(qrScan, new IntentFilter("broadcastQrResult"));
        if(intent.getStringExtra("ccScanQR") != null) {
            asnArrayList = (List<AsnGetSet>) intent.getSerializableExtra("asnArrayList");
            asnGetSet = (AsnGetSet) intent.getSerializableExtra("asnGetSet");
            tagList = (List<String>) intent.getSerializableExtra("tagList");
            cc_document = intent.getStringExtra("cc_document");
            warehouse_name = intent.getStringExtra("warehouse_name");
            type = intent.getStringExtra("type");
            cc_type = intent.getStringExtra("cc_type");
            asnOL.setText(intent.getStringExtra("asnOL"));
            adapter.notifyDataSetChanged();
            if(intent.getStringExtra("ccScanQR").contains("OL")) {
                params.put("outer_label", intent.getStringExtra("ccScanQR"));
                params.put("cc_code", cc_document);
                getOuterLabelData(params);
            } else {
                // getSerial(cc_document, cc_type, type, intent.getStringExtra("ccScanQR"), asnOL.getText().toString());
                String jsonParam = "{\"cc_code\":\"" + cc_document + "\",\"location\":\"" + type + "\",\"outer_label\":\"" + asnOL.getText().toString() + "\",\"serial_number\":[\"" + asnQr.getText().toString() + "\"]}";
                new postBulkASync(jsonParam);
            }
            //parseQR(intent.getStringExtra("ccScanQR"), intent.getStringExtra("tallyQty"));
        }

        if(intent.getStringExtra("ccScanOL") != null) {
            asnArrayList = (List<AsnGetSet>) intent.getSerializableExtra("asnArrayList");
            asnGetSet = (AsnGetSet) intent.getSerializableExtra("asnGetSet");
            tagList = (List<String>) intent.getSerializableExtra("tagList");
            cc_document = intent.getStringExtra("cc_document");
            warehouse_name = intent.getStringExtra("warehouse_name");
            type = intent.getStringExtra("type");
            cc_type = intent.getStringExtra("cc_type");
            asnOL.setText(intent.getStringExtra("ccScanOL"));
            asnQr.requestFocus();
            asnQr.setFocusableInTouchMode(true);
            asnQr.setFocusable(true);
            asnQr.setText("");
            asnQty.setText("1");
            totalItems.setText(Integer.toString(asnArrayList.size()));
            adapter.notifyDataSetChanged();
        }
    }

    private void postCC(final Map<String, String> parameters) {
        String postUrl = this.url + "/Api_cycle_count/tempPost";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        /*Toast.makeText(getApplicationContext(), "Done", Toast.LENGTH_SHORT).show();
                        finish();
                        Intent intent2 = new Intent(getApplicationContext(), Inventory.class);
                        intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent2);*/
                        Intent intent = new Intent(getApplicationContext(), InventoryCycleCountType.class);
                        intent.putExtra("cc_document", cc_document);
                        intent.putExtra("warehouse_name", warehouse_name);
                        intent.putExtra("cc_type", cc_type);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;

            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    private void parseQR(String asnQrText, String asnQtyText) {
        String qr = "";

        qr = asnQrText.toUpperCase();
        try {
            if(asnQtyText.length() > 0 && Float.parseFloat(asnQtyText) > 0) {
                if(qr.length() == 40 || qr.length() == 42 || qr.length() == 44) { // Hex scanned
                    if(tagList.size() > 0) {
                        if(tagList.contains(qr)) {
                            System.out.println("tagList : " + tagList);
                            Toast.makeText(getApplicationContext(), "This QR is already scanned.", Toast.LENGTH_SHORT).show();
                        } else {
                            tagList.add(qr.toUpperCase());
                            String qrText = asnScanRFID.hexToString(qr);
                            asnGetSet = new AsnGetSet(qr, qrText.substring(0, 20), qrText.substring(0, 12).toUpperCase(), "1", asnQtyText);
                            asnArrayList.add(asnGetSet);
                        }
                    } else {
                        tagList.add(qr.toUpperCase());
                        String qrText = asnScanRFID.hexToString(qr);
                        asnGetSet = new AsnGetSet(qr, qrText.substring(0, 20), qrText.substring(0, 12).toUpperCase(), "1", asnQtyText);
                        asnArrayList.add(asnGetSet);
                    }
                    System.out.println("tagList2 : " + tagList);
                } else {
                    if(qr.contains("OL")) {
                        params.clear();
                        params.put("cc_code", cc_document);
                        params.put("outer_label", qr);
                        getOuterLabelData(params);
                    } else {
                        String[] qrArr = splitStringEvery(qr, 17);

                        int qrArrLen = qrArr.length;

                        for(int i = 0; i < qrArr.length; i++) {
                            if(qrArr[i].substring(0,3).equals("SKU")) {
                                qrArr[i] = qrArr[i].substring(3, qrArr[i].length());
                            }
                            asnGetSet = new AsnGetSet(toHex(qrArr[i].substring(0, 20)).toUpperCase(), qrArr[i].substring(0, 20).toUpperCase(), qrArr[i].substring(0, 12).toUpperCase(), "1", asnQtyText);
                            asnArrayList.add(asnGetSet);
                        }
                    }
                }
            } else {
                // showMessage("Alert", "Qty must be not null and bigger than 0.");
                Toast.makeText(getApplicationContext(), "Qty must be not null and bigger than 0.", Toast.LENGTH_SHORT).show();
            }
        } catch (NumberFormatException nfe) {
            // showMessage("Alert", "Qty must be not null and bigger than 0.");
            Toast.makeText(getApplicationContext(), "Qty must be not null and bigger than 0.", Toast.LENGTH_SHORT).show();
        } catch (StringIndexOutOfBoundsException e) {
            Toast.makeText(getApplicationContext(), "Format barcode tidak sesuai.", Toast.LENGTH_SHORT).show();
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                asnQr.requestFocus();
                asnQr.setFocusableInTouchMode(true);
                asnQr.setFocusable(true);
                asnQr.setText("");
                asnQty.setText("1");
                totalItems.setText(Integer.toString(asnArrayList.size()));
                adapter.notifyDataSetChanged();
            }
        });
    }

    public void getSerial(final String cycle_code, final String type, final String item_code, final String serial_number, final String outer_label) {
        String postUrl = this.url + "/Api_cycle_count/get_serial/";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                        final String usernameKey = prefs.getString("usernameKey", "No name defined");

                        Calendar calendar = Calendar.getInstance();
                        SimpleDateFormat mdformat = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
                        final String time = mdformat.format(calendar.getTime());
                        // response
                        Log.d("Cycle Count getSerial:", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if (jsonObject.getInt("status") == 200) {
                                JSONArray jsonArray1 = jsonObject.getJSONArray("results");

                                JSONObject jsonObject1 = jsonArray1.getJSONObject(0);
                                if(!jsonObject1.getString("type1").equals("null")){
                                    type1 = jsonObject1.getString("type1");
                                }
                                if(!jsonObject1.getString("kd_qc").equals("null")){
                                    kd_qc = jsonObject1.getString("kd_qc");
                                }
                                if(!jsonObject1.getString("tgl_exp").equals("null")){
                                    tgl_exp = jsonObject1.getString("tgl_exp");
                                }
                                if(!jsonObject1.getString("tgl_in").equals("null")){
                                    tgl_in = jsonObject1.getString("tgl_in");
                                }
                                if(!jsonObject1.getString("putaway_time").equals("null")){
                                    putaway_time = jsonObject1.getString("putaway_time");
                                }

                                postCycleCount(cycle_code, item_code, type1, serial_number, "1", time, usernameKey, kd_qc, tgl_in, putaway_time, tgl_exp, outer_label);
                            } else {
//                                try {
//                                    AlertDialog.Builder builder = new AlertDialog.Builder(InventoryCycleCountScanQR.this);
//                                    builder.setTitle("Alert");
//                                    builder.setMessage("Serial number ini ("+asnScanRFID.hexToString(serial_number).substring(0, 20)+") memiliki perbedaan data dengan sistem, lanjutkan ?");
//                                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int which) {
                                            getSerialPositive(cycle_code, type, item_code, serial_number, outer_label);
//                                        }
//                                    });
//                                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int which) {
//                                            asnQr.requestFocus();
//                                            asnQr.setFocusableInTouchMode(true);
//                                            asnQr.setFocusable(true);
//                                            asnQr.setText("");
//                                            asnQty.setText("1");
//                                            totalItems.setText(Integer.toString(asnArrayList.size()));
//                                        }
//                                    });
//                                    AlertDialog dialog = builder.create();
//                                    dialog.show();
//                                } catch (WindowManager.BadTokenException bde) {
//                                    bde.printStackTrace();
//                                } catch (IllegalStateException ise) {
//                                    ise.printStackTrace();
//                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("cycle_code",cycle_code);
                params.put("type",type);
                params.put("item_code",item_code);
                params.put("serial_number",serial_number.toUpperCase());
                System.out.println("getSerial Params : " + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    public void getSerialPositive(final String cycle_code, final String type, final String item_code, final String serial_number, final String outer_label) {
        String postUrl = this.url + "/Api_cycle_count/get_positive_serial/";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                        final String usernameKey = prefs.getString("usernameKey", "No name defined");

                        Calendar calendar = Calendar.getInstance();
                        SimpleDateFormat mdformat = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
                        final String time = mdformat.format(calendar.getTime());
                        // response
                        Log.d("CycleCountgetPosSerial:", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if (jsonObject.getInt("status") == 200) {
                                JSONArray jsonArray1 = jsonObject.getJSONArray("results");

                                JSONObject jsonObject1 = jsonArray1.getJSONObject(0);
                                if(!jsonObject1.getString("type1").equals("null")){
                                    type1 = jsonObject1.getString("type1");
                                }
                                if(!jsonObject1.getString("kd_qc").equals("null")){
                                    kd_qc = jsonObject1.getString("kd_qc");
                                }
                                if(!jsonObject1.getString("tgl_exp").equals("null")){
                                    tgl_exp = jsonObject1.getString("tgl_exp");
                                }
                                if(!jsonObject1.getString("tgl_in").equals("null")){
                                    tgl_in = jsonObject1.getString("tgl_in");
                                }
                                if(!jsonObject1.getString("putaway_time").equals("null")){
                                    putaway_time = jsonObject1.getString("putaway_time");
                                }

                                postCycleCount(cycle_code, item_code, type1, serial_number, "1", time, usernameKey, kd_qc, tgl_in, putaway_time, tgl_exp, outer_label);
                            } else {
//                                try {
//                                    AlertDialog.Builder builder = new AlertDialog.Builder(InventoryCycleCountScanQR.this);
//                                    builder.setTitle("Alert");
//                                    builder.setMessage("Serial number ini belum pernah masuk ke dalam sistem, apakah ingin dimasukkan ?");
//                                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int which) {
                                            postCycleCount(cycle_code, item_code, asnScanRFID.hexToString(serial_number).substring(0, 12), serial_number, "1", time, usernameKey, "CC HOLD", time, time, tgl_exp, outer_label);
//                                        }
//                                    });
//                                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int which) {
//                                            asnQr.requestFocus();
//                                            asnQr.setFocusableInTouchMode(true);
//                                            asnQr.setFocusable(true);
//                                            asnQr.setText("");
//                                            asnQty.setText("1");
//                                            totalItems.setText(Integer.toString(asnArrayList.size()));
//                                        }
//                                    });
//                                    AlertDialog dialog = builder.create();
//                                    dialog.show();
//                                } catch (WindowManager.BadTokenException bde) {
//                                    bde.printStackTrace();
//                                } catch (IllegalStateException ise) {
//                                    ise.printStackTrace();
//                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put("cycle_code",cycle_code);
                params.put("type",type);
                params.put("item_code",item_code);
                params.put("serial_number",serial_number.toUpperCase());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    private class getSerialASync extends AsyncTask<Void, Void, Void> {
        String cycle_code = "";
        String type = "";
        String item_code = "";
        String serial_number = "";
        String outer_label = "";

        getSerialASync(final String cycle_code, final String type, final String item_code, final String serial_number, final String outer_label) {
            super();
            this.cycle_code = cycle_code;
            this.type = type;
            this.item_code = item_code;
            this.serial_number = serial_number;
            this.outer_label = outer_label;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            getSerial(cycle_code, type, item_code, serial_number, outer_label);
            return null;
        }
    }

    private class postBulkASync extends AsyncTask<Void, Void, Void> {
        String param = "";

        postBulkASync(final String param) {
            super();
            this.param = param;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            doneButton.setText("Posting...");
            doneButton.setEnabled(false);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            postBulk(param);
            return null;
        }
    }

    public void postBulk(final String param) {
        String postUrl = this.url + "/Api_cycle_count/post_bulk/";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Cycle Count Post Bulk ", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getInt("status") == 200) {
                                JSONObject jsonObject2 = new JSONObject(param);
                                JSONArray jsonArray = new JSONArray(jsonObject2.getString("serial_number"));
//                                for(int i = 0; i < jsonArray.length(); i++) {
//                                    parseQR(jsonArray.getString(i), "1");
//                                }

                                asnGetSet = new AsnGetSet(
                                        "",
                                        asnQr.getText().toString(),
                                        "",
                                        asnQty.getText().toString()
                                );

                                asnArrayList.add(asnGetSet);
                                tagList.add(asnQr.getText().toString());
                                adapter = new AsnListAdapter(asnArrayList, getApplicationContext(),4);
                                qrList.setAdapter(adapter);


                                Toast.makeText(getApplicationContext(),  jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                                doneButton.setText("Done");
                                doneButton.setEnabled(true);

                                /*Toast.makeText(getApplicationContext(),  jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), InventoryCycleCountType.class);
                                intent.putExtra("cc_document", cc_document);
                                intent.putExtra("warehouse_name", warehouse_name);
                                intent.putExtra("cc_type", cc_type);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);*/
                            } else {
                                doneButton.setText("Done");
                                doneButton.setEnabled(true);
                                Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("data", param);
                System.out.println("Post Bulk Cycle Count params : " + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    public void postCycleCount(final String cc_code, final String type, final String type1, final String serial_number, final String qty, final String time, final String uname, final String kd_qc, final String tgl_in, final String putaway_time, final String tgl_exp, final String outer_label) {
        String postUrl = this.url + "/Api_cycle_count/post/";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Cycle Count getSerial: ", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getInt("status") == 200) {
                                Toast.makeText(getApplicationContext(),  jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                                parseQR(serial_number, "1");
                            } else {
                                Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("cc_code", cc_code);
                params.put("type", type);
                params.put("type1", type1);
                params.put("serial_number", serial_number.toUpperCase());
                params.put("qty", qty);
                params.put("time", time);
                params.put("kd_qc", kd_qc);
                params.put("uname", uname);
                params.put("tgl_in", tgl_in);
                params.put("putaway_time", putaway_time);
                params.put("tgl_exp", tgl_exp);
                params.put("outer_label", outer_label);
                System.out.println("Post Cycle Count params : " + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    public void postPositiveCycleCount(final String cc_code, final String type, final String type1, final String serial_number, final String qty, final String time, final String uname, final String kd_qc, final String tgl_in, final String putaway_time, final String tgl_exp) {
        String postUrl = this.url + "/Api_cycle_count/post/";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Cycle Count getSerial:", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getInt("status") == 200) {
                                Toast.makeText(getApplicationContext(),  jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                                parseQR(serial_number, "1");
                            } else {
                                Toast.makeText(getApplicationContext(),  jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("cc_code", cc_code);
                params.put("type", type);
                params.put("type1", type1);
                params.put("serial_number", serial_number.toUpperCase());
                params.put("qty", qty);
                params.put("time", time);
                params.put("kd_qc", kd_qc);
                params.put("uname", uname);
                params.put("tgl_in", tgl_in);
                params.put("putaway_time", putaway_time);
                params.put("tgl_exp", tgl_exp);
                System.out.println("Post Cycle Count params : " + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    public void getOuterLabelData(final Map<String, String> parameters) {
        String postUrl = this.url + "/Api_cycle_count/getOuterLabelItem/";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);

                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            String message = jsonResponse.getString("message");
                            int status = jsonResponse.getInt("status");
                            if(status == 200) {
                                JSONArray jsonMessage = new JSONArray(message);
                                String snArray = "";

                                //asnArrayList.clear();
                                for(int i = 0; i < jsonMessage.length(); i++) {
                                    String unique_code = jsonMessage.getString(i);
                                    JSONObject jsonHex = new JSONObject(unique_code);
                                    String hex = jsonHex.getString("unique_code");
                                    String hexToText = asnScanRFID.hexToString(hex).toUpperCase();
                                    snArray = snArray + "\"" + hex + "\",";

                                    // new getSerialASync(cc_document, cc_type, type, hex, asnOL.getText().toString()).execute();
                                }
                                String jsonParam = "{\"cc_code\":\"" + cc_document + "\",\"location\":\"" + type + "\",\"outer_label\":\"" + asnOL.getText().toString() + "\",\"serial_number\":[" + snArray.substring(0, snArray.length() - 1) + "]}";
                                new postBulkASync(jsonParam).execute();
                            } else {
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException jsonEx) {
                            jsonEx.printStackTrace();
                        }

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                totalItems.setText(Integer.toString(asnArrayList.size()));
                                asnQr.setText("");
                                asnQr.requestFocus();
                                asnQr.setSelection(0);
                                adapter.notifyDataSetChanged();
                            }
                        });
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    public String toHex(String arg) {
        String result = String.format("%x", new BigInteger(1, arg.getBytes(/*YOUR_CHARSET?*/)));
        return result.toUpperCase();
    }

    public String[] splitStringEvery(String s, int interval) {
        int arrayLength = (int) Math.ceil(((s.length() / (double)interval)));
        String[] result = new String[arrayLength];

        int j = 0;
        int lastIndex = result.length - 1;
        for (int i = 0; i < lastIndex; i++) {
            result[i] = s.substring(j, j + interval);
            j += interval;
        } //Add the last bit
        result[lastIndex] = s.substring(j);

        return result;
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }
}