package largo.largo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static largo.largo.LoginActivity.MyPREFERENCES;

public class InventoryActiveStock extends AppCompatActivity {
    String TAG = InventoryActiveStock.class.getSimpleName();
    EditText serial_number;
    Button backButton, historyButton;
    AsnScanRFID asnScanRFID;

    private InventoryActiveStockGetSet inventoryActiveStockGetSet;
    private List<InventoryActiveStockGetSet> inventoryActiveStockGetSetList = new ArrayList<>();

    private TextView serial_number_text, sku_text, item_name_text, qty_text, bin_loc_text, exp_date_text, status_text;

    DatabaseHelper myDb;
    String url;

    private Intent intent;
    private ImageButton scanButton;
    private String fClass = this.getClass().getSimpleName();

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.active_stock_item_log);

        intent = getIntent();

        asnScanRFID = new AsnScanRFID();

        serial_number_text = findViewById(R.id.serial_number_text);
        sku_text = findViewById(R.id.sku_text);
        item_name_text = findViewById(R.id.item_name_text);
        qty_text = findViewById(R.id.qty_text);
        bin_loc_text = findViewById(R.id.bin_loc_text);
        exp_date_text = findViewById(R.id.exp_date_text);
        status_text = findViewById(R.id.status_text);

        myDb = new DatabaseHelper(this);
        getServerURL();

        backButton = findViewById(R.id.back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        historyButton = findViewById(R.id.history);
        historyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), InventoryActiveStockHistory.class);
                intent.putExtra("inventoryActiveStockGetSet", inventoryActiveStockGetSet);
                intent.putExtra("inventoryActiveStockGetSetList", (Serializable) inventoryActiveStockGetSetList);
                startActivity(intent);
            }
        });

        serial_number = findViewById(R.id.serial_number);
        serial_number.setShowSoftInputOnFocus(false);
        serial_number.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    getActiveStock(serial_number.getText().toString());
                }
                return false;
            }
        });

        scanButton = findViewById(R.id.scanButton);
        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getApplicationContext(), QRScanner.class);
                intent.putExtra("param", "inventoryActiveStock");
                intent.putExtra("fClass", fClass);
                startActivity(intent);
            }
        });

        if(intent.getStringExtra("inventoryActiveStock") != null) {
            getActiveStock(intent.getStringExtra("inventoryActiveStock"));
        }
    }

    private void getActiveStock(final String serialNumber) {
        StringRequest strReq = new StringRequest(Request.Method.GET, url+"/Api_active_stock/get/" + serialNumber.toUpperCase().replace("*", "%2A"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);

                try {
                    JSONObject data = new JSONObject(response);
                    Integer status = data.getInt("status");

                    if (status == 200) {
                        serial_number.requestFocus();
                        serial_number.setText("");
                        new getActiveStockHistoryASync(serialNumber).execute();

                        JSONArray jsonArray = data.getJSONArray("results");
                        JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                        serial_number_text.setText(serialNumber.toUpperCase());
                        sku_text.setText(jsonObject1.getString("kd_barang"));
                        item_name_text.setText(jsonObject1.getString("nama_barang"));
                        qty_text.setText(jsonObject1.getString("last_qty") + " " + jsonObject1.getString("unit_name"));
                        bin_loc_text.setText(jsonObject1.getString("location"));
                        exp_date_text.setText(jsonObject1.getString("tgl_exp"));
                        if(jsonObject1.getString("tgl_exp").equals("0000-00-00")) {
                            exp_date_text.setText("-");
                        }
                        status_text.setText(jsonObject1.getString("kd_qc"));
                    } else {
                        serial_number.setText("");
                        serial_number.requestFocus();
                        Toast.makeText(getApplicationContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (StringIndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }

    private class getActiveStockHistoryASync extends AsyncTask<Void, Void, Void> {
        final String parameters;

        getActiveStockHistoryASync(String parameters) {
            super();
            this.parameters = parameters;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    getActiveStockHistory(parameters);
                }
            });
            return null;
        }
    }

    private void getActiveStockHistory(final String serialNumber) {
        StringRequest strReq = new StringRequest(Request.Method.GET, url+"/Api_active_stock/get_history/" + serialNumber.toUpperCase(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);

                try {
                    JSONObject data = new JSONObject(response);
                    Integer status = data.getInt("status");

                    if (status == 200) {
                        JSONArray jsonArray = data.getJSONArray("history");
                        for(int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            inventoryActiveStockGetSet = new InventoryActiveStockGetSet(
                                    jsonObject1.getString("loc_name_old"),
                                    jsonObject1.getString("user_name_old"),
                                    jsonObject1.getString("pick_time"),
                                    jsonObject1.getString("loc_name_new"),
                                    jsonObject1.getString("user_name_new"),
                                    jsonObject1.getString("put_time"),
                                    jsonObject1.getString("process_name")
                            );
                            inventoryActiveStockGetSetList.add(inventoryActiveStockGetSet);
                        }
                    }
                } catch (JSONException e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;
            }
        };

        controller.AppController.getInstance(getApplicationContext()).addToRequestQueue(strReq);
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }
}
