package largo.largo;

import java.io.Serializable;

public class DeliveryNoteItemGetterSetter implements Serializable {
    String item_name, code, item_code, qty, pl_name, user_id, user_name;
    public DeliveryNoteItemGetterSetter(String item_name, String code, String item_code, String qty, String pl_name, String user_id, String user_name){
        this.item_name = item_name;
        this.code = code;
        this.qty = qty;
        this.pl_name = pl_name;
        this.item_code = item_code;
        this.user_id = user_id;
        this.user_name = user_name;
    }

    public String getItem_name(){return item_name;}
    public void setItem_name(String item_name){ this.item_name = item_name;}

    public String getUser_name(){return user_name;}
    public void setUser_name(String user_name){ this.user_name = user_name;}

    public String getUser_id(){return user_id;}
    public void setUser_id(String user_id){ this.user_id = user_name;}

    public String getCode(){return code;}
    public void setCode(String code){ this.code = code;}

    public String getItem_code(){return item_code;}
    public void setItem_code(String item_code){ this.item_code = item_code;}

    public String getQty(){return qty;}
    public void setQty(String item_name){ this.qty = qty;}

    public String getPl_name(){return pl_name;}
    public void setPl_name(String pl_name){ this.pl_name = pl_name;}
}
