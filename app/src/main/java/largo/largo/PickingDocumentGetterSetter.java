package largo.largo;

import java.io.Serializable;

public class PickingDocumentGetterSetter implements Serializable {
    String pl_name, qty, source, item_area, gate_id;

    public PickingDocumentGetterSetter(String pl_name, String qty, String source) {
        this.pl_name = pl_name;
        this.qty = qty;
        this.source = source;
    }

    public PickingDocumentGetterSetter(String pl_name, String qty, String source, String item_area) {
        this.pl_name = pl_name;
        this.qty = qty;
        this.source = source;
        this.item_area = item_area;
    }

    public PickingDocumentGetterSetter(String pl_name, String qty, String source, String item_area, String gate_id) {
        this.pl_name = pl_name;
        this.qty = qty;
        this.source = source;
        this.item_area = item_area;
        this.gate_id = gate_id;
    }


    public String getPlName() {
        return pl_name;
    }
    public String getGate_id() {
        return gate_id;
    }

    public String getQty() {
        return qty;
    }

    public String getSource() {
        return source;
    }

    public String getItem_area() {
        return String.valueOf(item_area.charAt(0));
    }
}
