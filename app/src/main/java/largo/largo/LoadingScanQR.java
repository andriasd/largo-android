package largo.largo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LoadingScanQR extends AppCompatActivity {
    String TAG = this.getClass().getSimpleName();

    private Button backButton, nextButton;
    private TextView headerLoading;
    private ImageButton scanButton, scanSNButton;
    private EditText plateNumber, serialNumber;
    private String param, loadingDocument, destination_name = "", picking = "";
    private String fClass = this.getClass().getSimpleName();
    private Intent intent;
    private ListView listView;
    private List<LoadingDocumentGetterSetter> loadingDocItemList;
    private LoadingDocumentGetterSetter loadingDocGetSet;
    private LoadingDokumenListAdapter adapter;
    private List<String> docList;
    int checkPost, flagComplete = 0;

    private JSONObject paramsJson;
    public static final Map<String, String> params = new HashMap<String, String>();
    public static final String MyPREFERENCES = "MyPrefs" ;

    DatabaseHelper myDb;
    String url;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loading_scan_qr);

        intent = getIntent();
        param = this.getClass().getSimpleName();
        checkPost = 0;

        myDb = new DatabaseHelper(this);
        getServerURL();

        backButton = findViewById(R.id.back);
        nextButton = findViewById(R.id.next);
        headerLoading = findViewById(R.id.header_loading);
        listView = findViewById(R.id.QRList);
        plateNumber = findViewById(R.id.plate_number);
        plateNumber.setText(intent.getStringExtra("license"));
        serialNumber = findViewById(R.id.serial_number);
        serialNumber.setShowSoftInputOnFocus(false);

        docList = new ArrayList<>();
        loadingDocItemList = new ArrayList<>();

        if(intent.getStringExtra("loading_document") != null) {
            loadingDocument = intent.getStringExtra("loading_document");
            destination_name = intent.getStringExtra("destination_name");
            picking = intent.getStringExtra("picking");

            headerLoading.setText("Loading - " + loadingDocument);
            getItems(flagComplete);
        }

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                plateNumber.setText("");
                serialNumber.setText("");
            }
        });

        plateNumber.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 0) {
                    if (plateNumber.length() != 0) {
                        if(checkPost == 0) {
                            params.put("loading_code", loadingDocument);
                            params.put("plate_number", plateNumber.getText().toString());
                            postPlateNumber(params);
                            checkPost = 1;
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Please insert plate number first.", Toast.LENGTH_SHORT).show();
                    }
                }
                return false;
            }
        });

        serialNumber.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == 0) {
                    if (plateNumber.length() != 0) {
                        SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                        String restoredText = prefs.getString("handheldsessioncodeKey", null);

                        String usernameKey = prefs.getString("usernameKey", "No name defined");
                        String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                        params.put("picking_code", loadingDocument);
                        params.put("vehicle_police", plateNumber.getText().toString());
                        params.put("packing_code", "");
                        params.put("item_code", "");
                        params.put("serial_number", serialNumber.getText().toString());
                        params.put("time", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                        params.put("user", usernameKey);
                        params.put("put_time", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                        System.out.println(params);
                        postLoading(params);
                    } else {
                        Toast.makeText(getApplicationContext(), "Packing Number must be not null.", Toast.LENGTH_SHORT).show();
                    }
                }
                return false;
            }
        });

        scanButton = findViewById(R.id.scanButton);
        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getApplicationContext(), QRScanner.class);
                intent.putExtra("param", "loadingPlateNumber");
                intent.putExtra("fClass", fClass);
                intent.putExtra("docList", (Serializable) docList);
                intent.putExtra("loadingDocItemList", (Serializable) loadingDocItemList);
                intent.putExtra("loadingDocGetSet", loadingDocGetSet);
                intent.putExtra("loading_document", loadingDocument);
                startActivity(intent);
            }
        });

        scanSNButton = findViewById(R.id.scanSNButton);
        scanSNButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getApplicationContext(), QRScanner.class);
                intent.putExtra("param", "loadingQR");
                intent.putExtra("fClass", fClass);
                intent.putExtra("docList", (Serializable) docList);
                intent.putExtra("loadingDocItemList", (Serializable) loadingDocItemList);
                intent.putExtra("loadingDocGetSet", loadingDocGetSet);
                intent.putExtra("loadingPlateNumberText", plateNumber.getText().toString());
                intent.putExtra("loading_document", loadingDocument);
                startActivity(intent);
            }
        });
        // LocalBroadcastManager.getInstance(this).registerReceiver(qrScan, new IntentFilter("broadcastQrResult"));
        if(intent.getStringExtra("loadingPlateNumber") != null) {
            plateNumber.setText(intent.getStringExtra("loadingPlateNumber"));
            params.put("loading_code", loadingDocument);
            params.put("plate_number", intent.getStringExtra("loadingPlateNumber"));
            postPlateNumber(params);
        }
        if(intent.getStringExtra("loadingQR") != null) {
            plateNumber.setText(intent.getStringExtra("loadingPlateNumberText"));
            serialNumber.setText(intent.getStringExtra("loadingQR").toUpperCase());
            getSerialNumber(loadingDocument, intent.getStringExtra("loadingQR").toUpperCase());
        }
    }

    private void postPlateNumber(final Map<String, String> parameters) {
        String postUrl = this.url + "/Api_loading/postPlateNumber";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        checkPost = 0;

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if(jsonObject.getInt("status") == 200){
                                serialNumber.requestFocus();
                                Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            } else {
                                // showMessage("Alert", jsonObject.getString("message"));
								Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                                plateNumber.setText("");
                                plateNumber.requestFocus();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;

            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    private void getSerialNumber(final String loadDoc, final String sn){
//        final ArrayList<String> items = new ArrayList<String>();

        RequestQueue requestQueue=Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, url + "/Api_loading/get_serial/" + loadDoc + "/" + sn.toUpperCase().replace("*", "%2A"), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                SharedPreferences prefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
                String usernameKey = prefs.getString("usernameKey", "No name defined");

                Log.d(TAG + "getSerialNumber", response);
                try{
                    JSONObject jsonObject=new JSONObject(response);
                    if(jsonObject.getInt("status") == 200){

                        params.put("picking_code", loadingDocument);
                        params.put("vehicle_police", plateNumber.getText().toString());
                        params.put("packing_code", "");
                        params.put("item_code", "");
                        params.put("serial_number", sn);
                        params.put("time", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                        params.put("user", usernameKey);
                        params.put("put_time", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                        System.out.println(params);
                        postLoading(params);
                    } else {
                        // showMessage("Alert", jsonObject.getString("message"));
						Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        serialNumber.setText("");
                        serialNumber.requestFocus();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;


            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void getItems(final int flag) {
        String postUrl = this.url + "/Api_loading/get_item/";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, response);
                        try {
                            JSONObject data = new JSONObject(response);
                            JSONArray resultArr = data.getJSONArray("results");
                            LoadingItemsGetSet loadingItemsGetSet;
                            LoadingItemsListAdapter loadingItemsListAdapter;
                            List<LoadingItemsGetSet> loadingItemsGetSetList = new ArrayList<>();
                            String complete = "";
                            if(flag == 1) {
                                complete = "\nCOMPLETED";
                            }

                            for(int i = 0; i < resultArr.length(); i++) {
                                JSONObject resultData = resultArr.getJSONObject(i);
                                loadingItemsGetSet = new LoadingItemsGetSet(
                                        resultData.getString("kd_barang"),
                                        resultData.getString("kd_barang"),
                                        resultData.getString("kd_barang"),
                                        resultData.getString("kd_barang"),
                                        resultData.getString("kd_barang"),
                                        resultData.getString("kd_barang")
//                                        resultData.getString("packing_number")
                                );
                                loadingItemsGetSetList.add(loadingItemsGetSet);
                            }

                            loadingItemsListAdapter = new LoadingItemsListAdapter(loadingItemsGetSetList, getApplicationContext(), 0);
                            listView.setAdapter(loadingItemsListAdapter);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                params.clear();
                params.put("loading_code", loadingDocument);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;

            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    private void postLoading(final Map<String, String> parameters) {
        String postUrl = this.url + "/Api_loading/post/";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if(jsonObject.getInt("status") == 200){
                                Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                                if(jsonObject.getString("message").contains("completed")) {
                                    flagComplete = 1;
                                }
                                serialNumber.setText("");
                                serialNumber.requestFocus();
                                getItems(flagComplete);
                            } else {
                                // showMessage("Alert", jsonObject.getString("message"));
								Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                return parameters;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;

            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }
}
