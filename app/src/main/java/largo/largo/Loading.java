package largo.largo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Loading extends AppCompatActivity {
    String TAG = this.getClass().getSimpleName();

    private Button backButton, nextButton;
    private ImageButton scanButton;
    private EditText loadingDocument;
    private String param;
    private String fClass = this.getClass().getSimpleName();
    private Intent intent;
    private ListView listView;
    private List<LoadingDocumentGetterSetter> loadingDocItemList;
    private LoadingDocumentGetterSetter loadingDocGetSet;
    private List<String> docList;

    DatabaseHelper myDb;
    String url;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loading_document);

        intent = getIntent();
        param = this.getClass().getSimpleName();

        myDb = new DatabaseHelper(this);
        getServerURL();

        backButton = findViewById(R.id.back);
        nextButton = findViewById(R.id.next);
        listView = findViewById(R.id.ListLoadingDoc);
        loadingDocument = findViewById(R.id.loading_document);

        docList = new ArrayList<>();
        loadingDocItemList = new ArrayList<>();
        DataListDokument(url + "/Api_loading/load");

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(loadingDocument.getText().toString().length() > 0) {
                    Integer position = 0;
                    for(int i = 0; i < loadingDocItemList.size(); i++) {
                        if(loadingDocItemList.get(i).getPlName().equals(loadingDocument.getText().toString())) {
                            position = i;
                        }
                    }

                    Intent intent = new Intent(getApplicationContext(), LoadingScanQR.class);
                    intent.putExtra("loading_document", loadingDocument.getText().toString());
                    intent.putExtra("destination_name", loadingDocItemList.get(position).getDestination_name());
                    intent.putExtra("picking", loadingDocItemList.get(position).getPicking());
                    intent.putExtra("license", loadingDocItemList.get(position).getLicense());
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "Please input loading document first.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                loadingDocument.setText(loadingDocItemList.get(position).getPlName());
                Intent intent = new Intent(getApplicationContext(), LoadingScanQR.class);
                intent.putExtra("loading_document", loadingDocItemList.get(position).getPlName());
                intent.putExtra("destination_name", loadingDocItemList.get(position).getDestination_name());
                intent.putExtra("picking", loadingDocItemList.get(position).getPicking());
                intent.putExtra("license", loadingDocItemList.get(position).getLicense());
                startActivity(intent);
            }
        });

        scanButton = findViewById(R.id.scanButton);
        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(getApplicationContext(), QRScanner.class);
                intent.putExtra("param", "loadingDoc");
                intent.putExtra("fClass", fClass);
                intent.putExtra("docList", (Serializable) docList);
                intent.putExtra("loadingDocItemList", (Serializable) loadingDocItemList);
                intent.putExtra("loadingDocGetSet", loadingDocGetSet);
                startActivity(intent);
            }
        });
        // LocalBroadcastManager.getInstance(this).registerReceiver(qrScan, new IntentFilter("broadcastQrResult"));
        if(intent.getStringExtra("loadingDoc") != null) {
            docList = (List<String>) intent.getSerializableExtra("docList");
            if(docList.contains(intent.getStringExtra("loadingDoc"))) {
                Intent intent2 = new Intent(getApplicationContext(), LoadingScanQR.class);
                intent2.putExtra("loading_document", intent.getStringExtra("loadingDoc"));
                startActivity(intent2);
            } else {
                Toast.makeText(getApplicationContext(), "Document not available.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void DataListDokument(String getDoc){
//        final ArrayList<String> items = new ArrayList<String>();

        RequestQueue requestQueue=Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, getDoc, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);
                try{
                    JSONObject jsonObject=new JSONObject(response);
                    if(jsonObject.getInt("status")==200){
                        JSONArray jsonArray1=jsonObject.getJSONArray("results");
                        for(int i=0;i<jsonArray1.length();i++){
                            JSONObject jsonObject1=jsonArray1.getJSONObject(i);
                            loadingDocGetSet = new LoadingDocumentGetterSetter(
                                    jsonObject1.getString("pl_name"),
                                    jsonObject1.getString("license"),
//                                    jsonObject1.getString("id_manifest"),
                                    "",
                                    jsonObject1.getString("destination_name")
                            );
                            loadingDocItemList.add(loadingDocGetSet);
                            docList.add(jsonObject1.getString("pl_name"));
                        }
                        LoadingDokumenListAdapter adapter = new LoadingDokumenListAdapter(loadingDocItemList, getApplicationContext());

                        listView.setAdapter(adapter);

                    }
//                    spinner.setAdapter(new ArrayAdapter<String>(TallyReceivingItemSerial.this, android.R.layout.simple_spinner_dropdown_item, QC));
                }catch (JSONException e){e.printStackTrace();}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences("MyPrefs", MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("User",usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;


            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }

    private BroadcastReceiver qrScan = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getStringExtra(param) != null) {
                String getParam = intent.getStringExtra(param);
                intent.removeExtra(param);

                if(docList.contains(getParam)) {
                    Intent intent2 = new Intent(getApplicationContext(), AsnItem.class);
                    intent2.putExtra("receiving_document", getParam);
                    startActivity(intent2);
                } else {
//                    Toast.makeText(getApplicationContext(), "Document not available.", Toast.LENGTH_SHORT).show();
                }
            }
        }
    };
}
