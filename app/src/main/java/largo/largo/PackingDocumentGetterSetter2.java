package largo.largo;

import java.io.Serializable;

public class PackingDocumentGetterSetter2 implements Serializable {
    String item_code, item_name, outbound_id, outbound_code, destination_code, destination_name, destination_address, picking_id, picking_code, uom, qty_picked, qty_packed;

    public PackingDocumentGetterSetter2(String item_code, String item_name, String outbound_id, String outbound_code, String destination_code, String destination_name, String destination_address, String picking_id, String picking_code, String uom, String qty_picked, String qty_packed) {
        this.item_code = item_code;
        this.item_name = item_name;
        this.outbound_id = outbound_id;
        this.outbound_code = outbound_code;
        this.destination_code = destination_code;
        this.destination_name = destination_name;
        this.destination_address = destination_address;
        this.picking_id = picking_id;
        this.picking_code = picking_code;
        this.uom = uom;
        this.qty_picked = qty_picked;
        this.qty_packed = qty_packed;
    }

    public String getItem_code() {
        return item_code;
    }

    public String getItem_name() {
        return item_name;
    }

    public String getOutbound_id() {
        return outbound_id;
    }

    public String getOutbound_code() {
        return outbound_code;
    }

    public String getDestination_code() {
        return destination_code;
    }

    public String getDestination_name() {
        return destination_name;
    }

    public String getDestination_address() {
        return destination_address;
    }

    public String getPicking_id() {
        return picking_id;
    }

    public String getPicking_code() {
        return picking_code;
    }

    public String getUom() {
        return uom;
    }

    public String getQty_picked() {
        return qty_picked;
    }

    public String getQty_packed() {
        return qty_packed;
    }
}
