package largo.largo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.zebra.rfid.api3.Antennas;
import com.zebra.rfid.api3.ENUM_TRANSPORT;
import com.zebra.rfid.api3.ENUM_TRIGGER_MODE;
import com.zebra.rfid.api3.HANDHELD_TRIGGER_EVENT_TYPE;
import com.zebra.rfid.api3.InvalidUsageException;
import com.zebra.rfid.api3.MEMORY_BANK;
import com.zebra.rfid.api3.OperationFailureException;
import com.zebra.rfid.api3.RFIDReader;
import com.zebra.rfid.api3.ReaderDevice;
import com.zebra.rfid.api3.Readers;
import com.zebra.rfid.api3.RfidEventsListener;
import com.zebra.rfid.api3.RfidReadEvents;
import com.zebra.rfid.api3.RfidStatusEvents;
import com.zebra.rfid.api3.START_TRIGGER_TYPE;
import com.zebra.rfid.api3.STATUS_EVENT_TYPE;
import com.zebra.rfid.api3.STOP_TRIGGER_TYPE;
import com.zebra.rfid.api3.TAG_FIELD;
import com.zebra.rfid.api3.TagAccess;
import com.zebra.rfid.api3.TagData;
import com.zebra.rfid.api3.TagStorageSettings;
import com.zebra.rfid.api3.TriggerInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class AsnPickingScanRFID extends AppCompatActivity {

    Button cancelButton, doneButton, scanButton, scan32Button;
    String scan32;
    private static AsyncTask<Void, Void, Boolean> connectReader;
    private static Readers readers;
    private static ArrayList availableRFIDReaderList;
    private static ReaderDevice readerDevice;
    public static RFIDReader reader;
    private String param, TAG, pickingDocument;
    private String fClass = this.getClass().getSimpleName();
    private List<String> skuList, tagList;
    private ListView rfidListView;
    private List<AsnGetSet> asnArrayList;
    private AsnGetSet asnGetSet;
    private AsnListAdapter adapter;
    private EventHandler eventHandler;
    private Intent intent;
    private TextView totalItems, textView40;

    private TagAccess tagAccess;
    private TagAccess.ReadAccessParams readAccessParams;

    private JSONObject paramsJson;
    public static final Map<String, String> params = new HashMap<String, String>();
    public static final String MyPREFERENCES = "MyPrefs" ;

    DatabaseHelper myDb;
    String url;

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.outbound_picking_by_asn_rfid);

        TAG = this.getClass().getSimpleName();
        myDb = new DatabaseHelper(this);
        getServerURL();

        intent = getIntent();
        param = this.getClass().getSimpleName();

        rfidListView = findViewById(R.id.rfid_list);
        totalItems = findViewById(R.id.qty);
        totalItems.setText("0");
        textView40 = findViewById(R.id.textView40);

        tagList = new ArrayList<>();
        if(intent.getSerializableExtra("tagList") != null) {
            tagList = (List<String>) intent.getSerializableExtra("tagList");
            System.out.println("tagListAfter : " + tagList);
        }
        asnArrayList = new ArrayList<>();
        if(intent.getSerializableExtra("asnArrayList") != null) {
            asnArrayList = (List<AsnGetSet>) intent.getSerializableExtra("asnArrayList");
            totalItems.setText(Integer.toString(asnArrayList.size()));
            try {
                if(reader != null) {
                    reader.Config.setTriggerMode(ENUM_TRIGGER_MODE.RFID_MODE, true);
                }
            } catch (InvalidUsageException e) {
                e.printStackTrace();
            } catch (OperationFailureException e) {
                e.printStackTrace();
            }
        }

        tagAccess = new TagAccess();
        readAccessParams = tagAccess.new ReadAccessParams();
        readAccessParams.setAccessPassword(0);
        readAccessParams.setCount(8);
        readAccessParams.setMemoryBank(MEMORY_BANK.MEMORY_BANK_EPC);
        readAccessParams.setOffset(2);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter = new AsnListAdapter(asnArrayList, getApplicationContext(), 2);
                rfidListView.setAdapter(adapter);
            }
        });

        skuList = new ArrayList<>();
        skuList = (List<String>) intent.getSerializableExtra("skuList");
        pickingDocument = intent.getStringExtra("picking_document");

        cancelButton = findViewById(R.id.cancel);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    finish();
                    Intent intent2 = new Intent(getApplicationContext(), AsnPickingItem.class);
                    intent2.putExtra("picking_document", pickingDocument);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        scanButton = findViewById(R.id.scan);
        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(scanButton.getText().equals("SCAN")) {
                    scan32 = "1";
                    triggerStart();
                } else if(scanButton.getText().equals("STOP")) {
                    triggerStop();
                } else {
                    triggerStop();
                    triggerStart();
                }
            }
        });

        scan32Button = findViewById(R.id.scan32);
        scan32Button.setVisibility(View.GONE);
        scan32Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(scan32Button.getText().equals("SCAN32")) {
                    scan32 = "1";
                    triggerStart();
                } else if(scan32Button.getText().equals("STOP32")) {
                    triggerStop();
                } else {

                }
            }
        });

        doneButton = findViewById(R.id.done);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(int i = 0; i < asnArrayList.size(); i++) {
                    if(!asnArrayList.get(i).getPcs().equals("2")) {
                        new postPickingASync(pickingDocument, asnArrayList.get(i).getUpc()).execute();
                    }
                }
                try {
                    if (reader != null) {
                        triggerStop();
                        reader.Config.setTriggerMode(ENUM_TRIGGER_MODE.BARCODE_MODE, true);
                        reader.Events.removeEventsListener(eventHandler);
                        reader.disconnect();
                        Toast.makeText(getApplicationContext(), "Disconnecting reader", Toast.LENGTH_SHORT).show();
                        reader = null;
                        readers.Dispose();
                        readers = null;
                        scanButton.setText("Connect to Reader");
                        scanButton.setBackgroundColor(Color.parseColor("#FFBEBE"));
                        scan32Button.setText("Connect to Reader");
                        scan32Button.setBackgroundColor(Color.parseColor("#FFBEBE"));
                    }
                } catch (InvalidUsageException e) {
                    e.printStackTrace();
                } catch (OperationFailureException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                intent = new Intent(getApplicationContext(), AsnPickingLocation.class);
                intent.putExtra("picking_document", pickingDocument);
                startActivity(intent);
            }
        });

        // SDK
        if (readers == null) {
            readers = new Readers(this, ENUM_TRANSPORT.SERVICE_SERIAL);
        }

        connectReader = new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... voids) {
                try {
                    if (readers != null) {
                        if (readers.GetAvailableRFIDReaderList() != null) {
                            availableRFIDReaderList = readers.GetAvailableRFIDReaderList();
                            if (availableRFIDReaderList.size() != 0) {
                                // get first reader from list
                                readerDevice = (ReaderDevice) availableRFIDReaderList.get(0);
                                reader = readerDevice.getRFIDReader();
                                if (!reader.isConnected()) {
                                    // Establish connection to the RFID Reader
                                    reader.connect();
                                    ConfigureReader();
                                    return true;
                                }
                            }
                        }
                    }
                } catch (InvalidUsageException e) {
                    e.printStackTrace();
                } catch (OperationFailureException e) {
                    e.printStackTrace();
                    Log.d(TAG, "OperationFailureException " + e.getVendorMessage());
                }
                return false;
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                if (aBoolean) {
                    Toast.makeText(getApplicationContext(), "Reader Connected", Toast.LENGTH_SHORT).show();
                    scanButton.setText("SCAN");
                    scanButton.setBackgroundColor(Color.parseColor("#32C5D2"));
                    scan32Button.setText("SCAN32");
                    scan32Button.setBackgroundColor(Color.parseColor("#32C5D2"));
                }
            }
        };

        Toast.makeText(AsnPickingScanRFID.this, "Connecting to Reader...", Toast.LENGTH_SHORT).show();
        connectReader.execute();
    }

    private void triggerStop() {
        try {
            reader.Actions.Inventory.stop();
        } catch (InvalidUsageException e) {
            e.printStackTrace();
        } catch (OperationFailureException e) {
            e.printStackTrace();
        }
        scanButton.setText("SCAN");
        scanButton.setBackgroundColor(Color.parseColor("#32C5D2"));
        scan32Button.setText("SCAN32");
        scan32Button.setBackgroundColor(Color.parseColor("#32C5D2"));
    }

    private void triggerStart() {
        try {
            reader.Actions.Inventory.perform();
        } catch (InvalidUsageException e) {
            e.printStackTrace();
        } catch (OperationFailureException e) {
            e.printStackTrace();
        }
        scanButton.setText("STOP");
        scanButton.setBackgroundColor(Color.parseColor("#FFBEBE"));
        scan32Button.setText("STOP32");
        scan32Button.setBackgroundColor(Color.parseColor("#FFBEBE"));
//        totalItems.setText("0");

//        try {
//            if(asnArrayList != null) {
//                asnArrayList.clear();
//            }
//            if(tagList != null) {
//                tagList.clear();
//            }
//            if(adapter != null) {
//                adapter.notifyDataSetChanged();
//            }
//        } catch(Exception e) {
//            e.printStackTrace();
//        }
    }

    private void ConfigureReader() {
        if (reader.isConnected()) {
            TriggerInfo triggerInfo = new TriggerInfo();
            triggerInfo.StartTrigger.setTriggerType(START_TRIGGER_TYPE.START_TRIGGER_TYPE_IMMEDIATE);
            triggerInfo.StopTrigger.setTriggerType(STOP_TRIGGER_TYPE.STOP_TRIGGER_TYPE_IMMEDIATE);

            TagStorageSettings tagStorageSettings = new TagStorageSettings();
            tagStorageSettings.setTagFields(TAG_FIELD.ALL_TAG_FIELDS);

            try {
                Antennas.AntennaRfConfig antennaRfConfig = reader.Config.Antennas.getAntennaRfConfig(1);
                antennaRfConfig.setrfModeTableIndex(0);
                antennaRfConfig.setTari(0);
                antennaRfConfig.setTransmitPowerIndex(150);
                // set the configuration
                reader.Config.Antennas.setAntennaRfConfig(1, antennaRfConfig);

                // receive events from reader
                if (eventHandler == null)
                    eventHandler = new EventHandler();
                reader.Events.addEventsListener(eventHandler);
                // HH event
                reader.Events.setHandheldEvent(true);
                // tag event with tag data
                reader.Events.setTagReadEvent(true);
                // application will collect tag using getReadTags API
                reader.Events.setAttachTagDataWithReadEvent(false);
                // set trigger mode as rfid so scanner beam will not come
                reader.Config.setTriggerMode(ENUM_TRIGGER_MODE.RFID_MODE, true);
                // set start and stop triggers
                reader.Config.setStartTrigger(triggerInfo.StartTrigger);
                reader.Config.setStopTrigger(triggerInfo.StopTrigger);
                reader.Config.setUniqueTagReport(true);
                reader.Config.setTagStorageSettings(tagStorageSettings);

            } catch (InvalidUsageException e) {
                e.printStackTrace();
            } catch (OperationFailureException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (reader != null) {
                triggerStop();
                reader.Config.setTriggerMode(ENUM_TRIGGER_MODE.BARCODE_MODE, true);
                reader.Events.removeEventsListener(eventHandler);
                reader.disconnect();
                Toast.makeText(getApplicationContext(), "Disconnecting reader", Toast.LENGTH_SHORT).show();
                reader = null;
                readers.Dispose();
                readers = null;
                scanButton.setText("Connect to Reader");
                scanButton.setBackgroundColor(Color.parseColor("#FFBEBE"));
                scan32Button.setText("Connect to Reader");
                scan32Button.setBackgroundColor(Color.parseColor("#FFBEBE"));
            }
        } catch (InvalidUsageException e) {
            e.printStackTrace();
        } catch (OperationFailureException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Read/Status Notify handler
    // Implement the RfidEventsLister class to receive event notifications
    public class EventHandler implements RfidEventsListener {
        // Read Event Notification
        public void eventReadNotify(RfidReadEvents e) {
            try {
                // Recommended to use new method getReadTagsEx for better performance in case of large tag population
                final TagData[] myTags = reader.Actions.getReadTags(500);
                String rfidTagHex = "";
                String rfidTagText = "";
//            if(adapter != null) {
//                adapter.notifyDataSetChanged();
//            }

                if (myTags != null) {
                    for (int index = 0; index < myTags.length; index++) {
                        rfidTagHex = myTags[index].getTagID().toUpperCase();
                        if(rfidTagHex.equals("111122223333444455559999")) {
                            rfidTagHex = "36314a4130304d32323038313930303031".toUpperCase();
                        } else if(rfidTagHex.equals("36314A4130304D3000000009")) {
                            rfidTagHex = "36314a4130304d32323038313930303032".toUpperCase();
                        } else if(rfidTagHex.equals("0000000000000029")) {
                            rfidTagHex = "36314a4130304d32323038313930303033".toUpperCase();
                        } else if(rfidTagHex.equals("00000000010101122825000000000028")) {
                            rfidTagHex = "36314a4130304d32323038313930303034".toUpperCase();
                        }
                        rfidTagText = hexToString(rfidTagHex).toUpperCase();
                        System.out.println(rfidTagHex);

                        try {
                            if(tagList != null) {
                                if(!tagList.contains(rfidTagHex)) {
                                    final String finalRfidTagHex = rfidTagHex;
                                    final String finalRfidTagText = rfidTagText;
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                tagList.add(finalRfidTagHex);
                                                if(skuList.contains(finalRfidTagText.substring(0, 12))){
                                                    asnGetSet = new AsnGetSet(finalRfidTagHex, finalRfidTagText.substring(0, 20), finalRfidTagText.substring(0, 12), "1", "1");
                                                } else {
                                                    asnGetSet = new AsnGetSet(finalRfidTagHex, finalRfidTagText.substring(0, 20), finalRfidTagText.substring(0, 12), "0", "1");
                                                }
                                                asnArrayList.add(asnGetSet);
                                                adapter.notifyDataSetChanged();
//                                        adapter = new AsnListAdapter(asnArrayList, getApplicationContext(), 2);
//                                        rfidListView.setAdapter(adapter);
//                                        adapter.clear();
//                                        adapter.addAll(asnGetSet);
//                                        textView40.setText("Total Item Scanned");
                                                totalItems.setText(Integer.toString(asnArrayList.size()));
//                                        System.out.println("asnArrayList : " + asnArrayList.get(0).getUpc());

//                                        triggerStop();
//                                        parseRFIDby1(rfidTagHex);
//                                        adapter.notifyDataSetChanged();
                                            } catch(IndexOutOfBoundsException ex) {
                                                ex.printStackTrace();
                                            }
                                        }
                                    });
                                }
                            } else {
                                final String finalRfidTagHex1 = rfidTagHex;
                                final String finalRfidTagText1 = rfidTagText;
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            tagList.add(finalRfidTagHex1);
                                            if(skuList.contains(finalRfidTagText1.substring(0, 12))){
                                                asnGetSet = new AsnGetSet(finalRfidTagHex1, finalRfidTagText1.substring(0, 20), finalRfidTagText1.substring(0, 12), "1", "1");
                                            } else {
                                                asnGetSet = new AsnGetSet(finalRfidTagHex1, finalRfidTagText1.substring(0, 20), finalRfidTagText1.substring(0, 12), "0", "1");
                                            }
                                            asnArrayList.add(asnGetSet);
                                            adapter.notifyDataSetChanged();
//                                        adapter = new AsnListAdapter(asnArrayList, getApplicationContext(), 2);
//                                        rfidListView.setAdapter(adapter);
//                                        adapter.clear();
//                                        adapter.addAll(asnGetSet);
//                                        textView40.setText("Total Item Scanned");
                                            totalItems.setText(Integer.toString(asnArrayList.size()));
//                                        System.out.println("asnArrayList : " + asnArrayList.get(0).getUpc());

//                                        triggerStop();
//                                        parseRFIDby1(rfidTagHex);
//                                        adapter.notifyDataSetChanged();
                                        } catch(IndexOutOfBoundsException ex) {
                                            ex.printStackTrace();
                                        }
                                    }
                                });
                            }
                        } catch (StringIndexOutOfBoundsException ex) {
                            Toast.makeText(getApplicationContext(), "Format barcode tidak sesuai.", Toast.LENGTH_SHORT).show();
                        } catch (IndexOutOfBoundsException ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            } catch(IllegalStateException ex) {
                ex.printStackTrace();
            }
        }

        // Status Event Notification
        public void eventStatusNotify(RfidStatusEvents rfidStatusEvents) {
            Log.d(TAG, "Status Notification: " + rfidStatusEvents.StatusEventData.getStatusEventType());
            if (rfidStatusEvents.StatusEventData.getStatusEventType() == STATUS_EVENT_TYPE.HANDHELD_TRIGGER_EVENT) {
                if (rfidStatusEvents.StatusEventData.HandheldTriggerEventData.getHandheldEvent() == HANDHELD_TRIGGER_EVENT_TYPE.HANDHELD_TRIGGER_PRESSED) {
                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... voids) {
                            try {
                                reader.Actions.Inventory.perform();
                            } catch (InvalidUsageException e) {
                                e.printStackTrace();
                            } catch (OperationFailureException e) {
                                e.printStackTrace();
                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            scanButton.setText("STOP");
                            scanButton.setBackgroundColor(Color.parseColor("#FFBEBE"));
                            scan32Button.setText("STOP32");
                            scan32Button.setBackgroundColor(Color.parseColor("#FFBEBE"));
//                            totalItems.setText("0");

//                            try {
//                                if(asnArrayList != null) {
//                                    asnArrayList.clear();
//                                }
//                                if(tagList != null) {
//                                    tagList.clear();
//                                }
//                                if(adapter != null) {
//                                    adapter.notifyDataSetChanged();
//                                }
//                            } catch(Exception e) {
//                                e.printStackTrace();
//                            }
                        }
                    }.execute();
                }
                if (rfidStatusEvents.StatusEventData.HandheldTriggerEventData.getHandheldEvent() == HANDHELD_TRIGGER_EVENT_TYPE.HANDHELD_TRIGGER_RELEASED) {
                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... voids) {
                            try {
                                reader.Actions.Inventory.stop();
                            } catch (InvalidUsageException e) {
                                e.printStackTrace();
                            } catch (OperationFailureException e) {
                                e.printStackTrace();
                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            scanButton.setText("SCAN");
                            scanButton.setBackgroundColor(Color.parseColor("#32C5D2"));
                            scan32Button.setText("SCAN32");
                            scan32Button.setBackgroundColor(Color.parseColor("#32C5D2"));
                        }
                    }.execute();
                }
            }
        }
    }

    private class postPickingASync extends AsyncTask<Void, Void, Void> {
        String pickingCodeParam = "";
        String serialNumberParam = "";

        postPickingASync(String pickingCodeParam, String serialNumberParam) {
            super();
            this.pickingCodeParam = pickingCodeParam;
            this.serialNumberParam = serialNumberParam;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    postPicking(pickingCodeParam, serialNumberParam);
                }
            });
            return null;
        }
    }

    private void postPicking(final String pickingCodeParam, final String serialNumberParam) {
//        System.out.println("PostPicking, " + params);
//        checkPost = 0;
        String postUrl = this.url + "/Api_picking/post_serial/";

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, postUrl,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);

                        try {
                            JSONObject data = new JSONObject(response);
                            Integer status = data.getInt("status");
                            String message = data.getString("message");
                            if(status == 200) {
                                //Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                for(int i = 0; i < asnArrayList.size(); i++) {
                                    if(asnArrayList.get(i).getUpc().toUpperCase().equals(serialNumberParam.toUpperCase())) {
                                        asnGetSet = new AsnGetSet(serialNumberParam, serialNumberParam.substring(0, 20), serialNumberParam.substring(0, 12), "2", "1");
                                        asnArrayList.set(i, asnGetSet);
                                        adapter.notifyDataSetChanged();
                                    }
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String usernameKey = prefs.getString("usernameKey", "No name defined");

                params.put("picking_code", pickingCodeParam);
                params.put("serial_number", serialNumberParam);
                params.put("qty", "1");
                params.put("kit_time", new SimpleDateFormat("YYYY-MM-dd HH:mm:ss").format(new Date()));
                params.put("pick_time", new SimpleDateFormat("YYYY-MM-dd HH:mm:ss").format(new Date()));
                params.put("user", usernameKey);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String restoredText = prefs.getString("handheldsessioncodeKey", null);

                String usernameKey = prefs.getString("usernameKey", "No name defined");
                String handheldsessioncodeKey = prefs.getString("handheldsessioncodeKey", "No name defined");
                Log.d(TAG, usernameKey);
                Log.d(TAG, handheldsessioncodeKey);

                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("User", usernameKey);
                headers.put("Authorization", handheldsessioncodeKey);
                return headers;

            }
        };
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        queue.add(postRequest);
    }

    public static String hexToString(final String tagID) {
        if(tagID != null) {
            List<String> forbiddenChar = new ArrayList<>();
            forbiddenChar.add("");

            StringBuilder sb = new StringBuilder();
            char[] hexData = tagID.toCharArray();
            for (int count = 0; count < hexData.length - 1; count += 2) {
                int firstDigit = Character.digit(hexData[count], 16);
                int lastDigit = Character.digit(hexData[count + 1], 16);
                int decimal = firstDigit * 16 + lastDigit;
                if(!forbiddenChar.contains((char)decimal)) {
                    sb.append((char)decimal);
                }
            }
            return sb.toString();
        } else {
            return "";
        }
    }

    private void getServerURL(){
        Cursor res1 = myDb.getServerAPI();

        while (res1.moveToNext()) {
            if(res1.getString(0) != null) {
                url = res1.getString(0);
            }
        }
    }
}