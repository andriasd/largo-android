package largo.largo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class PickingDocumentListAdapter extends ArrayAdapter<PickingDocumentGetterSetter> {

    private List<PickingDocumentGetterSetter> pickingDocumentItemList;

    private Context context;

    public PickingDocumentListAdapter(List<PickingDocumentGetterSetter> pickingDocumentItemList, Context context) {
        super(context, R.layout.list_view_picking_document, pickingDocumentItemList);
        this.pickingDocumentItemList = pickingDocumentItemList;
        this.context = context;
    }



    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);

        View listViewItem = inflater.inflate(R.layout.list_view_picking_document, null, true);

        TextView textViewPickingDoc = listViewItem.findViewById(R.id.textViewPickingDoc);
        TextView textViewQty = listViewItem.findViewById(R.id.textViewQty);
        TextView textViewCustomer = listViewItem.findViewById(R.id.textViewCustomer);


        PickingDocumentGetterSetter pickingDocument = pickingDocumentItemList.get(position);

        textViewPickingDoc.setText(String.valueOf(pickingDocument.getPlName()));
//        textViewQty.setText(pickingDocument.getQty() + " pcs");

        if(pickingDocument.getSource().length() > 0) {
            textViewCustomer.setText(pickingDocument.getSource());
        } else {
            textViewCustomer.setVisibility(View.GONE);
        }

        if(pickingDocument.getQty().length() > 0) {
            textViewQty.setText(pickingDocument.getQty());
        } else {
            textViewQty.setVisibility(View.GONE);
        }

        return listViewItem;
    }

    public void getAllValues(){
        for(int i = 0; i<pickingDocumentItemList.size(); i++){

        }
    }
}
