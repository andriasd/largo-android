package largo.largo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

public class PickingItemListAdapter extends ArrayAdapter<PickingItemGetterSetter> {

    private List<PickingItemGetterSetter> pickingItemList;
    boolean cb;

    private Context context;

    public PickingItemListAdapter(List<PickingItemGetterSetter> pickingItemList, Context context) {
        super(context, R.layout.list_view_picking_item, pickingItemList);
        this.pickingItemList = pickingItemList;
        this.context = context;
    }



    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);

        View listViewItem = inflater.inflate(R.layout.list_view_picking_item, null, true);

        TextView textViewPickingItem = listViewItem.findViewById(R.id.textViewPickingItem);
        TextView textViewQty = listViewItem.findViewById(R.id.textViewQty);
        TextView textViewLocation = listViewItem.findViewById(R.id.textViewLocation);

        PickingItemGetterSetter pickingItem = pickingItemList.get(position);

        if(pickingItem.getSku().length() > 0) {
            textViewPickingItem.setText(String.valueOf(pickingItem.getSku()));
        } else {
            textViewPickingItem.setVisibility(View.GONE);
        }

        if(pickingItem.getPickedQty().length() > 0) {
            textViewQty.setText(pickingItem.getPickedQty() + "/" + pickingItem.getQty());
        } else {
            textViewQty.setVisibility(View.GONE);
        }

        String[] split = pickingItem.getLocation().split("<90>");

        try{
            if(split[1] != null) {
                textViewQty.setText(split[0]);
                textViewQty.setVisibility(View.VISIBLE);
                textViewLocation.setText(split[1]);
            } else {
                textViewLocation.setText(pickingItem.getLocation());
            }
        } catch(ArrayIndexOutOfBoundsException e) {
            textViewLocation.setText(pickingItem.getLocation());
        }

        pickingItem = pickingItemList.get(position);

        return listViewItem;
    }
}
